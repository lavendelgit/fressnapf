import net.pricefx.common.api.InputType
import net.pricefx.server.dto.calculation.ConfiguratorEntry
import net.pricefx.server.dto.calculation.ContextParameter


ConfiguratorEntry inputConfigurator = Library.createInputConfiguratorWithHiddenStoredValue('CompCategoryConfigurator')
inputConfigurator.setMessage("<span style='font-weight:bold;color:silver;'> COMPARISION FILTER</span><hr>")
Library.addCompareOnOption(inputConfigurator)
Library.addCategoryOptions(inputConfigurator, 'Comp')
Library.addBrandOptions(inputConfigurator, 'Comp')

return inputConfigurator

