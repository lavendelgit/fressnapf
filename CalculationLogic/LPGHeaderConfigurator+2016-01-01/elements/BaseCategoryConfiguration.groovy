import net.pricefx.common.api.InputType
import net.pricefx.server.dto.calculation.ConfiguratorEntry
import net.pricefx.server.dto.calculation.ContextParameter


ConfiguratorEntry inputConfigurator = Library.createInputConfiguratorWithHiddenStoredValue('BaseCategoryConfigurator')
String msg = "<span style='font-weight:bold;color:silver;'> BASE FILTER</span><hr>"
msg += "<p style='font-style:italic;color:green;font-size:11px'>To analyse the data, please provide valid data below (<b>↓</b>), click on 'Save' button (<b>↗</b>) and then click on 'Recalculate Header' button (<b>↗</b>). </p>"
msg += "<ul style='font-style:italic;color:grey;font-size:9px'><li><b>NO FILTER</b> : Compare Average Online and Offline price of all lineitems in the Pricelist.</li>"
msg += "<li><b>ONLY BASE FILTER</b> : Compare Average Online and Offline price of filtered lineitems in the Pricelist.</li>"
msg += "<li><b>BOTH BASE & COMP FILTER</b> : Compare Average Selected Price of lineitems filtered using Base Filter and Average Selected Price of lineitems filtered using Comparision Filter.</li></ul>"
inputConfigurator.setMessage(msg)

Library.addCategoryOptions(inputConfigurator, 'Base')
Library.addBrandOptions(inputConfigurator, 'Base')

return inputConfigurator

