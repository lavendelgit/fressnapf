import net.pricefx.common.api.InputType
import net.pricefx.server.dto.calculation.ConfiguratorEntry
import net.pricefx.server.dto.calculation.ContextParameter


protected ContextParameter createOptionsField(ConfiguratorEntry configuratorEntry, String fieldName, String fieldLabel, List options, String selValue, String defaultValue, Boolean isRequired = false, Boolean isReadOnly = false, Boolean isNoRefresh = false) {
    return createSelectField(configuratorEntry, InputType.OPTIONS, fieldName, fieldLabel, options, selValue, defaultValue, isRequired, isReadOnly, isNoRefresh)
}

protected ContextParameter createOptionField(ConfiguratorEntry configuratorEntry, String fieldName, String fieldLabel, List options, String selValue, String defaultValue, Boolean isRequired = false, Boolean isReadOnly = false, Boolean isNoRefresh = false) {
    return createSelectField(configuratorEntry, InputType.OPTION, fieldName, fieldLabel, options, selValue, defaultValue, isRequired, isReadOnly, isNoRefresh)
}

protected ContextParameter createSelectField(ConfiguratorEntry configuratorEntry, InputType inputType, String fieldName, String fieldLabel, List options, String selValue, String defaultValue, Boolean isRequired = false, Boolean isReadOnly = false, Boolean isNoRefresh = false) {
    ContextParameter optionInput = configuratorEntry.createParameter(inputType, fieldName)
    optionInput.setRequired(isRequired)
    optionInput.setReadOnly(isReadOnly)
    optionInput.setLabel(fieldLabel)
    optionInput.setValueOptions(options)
    return optionInput
}

protected void addCategoryOptions(ConfiguratorEntry inputConfigurator, String contextName) {
    List options = loadProductAttributes('category')
    String fieldName = contextName + 'Category'
    ContextParameter categoryPicker = createOptionsField(inputConfigurator, fieldName, fieldName, options, null, null, false, false, true)
    categoryPicker.setLabel("Select ${contextName} Category(s)")
}

protected void addCommodityGroupOptions(ConfiguratorEntry inputConfigurator, String contextName) {
    List options = libs.FressnapfLib.DataUtils.getCommodityGroupMaster()?.collect { String categoryName, Map categoryInfo ->
        categoryInfo.description ?: categoryName
    }
    String fieldName = contextName + 'CGR NO'
    ContextParameter categoryPicker = createOptionsField(inputConfigurator, fieldName, fieldName, options, null, null, false, false, false)
    categoryPicker.setLabel("Select ${contextName} CommodityGroup(s)")
}

protected void addBrandOptions(ConfiguratorEntry inputConfigurator, String contextName) {
    String fieldName = contextName + 'Brand'
    List options = loadProductAttributes('brand')
    ContextParameter brandPicker = createOptionsField(inputConfigurator, fieldName, fieldName, options, null, null, false, false, true)
    brandPicker.setLabel("Select ${contextName} Brand(s)")
}

protected void addCompareOnOption(ConfiguratorEntry inputConfigurator) {
    Map options = ['Online': 'Online Price', 'Offline': 'Offline Price']
    String fieldName = 'ComparisionOn'
    ContextParameter compareOnPicker = createOptionField(inputConfigurator, fieldName, fieldName, options.keySet() as List, null, null, false, false, false)
    compareOnPicker.setLabel("Compare")
    compareOnPicker.addParameterConfigEntry("labels", options)
}

List loadProductAttributes(String fieldName) {
    List filters = []
    return libs.FressnapfLib.DataUtils.findAll() { int startRow, int maxRowsLimit ->
        api.find("P", startRow, maxRowsLimit, fieldName, [fieldName], true, *filters)
    }?.collect { Map row ->
        row.getAt(fieldName)
    }?.findAll {
        it != null
    }?.unique()?.sort()
}


/**
 * This is a "hack" to save the input value that is not persisted between logic refreshes on changes
 * @param calculationType
 * @return
 */
protected ConfiguratorEntry createInputConfiguratorWithHiddenStoredValue(String calculationType) {
    def inputConfigurator = api.createConfiguratorEntry(InputType.HIDDEN, "calculationType")
    if (calculationType != null) {
        inputConfigurator.getFirstInput().setValue(calculationType)
    }

    return inputConfigurator
}
