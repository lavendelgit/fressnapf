if(!api.global.competitorsCache){
    streamRecords = api.stream("PCOMP",null,["sku","competitionType","competitorSku","price","currency","country","infoDate"])
    api.global.competitorsCache = streamRecords?.collect{it}?.groupBy {(it.sku)?.replaceFirst("^0*", "")}
    streamRecords?.close()
}
return
