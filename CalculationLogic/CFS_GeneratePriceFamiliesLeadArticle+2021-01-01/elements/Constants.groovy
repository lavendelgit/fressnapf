import groovy.transform.Field

@Field final String Default_Calc_Method = "AVG"
@Field final Map Country_Currency_Map = ["Germany" : "EUR",
                                         "Poland" : "PLN",
                                         "Austria" : "EUR",
                                         "Belgium" : "EUR",
                                         "Denmark" : "DKK",
                                         "France" : "EUR",
                                         "Hungary" : "HUF",
                                         "Italy" : "EUR",
                                         "Luxemburg" : "EUR",
                                         "Republic of Ireland" : "EUR",
                                         "Switzerland" : "CHF",]
@Field final String Online_Channel = "Online"
@Field final String Online_Offline_Channel = "Online&Offline"