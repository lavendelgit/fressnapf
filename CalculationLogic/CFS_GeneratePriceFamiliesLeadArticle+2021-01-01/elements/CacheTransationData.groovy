if (!api.global.transationData) {
    api.global.transationData = []
    def ctx = api.getDatamartContext()
    def dm = ctx.getDataSource("Transaction")
    def query = ctx?.newQuery(dm)
            ?.select('ArticleID', 'ArticleID')
            ?.select('PCSTotal', 'PCSTotal')
    streamResult = ctx?.streamQuery(query)
    while(streamResult.next()){
        api.global.transationData << streamResult.get()
    }
    streamResult.close()

}
return
