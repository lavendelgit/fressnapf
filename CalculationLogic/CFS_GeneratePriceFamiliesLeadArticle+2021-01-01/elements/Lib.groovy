BigDecimal calculateCompetitorSumPrice(List competitorData){
    competitorsPriceSum = 0.0
    competitorsSize = competitorData?.size()
    for(competitor in competitorData){
        competitorsPriceSum += competitor?.price
    }
    return competitorsPriceSum/competitorsSize
}

BigDecimal calculateStandardDeviation(List competitorsDate, BigDecimal competitorsAvg){
    standardDeviationSum = 0.0
    for(competitor in competitorsDate){
        standardDeviationSum += Math.pow((competitor?.price - competitorsAvg),2)
    }
    competitorVariance = (1/competitorsDate?.size()) * standardDeviationSum
    return Math.sqrt(competitorVariance)
}

String validateLeadArticle(Map articleAvgDetails, List articles){
    Map exchangeRates = libs.FressnapfLib.DataUtils.getExchangeRatesMaster()
    List validPrices = []
    for (articleID in articles){
        competitorData = api.global.competitorsCache[articleID]?.sort{it.infoDate}

        for (competitor in competitorData){
            if(!Constants.Country_Currency_Map[competitor?.country] == competitor?.currency){
               exchangeRate = exchangeRates?.findAll {it.key == competitor?.currency+"_"+Constants.Country_Currency_Map[competitor?.country]}?.values()?.getAt(0)?.currencyExchangeRate
            }
            sku = competitor?.sku?.replaceFirst("^0*", "")
            avgAdded = articleAvgDetails[sku]?.Avg + articleAvgDetails[sku]?.StdDeviation
            avgSubract = articleAvgDetails[sku]?.Avg - articleAvgDetails[sku]?.StdDeviation
            if((competitor?.price>= avgSubract)&& (competitor?.price<= avgAdded)){
                validPrices << [
                        "Article" : sku,
                        "competitor" : competitor?.competitorSku,
                        "channel"    : api.global.articleChannelCache[sku]?.channel == "Online&Offline"?"Online":api.global.articleChannelCache[sku]?.channel,
                        "price"      : Constants.Country_Currency_Map[competitor?.country] == competitor?.currency? competitor?.price : competitor?.price * (exchangeRate?:1)
                ]
            }
        }
    }
    if(validPrices){
        return getLeadArticle(validPrices)
    }else{
        return fallBackLeadArticle(articles)
    }


}

String getLeadArticle(List validPrices){
    String leadArticle
    boolean flag
    for(article in validPrices){
        sku = article?.Article
        competitor = article?.competitor
        basket = basket = api.global.basketMapping[sku]?.basket
        channel = article?.channel
        calculationMethod = api.global.competierBasketMappingCache[competitor]?.find {it.key1 == basket && it.key3 == channel  }?.attribute2?:Constants.Default_Calc_Method
        if(calculationMethod){
            switch(calculationMethod){
                case "MIN" :
                    leadArticle = validPrices?.min{it.price}?.Article
                    flag = true
                    break
                case "AVG" :
                    avg = validPrices?.price?.sum() / validPrices?.size()
                    avgPrice = validPrices?.price?.sort { (it - avg)?.abs() }?.getAt(0)
                    leadArticle = validPrices?.find{it.price == avgPrice}?.Article
                    flag = true
                    break
            }
        }
        if(flag){
            return leadArticle
        }
    }
    if(!leadArticle){

        leadArticle = api.global.priceFamiliesCache[out.PriceFamily]?.sku?.sort()?.getAt(0)
    }
    return leadArticle
}

String fallBackLeadArticle(List articles){
    validTranscation = api.global.transationData?.findAll{articles?.contains(it.ArticleID)}
    if(validTranscation){
        return validTranscation?.sort{a,b->b.PCSTotal<=>a.PCSTotal}?.getAt(0)?.ArticleID
    }else{
        return articles?.sort{a,b-> a as Integer <=> b as Integer}?.getAt(0) as String
    }

}