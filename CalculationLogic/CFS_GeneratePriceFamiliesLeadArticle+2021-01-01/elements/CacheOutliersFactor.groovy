if(!api.global.outliersFactor){
    api.global.outliersFactor = api.findLookupTableValues("OutliersFactor")?.collectEntries {[(it.name): it.attribute1]}
}
return