if(!api.global.basketMapping){
    streamRecords = api.stream("PX8",null,["sku","attribute1","attribute4"], Filter.equal("name", libs.FressnapfLib.Constants.ItemClassBasketMapping_PX))
    api.global.basketMapping = streamRecords?.collectEntries{[(it.sku):[
            "country" : it.attribute1,
            "basket" : it.attribute4]]
    }
    streamRecords?.close()
}
return
