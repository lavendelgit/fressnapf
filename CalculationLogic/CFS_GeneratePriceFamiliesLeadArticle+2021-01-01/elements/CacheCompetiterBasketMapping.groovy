if(!api.global.competierBasketMappingCache){
    api.global.competierBasketMappingCache = api.findLookupTableValues(libs.FressnapfLib.Constants.CompetitorBasketMapping_PP,["key1","key2","key3","key4","attribute2"],null)?.collect{it}?.groupBy {it.key2}
}
return