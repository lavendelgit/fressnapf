if(!api.global.priceFamiliesCache){
    List filter = [
            Filter.equal("name","PriceFamilies"),
            Filter.isNotNull("attribute1")
    ]
    streamRecords = api.stream("PX3",null,["sku","attribute1"],*filter)
    api.global.priceFamiliesCache = streamRecords?.collect{it}?.groupBy {it.attribute1}
    streamRecords?.close()
}
return