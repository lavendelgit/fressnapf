if(!api.global.articleChannelCache){
    streamRecords = api.stream("PX10",null,["sku","attribute1","attribute2"], Filter.equal("name", libs.FressnapfLib.Constants.ArticleChannelMapping_PX))
    api.global.articleChannelCache = streamRecords?.collectEntries{[(it.sku):[
            "country" : it.attribute1,
            "channel" : it.attribute2]]
    }
    streamRecords?.close()
}
return
