if(out.ArticleID && out.PriceFamily){
    Map articleAvgDetails = [:]
    String leadArticle
    !api.global.priceFamilywithLead ? api.global.priceFamilywithLead = [:] : ""
    priceFamilyLead = api.global.priceFamilywithLead[out.PriceFamily]
    if(!priceFamilyLead && out.ArticleID!=out.PriceFamily){
        for(article in  api.global.priceFamiliesCache[out.PriceFamily]?.sku){
            competitorData = api.global.competitorsCache[article]
            competitorStdDeviation = 0.0
            if(competitorData?.size()>1){
                competitorsAvg = Lib.calculateCompetitorSumPrice(competitorData)
                competitorStdDeviation = Lib.calculateStandardDeviation(competitorData, competitorsAvg)
                channel = api.global.articleChannelCache[article]?.channel == Constants.Online_Channel ?Constants.Online_Offline_Channel :api.global.articleChannelCache[article]?.channel
                competitorStdDeviation = competitorStdDeviation * (api.global.outliersFactor[channel] as BigDecimal)
                articleAvgDetails[article] = [
                        "articleID" : article,
                        "Avg" : competitorsAvg,
                        "StdDeviation" : competitorStdDeviation]
            }else if(competitorData?.size() == 1){
                articleAvgDetails[article] = [
                        "articleID" : article,
                        "Avg" : competitorData?.getAt(0)?.price,
                        "StdDeviation" : 0.0]
            }
        }
        leadArticle = Lib.validateLeadArticle(articleAvgDetails,api.global.priceFamiliesCache[out.PriceFamily]?.sku)
        api.global.priceFamilywithLead?.putAt(out.PriceFamily,leadArticle)
    }else if (out.ArticleID==out.PriceFamily) {

        leadArticle = out.PriceFamily

    }else if (priceFamilyLead) {

        leadArticle =  priceFamilyLead

    }
    return leadArticle
}
return
