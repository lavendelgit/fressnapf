api.logInfo("***** CFS are Triggered for |Generate ItemClass and Basket|Generate Price Families Lead Article|Populate Set Tray Purchase Price   ***** ")
    actionBuilder.addCalculatedFieldSetAction("Generate ItemClass and Basket").setCalculate(true)
    actionBuilder.addCalculatedFieldSetAction("Generate Price Families Lead Article").setCalculate(true)
    actionBuilder.addCalculatedFieldSetAction("Populate Set Tray Purchase Price").setCalculate(true)
    actionBuilder.addDataLoadAction("HAWA Purchase Prices","Calculation","HAWA Purchase Prices").setCalculate(true)
return