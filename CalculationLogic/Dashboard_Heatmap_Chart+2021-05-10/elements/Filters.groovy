
def productDimensions = [
        "category"    : "Category",
        "subCategory"    : "Sub Category",
        "brandName"    : "Brand Name",
        "subBrand"    : "Sub Brand",
        "brandType"    : "Brand Type",
]
api.local.productDimension = api.option("ProductDimension",
        productDimensions.keySet() as List, productDimensions)


param = api.getParameter("ProductDimension")
if (param && !param.getValue()) {
    param.setValue("Productgroupname")
    param.setLabel("Product Dimension")
    param.setRequired(true)
}

return
