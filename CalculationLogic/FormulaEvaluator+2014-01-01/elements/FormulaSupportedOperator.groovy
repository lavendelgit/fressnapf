/**
 * Check if value is null or not
 * @param : args. List of argument, first value will be used to check
 * @return : boolean value whether args[0] is null
 * */
def IS_NULL(args) {
    if (args == null || args?.size() < 1) {
        api.throwException("Invalid arguments ${args}")
    }
    def arg = args?.getAt(0)
    return arg == null
}

/**
 * Cast arguments to string, if there are multiple argument, join them using comma (,)
 * @param : args. arguments
 * @return: string*                  */
def AS_STRING(args, Map... contexts) {
    if (args == null || args?.size() < 1) {
        api.throwException("Invalid arguments ${args}")
    }
    def arg
    return args?.inject([]) { res, it ->
        arg = libs.FormulaEvaluator.Library.getBinding(it, *contexts).value as String
        res.add(arg)
        return res
    }?.join(",")

}

/**
 * Cast argument to number
 * @param : args. List of arguments passed by FormulaEvaluator
 * @return : number
 * */
def AS_NUMBER(args) {
    if (args == null || args?.size() < 1) {
        api.throwException("Invalid arguments ${args}")
    }
    def arg = args[0]
    return arg as BigDecimal
}

/**
 * Get the size of a list
 * @param : args. List of arguments passed by FormulaEvaluator
 * @return : size of list from first arguments
 * */
def SIZE(args) {
    if (args == null || args?.size() < 1) {
        api.throwException("Invalid arguments ${args}")
    }
    def arg = args?.getAt(0)
    return arg?.size()
}

/**
 * Check boolean value from args[0], if true return args[1], otherwise return args[2]
 * @param : args. Arguments
 * @return : arg[1] if arg[0] == true, otherwise arg[2]
 * */
def IF(args) {
    if (args == null || args?.size() < 3) {
        api.throwException("Invalid arguments ${args}")
    }
    def condition = args?.getAt(0)
    def thenTrue = args?.getAt(1)
    def thenFalse = args?.getAt(2)
    if (condition == true) {
        return thenTrue
    } else {
        return thenFalse
    }
}

def access(Object source, Object key, Map operatorToken, Map... contexts) {
    def commonChars = libs.FormulaEvaluator.Library.getCommonCharacter()
    def bindedSource = libs.FormulaEvaluator.Library.getBinding(source, *contexts)?.value
    if (operatorToken.nextToken().value == commonChars.leftParenthese) {
        def bindedKey = libs.FormulaEvaluator.Library.getBinding(key, *contexts)?.value
        return bindedSource?.getAt(bindedKey)
    } else {
        return bindedSource?.getAt(key.value)
    }
}

/**
 * Supported functional operators for FormulaEvaluator
 * @return : Map of supported functional
 * */
def getFunctionalOperators() {
    return ["IF"       : [precedence: 1000,
                          exec      : { args -> IF(args)
                          }],
            "IS_NULL"  : [precedence: 1000,
                          exec      : { args -> IS_NULL(args)
                          }],
            "SIZE"     : [precedence: 1000,
                          exec      : { args -> SIZE(args)
                          }],
            "AS_STRING": [precedence  : 1000,
                          exec        : { args -> AS_STRING(args, *contexts)
                          },
                          tokenAsParam: true],
            "AS_NUMBER": [precedence: 1000,
                          exec      : { args -> AS_NUMBER(args)
                          }]]
}

/**
 * Supported infix operators for FormulaEvaluator
 * @return : Map of supported infix operators
 * */
def getInfixOperators() {
    return ["AND": [precedence: -1,
                    exec      : { x, y -> return x && y
                    }],
            "OR" : [precedence: -2,
                    exec      : { x, y -> return x || y
                    }],
            "+"  : [precedence: 1,
                    exec      : { x, y -> x + y }],
            "-"  : [precedence: 1,
                    exec      : { x, y -> x - y }],
            "*"  : [precedence: 2,
                    exec      : { x, y -> x * y }],
            "/"  : [precedence: 2,
                    exec      : { x, y -> x / y }],
            "||" : [precedence: -2,
                    exec      : { x, y -> x || y }],
            "&&" : [precedence: -1,
                    exec      : { x, y -> x && y }],
            "==" : [precedence: 0,
                    exec      : { x, y -> x == y }],
            "!=" : [precedence: 0,
                    exec      : { x, y -> x != y }],
            ">"  : [precedence: 0,
                    exec      : { x, y -> x > y }],
            "<"  : [precedence: 0,
                    exec      : { x, y -> x < y }],
            ">=" : [precedence: 0,
                    exec      : { x, y -> x >= y }],
            "<=" : [precedence: 0,
                    exec      : { x, y -> x <= y }],
            "."  : [precedence  : 2000,
                    token       : null, // this is for delegate strategy, exec closure can access current operator config map
                    tokenAsParam: true,
                    exec        : { x, y -> access(x, y, operator, *contexts) }]]
}

/**
 * Supported prefix operators for FormulaEvaluator
 * @return : Map of supported prefix operators
 * */
def getPrefixOperators() {
    return ["-": [precedence: 1000,
                  exec      : { x -> return -x
                  }],
            "!": [precedence: 1000,
                  exec      : { x -> return !x
                  }]]
}

/**
 * Supported postfix operators for FormulaEvaluator
 * @return : Map of supported postfix operators
 * */
def getPostfixOperators() {
    return [:]
}