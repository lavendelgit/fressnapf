/**
 * Evaluate expression string with binding object
 * @param : expr. Expression String
 * @param : binding. Variables binding
 * @return : Expression evaluated result
 * */
def eval(String expression, Map... bindings) {
    def parsed = libs.FormulaEvaluator.FormulaParser.parse(expression)
    return evalParsedExpression(parsed, *bindings)
}


/**
 * Evaluate postfix list of parsed expression with binding object
 * @param : parsed. postfix list of parsed expression
 * @param : binding. Variables binding
 * @return : Expression evaluated result
 * */
def evalParsedExpression(List parsed, Map... bindings) {
    def operatorPosition = libs.FormulaEvaluator.Library.getOperatorPosition()
    def tokenType = libs.FormulaEvaluator.Library.getTokenType()
    def commonChar = libs.FormulaEvaluator.Library.getCommonCharacter()
    def outputs = []
    def opr1, opr2
    def operator
    for (it in parsed) {
        switch (it.type) {
            case tokenType.variable:
            case tokenType.number:
            case tokenType.string:
                outputs << it
                break
            case tokenType.squares:
                if (it.value == commonChar.leftSquare) {
                    outputs << it
                }
                else {
                    def key = outputs.removeAt(outputs.size() - 1)
                    def token = outputs.removeAt(outputs.size() - 1) // remove '[' from output
                    def source = outputs.size() ? outputs.removeAt(outputs.size() - 1) : null
                    def _prev = token.previousToken() // previous token from the expr
                    if (source == null || !(_prev.value in [commonChar.rightParenthese, commonChar.rightSquare] || _prev.type == tokenType.variable)) {
                        libs.FormulaEvaluator.Library.throwException("Cannot access property on null")
                    }
                    else {
                        def _sourceBinded = libs.FormulaEvaluator.Library.getBinding(source, *bindings)?.value
                        def _keyBinded = libs.FormulaEvaluator.Library.getBinding(key, *bindings).value
                        outputs << libs.FormulaEvaluator.Library.toBindedResult(token, _sourceBinded?.getAt(_keyBinded))
                    }
                }
                break
            case tokenType.function:
                def args = []
                def arg
                operator = libs.FormulaEvaluator.Library.getOperator(it)
                operator.operator = it
                operator.contexts = bindings
                operator.exec.delegate = operator
                for (def i = 0; i < it.params; i++) {
                    arg = outputs.removeAt(outputs.size() - 1)
                    args << (operator.tokenAsParam ? arg : libs.FormulaEvaluator.Library.getBinding(arg, *bindings)?.value)
                }
                outputs << libs.FormulaEvaluator.Library.toBindedResult(it, operator.exec(args.reverse()))
                break
            case tokenType.operator:
                def p1, p2
                operator = libs.FormulaEvaluator.Library.getOperator(it)
                operator.operator = it
                operator.contexts = bindings
                operator.exec.delegate = operator
                p1 = outputs.removeAt(outputs.size() - 1)
                opr1 = operator.tokenAsParam ? p1 : libs.FormulaEvaluator.Library.getBinding(p1, *bindings)?.value
                if (it.position == operatorPosition.infix) {
                    p2 = outputs.removeAt(outputs.size() - 1)
                    opr2 = operator.tokenAsParam ? p2 : libs.FormulaEvaluator.Library.getBinding(p2, *bindings)?.value
                    outputs << libs.FormulaEvaluator.Library.toBindedResult(it, operator.exec(opr2, opr1))
                }
                else {
                    outputs << libs.FormulaEvaluator.Library.toBindedResult(it, operator.exec(opr1))
                }

                break
            default:
                break
        }
    }
    def returnToken = outputs.removeAt(outputs.size() - 1)
    return libs.FormulaEvaluator.Library.getBinding(returnToken, *bindings).value
}

