/**
 * Parse expression string to list of postfix using shuntingYard algorithm
 * @param : expr. Expression string to parse
 * @return : expression as a postfix list
 * */
def parse(expr) {
    def outputs = []
    def token
    def operators = []
    def commaList = []
    def lastOperator
    def tokenizer = getTokenizer(expr)
    def functionCount = 0
    def functionParamCount = 1
    def tokenType = libs.FormulaEvaluator.Library.getTokenType()
    def commonChar = libs.FormulaEvaluator.Library.getCommonCharacter()
    def isLeftParenthese = { item -> return item.type == tokenType.parentheses && item.value == commonChar.leftParenthese }
    def isLeftSquare = { item -> return item.type == tokenType.squares && item.value == commonChar.leftSquare }
    def isLeftAssoc = { op ->
        def optDef = libs.FormulaEvaluator.Library.getOperator(op)
        return op.type == tokenType.operator && (optDef.associativily == null || optDef.associativily == "left")
    }
    def isFirstPrecedence = { op1, op2 ->
        def optDef1 = libs.FormulaEvaluator.Library.getOperator(op1)
        def optDef2 = libs.FormulaEvaluator.Library.getOperator(op2)
        return op1.type == tokenType.operator && op2.type == tokenType.operator && (optDef1.precedence > optDef2.precedence || (optDef1.precedence == optDef2.precedence && isLeftAssoc(op1)))
    }

    while (tokenizer.hasNext()) {
        token = tokenizer.next()
        switch (token.type) {
            case tokenType.variable:
                outputs << token
                break
            case tokenType.number:
                outputs << token
                break
            case tokenType.string:
                outputs << token
                break
            case tokenType.squares:
                if (token.value == commonChar.leftSquare) {
                    operators << token
                }
                else {
                    while (!operators.isEmpty() && operators.last().value != commonChar.leftSquare) {
                        lastOperator = operators.removeAt(operators.size() - 1)
                        if (lastOperator.type == tokenType.comma) {
                            libs.FormulaEvaluator.Library.throwException("Comma not allow at position ${token.pos}")
                        }
                        else {
                            outputs << lastOperator
                        }
                    }
                    if (!operators.isEmpty()) {
                        operators.removeAt(operators.size() - 1)
                    }
                }
                outputs << token
                break
            case tokenType.parentheses:
                if (token.value == commonChar.leftParenthese) {
                    operators << token
                }
                else {
                    while (!operators.isEmpty() && operators.last().value != commonChar.leftParenthese) {
                        lastOperator = operators.removeAt(operators.size() - 1)
                        if (lastOperator.type == tokenType.comma) {
                            assertPossitiveFuncCount(functionCount, lastOperator)
                            functionParamCount++
                        }
                        else {
                            outputs << lastOperator
                        }
                    }
                    operators.removeAt(operators.size() - 1)
                    if (!operators.isEmpty() && operators.last().type == tokenType.function) {
                        lastOperator = operators.removeAt(operators.size() - 1)
                        lastOperator.params = functionParamCount
                        outputs << lastOperator
                        functionParamCount = 1
                        functionCount--
                    }
                }

                break
            case tokenType.function:
                operators << token
                functionCount++
                break
            case tokenType.comma:
                while (!operators.isEmpty() && operators.last().value != commonChar.leftParenthese) {
                    lastOperator = operators.removeAt(operators.size() - 1)
                    if (lastOperator.type != tokenType.comma) {
                        outputs << lastOperator
                    }
                    else {
                        commaList << lastOperator
                    }
                }

                while (!commaList.isEmpty()) {
                    operators << commaList.removeAt(commaList.size() - 1)
                }
                operators << token
                break
            case tokenType.operator:
                while (!operators.isEmpty() && operators.last().type != tokenType.comma && !isLeftParenthese(operators.last()) && !isLeftSquare(operators.last()) && (operators.last().type == tokenType.function || isFirstPrecedence(operators.last(), token))) {
                    lastOperator = operators.removeAt(operators.size() - 1)
                    outputs << lastOperator
                }
                operators << token
                break
            default:
                break
        }
    }

    while (!operators.isEmpty()) {
        outputs << operators.removeAt(operators.size() - 1)
    }
    return outputs
}

protected void assertPossitiveFuncCount(funcCount, lastOperator) {
    if (funcCount <= 0) {
        libs.FormulaEvaluator.Library.throwException("Unexpected character '$lastOperator.value' at position $lastOperator.pos")
    }
}

/**
 * Parse expression string to expression token
 * @param : expr. Expression string
 * @return : tokenizer liked object
 * [
 *  hasNext : check if there are any token from the expression string,
 *  next: get next token from the string
 * ]
 * */
def getTokenizer(String expr) {
    def pos = 0
    def tokenIdx = 0
    def tokenList = []
    def tokenListSize = 0
    def input = expr.trim()
    def iSize = input.length()
    def previousToken
    def commonChar = libs.FormulaEvaluator.Library.getCommonCharacter()
    def decimal_seperator = commonChar.decimal_seperator
    def underscore = commonChar.underscore
    def doubleQuote = commonChar.doubleQuote
    def singleQuote = commonChar.singleQuote
    def leftParenthese = commonChar.leftParenthese
    def rightParenthese = commonChar.rightParenthese
    def leftSquare = commonChar.leftSquare
    def rightSquare = commonChar.rightSquare
    def escapeChar = commonChar.strEscape
    def comma = commonChar.comma
    def operatorPosition = libs.FormulaEvaluator.Library.getOperatorPosition()
    def tokenType = libs.FormulaEvaluator.Library.getTokenType()
    def peekNextToken
    def parentheses = [leftParenthese, rightParenthese]
    def squares = [leftSquare, rightSquare]

    def createEmptyToken = {
        def _nextToken, _previousToken
        return [type         : null,
                nextToken    : { _token = null, force = false ->
                    if (_token || force) {
                        _nextToken = _token
                    }
                    return _nextToken
                },
                previousToken: { _token = null, force = false ->
                    if (_token || force) {
                        _previousToken = _token
                    }
                    return _previousToken
                },
                value        : null]
    }
    def createToken = { _type, _value ->
        def _token = createEmptyToken()
        Map _previousToken = tokenListSize > 0 ? tokenList[-1] : null
        _previousToken?.nextToken(_token)
        _token.type = _type
        _token.value = _value
        _token.previousToken(_previousToken)
        _token.pos = pos
        _token.tokenPos = tokenListSize
        tokenList.add(_token)
        tokenListSize++
        return _token
    }
    def hasNext = { return pos < iSize }
    def hasNextToken = { return tokenIdx < tokenListSize }
    def peek = {
        if (hasNext()) {
            return input.getAt(pos)
        }
        return null
    }

    def nextChar = {
        if (hasNext()) {
            def currentCh = input.getAt(pos)
            pos++
            return currentCh
        }
        else {
            return null
        }
    }

    def stringToken = { String ch ->
        def open = ch
        def str = """"""
        def sCount = 0
        while (ch != null) {
            if (ch == open) {
                sCount++
            }
            if (ch != open) {
                if (ch == escapeChar) {
                    ch = nextChar()
                }
                str = """$str$ch"""
            }

            if (sCount == 2) {
                break
            }
            ch = nextChar()
        }
        if (sCount < 2) {
            libs.FormulaEvaluator.Library.throwException("Invalid string expression $str")
        }
        def _token = createToken(tokenType.string, str?.toString())
        _token.startChar = open
        return _token
    }

    def isLetter = { String ch ->
        def _lowerCase = { (ch >= 'a' && ch <= 'z') }
        def _upperCase = { (ch >= 'A' && ch <= 'Z') }
        return _lowerCase() || _upperCase()
    }

    def isValidVariableChar = { String ch ->
        def _isNumber = { ch.isNumber() }
        def _isLetter = { isLetter(ch) }
        def _validChar = { ch in [underscore] }
        return _isNumber() || _isLetter() || _validChar()
    }


    def isOperator = { token, position = null ->
        def _isOperator = { token.type == tokenType.operator }
        // operator position: prefix/infix/postfix
        def _isPosition = { (position ? token.position == position : true) }
        return token && _isOperator() && _isPosition()
    }

    def isOperand = { token ->
        def _validType = { token?.type in [tokenType.variable, tokenType.string, tokenType.number] }
        return _validType()
    }

    def isParentheses = { token, value = null ->
        def _isParentheses = { token.type == tokenType.parentheses }
        def _isValue = { (value ? token.value == value : true) }
        return token && _isParentheses() && _isValue()
    }

    def operatorShouldBePrefix = {
        def _notPrevious = { !previousToken }
        def _previousIsInfix = { isOperator(previousToken, operatorPosition.infix) }
        def _isComma = { previousToken.type == tokenType.comma }
        def _isLeftParentheses = { isParentheses(previousToken, leftParenthese) }
        return _notPrevious() || _previousIsInfix() || _isComma() || _isLeftParentheses()
    }

    def operatorShouldBeInfix = {
        def _previousIsNotInfixToken = { !isOperator(previousToken, operatorPosition.infix) }
        return previousToken && _previousIsNotInfixToken()
    }

    def operatorShouldBePostfix = {
        def _nextToken = peekNextToken()
        def _end = { !_nextToken }
        def _isParentheses = { isParentheses(_nextToken, rightParenthese) }
        return _end() || _isParentheses()
    }

    def operatorClassification = { token ->
        def optDef = [:]
        if (token) {
            if (operatorShouldBePrefix()) {
                token.position = operatorPosition.prefix
            }
            else if (operatorShouldBeInfix()) {
                token.position = operatorPosition.infix
            }

            if (operatorShouldBePostfix()) {
                token.position = operatorPosition.postfix
            }
        }
        token.optDef = optDef
        return token
    }

    def variableShouldBeFunction = {
        def _nextToken = peekNextToken()
        def _notPrevious = { !previousToken }
        def _previousIsInfix = { isOperator(previousToken, operatorPosition.infix) }
        def _previousIsPrefix = { isOperator(previousToken, operatorPosition.prefix) }
        def _isComma = { previousToken.type == tokenType.comma }
        def _previousIsLeftParenthese = { isParentheses(previousToken, leftParenthese) }
        def _nextTokenIsLeftParenthese = { isParentheses(_nextToken, leftParenthese) }
        def _previousCheck = { _notPrevious() || _previousIsInfix() || _previousIsPrefix() || _isComma() || _previousIsLeftParenthese() }
        return _previousCheck() && _nextTokenIsLeftParenthese()
    }

    def variableShouldBeInfix = {
        def _hasPrevious = { previousToken }
        def _previousIsNotInfix = { !isOperator(previousToken, operatorPosition.infix) }
        def _previousIsOperand = { isOperand(previousToken) }
        def _previousIsRightParenthese = { isParentheses(previousToken, rightParenthese) }
        def _previousCheck = { ((_previousIsNotInfix() && _previousIsOperand()) || _previousIsRightParenthese()) }
        return _hasPrevious() && _previousCheck()
    }

    def variableClassification = { token ->
        if (token) {
            if (variableShouldBeFunction()) {
                token.type = tokenType.function
                token.position = operatorPosition.prefix
            }
            else if (variableShouldBeInfix()) {
                token.type = tokenType.operator
                token.position = operatorPosition.infix
            }
        }
        return token

    }

    def isValidOperatorChar = { ch ->
        def _validVarChar = { isValidVariableChar(ch) }
        def _isWhiteSpace = { ch.isAllWhitespace() }
        def _isValidChar = { ch.toString() in [rightSquare, leftSquare, rightParenthese, comma, leftParenthese, doubleQuote, singleQuote] }
        return !(_validVarChar() || _isWhiteSpace() || _isValidChar())
    }

    def numberToken = { String ch ->
        def currentPos = pos
        def _res = """$ch"""
        def _decimalCount = 0
        def _isNumber = true
        def _isValidNumChar = { _ch -> _ch.isNumber() || _ch == decimal_seperator }
        def _isContinue = { _ch -> _ch && _isValidNumChar(_ch) }
        while (_isContinue(peek())) {
            ch = nextChar()
            if (ch == decimal_seperator) {
                _decimalCount++
            }
            if (_decimalCount >= 2) {
                pos = currentPos
                _isNumber = false
                break
            }
            _res = """$_res$ch"""
        }
        boolean _isBigDecimal = _res?.toString()?.contains(decimal_seperator)
        def _value = _isBigDecimal ? (_res?.toString() as BigDecimal) : (_res?.toString() as Integer)
        return _isNumber ? createToken(tokenType.number, _value) : null
    }

    def getNextToken = {
        def ch = nextChar()
        while (ch && ch.isAllWhitespace()) {
            ch = nextChar()
        }

        if (ch) {
            def token
            if (ch.isNumber()) {
                token = numberToken(ch)
                if (token) {
                    return token
                }
            }

            if (ch.toString() in [doubleQuote, singleQuote]) {
                token = stringToken(ch)
            }
            else if (ch == comma) {
                token = createToken(tokenType.comma, ch.toString())
            }
            else if (ch.toString() in parentheses) {
                token = createToken(tokenType.parentheses, ch.toString())
            }
            else if (ch.toString() in squares) {
                token = createToken(tokenType.squares, ch.toString())
            }
            else if (isValidVariableChar(ch?.toString())) {
                def tokenValue = """$ch"""
                while (peek() && isValidVariableChar(peek())) {
                    ch = nextChar()
                    tokenValue = """$tokenValue$ch"""
                }
                token = createToken(tokenType.variable, tokenValue.toString())
            }
            else {
                def tokenValue = """$ch"""
                while (peek() && isValidOperatorChar(peek())) {
                    ch = nextChar()
                    tokenValue = """$tokenValue$ch"""
                }

                token = createToken(tokenType.operator, tokenValue.toString())
                token.optDef = null
                token.position = null
            }

            return token
        }
        else {
            return [type: null, value: null]
        }

    }


    peekNextToken = {
        def token
        if (hasNextToken()) {
            token = tokenList.get(tokenIdx)
        }
        else if (hasNext()) {
            getNextToken()
            token = peekNextToken()
        }
        return token
    }

    def expressionVerification = { token ->
        if ((previousToken && token && isOperator(previousToken, operatorPosition.infix) && isOperator(token, operatorPosition.infix)) || (isOperand(previousToken) && isOperand(token))) {
            libs.FormulaEvaluator.Library.throwException("Invalid expression at position $previousToken.pos")
        }
    }

    def next = {
        def token = peekNextToken()
        if (token) {
            tokenIdx++
            switch (token.type) {
                case "operator":
                    token = operatorClassification(token)
                    break
                case "variable":
                    token = variableClassification(token)
                    break
                default:
                    break
            }
            expressionVerification(token)
            previousToken = token
        }
        return token
    }

    def tokenizer = [next   : next,
                     hasNext: { return hasNext() || hasNextToken() }]

    return tokenizer

}