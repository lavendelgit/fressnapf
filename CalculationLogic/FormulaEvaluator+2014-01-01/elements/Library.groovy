import groovy.transform.Field

@Field TOKEN_TYPE = [string       : "string",
                     number       : "number",
                     variable     : "variable",
                     parentheses  : "parentheses",
                     squares      : "squares",
                     comma        : "comma",
                     bindingResult: "bindingResult",
                     function     : "function",
                     operator     : "operator",]

@Field COMMON_CHAR = [decimal_seperator: ".",
                      underscore       : "_",
                      doubleQuote      : '"',
                      singleQuote      : "'",
                      leftParenthese   : "(",
                      rightParenthese  : ")",
                      leftSquare       : "[",
                      rightSquare      : "]",
                      strEscape        : "\\",
                      comma            : ","]

@Field OPERATOR_POS = [infix  : "infix",
                       postfix: "postfix",
                       prefix : "prefix"]
/**
 * Throw exception
 * @param : message. Error message
 * */
def throwException(message) {
    api.throwException(message)
}

/**
 * Get operator definition
 * @param: token. Parsed operator token
 * @return: Operator definition
 * */
def getOperator(token) {
    def operatorPosition = getOperatorPosition()
    def tokenType = getTokenType()
    def functionalOperators = libs.FormulaEvaluator.FormulaSupportedOperator.getFunctionalOperators()
    def infixOperators = libs.FormulaEvaluator.FormulaSupportedOperator.getInfixOperators()
    def prefixOperators = libs.FormulaEvaluator.FormulaSupportedOperator.getPrefixOperators()
    def postfixOperators = libs.FormulaEvaluator.FormulaSupportedOperator.getPostfixOperators()
    def operator
    switch ((token.type)) {
        case tokenType.function:
            operator = functionalOperators.get(token.value)
            break
        case tokenType.operator:
            if (token.position == operatorPosition.infix) {
                operator = infixOperators.get(token.value)
            }
            else if (token.position == operatorPosition.prefix) {
                operator = prefixOperators.get(token.value)
            }
            else if (token.position == operatorPosition.postfix) {
                operator = postfixOperators.get(token.value)
            }
            else {
                throwException("Unsupported operator position $token.position")
            }
            break
        default:
            break
    }
    if (!operator) {
        throwException("Operator ${token?.value} undefined at position ${(token?.pos) ?: 0} ")
    }
    return operator
}

/**
 * List of operator position supported
 * */
def getOperatorPosition() {
    return OPERATOR_POS
}

/**
 * List of token type supported
 * */
def getTokenType() {
    return TOKEN_TYPE
}

/**
 * List of common character
 * */
def getCommonCharacter() {
    return COMMON_CHAR
}

/**
 * Utility to convert to binded result
 * @param : token. Parsed token
 * @param : binded. Binded value
 * @return : Binded result object
 * */
def toBindedResult(token, binded) {
    return [type: TOKEN_TYPE.bindingResult, value: binded, token: token]
}

/**
 * Get variable from binding object
 * @param : token. Parsed token
 * @param : binding. Variables binding
 * @return : token binded result
 * */
def getBinding(token, Map... bindings) {
    if (!token) {
        return null
    }
    def tokenType = getTokenType()
    if (token.type == tokenType.bindingResult) {
        return token
    }

    def variable = token.value
    def result
    def firstBinding = bindings?.find { Map it -> it?.containsKey(variable) }
    if (firstBinding) {
        result = firstBinding.getAt(variable)
    }
    else if (variable == "null") {
        result = null
    }
    else if (variable == "true") {
        result = true
    }
    else if (variable == "false") {
        result = false
    }
    else if (token.type in [tokenType.number, tokenType.string]) {
        result = token.value
    }
    else {
        throwException("Binding for $token.value not found")
    }

    return toBindedResult(token, result)
}
