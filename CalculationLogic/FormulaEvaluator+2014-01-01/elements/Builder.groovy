/**
 * Set formula and return a formula evaluator builder
 * @param : formula. Expression string to evaluate
 * @return : builder liked Map:
 *  [
 *      setBinding: set variables binding for expression,
 *      setCached: provide cached for parsed expression,
 *      setExpression: set expression to evaluate,
 *      getParsedVariables: get all variable from the expression,
 *      eval: evaluate the expression and return result
 *  ]
 * */
def setFormula(formula) {
    Map globalCached = [:]
    String expression = formula
    Map emtpyBinding = [:]
    List contexts = [emtpyBinding]
    Map builder
    Closure getParsedExpression = {
        def parsed = globalCached?.getAt(expression)
        if (parsed) {
            return parsed
        }
        globalCached?.putAt(expression, libs.FormulaEvaluator.FormulaParser.parse(expression))
        return globalCached?.getAt(expression)
    }
    builder = [addBindings       : { Map... bindings ->
                   contexts.addAll(bindings)
                   return builder
               },
               addBinding        : { Map binding ->
                   contexts << binding
                   return builder
               },
               setBinding        : { Map... bindings ->
                   contexts = bindings?.toList()?.findAll()
                   return builder
               },
               setCached         : { cached ->
                   globalCached = cached
                   return builder
               },
               setExpression     : { expr ->
                   expression = expr
                   return builder
               },
               getParsedVariables: {
                   def tokens = getParsedExpression()
                   def tokenType = libs.FormulaEvaluator.Library.getTokenType()
                   return tokens?.findAll {
                       return (it.type in [tokenType.variable, tokenType.string, tokenType.number])
                   }
               },
               eval              : {
                   def tokens = getParsedExpression()
                   return libs.FormulaEvaluator.Evaluator.evalParsedExpression(tokens, *contexts)
               }

    ]
    return builder
}