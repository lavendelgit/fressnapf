def archivingConfiguration = out.ArchiverConfiguration
for (configuration in archivingConfiguration) {
    processArchiving(configuration)
}
return

def processArchiving(configuration) {
    if (configuration?.SourceTable ) {
        businessKey = configuration["BusinessKey"]?.split(',')*.replaceAll("[\n\r{\\\"\\\":\\\"}]", "")*.trim()
        exceptionKeys = configuration["ExceptionKey"]?.split(',')*.replaceAll("[\n\r{\\\"\\\":\\\"}]", "")*.trim()
        if (configuration?.SourceTable == "Competition Data") {
            List filter = []
            if(configuration?.Incremental=="Yes"){
                filter << Filter.greaterOrEqual("lastUpdateDate", out.CFLastRunDate)
            }
            List filterAttributes = businessKey
            iterarter = api.stream(configuration?.SourceTypeCode, "lastUpdateDate", filterAttributes, *filter)
            duplicatRecords = iterarter?.collect { it }
            iterarter?.close()
            filter = []
            businessKey?.each {
                filter << Filter.in(it, duplicatRecords[it])
            }
            String key
            iterarter = api.stream(configuration?.SourceTypeCode, "-lastUpdateDate", *filter)
            iterarter?.collect { attribute ->
                key = ""
                businessKey?.each { it ->
                    key = key ? key + "_" + attribute[it] : attribute[it]
                }
                if (!duplicateData[key]) {
                    duplicateData[key] = []
                    duplicateData[key] << attribute
                } else {
                    duplicateData[key] << attribute
                }
            }
            iterarter?.close()
            delete(duplicateData, configuration?.SourceTypeCode)
        } else if(configuration?.RemoveInactiveData == "Yes"){
            List oldData = []
            List filter = [Filter.equal("name", configuration?.SourceTable),
            Filter.lessThan(configuration?.ExceptionKey,api.targetDate()?.format("YYYY-MM-dd"))]
            oldData = api.find(configuration?.SourceTypeCode,1,api.getMaxFindResultsLimit(),null,*filter)
            deleteWholeSet(oldData, configuration?.SourceTypeCode)
        }else {
            duplicateData = [:]
            duplicateDetails = findDuplicateEntrys(configuration?.SourceTable, configuration?.SourceTypeCode, businessKey)
            List filter = [Filter.equal("name", configuration?.SourceTable),
                           /* Filter.lessOrEqual("PPRetailValidFrom",new Date()?.format("YYYY-MM-dd")),
                            Filter.lessOrEqual("PPOnlineValidFrom",new Date()?.format("YYYY-MM-dd")),
                            Filter.lessOrEqual("PPOfflineValidFrom",new Date()?.format("YYYY-MM-dd"))*/]
            businessKey?.each {
                filter << Filter.in(it, duplicateDetails[it])
            }
            exceptionKeys?.each {
                filter << Filter.lessOrEqual(it, new Date()?.format("YYYY-MM-dd"))
            }
            String key
            iterarter = api.stream(configuration?.SourceTypeCode, "-lastUpdateDate", *filter)
            iterarter?.collect { attribute ->
                key = ""
                businessKey?.each { it ->
                    key = key ? key + "_" + attribute[it] : attribute[it]
                }
                if (!duplicateData[key]) {
                    duplicateData[key] = []
                    duplicateData[key] << attribute
                } else {
                    duplicateData[key] << attribute
                }
            }
            iterarter?.close()
            delete(duplicateData, configuration?.SourceTypeCode)
        }

    }
}

List findDuplicateEntrys(String sourceTable, String sourceTypeCode, List bKey) {
    List filter = [Filter.equal("name", sourceTable),
                   Filter.greaterOrEqual("lastUpdateDate", out.CFLastRun),
    ]
    List filterAttributes = bKey
    iterator = api.stream(sourceTypeCode, "-lastUpdateDate", filterAttributes, *filter)
    duplicateRecords = iterator?.collect { it }
    iterator?.close()
    return duplicateRecords
}

void delete(Object duplicateData, String typeCode) {
    for (data in duplicateData)
        if (data?.value?.size() > 1) {
            duplicateRecord = data?.value?.minus(data?.value?.getAt(0))
            if(typeCode == "PCOMP" && duplicateRecord?.size()>0){
                duplicateRecord = duplicateRecord?.findAll {it.infoDate != (data?.value?.getAt(0))?.infoDate}
            }
            duplicateRecord?.each { record ->
                api.delete(typeCode, record)
            }
        }
}
void deleteWholeSet(Object data, String typeCode){
            data?.each { record ->
                api.delete(typeCode, record)
            }
}