List duplicateRecord = []
List deleteDuplicateRecord = []
api.local.duplicateRecords?.each{record->
    duplicateRecord = []
    if(record?.value?.size()>1){
        duplicateRecord << record?.value?.remove(0)
        delete(duplicateRecord)
    }
}
return

void delete(Object duplicateRecord){
    duplicateRecord?.each{ record->
        api.delete("PX10",record)
    }
}