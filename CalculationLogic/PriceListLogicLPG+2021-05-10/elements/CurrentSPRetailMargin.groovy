if(out.PurchasePriceRetail!=0 && out.PurchasePriceRetail&& out["SalesPrice"+out.CalculationChannel+"EXVAT"]) {
    BigDecimal listPrice = out["SalesPrice"+out.CalculationChannel+"EXVAT"]
    BigDecimal markUpPercentage = listPrice > 0 ?
            ((listPrice - out.PurchasePriceRetail) / listPrice)
            : 0.0
    return out.CommonLib.UIUtils.getArrowedCell(markUpPercentage)
}
return