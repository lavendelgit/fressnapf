BigDecimal previousPurchasePrice = out.FuturePurchasePriceRetailOffline as BigDecimal
BigDecimal currentPurchasePrice = out.PurchasePriceRetail as BigDecimal
if (currentPurchasePrice && previousPurchasePrice && previousPurchasePrice != currentPurchasePrice) {
    percentage = (previousPurchasePrice - currentPurchasePrice) / currentPurchasePrice
    if (percentage > 0.05) {
        api.redAlert("Percentage difference is greater than 5%")
    } else if (percentage < -0.05) {
        api.redAlert("Percentage difference is less than -5%")
    }
    return out.CommonLib.UIUtils.getArrowedCell(percentage)
    // return out.CommonLib.UIUtils.getArrowedCell((currentPurchasePrice - previousPurchasePrice) / previousPurchasePrice)
}