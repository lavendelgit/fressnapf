listPrice = out.CalculatedListPrice!=out.ListPrice && out.ListPrice && out.ListPrice!=0 ? out.ListPrice : out.CalculatedListPrice
if(out["PurchasePrice"+out.CalculationChannel] && listPrice){
    api.local.priceBuildupMatrix.ActualMargin =  out.CommonLib.RoundingUtils.roundNumber(((listPrice - out["PurchasePrice"+out.CalculationChannel])/listPrice)*100, 2)
}
return