def sortofArticle = out.MaterialType
def sku = out.Article
api.local.beforeCConversionOnline = [:]

if ((api.local.priceBuildupMatrix?.marketingCamp && api.local.contextValues) || api.local.contextValues) {
    return out.ArticleContext?.FuturePurchasePriceOnline ?: null

}

if (sortofArticle == "HAWA") {
    def country = out.Country
    def countryCode = api.global.countriesMap[country]
    def getPurchasePriceMaster = api.global.purchasePrice?.getAt(out.Article)?.sort { it.attribute9 }?.find { it.attribute9 > out.PricingDate?.format("YYYY-MM-dd") }
    api.local.priceBuildupMatrix.FutureOnlinePrice = getPurchasePriceMaster?.attribute1
    api.local.priceBuildupMatrix.FutureOnlineCurrency = getPurchasePriceMaster?.attribute2
    api.local.futurePPOValidFrom = getPurchasePriceMaster?.attribute9
    def purPriceOnlinePX = getPurchasePriceMaster?.attribute1 ?: 0
    def purPriceOnlineCurrency = getPurchasePriceMaster?.attribute2
    def purPriceOnline = purPriceOnlinePX //* (1 - out.TripleNetFactor)
/*api.trace("purPriceOnlinePX", purPriceOnlinePX)
api.trace("TripleNetFactor", (1 - out.TripleNetFactor))*/
    return libs.FressnapfLib.RoundingUtils.roundNumber(out.CommonLib.CommonUtils.applyExchangeRate(purPriceOnline,
            purPriceOnlineCurrency,
            out.TargetCurrency,
            out.PricingDate), 5) ?: null
} else {
    def setTrayCostData = api.global.batch[sku].SetTrayCost
    def cost = (setTrayCostData)?.attribute1
    //!cost && out.CalculationChannel == "Online" ?api.addWarning("Purchase Price Online not available for "+out.Article):""
    cost = (cost) ?: 0
    return libs.FressnapfLib.RoundingUtils.roundNumber(out.CommonLib.CommonUtils.applyExchangeRate(cost,
            out.PurchasePriceOnlineCurrency,
            out.TargetCurrency,
            out.PricingDate), 5) ?: null
}