import net.pricefx.server.dto.calculation.AttributedResult

api.local.appliedCurrencyExchange = [:]
api.local.setTrayPrice = [:]
api.local.PRGBasePriceFamily = [:]
api.local.PRGRelatedPriceFamily = [:]
api.local.PRGPricingApplied = [:]
api.local.bomArticleComp
api.local.appliedCurrencyExchange = [:]
api.local.appliedConversionFactor = [:]
Map exchangeRates

BigDecimal getCompetitorBasedPricing() {
    String channelPresp = out.CalculationChannel
    String competitionType = out.CalculationChannel
    Map competiterMasterPP = out.CommonLib.DataUtils.getCompetitorrPP()
    String productSku = out.Product?.sku
    String basket = out.Basket
    String articleStatus = out.Product?.attribute14
    def country = out.Country
    def cgrNo = out.CGRNo
    BigDecimal targetIndexMap = (out.CommonLib.DataUtils.getTargetPriceIndexMaster(country, cgrNo, channelPresp)?.getAt(cgrNo + '-' + basket)) ?: 1
    BigDecimal suggestedPrice = 0
    String competitionDataMasterKey = productSku
    Map competitionData = api.productCompetition()?.collectEntries { [(it.competitorSku): it] }
    List competitionNumbersList = competitionData?.collect { key, value -> key }
    Object aggregation = [:]
    String currency = ''
    def compBasketMapping = out.CommonLib.DataUtils.getCompetitorBasketMappingMaster(country)
    List competitorList = []
    BigDecimal minValid
    BigDecimal maxValid
    competitionData?.each { key, value ->
        Object tempCBM = value
        String compBasketMappingKey = channelPresp + '_' + basket + "_" + tempCBM.competitorSku
        Map compMasterValue = competiterMasterPP[tempCBM.competitorSku + '_' + out.Country + "_" + tempCBM.competitionType]
        minValid = (compMasterValue?.DeviationFactorMin ?: 0) * (out['SalesPrice' + out.CalculationChannel] ?: 0)
        maxValid = (compMasterValue?.DeviationFactorMax ?: 0) * (out['SalesPrice' + out.CalculationChannel] ?: 0)
        if (tempCBM.price >= minValid && tempCBM.price <= maxValid) {
            basketRecord = compBasketMapping[compBasketMappingKey]
            if (basketRecord) tempCBM = tempCBM + basketRecord
            tempCBM['targetPriceIndex'] = targetIndexMap
            currency = tempCBM.currency
            switch (tempCBM?.calculationMode) {
                case 'AVG':
                    if (aggregation[tempCBM?.prio] == null) {
                        aggregation[tempCBM?.prio] = new ArrayList<>()
                    }
                    aggregation[tempCBM?.prio].add(tempCBM.weight * tempCBM.price)
                    break
                case 'MIN':
                    BigDecimal compPrice = tempCBM.price as BigDecimal
                    BigDecimal oldCompPrice = ((aggregation[tempCBM?.prio]?.getClass() == ArrayList ? aggregation[tempCBM?.prio]?.getAt(0) : aggregation[tempCBM?.prio]) ?: 0) as BigDecimal
                    aggregation[tempCBM?.prio] = (oldCompPrice == null || (oldCompPrice < compPrice && oldCompPrice != 0) ? oldCompPrice : compPrice) as BigDecimal
                    break
                case 'MAX':
                    BigDecimal compPrice = tempCBM.price as BigDecimal
                    BigDecimal oldCompPrice = (aggregation[tempCBM?.prio] ?: 0) as BigDecimal
                    aggregation[tempCBM?.prio] = (oldCompPrice == null || (oldCompPrice > compPrice && oldCompPrice != 0) ? oldCompPrice : compPrice) as BigDecimal
                    break
            }
            competitorList << tempCBM
        }
    }
    api.local.priceBuildupMatrix.CompetitorList = competitorList
    api.local.priceBuildupMatrix.CompetitorList = competitorList
    api.local.currency = currency
    api.local.aggregation = aggregation
    api.local.tp = targetIndexMap
    aggregation = aggregation?.sort { it.key }
    aggregation?.each { key, value ->
        if (key) {
            BigDecimal result = 0.0
            BigDecimal aggregationValue = 0.0
            if (value in List) {
                aggregationValue = (value.sum() / value.size())
                result = ((value.sum() / value.size()) / targetIndexMap) * 100
            } else {
                aggregationValue = value
                result = (value / targetIndexMap) * 100
            }
            if (suggestedPrice == null || suggestedPrice == 0) {
                api.local.priceBuildupMatrix.SelectedCompetitorPrice = aggregationValue
                api.local.priceBuildupMatrix.CompetitorPriority = key as int
                api.local.priceBuildupMatrix.PriceWithTargetIndex = result
                suggestedPrice = result
            }

            aggregation[key] = result
        }
    }
    if (!competitionData && api.local.PricingMethod?.getAt(0) == 'Competitor-based Pricing') {
        api.addWarning('Competition Data not available for Article : ' + out.Article)
    } else if (!aggregation && api.local.PricingMethod?.getAt(0) == "Competitor-based Pricing" && competitionData) {
        api.addWarning('Competitor Basket Mapping not available for Basket ' + out.Basket + ' Competitor ' + competitorList?.competitorSku?.getAt(0))
    }
    return suggestedPrice
}

if (!api.global.LPGCount) {
    def date1 = new Date()
    api.global.LPGCount = 0
    api.global.LPGStartTime = date1.format('yyyyMMdd-HH:mm:ss.SSS')
}

BigDecimal calculateCostPlusPricing(String pricingMethod) {
    BigDecimal costPlusPrice = 0.0
    BigDecimal price
    BigDecimal futurePrice
    BigDecimal sp
    BigDecimal ppDelta
    channel = out.CalculationChannel
    api.local.priceBuildupMatrix.ppApplied = ""
    if (channel == 'Offline') {
        price = out.PurchasePriceConfiguration == 'PP Offline Price' ? out.PurchasePriceOffline : out.PurchasePriceRetail
        futurePrice =  out.PurchasePriceConfiguration == 'PP Offline Price' ? out.FuturePurchasePriceOffline : out.FuturePurchasePriceRetailOffline
        ppDelta = out.PurchasePriceConfiguration == 'PP Offline Price' ? out.PurchasePriceOfflineDelta : out.PurchasePriceRetailDelta
        preferedMargin = out.PurchasePriceConfiguration == 'PP Offline Price' ? out.SalesPriceMarginPercentageOffline : out.CurrentSPRetailMargin
        sp = out.SalesPriceOffline
        futurePrice = out.SupplyMode == "03"? out.FuturePurchasePriceOffline : futurePrice
        price = out.SupplyMode == "03"? out.PurchasePriceOffline : price
        preferedMargin = out.SupplyMode == "03" ? out.SalesPriceMarginPercentageOffline : preferedMargin
        api.local.priceBuildupMatrix.appliedCostPlusmargin = preferedMargin
        api.local.priceBuildupMatrix.appliedCostPlusmarginCompRes = out.PurchasePriceConfiguration == 'PP Offline Price' ? 'Current Selling Margin Offline' :( out.SupplyMode == "03"?'Current Selling Margin Offline': 'Current Selling Retail Margin')
    } else {
        price = out['PurchasePrice' + channel]
        sp = out.SalesPriceOnline
        futurePrice = out.FuturePurchasePriceOnline
        ppDelta = out.PurchasePriceOnlineDelta
        preferedMargin = out.SalesPriceMarginPercentageOnline
        api.local.priceBuildupMatrix.appliedCostPlusmargin = preferedMargin
        api.local.priceBuildupMatrix.appliedCostPlusmarginCompRes = 'Current Selling Margin Online'
    }
if((pricingMethod=="CostPlus - CurrentMargin" || pricingMethod=="CostPlus - CurrentMargin (IncreaseOnly)") && preferedMargin){
if(!futurePrice){
    api.addWarning(pricingMethod + ' No Future PP - Roll back to SP')
    api.local.costPlusWarning = ' No Future PP - Roll back to SP'
    api.local.costPlusMarginprice = out['SalesPrice' + out.CalculationChannel]
    return api.local.costPlusMarginprice
}
    if(pricingMethod=="CostPlus - CurrentMargin (IncreaseOnly)" && ppDelta && (ppDelta*100) < 0 && futurePrice && preferedMargin){
        api.addWarning(pricingMethod + ' PP Decreased - Roll back to SP')
        api.local.costPlusWarning = ' PP Decreased - Roll back to SP'
        api.local.costPlusMarginprice = out['SalesPrice' + out.CalculationChannel]
        return api.local.costPlusMarginprice
    }
    api.local.priceBuildupMatrix.ppAppliedMargin = "Current Margin"
    api.local.priceBuildupMatrix.ppApplied = "Future "
    api.local.priceBuildupMatrix.ppPrice = futurePrice
    api.local.priceBuildupMatrix.CostPlusmargin = preferedMargin
    costPlusPrice = (-futurePrice / (preferedMargin - 1))
}else{
    List productAttributes = [out.Country ?: '*', channel, out.Category ?: "*", out.SubCategory ?: "*", out.CGRNo ?: "*", out.BrandType ?: "*", out.Brand ?: "*", out.SubBrand ?: "*"]
    margin = getMargin(libs.FressnapfLib.DataUtils.getCostPlusPricingMaster(out.Country), productAttributes, pricingMethod)
    if (margin && price) {
        api.local.priceBuildupMatrix.CostPlusmargin = margin
        if ((margin / 100) - 1 != 0) {
            costPlusPrice = (-price / ((margin / 100) - 1))
        } else {
            costPlusPrice = 0
            api.addWarning(pricingMethod + ': Calculation terminated due Margin : ' + margin + '% Exception Divide by Zero')
        }

    } else if (!margin && api.local.PricingMethod?.getAt(0) != 'Competitor-based Pricing') {
        api.addWarning('Margin not available')
    } else if (price) {
        api.addWarning('Purchase Price not available')
    }
    api.local.priceBuildupMatrix.CostPlusmarginCompareRes = (preferedMargin) ? (preferedMargin * 100) > margin : false
    if (api.local.priceBuildupMatrix.CostPlusmarginCompareRes) {
        api.addWarning(pricingMethod + ' is greater than Current Margin')
        api.local.costPlusMarginprice = out['SalesPrice' + out.CalculationChannel]
        return api.local.costPlusMarginprice
    }
}

    api.local.costPlusMarginprice = costPlusPrice
    api.local.beforeVATPrice = out.VAT ? costPlusPrice : 0.0
    costPlusPrice = costPlusPrice && out.VAT ? (costPlusPrice * (1 + (out.VAT / 100))) : costPlusPrice
    api.local.afterVATPrice = out.VAT ? costPlusPrice : 0.0
    return costPlusPrice
}

String frameKey(List keys) {
    String formattedKey
    keys.each {
        key ->
            formattedKey = formattedKey ? (formattedKey += '_' + key) : key
    }
    return formattedKey
}

Map getSortedCombinations(List productAttributes) {
    List combinations = []
    for (attributeValue in productAttributes) {
        combinations << [attributeValue, '*']
    }
    List combinationList = combinations?.combinations()
    return combinationList?.groupBy({ getWeightage(it, '*') })?.sort { -it.key }
}

Integer getWeightage(List keys, String defaultValue) {
    Integer weightage = 0
    Integer weightageIndex
    keys.eachWithIndex { key, index ->
        weightageIndex = index ? 2.power(index) : 1
        weightage += ((defaultValue.equals(key) ? 0 : weightageIndex))
    }
    return weightage
}

BigDecimal getMargin(Map marginCache, List productAttributes, String pricingMethod, boolean trim = true) {
    Map sortedCombinations = getSortedCombinations(productAttributes)
    method = trim ? pricingMethod?.split('-')*.trim()?.getAt(1) : pricingMethod
    String key
    for (combination in sortedCombinations) {
        key = frameKey(combination.value.getAt(0))
        margin = marginCache?.getAt(key)?.getAt(method)
        if (margin || margin == 0) {
            return margin

        }
    }
}

Map pricingMethod(Map PricingMethodCache, List productAttributes) {
    Map pricingMethod = [:]
    Map sortedCombinations = getSortedCombinations(productAttributes)
    String key
    for (combination in sortedCombinations) {
        key = frameKey(combination.value.getAt(0))
        pricingMethod = PricingMethodCache?.getAt(key)
        if (pricingMethod != [:] && pricingMethod) {
            return pricingMethod

        }
    }
}

BigDecimal getPriceByBOM() {
    def bomData = api.bomList()
    if (!bomData) api.addWarning('No BOM data available for Set/Tray Logic')
    def componentCount = 0
    def setTrayPrice = 0
    def hawaSKUContext
    def hawaskuPrice = 0
    String bomArticleComp

    bomData.each {
        hawasku = it.label
        quantity = it.quantity
        sellingPrice = api.global.sellingPrices[hawasku]?.find { it.salesOrg == out.CalculationChannelCode }?.salesPrice ?: api.global.sellingPrices[hawasku]?.find { it.salesOrg == out.OppositeCalculationChannelCode }?.salesPrice
        hawaSKUContext = api.currentContext(hawasku)
        hawaskuPrice = sellingPrice ?: hawaSKUContext?.ResultPrice as BigDecimal
        bomArticleComp = bomArticleComp ? bomArticleComp + ' | ' + hawasku + '-(Quantity:' + (quantity as Integer) + ')(Price:' + hawaskuPrice + ')' : hawasku + "-(Quantity:" + (quantity as Integer) + ")(Price:" + hawaskuPrice + ")"
        if (!hawaskuPrice) {
            componentCount += 1
        }
        if (hawaskuPrice) setTrayPrice += (hawaskuPrice * quantity)

    }
    if (bomData && (componentCount == bomData.size())) {
        api.addWarning('HAWA Price Data Not Available for Set/Tray Logic')
    }
    api.local.priceBuildupMatrix.setTrayPricebomArticleComp = bomArticleComp
    api.local.priceBuildupMatrix.setTrayPricecomponentCount = bomArticleComp && componentCount == 0 ? 1 : componentCount
    api.local.priceBuildupMatrix.setTrayPriceSetTrayPrice = setTrayPrice
    return setTrayPrice
}

BigDecimal applySetTrayDiscount(setTrayPriceByComponent) {

    def channel = out.CalculationChannel
    def cgrNo = out.CGRNo
    def brandType = out.BrandType
    def basket = out.Basket
    def discountKey = channel + '~' + cgrNo + "~" + brandType + "~" + basket
    def setTrayDiscountMap = api.global.setTrayDiscountMap
    def discount = (setTrayDiscountMap) ? setTrayDiscountMap[discountKey] : 0
    setTrayPriceByComponent = (setTrayPriceByComponent) ? setTrayPriceByComponent as BigDecimal : 0
    discount = (discount) ? discount as BigDecimal : 0
    def setTrayDiscountedPrice = setTrayPriceByComponent * ((100 + discount) / 100)
    api.local.priceBuildupMatrix.setTrayPriceDiscount = discount
    //api.local.setTrayPrice["DiscountedPrice"] = setTrayDiscountedPrice
    return (setTrayDiscountedPrice) ?: 0
}


BigDecimal PRGPricing() {
    api.global.prgPriceFamily = [:]
    Map PRG = libs.FressnapfLib.DataUtils.getPRGMaster(out.Country)
    relatedFamilyValue = PRG?.values()?.find { it.RelatedFamily == out.PriceFamilyCode }
    BigDecimal PRGPrice = 0.0
    BigDecimal baseFamilyPrice = 0.0
    BigDecimal baseContent
    String baseContentUnit
    if (relatedFamilyValue) {
        String lookupBasePriceChannel = out.CalculationChannel == 'Online' ? '1110' : 'DE01'
        api.local.priceBuildupMatrix.PRGPricingApplied = false
        baseFamily = relatedFamilyValue?.BaseFamily
        api.local.priceBuildupMatrix.PRGBasePriceFamily = baseFamily
        api.local.priceBuildupMatrix.PRGRelatedPriceFamily = out.PriceFamilyCode
        baseChannel = libs.FressnapfLib.DataUtils.getArticleChannelMappingMaster(out.Country)[api.global.priceFamilyLeadMap?.getAt(baseFamily)]
        api.local.priceBuildupMatrix.PRGRelatedPriceFamilyNetContent = out.NetContent
        api.local.priceBuildupMatrix.PRGRelatedPriceFamilyContentUnit = out.ContentUnit
        baseArticleDetails = api.global.sellingPrices?.getAt(api.global.priceFamilyLeadMap?.getAt(baseFamily))
//api.currentContext(api.global.priceFamilyLeadMap?.getAt(baseFamily))
        api.local.priceBuildupMatrix.PRGBasePriceFamilyLeadArticle = api.global.priceFamilyLeadMap?.getAt(baseFamily)
        if (baseChannel && baseArticleDetails) {
            baseFamilyPrice = baseArticleDetails?.find { it.salesOrg == lookupBasePriceChannel }?.salesPrice
            api.local.priceBuildupMatrix.PRGBasePriceFamilyPrice = baseFamilyPrice ?: 0.0
            baseContent = api.product('NetContent', api.global.priceFamilyLeadMap?.getAt(baseFamily))
//baseArticleDetails?.NetContent
            api.local.priceBuildupMatrix.PRGBasePriceFamilyNetContent = baseContent ?: 0.0
            baseContentUnit = api.product('Product Unit', api.global.priceFamilyLeadMap?.getAt(baseFamily))
//baseArticleDetails?.ContentUnit
            api.local.priceBuildupMatrix.PRGBasePriceFamilyContentUnit = baseContentUnit ?: ''
            api.local.priceBuildupMatrix.PRGRelatedPriceFamilyNetContent = out.NetContent ?: 0.0
            api.local.priceBuildupMatrixPRGRelatedPriceFamilyContentUnit = out.ContentUnit ?: ''
        }
        if (relatedFamilyValue && out.NetContent && baseContent && baseFamilyPrice) {

            if (relatedFamilyValue?.FactorPct && !relatedFamilyValue?.FactorAbs) {
                baseContent = out.CommonLib.CommonUtils.applyUnitConversion(baseContent, baseContentUnit, out.ContentUnit)
                api.local.priceBuildupMatrix.PRGPricingAppliedMethod = 'Factor %' + '(' + relatedFamilyValue?.FactorPct + ')'
                api.local.priceBuildupMatrix.PRGPricingAppliedStringFormula = 'Price of Article from Basic Family * (Net Content Related Family / Net Content Basic Family) * (1 - Factor%)'
                api.local.priceBuildupMatrix.PRGPricingAppliedFormula = baseFamilyPrice + ' * (' + out.NetContent + ' / ' + baseContent + ' ) * (1 -  ' + relatedFamilyValue?.FactorPct + ')'
                PRGPrice = baseFamilyPrice * (out.NetContent / baseContent) * (1 - relatedFamilyValue?.FactorPct)
                PRGPrice > 0 ? api.global.prgPriceFamily[out.PriceFamilyCode] = PRGPrice : ''
            } else if (relatedFamilyValue?.FactorAbs && !relatedFamilyValue?.FactorPct) {
                baseContent = out.CommonLib.CommonUtils.applyUnitConversion(baseContent, baseContentUnit, out.ContentUnit)
                api.local.priceBuildupMatrix.PRGPricingAppliedMethod = 'Abs' + '(' + relatedFamilyValue?.FactorAbs + ')'
                api.local.priceBuildupMatrix.PRGPricingAppliedStringFormula = '(Price of Article from Basic Family * (Net Content Related Family / Net Content Basic Family)) - Factor abs'
                api.local.priceBuildupMatrix.PRGPricingAppliedFormula = baseFamilyPrice + ' * (' + out.NetContent + ' / ' + baseContent + ' ) - ' + relatedFamilyValue?.FactorAbs
                PRGPrice = baseFamilyPrice * (out.NetContent / baseContent) - relatedFamilyValue?.FactorAbs
                PRGPrice > 0 ? api.global.prgPriceFamily[out.PriceFamilyCode] = PRGPrice : ''
            } else if (!relatedFamilyValue?.FactorPct && !relatedFamilyValue?.FactorAbs) {
                baseContent = out.CommonLib.CommonUtils.applyUnitConversion(baseContent, baseContentUnit, out.ContentUnit)
                api.local.priceBuildupMatrix.PRGPricingAppliedMethod = 'Factor (0)'
                api.local.priceBuildupMatrix.PRGPricingAppliedStringFormula = 'Price of Article from Basic Family * (Net Content Related Family / Net Content Basic Family) * (1 - Factor)'
                api.local.priceBuildupMatrix.PRGPricingAppliedFormula = baseFamilyPrice + ' * (' + out.NetContent + ' / ' + baseContent + ' ) * (1 - 0)'
                PRGPrice = baseFamilyPrice * (out.NetContent / baseContent) * (1)
                PRGPrice > 0 ? api.global.prgPriceFamily[out.PriceFamilyCode] = PRGPrice : ''
            }
        } else {
            api.local.priceBuildupMatrix.PRGPricingFailReason = !baseFamilyPrice ? 'PRG aborted, Price for either Base Family is missing' : api.local.priceBuildupMatrix.PRGPricingFailReason
            api.local.priceBuildupMatrix.PRGPricingFailReason = !out.NetContent || !baseContent ? 'PRG aborted, Net content for either Base Family or Related Family is missing' : api.local.priceBuildupMatrix.PRGPricingFailReason
            out.NetContent ?: api.addWarning('Related Family(' + api.local.priceBuildupMatrix.PRGRelatedPriceFamily + ') article does not have Net content ')
            baseContent ?: api.addWarning('Base Family(' + api.local.priceBuildupMatrix.PRGBasePriceFamily + ')article does not have Net content')
        }
        return PRGPrice ?: 0.0
    }
}

AttributedResult getArrowedCell(BigDecimal numberValue) {
    return getArrowedCell(numberValue, null)
}

AttributedResult getArrowedCell(BigDecimal numberValue, String suffix) {
    return getArrowedCell(numberValue, numberValue ? (numberValue < 0 ? -1 : 1) : 0, suffix)
}

AttributedResult getArrowedCell(BigDecimal numberValue, int equals, String suffix) {
    bColor = api.local.bcColor
    tColor = api.local.txColor
    def lib = libs.FressnapfLib
    String compareCode = numberValue ? (numberValue < 0 ? 'DECREASE' : 'INCREASE') : 'FLAT'
    suffix = (suffix ? ' ' + suffix : '') + lib.Constants.SYMBOLS[compareCode]
    suffix = api.local.sufixAdd ? suffix + '  ' + api.local.sufixAdd : suffix
    return api.attributedResult(lib.RoundingUtils.roundNumber(numberValue, 4))
            .withTextColor(tColor)
            .withSuffix(suffix)
            .withBackgroundColor(bColor)
}

def validateFPPMargin(def percentage) {
    api.local.sufixAdd = ''
    if (percentage > 0 && percentage <= 0.05) {
        api.local.bcColor = 'red'
        api.local.txColor = 'white'
        return getArrowedCell(percentage)
    } else if (percentage > 0.05) {
        api.redAlert('Percentage difference is greater than 5%')
        api.local.txColor = 'white'
        api.local.bcColor = 'red'
        return getArrowedCell(percentage)
    } else if (percentage < 0 && percentage >= -0.05) {
        api.local.bcColor = '#bbfc7e'
        api.local.txColor = 'black'
        return getArrowedCell(percentage)
    } else if (percentage < -0.05) {
        api.local.bcColor = 'green'
        api.local.txColor = 'white'
        api.local.sufixAdd = '⚠️'
        return getArrowedCell(percentage)
    }
}

return