if(api.global.calculationEndDate!= 0 && api.local.contextValues){
    return out.ArticleContext?.QuantityDataSource
}
BigDecimal currentSellingPrice = out["SalesPrice"+out.CalculationChannel+"EXVAT"]
BigDecimal newRP = out.PriceExVAT
Integer quantity = out.QuantityDataSource?:0//api.global.batch[out.Article]?.SalesQuantity
if(quantity&&currentSellingPrice&&newRP){
    api.local.priceBuildupMatrix.impactForecastStringFormula = "(New RP $out.CalculationChannel net - Current SP $out.CalculationChannel net) * Quantity(Data Source - 12M)"
    api.local.priceBuildupMatrix.impactForecastActualFormula = "(("+newRP+"-"+currentSellingPrice+") *"+quantity+")"//"(New RP $out.CalculationChannel net - Current SP $out.CalculationChannel net) * Quantity(Data Source - 12M)"
    BigDecimal priceChange = newRP - currentSellingPrice
    return priceChange * quantity
}