BigDecimal previousPurchasePrice = (api.global.previousPurchasePrice?.getAt(out.Article)?.NPPOfflineWholesale) ?: out.ArticleContext?.PurchasePriceOffline
if (previousPurchasePrice && previousPurchasePrice != out.PurchasePriceOffline) {
    return previousPurchasePrice as BigDecimal
} else if (out.ArticleContext?.getAt("PreviousPurchasePriceOffline") && out.ArticleContext?.getAt("PreviousPurchasePriceOffline") != out.PurchasePriceOffline) {
    return out.ArticleContext?.getAt("PreviousPurchasePriceOffline") as BigDecimal
}