def sortofArticle = out.MaterialType
def sku = out.Article
api.local.beforeCConversionRetail = [:]
if((api.local.priceBuildupMatrix?.marketingCamp && api.local.contextValues) || api.local.contextValues){
    return out.ArticleContext?.PurchasePriceRetail?:null

}

if (sortofArticle == "HAWA") {
    def country = out.Country
    def countryCode = api.global.countriesMap[country]
    def getPurchasePriceMaster = api.global.purchasePrice
    if(getPurchasePriceMaster && getPurchasePriceMaster?.getAt(out.Article)?.purchaseCurrencyRetail != out.Currency){
        api.local.beforeCConversionRetail.Price = getPurchasePriceMaster?.getAt(out.Article)?.purchasePriceRetail
        api.local.beforeCConversionRetail.Currency = getPurchasePriceMaster?.getAt(out.Article)?.purchaseCurrencyRetail
    }

    def purPriceOfflinePX = getPurchasePriceMaster?.getAt(out.Article)?.purchasePriceRetail ?: 0
    def purPriceOfflineCurrency = getPurchasePriceMaster?.getAt(out.Article)?.purchaseCurrencyRetail
    def purPriceOffline = purPriceOfflinePX //* (1 - out.TripleNetFactor)
/*api.trace("purPriceOfflinePX", purPriceOfflinePX)
api.trace("TripleNetFactor", (1 - out.TripleNetFactor))*/

    return libs.FressnapfLib.RoundingUtils.roundNumber(out.CommonLib.CommonUtils.applyExchangeRate(purPriceOffline,
            purPriceOfflineCurrency,
            out.TargetCurrency,
            out.PricingDate),5)?:null
}else{
    def setTrayCostData = api.global.batch[sku].SetTrayCost
    def cost = (setTrayCostData)?.attribute6
    def retailCurr = (setTrayCostData)?.attribute7
    retailCurr = (retailCurr)?:out.TargetCurrency
    cost = (cost)?:0
    return libs.FressnapfLib.RoundingUtils.roundNumber(out.CommonLib.CommonUtils.applyExchangeRate(cost,
            retailCurr,
            out.TargetCurrency,
            out.PricingDate),5)?:null
}