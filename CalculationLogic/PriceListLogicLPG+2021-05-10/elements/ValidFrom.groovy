List day = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
if(api.local.priceBuildupMatrix?.marketingCamp){
    def channel = out.CalculationChannel
    def sku = out.Article
    def salesOrg = api.global.countryChannelMap[channel]
    def sellingPriceRecord = api.global.batch[sku]?.SellingPrice
    def offlineSellingData = (sellingPriceRecord)?.findAll { it.attribute2 == salesOrg }
    def SellingDate = (offlineSellingData) ? offlineSellingData[0].attribute5 : out.ArticleContext?.ValidFrom
    return SellingDate
}
if(api.local.contextValues){
    return out.ArticleContext?.ValidFrom
}

if(api.currentItem()?.approvalState == "APPROVED" && (api.currentItem()?.approvalDate?.split('T')?.getAt(0)) == (api.targetDate()?.format("yyyy-MM-dd"))){
    return out.ArticleContext?.ValidFrom
}

if(out.AutoApprove && !out.CategoryManagerApprovel && out.CalculationChannel == "Offline"){
    activeDay = day.indexOf(api.local.priceChangeThreshold?.ApprovalDeadLineDay)+1
    actualday = (day.indexOf(out.PricingDate?.format('EEEE')))
    differenceDay = activeDay<actualday? actualday - activeDay : activeDay - actualday
    return out.PricingDate + differenceDay
}else if(out.AutoApprove && !out.CategoryManagerApprovel && out.CalculationChannel == "Online"){
    return out.PricingDate + 1
}

if(api.currentItem()?.ResultPrice == out.ListPrice){
    if((api.currentItem()?.approvalState == "APPROVED" || api.currentItem()?.approvalState == "AUTO_APPROVED") && !out.CategoryManagerApprovel){
        String pricingDate = out.PricingDate?.format("yyyy-MM-dd")
        String approveDate = api.currentItem()?.approvalDate?.split('T')?.getAt(0)
        if(pricingDate == approveDate){
            activeDay = day.indexOf(api.local.priceChangeThreshold?.ApprovalDeadLineDay)+1
            actualday = (day.indexOf(out.PricingDate?.format('EEEE')))
            differenceDay = activeDay<actualday? actualday - activeDay : activeDay - actualday
            return out.PricingDate + differenceDay
        }else{
            activeDay = day.indexOf(api.local.priceChangeThreshold?.ApprovalDeadLineDay)+1
            actualday = (day.indexOf(api.parseDate("yyyy-MM-dd",approveDate).format('EEEE')))
            differenceDay = activeDay<actualday? actualday - activeDay : activeDay - actualday
            return out.PricingDate + differenceDay
        }
    }
}

return