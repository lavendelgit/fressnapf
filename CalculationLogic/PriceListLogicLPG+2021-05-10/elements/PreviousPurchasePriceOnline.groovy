BigDecimal previousPurchasePrice = (api.global.previousPurchasePrice?.getAt(out.Article)?.NPPOnline) ?: out.ArticleContext?.PurchasePriceOnline
if (previousPurchasePrice && previousPurchasePrice != out.PurchasePriceOnline) {
    return previousPurchasePrice as BigDecimal
} else if (out.ArticleContext?.getAt("PreviousPurchasePriceOnline") && out.ArticleContext?.getAt("PreviousPurchasePriceOnline") != out.PurchasePriceOnline) {
    return out.ArticleContext?.getAt("PreviousPurchasePriceOnline") as BigDecimal
}