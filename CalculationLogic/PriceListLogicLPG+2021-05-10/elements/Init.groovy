//Added this element for FRES-75
api.local.priceBuildupMatrix = [:]
api.local.pid = out.Article
def country = out.Country

if (!api.global.priceFamilyLeadMap) {
    List filter = [
            Filter.isNotNull("attribute1"),
            Filter.isNotNull("attribute2")
    ]
    streamRecords = api.stream("PX3", null, ["attribute1", "attribute2"], *filter)
    api.global.priceFamilyLeadMap = streamRecords?.collectEntries { [(it.attribute1): it.attribute2] }
}
if (!api.global.pastDay) {
    Date previousDate = api.targetDate()
    api.global.pastDay = previousDate?.minus(365)
}
if(!api.global.countryCode)
{
    api.global.countryCode=api.findLookupTableValues("Countries")?.collectEntries{[(it.value):it.name]}
}
def countryCode
if (!api.global.previousPurchasePrice) {

    def ctx = api.getDatamartContext()
    def dm = ctx.getDataSource("HistoricalHAWAPurchasePrices")
    def query = ctx?.newQuery(dm, false)
            ?.select("ArticleID", "ArticleID")
            ?.select("NPPOnline", "NPPOnline")
            ?.select("NPPOfflineWholesale", "NPPOfflineWholesale")
            ?.select("PPOfflineRetail", "PPOfflineRetail")
            ?.select("lastUpdateDate", "lastUpdateDate")
            ?.orderBy("lastUpdateDate")
    api.global.previousPurchasePrice = ctx?.executeQuery(query)?.data?.toResultMatrix()?.entries?.collectEntries { [(it.ArticleID): it] }

    /* api.global.purchasePriceUpdate = api.find("PX10",0,1,"-lastUpdateDate",Filter.equal("name","PurchasePrice"))?.getAt(0)?.lastUpdateDate
     api.global.onlineLPGLastRun = api.find("PG",0,1,"-lastUpdateDate")?.find{it.label.contains("Online")}?.lastUpdateDate
     api.global.offlineLPGLastRun = api.find("PG",0,1,"-lastUpdateDate")?.find{it.label.contains("Offline")}?.lastUpdateDate
 */
}

if (!api.global.countriesMap) {
    api.global.countriesMap = [:]

    def countriesList = api.find("LTV", Filter.equal("lookupTable.id", api.findLookupTable(libs.FressnapfLib.Constants.Countries_PP).id))
    api.global.countriesMap = countriesList.collectEntries {

        [(it.value): it.name]
    }

}

if (!api.global.countryChannelMap) {
    api.global.countryChannelMap = [:]

    api.global.countryChannelMap = api.findLookupTableValues("CountrySalesOrgMapping", Filter.equal("key1", country))?.collectEntries {
        [(it.attribute1): it.key2]
    }
}

if (!api.global.setTrayDiscountMap) {
    api.global.setTrayDiscountMap = [:]
    api.global.setTrayDiscountMap = api.findLookupTableValues("SetandTrayDiscounts", Filter.equal("key1", country))?.collectEntries { [(it.key2 + "~" + it.key3 + "~" + it.key4 + "~" + it.key5): it.attribute1] }
}

countryCode = api.global.countriesMap[country]

if (!api.global.batch) {
    api.global.batch = [:]
}

if (!api.global.batch[api.local.pid]) {
    api.global.batch.clear()
    def batch = api.getBatchInfo()?.collect { it[0] } ?: [api.local.pid] as Set

    def articleChannelMap = [:]

    def filters = [
            Filter.equal("name", libs.FressnapfLib.Constants.ArticleChannelMapping_PX),
            Filter.in("sku", batch),
            Filter.equal("attribute1", country)
    ]

    articleChannelMap = api.find("PX10", 0, api.getMaxFindResultsLimit(), null, *filters).collectEntries {
        [(it.sku): it.attribute2]
    }
    def articleBasketMap = [:]

    def basketFilters = [
            Filter.equal("name", libs.FressnapfLib.Constants.ItemClassBasketMapping_PX),
            Filter.in("sku", batch),
            Filter.equal("attribute1", country)
    ]

    articleBasketMap = api.find("PX8", 0, api.getMaxFindResultsLimit(), null, *basketFilters).collectEntries {
        [(it.sku): it]
    }
    def articleSellingPriceMap = [:]

    def sellingPriceFilters = [
            Filter.equal("name", libs.FressnapfLib.Constants.ArticleSellingPrice_PX),
            Filter.in("sku", batch),
            Filter.equal("attribute1", country)
    ]

    def articleSellingPrice = api.find("PX20", 0, api.getMaxFindResultsLimit(), "-attribute5", *sellingPriceFilters)

    articleSellingPrice.each {
        if (articleSellingPriceMap[it.sku]) {
            articleSellingPriceMap[it.sku] << it
        } else {
            articleSellingPriceMap[it.sku] = [it]
        }
    }
    def articleDescriptionMap = [:]

    def articleFilters = [
            Filter.equal("name", libs.FressnapfLib.Constants.ArticleDescription_PX),
            Filter.in("sku", batch)
    ]

    articleDescriptionMap = api.find("PX20", 0, api.getMaxFindResultsLimit(), null, *articleFilters).collectEntries {
        [(it.sku): it]
    }

    def articleCountryAttributesMap = [:]

    def articleCountryAttributeFilters = [
            Filter.equal("name", libs.FressnapfLib.Constants.ArticleCountryAttributes_PX),
            Filter.in("sku", batch),
            Filter.equal("attribute7", country)
    ]

    articleCountryAttributesMap = api.find("PX20", 0, api.getMaxFindResultsLimit(), null, *articleCountryAttributeFilters).collectEntries {
        [(it.sku): it]
    }

    List priceFamiliesFilter = [
            Filter.equal("name", libs.FressnapfLib.Constants.PriceFamilies_PX),
            Filter.in("sku", batch),
            Filter.in("attribute3", country)
    ]
    priceFamiliesMap = api.find("PX3", 0, api.getMaxFindResultsLimit(), null, *priceFamiliesFilter).collectEntries {
        [(it.sku): it]
    }
    Map approvedPriceOffline = [:]
    Map approvedPriceOnline = [:]

/*

    approvedPriceOffline =  api.find("PGI", 0,api.getMaxFindResultsLimit(),"approvalDate", *offlineFilter)?.collectEntries{
        [(it.sku):it]}//?.getAt(0)?.resultPrice

    approvedPriceOnline = api.find("PGI", 0,api.getMaxFindResultsLimit(),"approvalDate", *onlineFilter)?.collectEntries{
        [(it.sku):it]}//?.getAt(0)?.resultPrice*/


    List countryAttributesfilters = [
            Filter.equal("name", libs.FressnapfLib.Constants.ArticleCountryAttributes_PX),
            Filter.equal("attribute7", out.Country),
            Filter.in("sku", batch)
    ]


    api.global.articleAttributes = api.find("PX20", 0, api.getMaxFindResultsLimit(), null, *countryAttributesfilters)?.collectEntries { row ->
        [
                (row.sku + "_" + row.attribute1 + "_" + row.attribute7): [
                        article              : row.sku,
                        salesorg             : row.attribute1,
                        articlesStatusOverall: row.attribute2,
                        articlesStatus       : row.attribute3,
                        dChainSpecStatus     : row.attribute4,
                        disturbtionChannel   : row.attribute5,
                        purchaseOrg          : row.attribute6,
                        countryKey           : row.attribute7,
                        supplyMode           : row.attribute8,
                        supplyModeVTL        : row.attribute9,
                        VAT                  : row.attribute10,
                ]
        ]
    }

    List purchasePriceFilter = [
            Filter.equal("name", libs.FressnapfLib.Constants.PurchasePrice_PX),
            Filter.equal("attribute5", out.Country),
            Filter.in("sku", batch)
    ]
    api.global.purchasePrice = api.find("PX10", 0, api.getMaxFindResultsLimit(), null, *purchasePriceFilter)?.groupBy { it.sku }
    /*api.global.purchasePrice = api.find("PX10",0, api.getMaxFindResultsLimit(), null,*purchasePriceFilter)?.collectEntries { row ->
        [
                (row.sku): [
                        articles               : row.sku,
                        purchasePriceOnline    : row.attribute1,
                        purchaseCurrencyOnline : row.attribute2,
                        purchasePriceOffline   : row.attribute3,
                        purchaseCurrencyOffline: row.attribute4,
                        country                : row.attribute5,
                        purchasePriceRetail    : row.attribute6,
                        purchaseCurrencyRetail : row.attribute7
                ]
        ]
    }*/


    if (!api.global.sellingPrices) {
        api.global.sellingPrices = libs.FressnapfLib.DataUtils.getSellingPriceMaster(out.Country)
    }
    List marketingCampaignsfilters = [
            Filter.equal("name", libs.FressnapfLib.Constants.MarketingCampaigns_PX),
            Filter.equal("attribute6", country),
            Filter.in("sku", batch)

    ]


    api.global.marketingCamp = api.find("PX8", 0, api.getMaxFindResultsLimit(), null, *marketingCampaignsfilters)?.collect { row ->
        [
                article   : row.sku,
                adType    : row.attribute1,
                eventId   : row.attribute2,
                eventName : row.attribute3,
                activeFrom: row.attribute4,
                activeTo  : row.attribute5
        ]
    }


    def setTrayCostAttributesMap = [:]

    def setTrayCostAttributesFilters = [
            Filter.equal("name", libs.FressnapfLib.Constants.SetTrayPurchasePrices_PX),
            Filter.in("sku", batch),
            Filter.equal("attribute5", country)
    ]

    setTrayCostAttributesMap = api.find("PX10", 0, api.getMaxFindResultsLimit(), null, *setTrayCostAttributesFilters).collectEntries {
        [(it.sku): it]
    }

    List dataSourceFilter = [
            Filter.in("ArticleID", batch),
            Filter.greaterOrEqual("Date", api.global.pastDay),
            Filter.equal("Country",api.global.countryCode?.getAt(out.Country)),
            Filter.equal("Channel", out.CalculationChannel)
    ]
    ctx = api.getDatamartContext()
    dm = ctx.getDataSource("Transaction_FN")
    query = ctx?.newQuery(dm)
            ?.select("ArticleID", "ArticleID")
            ?.select("SUM(Quantity)", "TotalQuantity")
            ?.where(*dataSourceFilter)
    queriedQuantity = ctx?.executeQuery(query)?.data?.toResultMatrix()?.entries?.collectEntries { [(it.ArticleID): it.TotalQuantity] }

    batch.each {
        api.global.batch[it] = [
                "ArticleChannel"          : (articleChannelMap[it] ?: null),
                "ArticleBasket"           : (articleBasketMap[it] ?: null),
                "SellingPrice"            : (articleSellingPriceMap[it] ?: null),
                "ArticleDescription"      : (articleDescriptionMap[it] ?: null),
                "ArticleCountryAttributes": (articleCountryAttributesMap[it] ?: null),
                "SetTrayCost"             : (setTrayCostAttributesMap[it] ?: null),
                "PriceFamily"             : (priceFamiliesMap[it]?.attribute1 ?: null),
                "LeadArticle"             : (priceFamiliesMap[it]?.attribute2 ?: null),
                "SalesQuantity"           : queriedQuantity[it] ?: null
        ]
    }
}
return api.global.batch