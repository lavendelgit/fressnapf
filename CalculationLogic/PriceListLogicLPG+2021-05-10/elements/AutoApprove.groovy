if(api.local.priceBuildupMatrix.marketingCamp){
    return false//true
}

if(api.local.contextValues ){
        return false//api.currentItem()?.approvalState == "AUTO_APPROVED" || api.currentItem()?.approvalState == "APPROVED"
}
def supplyMode=out.SupplyMode
if(supplyMode=="01")
{
    return false//api.local.priceBuildupMatrix.followupautoApprovel = false
}

if(out.ListPrice && api.local.priceChangeThreshold && !out.CategoryManagerApprovel && out.ListPrice !=0) {
   String channel = out.CalculationChannel
    newSuggestedPrice = out.ListPrice
    api.local.priceBuildupMatrix.followupAUTOPAppliedFactor = api.local.priceChangeThreshold["AutomaticPricePct"]
    api.local.priceBuildupMatrix.followupAUTOPApplied = false
    api.local.priceBuildupMatrix.followupautoApprovel = false
    api.local.priceBuildupMatrix.followupAUTOPAppliedCondition = "Automatic price change rate (MAX %) Auto Approval"
    api.local.priceBuildupMatrix.followupAUTOPAppliedStringFormula = "Abs(((New RP - SalesPrice" + channel + ") / (SalesPrice" + channel + "))*100) < Automatic price change rate (MAX %)"
    api.local.priceBuildupMatrix.followupAUTOPAppliedFormula = "Abs(((" + newSuggestedPrice + "-" + out["SalesPrice" + channel] + ") / (" + out["SalesPrice" + channel] + "))*100)  < " + (api.local.priceChangeThreshold["AutomaticPricePct"] * 100)
    difference = newSuggestedPrice && out["SalesPrice" + channel] ? newSuggestedPrice - out["SalesPrice" + channel] : 0.0
    difference = difference < 0 ? (difference * -1) : difference
    differencePct = out["SalesPrice" + channel] ? difference / out["SalesPrice" + channel] : 0
    if (differencePct * 100 < (api.local.priceChangeThreshold["AutomaticPricePct"] * 100) && (differencePct || differencePct == 0)) {
        api.local.priceBuildupMatrix.followupautoApprovel = true
        api.local.priceBuildupMatrix.followupAUTOPApplied = true
    }
}
return false//api.local.priceBuildupMatrix.followupautoApprovel