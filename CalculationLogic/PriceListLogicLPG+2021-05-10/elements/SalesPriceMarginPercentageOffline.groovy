if (out.PurchasePriceOffline) {
        BigDecimal markUpPercentage = out.SalesPriceOfflineEXVAT > 0 ?
            (((out.SalesPriceOfflineEXVAT - out.PurchasePriceOffline) / out.SalesPriceOfflineEXVAT))
            : 0.0
    return out.CommonLib.UIUtils.getArrowedCell(markUpPercentage)
}
else api.addWarning("Purchase Price Offline not available for margin calculation")