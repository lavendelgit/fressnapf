BigDecimal listPrice = out.ListPrice ?: 0
BigDecimal currentSellingPrice = out["SalesPrice" + out.CalculationChannel] ?: 0

if (listPrice && currentSellingPrice) {
    percentage = (listPrice - currentSellingPrice) / currentSellingPrice
    if (percentage > api.local.priceBuildupMatrix.followupMAXCPFactor) {
        api.redAlert("Percentage is greater than " + ((api.local.priceBuildupMatrix.followupMAXCPFactor as BigDecimal) ?: 0 * 100) + "%")
    } else if (percentage < (api.local.priceBuildupMatrix.followupMAXCPFactor) * -1) {
        api.redAlert("Percentage is lesser than -" + ((api.local.priceBuildupMatrix.followupMAXCPFactor as BigDecimal) ?: 0 * 100) + "%")
    }
    return out.CommonLib.UIUtils.getArrowedCell(percentage)
}