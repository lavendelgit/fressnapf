if((api.local.priceBuildupMatrix?.marketingCamp && api.local.contextValues) || api.local.contextValues){
    return out.ArticleContext?.SalesPriceOnline

}
//def getSalesPriceMaster = out.CommonLib.DataUtils.getSalesPriceMaster()
api.logInfo("true selling Price cache")
def channel = "Online"
def sku = out.Article
def salesOrg = api.global.countryChannelMap[channel]
def sellingPriceRecord = api.global.batch[sku]?.SellingPrice
def onlineSellingData = (sellingPriceRecord)?.findAll { it.attribute2 == salesOrg }?.find{ it.attribute5 <= out.PricingDate?.format("YYYY-MM-dd")}
def onlineSellingPrice = (onlineSellingData) ? onlineSellingData?.attribute8 : null

return out.CommonLib.CommonUtils.applyExchangeRate((onlineSellingPrice) ? onlineSellingPrice : 0,
        out.SalesPriceOnlineCurrency,
        out.TargetCurrency,
        out.PricingDate)

