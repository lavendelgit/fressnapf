if(api.global.calculationEndDate != 0 && api.local.contextValues){
    return out.ArticleContext?.CommodityGroupDescription
}
def getCommodityGroupMaster = out.CommonLib.DataUtils.getCommodityGroupMaster()
return getCommodityGroupMaster?.getAt(out.CGRNo)?.description ?: ""