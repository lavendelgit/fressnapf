if(api.global.calculationEndDate!= 0 && api.local.contextValues){
    return out.ArticleContext?.CategoryType
}
Map categoryTypeMapping = libs.FressnapfLib.DataUtils.getBrandRoleMappingMaster()?.values()?.findAll { it.country == out.Country }?.find { it.brand == out.Brand }
return categoryTypeMapping?.groceryDriven == "X" ? "Grocery" : "Specialist"