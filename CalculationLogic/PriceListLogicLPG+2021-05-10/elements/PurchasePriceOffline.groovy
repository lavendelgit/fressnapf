def sortofArticle = out.MaterialType
def sku = out.Article
api.local.beforeCConversionOffline = [:]
if((api.local.priceBuildupMatrix?.marketingCamp && api.local.contextValues) || api.local.contextValues){
    return out.ArticleContext?.PurchasePriceOffline?:null

}

if (sortofArticle == "HAWA") {
    def country = out.Country
    def countryCode = api.global.countriesMap[country]
    def getPurchasePriceMaster = api.global.purchasePrice
    if(getPurchasePriceMaster && getPurchasePriceMaster?.getAt(out.Article)?.purchaseCurrencyOffline != out.Currency){
        api.local.priceBuildupMatrix.OfflinePrice = getPurchasePriceMaster?.getAt(out.Article)?.purchasePriceOffline
        api.local.priceBuildupMatrix.OfflineCurrency = getPurchasePriceMaster?.getAt(out.Article)?.purchaseCurrencyOffline
        !api.local.priceBuildupMatrix.OfflinePrice && out.CalculationChannel == "Offline"?api.addWarning("Purchase Price Offline not available for "+out.Article):""
    }

    def purPriceOfflinePX = getPurchasePriceMaster?.getAt(out.Article)?.purchasePriceOffline ?: 0
       def purPriceOfflineCurrency = getPurchasePriceMaster?.getAt(out.Article)?.purchaseCurrencyOffline
    def purPriceOffline = purPriceOfflinePX //* (1 - out.TripleNetFactor)
    return libs.FressnapfLib.RoundingUtils.roundNumber(out.CommonLib.CommonUtils.applyExchangeRate(purPriceOffline,
            purPriceOfflineCurrency,
            out.TargetCurrency,
            out.PricingDate),5)?:null
}else{
    def setTrayCostData = api.global.batch[sku].SetTrayCost
    def cost = (setTrayCostData)?.attribute6
    !cost && out.CalculationChannel == "Offline"?api.addWarning("Purchase Price Offline not available for "+out.Article):""
    cost = (cost)?:0
    return libs.FressnapfLib.RoundingUtils.roundNumber(out.CommonLib.CommonUtils.applyExchangeRate(cost,
            out.PurchasePriceOfflineCurrency,
            out.TargetCurrency,
            out.PricingDate),5)?:null
}