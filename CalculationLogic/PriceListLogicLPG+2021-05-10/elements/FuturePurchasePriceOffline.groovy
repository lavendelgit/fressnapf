def sortofArticle = out.MaterialType
def sku = out.Article
api.local.beforeCConversionOffline = [:]
if ((api.local.priceBuildupMatrix?.marketingCamp && api.local.contextValues) || api.local.contextValues) {
    return out.ArticleContext?.FuturePurchasePriceOffline ?: null

}

if (sortofArticle == "HAWA") {
    def country = out.Country
    def countryCode = api.global.countriesMap[country]
    def getPurchasePriceMaster = api.global.purchasePrice?.getAt(out.Article)?.sort { it.attribute10 }?.find { it.attribute10 > out.PricingDate?.format("YYYY-MM-dd") }
    api.local.priceBuildupMatrix.FutureOfflinePrice = getPurchasePriceMaster?.attribute3
    api.local.priceBuildupMatrix.FutureOfflineCurrency = getPurchasePriceMaster?.attribute4
    api.local.futurePPOFValidFrom = getPurchasePriceMaster?.attribute10

    def purPriceOfflinePX = getPurchasePriceMaster?.attribute3 ?: 0
    def purPriceOfflineCurrency = getPurchasePriceMaster?.attribute4
    def purPriceOffline = purPriceOfflinePX //* (1 - out.TripleNetFactor)
    return libs.FressnapfLib.RoundingUtils.roundNumber(out.CommonLib.CommonUtils.applyExchangeRate(purPriceOffline,
            purPriceOfflineCurrency,
            out.TargetCurrency,
            out.PricingDate), 5) ?: null
} else {
    def setTrayCostData = api.global.batch[sku].SetTrayCost
    def cost = (setTrayCostData)?.attribute3
    //!cost && out.CalculationChannel == "Offline"?api.addWarning("Purchase Price Offline not available for "+out.Article):""
    cost = (cost) ?: 0
    return libs.FressnapfLib.RoundingUtils.roundNumber(out.CommonLib.CommonUtils.applyExchangeRate(cost,
            out.PurchasePriceOfflineCurrency,
            out.TargetCurrency,
            out.PricingDate), 5) ?: null
}