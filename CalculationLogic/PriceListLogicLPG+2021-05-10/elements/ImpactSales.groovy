def sales= (out.ListPrice?:0) - (out.SalesPriceOnline?:0)
def quantity= out.QuantityDataSource ?:0
def impactSales = (sales * quantity) as BigDecimal
return impactSales