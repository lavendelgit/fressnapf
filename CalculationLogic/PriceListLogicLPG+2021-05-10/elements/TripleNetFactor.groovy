if(api.global.calculationEndDate!= 0 && api.local.contextValues){
    return out.ArticleContext?.TripleNetFactor

}
def getTripleNetFactorMaster = out.CommonLib.DataUtils.getTripleNetFactorMaster()

def brand = out.CommonLib.DataUtils.getBrandsMaster()?.getAt(out.Brand)?.value

def subBrand = out.SubBrand ?: "*"
def tripleNetFactor = getTripleNetFactorMaster?.getAt(out.ChannelPerspTool + "_" + brand + "_" + subBrand)?.TriplenetFactor ?: 0
//def finalPurPriceOffline = purPriceOffline * (1 - tripleNetFactor)
/*api.trace("tripleNetFactorCalc", (1 - tripleNetFactor))
api.trace("out.ChannelPerspTool", out.ChannelPerspTool)
api.trace("subBrand", subBrand)
api.trace("out.Brand", brand)
api.trace("TripleNetFactor", tripleNetFactor)*/

return tripleNetFactor