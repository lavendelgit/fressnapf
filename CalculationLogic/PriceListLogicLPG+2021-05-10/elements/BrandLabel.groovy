if(api.global.calculationEndDate != 0 && api.local.contextValues){
    return out.ArticleContext?.BrandLabel
}
def getBrandsMaster = out.CommonLib.DataUtils.getBrandsMaster()
return getBrandsMaster?.getAt(out.Brand?:api.currentItem("Brand"))?.value?:""