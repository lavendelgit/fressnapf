import net.pricefx.server.dto.calculation.ResultMatrix
List camp = api.local.priceBuildupMatrix?.allMarketingCamp
/*if( api.local.contextValues || api.local.marketingCamp){
    camp = out.CalculationMap?.allMarketingCamp
}*/
if(camp){
    currentDate = api.targetDate()?.format("YYYY-MM-dd")
    ResultMatrix matrix = api.newMatrix("Article ID","Country","AD Type","Active From","Active Until")
    camp?.sort{it.activeTo}?.each {record->
        if(record?.activeTo <currentDate){
            matrix.addRow(matrix.styledCell(out.Article,"#ff7a7a",null),matrix.styledCell(out.Country,"#ff7a7a",null),
                    matrix.styledCell(record?.adType,"#ff7a7a",null),matrix.styledCell(record?.activeFrom,"#ff7a7a",null),matrix.styledCell(record?.activeTo,"#ff7a7a",null))
        }else if(record?.activeTo >currentDate){
            matrix.addRow(matrix.styledCell(out.Article,"#007523",null),matrix.styledCell(out.Country,"#007523",null),
                    matrix.styledCell(record?.adType,"#007523",null),matrix.styledCell(record?.activeFrom,"#007523",null)
                    ,matrix.styledCell(record?.activeTo,"#007523",null))
        }
    }
    return matrix
}
return
