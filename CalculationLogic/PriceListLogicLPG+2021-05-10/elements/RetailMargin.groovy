if(out.ListPrice && out.PurchasePriceRetail!=0 && out.PurchasePriceRetail) {
    BigDecimal listPrice = out.PriceExVAT != out.CalculatedListPrice ? (out.PriceExVAT ?: 0) : api.local.ListPriceOffline
    BigDecimal markUpPercentage = listPrice > 0 ?
            ((listPrice - out.PurchasePriceRetail) / listPrice)
            : 0.0
    return out.CommonLib.UIUtils.getArrowedCell(markUpPercentage)
}
return
