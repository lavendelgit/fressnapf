BigDecimal previousPurchasePrice = out.FuturePurchasePriceOffline as BigDecimal
BigDecimal currentPurchasePrice = out.PurchasePriceOffline as BigDecimal
if (previousPurchasePrice && previousPurchasePrice != currentPurchasePrice && currentPurchasePrice) {
    percentage = (previousPurchasePrice - currentPurchasePrice) / currentPurchasePrice
    if (percentage > 0.05) {
        api.redAlert("Percentage difference is greater than 5%")
    } else if (percentage < -0.05) {
        api.redAlert("Percentage difference is less than -5%")
    }
    return out.CommonLib.UIUtils.getArrowedCell(percentage)

}