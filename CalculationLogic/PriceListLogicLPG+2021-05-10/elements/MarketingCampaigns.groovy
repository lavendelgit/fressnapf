sku = out.Article
Map pricingChange = libs.FressnapfLib.DataUtils.getPriceChangeThresholdsMaster("Germany")
api.local.PriceChangeThresholdCondition = null
api.local.marketingCamp = false
api.local.markItemDirty = false
api.local.appliedCamp = [:]
String basket = api.global.batch[out.Article].ArticleBasket?.attribute4
String channel = api.global.batch[out.Article].ArticleChannel
channel = out.CalculationChannel
String key = out.Country + "_" + channel + "_" + basket
api.local.priceChangeThreshold = pricingChange[key]
String date = api.targetDate()?.format("yyyy-MM-dd")
Integer leadTime = 0
List marketingCamp = api.global.marketingCamp?.findAll { it.article == out.Article }
api.local.priceBuildupMatrix.allMarketingCamp = marketingCamp
String sourceChannel = api.global.batch[sku]?.ArticleChannel
String supportChannel = out.CalculationChannel
marketingCamp?.each { campaign ->
    leadTime = libs.FressnapfLib.DataUtils.getLeadTimesMaster(sourceChannel as String, supportChannel as String)?.find { it.ADTypeEN == campaign?.adType && it.RelevanceExclusionFlag == "Yes" }?.LeadTimeWeeks as Integer
    if (leadTime) {
        daysAddition = leadTime * 7
        LeadFromDate = (api.parseDate("yyyy-MM-dd", campaign?.activeFrom) - daysAddition)?.format("yyyy-MM-dd")
        campaign?.activeFrom = LeadFromDate
    }
}
Map Camp = marketingCamp?.findAll { it.activeFrom <= date && it.activeTo >= date }?.sort { api.parseDate("yyyy-MM-dd", it.activeFrom) - (api.parseDate("yyyy-MM-dd", it.activeTo)) }?.getAt(0)
if (leadTime && Camp && (Camp?.activeFrom <= date && Camp?.activeTo >= date)) {
    api.addWarning("Marketing Event ID " + Camp?.eventId + " in force from " + Camp?.activeFrom + " to " + Camp?.activeTo + " - No Price changes allowed")
    api.local.priceBuildupMatrix.PricingReason = " Price blocked - Marketing Campaign"
    api.local.priceBuildupMatrix.appliedCampEventID = Camp?.eventId
    api.local.priceBuildupMatrix.appliedCampType = Camp?.adType
//"The Marketing Campaigns : "+Camp?.adType+" is applied between "+Camp?.activeFrom +" to "+Camp?.activeTo
    api.local.priceBuildupMatrix.appliedCampFrom = leadTime ? Camp?.activeFrom + "(leadTime :" + leadTime + ")" : Camp?.activeFrom
    api.local.priceBuildupMatrix.appliedCampTo = Camp?.activeTo
    //api.local.contextValues =  api.global.calculationEndDate != 0 || api.currentContext(out.Article)?.ResultPrice ?: false
    api.local.markItemDirty = !(api.global.calculationEndDate != 0 || api.currentContext(out.Article)?.ResultPrice)
    api.local.priceBuildupMatrix.marketingCamp = true
}


if (api.currentContext(out.Article)?.ResultPrice && api.global.calculationEndDate != 0) {
    priceChangeDay = api.local.priceChangeThreshold?.PriceChangeDay ?: ""
    api.local.priceBuildupMatrix.PriceChangeThresholdConditionApplied = false
    if (api.local.priceChangeThreshold && api.local.priceChangeThreshold?.PriceChangeFrequency != "Daily" && api.global.calculationEndDate != 0) {
        if (!api.local.priceChangeThreshold?.PriceChangeDay) {
            api.addWarning("No Price change day mentioned for Weekly")
            return
        }
        Date days = api.parseDate("yyyy-MM-dd", api.currentContext(out.Article)?.PricingDate)
        Date targetDate = api.targetDate()
        days = days != targetDate ? targetDate : days
        List day = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
        additionalDays = (day.indexOf(api.local.priceChangeThreshold.ApprovalDeadLineDay))
        actualday = day.indexOf(api.local.priceChangeThreshold?.PriceChangeDay)
        additionalDays = additionalDays < actualday ? additionalDays + (actualday - additionalDays) : additionalDays
        api.local.priceBuildupMatrix.PriceChangeThresholdPeriod = api.local.priceChangeThreshold?.PriceChangeFrequency == "Daily" ? api.local.priceChangeThreshold?.PriceChangeFrequency : api.local.priceChangeThreshold?.PriceChangeFrequency + "(" + (day.subList(actualday, actualday + additionalDays))?.join(',') + ")"
        if (!((day.subList(actualday, actualday + additionalDays))?.contains(days?.format("EEEE")))) {
            days = api.parseDate("yyyy-MM-dd", api.currentContext(out.Article)?.PricingDate)
            days = days != targetDate ? days : targetDate
            api.addWarning("Price Change Threshold limit not exceeded")
            api.local.priceBuildupMatrix.PriceChangeThresholdCondition = "Price Change Period is below the frequency"
            api.local.priceBuildupMatrix.PriceChangeDay = api.local.priceChangeThreshold?.PriceChangeDay
            api.local.priceBuildupMatrix.PriceChangeThresholdPeriod = "Weekly(" + (day.subList(actualday, actualday + additionalDays))?.join(',') + ")"
            api.local.priceBuildupMatrix.PriceChangeThresholdApprovedDate = days
            api.local.priceBuildupMatrix.PriceChangeThresholdBlockedUntill = days + 7
            api.local.priceBuildupMatrix.PriceChangeThresholdConditionApplied = true
            api.local.contextValues = true
        }
    }
}
return
