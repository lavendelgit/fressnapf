def resultPrice = out.ListPrice
def vat = out.VAT
vat = (vat)?:0
def priceexVAT = (resultPrice)?(resultPrice/(1+(vat/100))):0
return priceexVAT