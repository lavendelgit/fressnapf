api.local.competitorSuggestedPrice = 0.0
api.local.costPlusSuggestedPrice = 0.0
BigDecimal suggestedPrice = 0.0
api.local.PricingMethod = []
api.local.priceFamilyPricing = false
api.markItemClean()
boolean dirtyItem = false
def sortOfArticle = out.MaterialType
def getLeadArticle = out.LeadArticle
def sku = out.Article
Map MCChannelBasketMappingMaster = out.CommonLib.DataUtils.getMCChannelBasketMappingMaster(out.Country)
String channel = out.CalculationChannel
api.local.priceBuildupMatrix?.PRGFamily = libs.FressnapfLib.DataUtils.getPRGMaster(out.Country)?.values()?.find { it.RelatedFamily == out.PriceFamilyCode }?.RelatedFamily ?: null
if (api.local.priceBuildupMatrix?.marketingCamp) {
    suggestedPrice = generateCalculatedListPrice(suggestedPrice)
    return out['SalesPrice' + channel]
} else if (api.global.calculationEndDate != 0 && api.local.contextValues) {
    suggestedPrice = generateCalculatedListPrice(suggestedPrice)
    return out.ArticleContext?.CalculatedListPrice
}

if (((sku == getLeadArticle) || (getLeadArticle == null)) && (api.getIterationNumber() == 0)) {
    api.local.priceBuildupMatrix.PRGPrice = api.local.priceBuildupMatrix.PRGFamily ? Lib.PRGPricing() : 0.0
    suggestedPrice = generateCalculatedListPrice(suggestedPrice)
}
if (api.getIterationNumber() == 0 && api.local.priceBuildupMatrix.PRGFamily && sku == getLeadArticle) {
    dirtyItem = true
    api.markItemDirty()
}
if (api.getIterationNumber() == 0 && api.local.markItemDirty && api.global.calculationEndDate == 0) {
    api.markItemDirty()
}
if (((api.getIterationNumber() == 0) && (getLeadArticle) && (sku != getLeadArticle)) ||
        (api.getIterationNumber() == 0 && (sortOfArticle == 'ZSET' || sortOfArticle == 'ZTRA'))) {
    dirtyItem = true
    api.markItemDirty()
}
if (api.getIterationNumber() == 1) {

    if (sortOfArticle == 'HAWA') {
        suggestedPrice = generateCalculatedListPrice(suggestedPrice)
        def leadArticleCurrentContext = api.currentContext(getLeadArticle)
        suggestedPrice = leadArticleCurrentContext?.ResultPrice as BigDecimal ?: suggestedPrice
        api.local.priceBuildupMatrix.PRGPrice = api.local.priceBuildupMatrix.PRGFamily ? Lib.PRGPricing() : 0.0
        api.local.priceBuildupMatrix.PricingReason = leadArticleCurrentContext?.ResultPrice && getLeadArticle != sku ? 'Family Price' : api.local.priceBuildupMatrix.PricingReason
    }
    if (sortOfArticle == 'ZSET' || sortOfArticle == 'ZTRA') {
        suggestedPrice = generateCalculatedListPrice(suggestedPrice)
        def currentSKUContext = api.currentContext(sku)
        suggestedPrice = currentSKUContext?.ResultPrice as BigDecimal ?: suggestedPrice
        api.local.priceBuildupMatrix.PricingReason = api.local.priceBuildupMatrix.PricingReason
        dirtyItem = true
        api.markItemDirty()
    }

}
if (api.getIterationNumber() == 2) {
    api.local.priceFamilyPricing = true
    if (sortOfArticle == 'ZSET' || sortOfArticle == 'ZTRA') {
        suggestedPrice = generateCalculatedListPrice(suggestedPrice)
        api.local.priceBuildupMatrix.setTrayApplied = true
        def currentSKUContext = api.currentContext(sku)
        def setTrayPriceByPricingMethod = suggestedPrice//currentSKUContext?.ResultPrice as BigDecimal
        def setTrayPriceByComponent = Lib.getPriceByBOM()
        setTrayPriceByComponent = Lib.applySetTrayDiscount(setTrayPriceByComponent)
        api.local.priceBuildupMatrix.setTrayPriceDiscounted = setTrayPriceByComponent ?: 0
        suggestedPrice = (setTrayPriceByPricingMethod < setTrayPriceByComponent) ? setTrayPriceByPricingMethod : setTrayPriceByComponent
        if (setTrayPriceByPricingMethod == 0) suggestedPrice = setTrayPriceByComponent
        if (setTrayPriceByComponent == 0) suggestedPrice = setTrayPriceByPricingMethod
        api.local.priceBuildupMatrix.setTrayApplied = suggestedPrice != 0.0 && !suggestedPrice ?: true
        api.local.priceBuildupMatrix.setTrayPricePricingApporach = (setTrayPriceByComponent == 0 || setTrayPriceByPricingMethod < setTrayPriceByComponent) ? api.local.priceBuildupMatrix.PricingReason : ' Set Tray Pricing'
        api.local.priceBuildupMatrix.setTrayPriceDiscountedPrice = suggestedPrice
        api.local.priceBuildupMatrix.setTrayCompareString = '( Pricing Approach Price < Set Tray Pricing By Component)'
        api.local.priceBuildupMatrix.setTrayCompareStringValue = '(' + libs.FressnapfLib.RoundingUtils.roundNumber(setTrayPriceByPricingMethod, 5) + ' < ' + libs.FressnapfLib.RoundingUtils.roundNumber(setTrayPriceByComponent, 5) + ')'
        api.local.priceBuildupMatrix.PRGPrice = api.local.priceBuildupMatrix.PRGFamily ? Lib.PRGPricing() : 0.0
        api.local.priceBuildupMatrix.suggestedApporach = (setTrayPriceByComponent == 0 || setTrayPriceByPricingMethod < setTrayPriceByComponent) ? api.local.priceBuildupMatrix.PricingReason : 'Set/Tray Pricing'
        setTrayFail = (setTrayPriceByComponent == 0 || setTrayPriceByPricingMethod < setTrayPriceByComponent)
        api.local.priceBuildupMatrix.PricingReason = (setTrayPriceByComponent == 0 || setTrayPriceByPricingMethod < setTrayPriceByComponent) ? api.local.priceBuildupMatrix.PricingReason : 'Bottom up Price (Set&Tray)'

    }
    if ((getLeadArticle) && (sku != getLeadArticle)) {
        dirtyItem = true
        api.markItemDirty()
    }
}
if (api.getIterationNumber() == 3) {
    suggestedPrice = generateCalculatedListPrice(suggestedPrice)
    api.local.priceFamilyPricing = true
    def leadArticleCurrentContext = api.currentContext(getLeadArticle)
    suggestedPrice = leadArticleCurrentContext?.ResultPrice as BigDecimal ?: suggestedPrice
    api.local.priceBuildupMatrix.PRGPrice = api.local.priceBuildupMatrix.PRGFamily ? Lib.PRGPricing() : 0.0
    api.local.priceBuildupMatrix.PricingReason = leadArticleCurrentContext?.ResultPrice ? 'Family Price' : api.local.priceBuildupMatrix.PricingReason
    api.local.priceBuildupMatrix.suggestedApporach = leadArticleCurrentContext?.ResultPrice ? 'Family Price' : api.local.priceBuildupMatrix.PricingReason
}

if (api.local.priceBuildupMatrix.PRGPrice > 0 && suggestedPrice && api.local.priceBuildupMatrix.PRGFamily && (suggestedPrice > api.local.priceBuildupMatrix.PRGPrice)) {
    api.local.priceBuildupMatrix.PRGPricingAppliedValidationStringFur = ' (suggestedPrice > PRGPrice)'
    api.local.priceBuildupMatrix.PRGPricingAppliedValidationFur = '(' + libs.FressnapfLib.RoundingUtils.roundNumber(suggestedPrice, 2) + ' > ' + libs.FressnapfLib.RoundingUtils.roundNumber(api.local.priceBuildupMatrix.PRGPrice, 2) + ')'
    api.local.priceBuildupMatrix.PRGPricingApplied = true
    api.local.priceBuildupMatrix.PRGPricingAppliedReasonFormula = '(' + suggestedPrice + '>' + api.local.priceBuildupMatrix.PRGPrice + ')'
    api.local.priceBuildupMatrix.PRGPrice = api.local.priceBuildupMatrix.PRGPrice
    api.local.priceBuildupMatrix.suggestedApporach = 'PRG'
    api.local.priceBuildupMatrix.PricingReason = 'PRG Price'
    suggestedPrice = api.local.priceBuildupMatrix.PRGPrice > 0 ? api.local.priceBuildupMatrix.PRGPrice : suggestedPrice
} else {
    api.local.priceBuildupMatrix.PRGPricingApplied = false
    message = !api.local.priceBuildupMatrix.PRGFamily ? 'NO PRG Price' : null
    api.local.priceBuildupMatrix.PRGPricingAppliedValidationStringFur = message ?: ' (suggestedPrice > PRGPrice)'
    api.local.priceBuildupMatrix.PRGPricingAppliedValidationFur = message ?: '(' + libs.FressnapfLib.RoundingUtils.roundNumber(suggestedPrice, 2) + ' > ' + libs.FressnapfLib.RoundingUtils.roundNumber(api.local.priceBuildupMatrix.PRGPrice, 2) + ')'
}
if (!dirtyItem) {
    suggestedPrice = priceChangeThreshold(suggestedPrice)
    suggestedPrice = priceGuardrailCheck(suggestedPrice)
    api.local.priceBuildupMatrix.SuggestedPrice = suggestedPrice
    api.local.pricebeforeDistance = suggestedPrice
    suggestedPrice = applyDistanceToKeyChannel(suggestedPrice, MCChannelBasketMappingMaster)
    api.local.priceBuildupMatrix.PricingReason = api.local.priceBuildupMatrix.PricingReason ?: out.FullCalculationResult?.PricingReason
}
if ((suggestedPrice != 0.0 || suggestedPrice != null) /*&&  api.local.priceBuildupMatrix.PricingReason != "Family Price"*/) {
    api.local.priceBuildupMatrix.PricingReason = suggestedPrice!=0.0?api.local.priceBuildupMatrix.PricingReason:""
    api.local.priceBuildupMatrix.IsRoundingSchemeApplied = 'Yes'
    api.local.priceBuildupMatrix.BeforeRoundingScheme = suggestedPrice
    suggestedPrice = roundingScheme(out.CommonLib.RoundingUtils.roundNumber(suggestedPrice, 2), api.local.priceBuildupMatrix.suggestedApporach)
    api.local.priceBuildupMatrix.AfterRoundingScheme = suggestedPrice
    suggestedPrice = out.CommonLib.RoundingUtils.roundNumber(suggestedPrice, 2) ?: 0.0
}
api.local.priceBuildupMatrix.SuggestedPrice = suggestedPrice
return suggestedPrice ? libs.FressnapfLib.RoundingUtils.roundNumber(suggestedPrice, 2) : 0.0


BigDecimal generateCalculatedListPrice(BigDecimal suggestedPrice) {
    String categoryType = out.Product?.attribute16
    String basket = out.Basket
    String CGRNo = out.Product?.attribute3
    String brandType = out.Product?.attribute13
    def country = out.Country
    def getPricingSegmentsMaster = out.CommonLib.DataUtils.getPricingSegmentsMaster()
    Object pricingSegment = Lib.pricingMethod(getPricingSegmentsMaster, [out.Country ?: '*', out.ChannelPerspTool ?: "*", out.Brand ?: "*", out.SubBrand ?: "*", out.CGRNo ?: "*", out.SubCategory ?: "*", out.Category ?: "*", out.CategoryType ?: "*", out.Basket ?: "*"])
    api.local.PricingMethod << pricingSegment?.pricingMethod1?.trim()
    api.local.PricingMethod << pricingSegment?.pricingMethod2?.trim()
    api.local.competitorSuggestedPrice = Lib.getCompetitorBasedPricing()
    costPlusPricing = api.local.PricingMethod?.getAt(0) == 'Competitor-based Pricing' ? api.local.PricingMethod?.getAt(1) : api.local.PricingMethod?.getAt(0)
    api.local.costPlusSuggestedPrice = Lib.calculateCostPlusPricing(costPlusPricing)
    api.local.DistanceToKeyChannel = 0
    api.local.ListPriceOnline = 0
    api.local.ListPriceOffline = 0
    if (api.local.PricingMethod?.getAt(0) == 'Competitor-based Pricing') {
        suggestedPrice = api.local.competitorSuggestedPrice ?: api.local.costPlusSuggestedPrice
        api.local.priceBuildupMatrix.suggestedApporach = api.local.competitorSuggestedPrice ? 'Competitor Based Pricing' : 'Cost Plus Pricing'
        api.local.priceBuildupMatrix.PricingReason = api.local.priceBuildupMatrix.suggestedApporach == 'Competitor Based Pricing' ? 'Competition-based Price' : 'CostPlus-based Price'
    } else if (api.local.PricingMethod != "Competitor-based Pricing") {
        suggestedPrice = api.local.costPlusSuggestedPrice ?: api.local.competitorSuggestedPrice
        api.local.priceBuildupMatrix.suggestedApporach = api.local.costPlusSuggestedPrice ? 'Cost Plus Pricing' : 'Competitor Based Pricing'
        api.local.priceBuildupMatrix.PricingReason = api.local.priceBuildupMatrix.suggestedApporach == 'Competitor Based Pricing' ? 'Competition-based Price' : 'CostPlus-based Price'
    }
    api.local.ListPriceOnline = suggestedPrice
    api.local.ListPriceOffline = suggestedPrice
    api.local.ListPriceOnline = suggestedPrice > 0 ? (suggestedPrice + api.local.DistanceToKeyChannel) : (
            out.SalesPriceOffline > 0 ? (out.SalesPriceOffline + api.local.DistanceToKeyChannel) : out.SalesPriceOnline
    )
    api.local.ListPriceOffline = suggestedPrice > 0 ? suggestedPrice : out.SalesPriceOffline
    return suggestedPrice
}


BigDecimal roundingScheme(BigDecimal price, String suggestedApporach) {
    String itemClass = out.ItemClass
    String finalCluster = "*"
    String materialType = out.MaterialType=="HAWA"?"HAWA":"ZSET, ZTRA"
    List roundingSchemeMaster = libs.FressnapfLib.DataUtils.getRoundingSchemeMaster("Germany",out.CalculationChannel)?.getAt(materialType)//out.CommonLib.DataUtils.getRoundingSchemeMaster(out.Country)//?.groupBy{it.key3}//?.getAt(out.CalculationChannel)
    Map clusterGroup  =roundingSchemeMaster?.groupBy {it.Cluster}
clusterGroup?.keySet()?.each{ it->
    desicion = (it.split(',')*.trim())?.contains(out.Cluster)
    if(desicion)  {
        finalCluster = it
    }
    }
    roundingSchemeMaster = clusterGroup?.getAt(finalCluster)
    //List maxAbsDeviation = out.CommonLib.DataUtils.getMaxAbsDeviationMaster()
    //BigDecimal targetIndex = out.CommonLib.DataUtils.getTargetPriceIndexMaster(out.Country, out.CGRNo, out.CalculationChannel)[out.Basket]
    Map roundingSchemeMap = roundingSchemeMaster?.find { out.CommonLib.RoundingUtils.roundNumber(price, 2) >= it.From && out.CommonLib.RoundingUtils.roundNumber(price, 2) <= it.To }
    //Map deviationRange = maxAbsDeviation?.find { out.CommonLib.RoundingUtils.roundNumber(price, 2) >= it.From && out.CommonLib.RoundingUtils.roundNumber(price, 2) <= it.To }
    BigDecimal response = 0
    api.local.priceBuildupMatrix.Roundingbefore = price
    api.local.priceBuildupMatrix.RoundingSchemeExistBetween = roundingSchemeMap?.From + ' to ' + roundingSchemeMap?.To
    api.local.priceBuildupMatrix.RoundingSchemColumn = 'Flat Price - Material Type : ' + materialType
    response = roundingSchemeMap?.Price
    /*if (targetIndex > 100 && price && (suggestedApporach == 'Competitor Based Pricing' || suggestedApporach?.startsWith('Comp'))) {
        if (api.local.priceBuildupMatrix?.SelectedCompetitorPrice && out.CommonLib.RoundingUtils.roundNumber(api.local.priceBuildupMatrix?.SelectedCompetitorPrice - (roundingSchemeMap?.MiscellaneousPrice as BigDecimal), 2) > deviationRange?.MaxAbsoluteDeviation) {
            api.local.priceBuildupMatrix.RoundingSchemColumn = 'Deviation Applied'
            List nextRoundingSchemeMap = roundingSchemeMaster.findAll { it.From >= price }?.sort { it.From }
            for (scheme in nextRoundingSchemeMap) {
                maxDeviation = maxAbsDeviation?.find { scheme?.MiscellaneousPrice >= it.From && scheme?.MiscellaneousPrice <= it.To }
                if (out.CommonLib.RoundingUtils.roundNumber(api.local.priceBuildupMatrix.SelectedCompetitorPrice - (scheme?.MiscellaneousPrice as BigDecimal), 2) <= maxDeviation?.MaxAbsoluteDeviation) {
                    api.local.priceBuildupMatrix.RoundingSchemeExistBetween = scheme?.From + ' to ' + scheme?.To + ' Deviation (' + maxDeviation?.From + ' to ' + maxDeviation?.To + ' : ' + maxDeviation?.MaxAbsoluteDeviation + ")"
                    response = scheme?.MiscellaneousPrice
                    break
                }
            }
        } else {
            api.local.priceBuildupMatrix.RoundingSchemColumn = 'Miscellaneous Price'
            response = roundingSchemeMap?.MiscellaneousPrice
        }
    }
    if ((materialType == 'ZSET' || materialType == 'ZTRA')) {
        api.local.priceBuildupMatrix.RoundingSchemColumn = 'Set/ Tray Price'
        response = roundingSchemeMap?.getAt('SetTrayPrice') as BigDecimal
        api.local.priceBuildupMatrix.RoundingSchemeExistBetween = roundingSchemeMap?.From + ' to ' + roundingSchemeMap?.To
        api.local.priceBuildupMatrix.BeforeRoundingScheme = price
        if (response <= 0 || !response) {
            api.local.priceBuildupMatrix.RoundingSchemColumn = 'No Value in Set Tray Column (New RP rounded - 2 Decimal)'
            response = out.CommonLib.RoundingUtils.roundNumber(price, 2)
        }
    } else {
        api.local.priceBuildupMatrix.RoundingSchemColumn = 'Miscellaneous Price'
        response = roundingSchemeMap?.getAt('MiscellaneousPrice') as BigDecimal
    }*/
    api.local.priceBuildupMatrix.appliedRoundingPrice = response
    return response
}

def sqlLiteralString(s) {
    String str = s as String
    return "'" + str.replace("'", "''") + "'"
}


BigDecimal priceChangeThreshold(BigDecimal suggestedPrice) {
    BigDecimal newSuggestedPrice = 0.0
    boolean followUpApplied = false
    newSuggestedPrice = suggestedPrice
    if (api.local.priceChangeThreshold && suggestedPrice) {
        keySet = api.local.priceChangeThreshold?.keySet()?.minus(['Channel', 'Country', 'Basket', 'PriceChangeFrequency'])
        channel = out.CalculationChannel
        if (api.local.priceChangeThreshold) {
            if (api.local.priceChangeThreshold['MinChangeAbs'] && suggestedPrice) {
                api.local.priceBuildupMatrix.followupMINAFactor = api.local.priceChangeThreshold['MinChangeAbs']
                api.local.priceBuildupMatrix.followupMINAApplied = false
                api.local.priceBuildupMatrix.followupMINAAppliedCondition = 'MIN price change (abs)'
                api.local.priceBuildupMatrix.followupMINAAppliedStringFormula = 'Abs((Suggested Price - Selling Price)) < MIN price change (abs)'
                api.local.priceBuildupMatrix.followupMINAAppliedFormula = 'Abs((' + newSuggestedPrice + '-' + out['SalesPrice' + channel] + ')) < ' + api.local.priceChangeThreshold['MinChangeAbs']
                difference = newSuggestedPrice - out['SalesPrice' + channel]
                difference = difference < 0 ? (difference * -1) : difference
                if (difference < api.local.priceChangeThreshold['MinChangeAbs'] && !followUpApplied && out['SalesPrice' + channel] && suggestedPrice != out["SalesPrice" + channel]) {
                    api.local.priceBuildupMatrix.followupMINAApplied = true
                    newSuggestedPrice = out['SalesPrice' + channel]
                    api.local.priceBuildupMatrix.followupMINAAppliedPrice = newSuggestedPrice
                    api.local.priceBuildupMatrix.PricingReason = 'MIN(abs) Price Change adjusted '
                    followUpApplied = true

                }
            }

            if (api.local.priceChangeThreshold['MaxChangePct'] && suggestedPrice) {
                api.local.priceBuildupMatrix.guardRailsVoilated = api.local.priceBuildupMatrix.guardRailsVoilated ?: false
                api.local.priceBuildupMatrix.followupMAXCPFactor = api.local.priceChangeThreshold['MaxChangePct']
                api.local.priceBuildupMatrix.followupMAXCPFinalPriceSF = '(SalesPrice' + channel + '((100 + MAX change rate (%))/100))'
                api.local.priceBuildupMatrix.followupMAXCPFinalPriceF = '(' + out['SalesPrice' + channel] + '* ((100 +' + api.local.priceChangeThreshold['MaxChangePct'] * 100 + ')/100))'
                api.local.priceBuildupMatrix.followupMAXCPAppliedCondition = 'MAX change rate (%)'
                api.local.priceBuildupMatrix.followupMAXCPAppliedStringFormula = '((SuggestedPrice > (SalesPrice' + channel + ' * ((100 + MAX change rate)/100)))'
                api.local.priceBuildupMatrix.followupMAXCPAppliedFormula = '(' + newSuggestedPrice + '>' + out['SalesPrice' + channel] + '* ((100 +' + api.local.priceChangeThreshold['MaxChangePct'] * 100 + ')/100))'
                api.local.priceBuildupMatrix.followupMAXCPApplied = false
                finalSellingPrice = out['SalesPrice' + channel] * ((100 + (api.local.priceChangeThreshold['MaxChangePct'] * 100)) / 100)
                if (newSuggestedPrice > finalSellingPrice && !followUpApplied && out['SalesPrice' + channel]) {
                    newSuggestedPrice = finalSellingPrice
                    api.local.priceBuildupMatrix.followupMAXCPApplied = true
                    api.local.priceBuildupMatrix.guardRailsVoilated = true
                    api.local.priceBuildupMatrix.followupMAXCPAppliedPrice = newSuggestedPrice
                    api.local.priceBuildupMatrix.PricingReason = 'Max% Price Change adjusted'
                    api.local.approvelCondition = 'Max% price change violated'
                }
            }
        }
    }
    return newSuggestedPrice
}

BigDecimal priceGuardrailCheck(BigDecimal suggestedPrice, boolean checklegalMin = false) {
    BigDecimal newSuggestedPrice = suggestedPrice
    if (suggestedPrice) {
        api.local.priceBuildupMatrix.guardRailsVoilated = api.local.priceBuildupMatrix.guardRailsVoilated ?: false
        Map guardrailMapping = libs.FressnapfLib.DataUtils.getPricingGuardrailsMaster(out.Country)
        api.local.priceBuildupMatrix.priceGuardRail = guardrailMapping ? 'Present' : 'Not'
        channel = out.CalculationChannel
        purchasePrice = channel == 'Offline' ? (out.PurchasePriceConfiguration == 'PP Offline Price' ? out.PurchasePriceOffline : out.PurchasePriceRetail) : out['PurchasePriceOnline'] ?: 0
        purchasePriceString = channel == 'Offline' ? (out.PurchasePriceConfiguration == 'PP Offline Price' ? 'Purchase Price Offline' : 'Purchase Price Retail') : 'Purchase Price Online'
        productAttributes = [out.Country, channel, out.Category ?: '*', out.SubCategory ?: "*", out.CGRNo ?: "*", out.BrandType ?: "*", out.Brand ?: "*", out.SubBrand ?: "*"]
        minGuardRails = (Lib.getMargin(guardrailMapping, productAttributes, 'GuardRailsMinMargin', false) ?: 0) / 100
        price = out.VAT ? (suggestedPrice / (1 + (out.VAT / 100))) : suggestedPrice
        margin = price && purchasePrice ? ((price - purchasePrice) / price) : 0.0
        api.local.priceBuildupMatrix.priceGuardRailApplied = false
        api.local.priceBuildupMatrix.priceGuardRailAppliedCondition = 'No GuardRails Applied'
        api.local.priceBuildupMatrix.priceGuardRailValidation = '(' + margin + ' < ' + minGuardRails + ')'
        api.local.priceBuildupMatrix.priceGuardRailConditionValidStringF = 'Sales Margin < Guardrail MIN Margin'
        api.local.priceBuildupMatrix.priceGuardRailAppliedMargin = minGuardRails * 100
        api.local.priceBuildupMatrix.priceGuardRailStringFormula = '(- ' + purchasePriceString + '/((Min GuardRails)-1)*(1+(VAT/100))'
        api.local.priceBuildupMatrix.priceGuardRailFormula = '( -' + purchasePrice + '/' + '((' + minGuardRails + ')-1) *(1+(' + out.VAT + '/100))'
        if (margin < minGuardRails && !checklegalMin) {
            api.local.priceBuildupMatrix.priceGuardRailApplied = true
            api.local.priceBuildupMatrix.priceGuardRailAppliedCondition = 'Guardrail MIN Margin'
            api.local.priceBuildupMatrix.guardRailsVoilated = true
            api.addWarning('MIN Margin Guardrail applied')
            newSuggestedPrice = (-purchasePrice / (minGuardRails - 1)) * (1 + (out.VAT / 100))
            api.local.priceBuildupMatrix.priceGuardRailPrice = newSuggestedPrice
            api.local.approvelCondition = 'Guardrail MIN margin violated'
            api.local.priceBuildupMatrix.PricingReason = 'Price follows MIN margin guidance'
        }
        legalMargin = Lib.getMargin(guardrailMapping, productAttributes, 'LegalMinMargin', false)
        if (legalMargin || legalMargin == 0 || checklegalMin) {
            conditionValue = purchasePrice ? (-purchasePrice / ((legalMargin / 100) - 1)) * (1 + (out.VAT / 100)) : 0
            api.local.priceBuildupMatrix.legalMargin = legalMargin
            api.local.priceBuildupMatrix.legalMinMarginVoilated = false
            api.local.priceBuildupMatrix.legalMinMarginCondition = ' legal MIN Margin not violated'
            api.local.priceBuildupMatrix.legalMinMarginValidation = '(' + newSuggestedPrice + ' < ' + conditionValue + ')'
            api.local.priceBuildupMatrix.legalMinMarginValidationStringF = 'Sales Margin < legal MIN Margin'
            if (newSuggestedPrice < conditionValue) {
                api.local.priceBuildupMatrix.legalMinMarginVoilated = true
                api.local.priceBuildupMatrix.guardRailsVoilated = true
                api.local.priceBuildupMatrix.legalMinMarginCondition = ' legal MIN Margin violated'
                api.local.priceBuildupMatrix.PricingReason = 'Price follows legal requirements'
                api.local.approvelCondition = api.local.approvelCondition ? api.local.approvelCondition + ' & Legal MIN margin violated' : api.local.approvelCondition
                api.addWarning('Legal MIN margin violated')
            }

        }

    }
    return newSuggestedPrice ?: suggestedPrice
}

BigDecimal applyDistanceToKeyChannel(BigDecimal suggestedPrice, Map MCChannelBasketMappingMaster) {
    api.local.DistanceToKey = [:]
    String mcChannelBasketMappingKey = out.ItemClass + '_' + out.CGRNo
    api.local.priceBuildupMatrix.DistanceToKeyApplied = false
    api.local.priceBuildupMatrix.SalesDistanceToKeyApplied = false
    mappingInfo = MCChannelBasketMappingMaster?.getAt(mcChannelBasketMappingKey)
    api.local.priceBuildupMatrix.DistanceToKeyChannelPercentage = (mappingInfo?.DistanceToKeyChannelPercentage?:0) * 100
    api.local.priceBuildupMatrix.DistanceToKeyChannelABS = mappingInfo?.DistanceToKeyChannelABS
    // api.local.priceBuildupMatrix.SalesDistanceToKeyChannel = MCChannelBasketMappingMaster?.getAt(mcChannelBasketMappingKey)?.DistanceToKeyChannel as Integer
    api.local.priceBuildupMatrix.DistanceToKeyAppliedCondition = 'No Distance to Key Channel '
    api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedCondition = 'No Distance to Key Channel '
    String keyChannel = out.CommonLib.DataUtils.getBasketKeyChannelMaster(out.Country)[out.Basket]?.keyChannel
    api.local.priceBuildupMatrix.DistanceToKeyKeyChannel = keyChannel
    // api.local.priceBuildupMatrix.SalesDistanceToKeyKeyChannel = keyChannel
    if ((out.ChannelPerspTool == 'Offline' && keyChannel == 'Online' && out.CalculationChannel == "Offline") || (out.ChannelPerspTool == "Online" && keyChannel == "Offline" && out.CalculationChannel == "Online")) {
        return suggestedPrice
    }
    if(out.MaterialType!="HAWA"){
        suggestedPrice = distanceKeyChannelCalculator("ST",keyChannel,suggestedPrice)
        return suggestedPrice
    }
    def price = ['SalesPrice', 'PriceGrid']
    def versions = ['Percentage', 'ABS']
    price.each { it ->
        if (it.contains('SalesPrice')) {
            BigDecimal channelPrice = keyChannel == 'Online' ? out.SalesPriceOnline : out.SalesPriceOffline
            BigDecimal sellingPrice = keyChannel == 'Online' ? out.SalesPriceOffline : out.SalesPriceOnline
            versions?.each{method->
                distance = (method == "Percentage" ? api.local.priceBuildupMatrix.DistanceToKeyChannelPercentage: api.local.priceBuildupMatrix.DistanceToKeyChannelABS)?:0.0
                suggestedPrice = (salespricedeviationCalculator(sellingPrice, channelPrice, keyChannel, suggestedPrice, method,distance))
                //api.trace("suggestedPrice sales",suggestedPrice)
                sellingPrice=suggestedPrice
            }
        }
        if (it.contains('PriceGrid')) {
            salesPrice = api.local.pricebeforeDistance
            BigDecimal channelPrice = keyChannel == 'Online' ? out.SalesPriceOnline : out.SalesPriceOffline
            versions?.each { method ->
                distance = (method == "Percentage" ? api.local.priceBuildupMatrix.DistanceToKeyChannelPercentage: api.local.priceBuildupMatrix.DistanceToKeyChannelABS)?:0.0
                suggestedPrice = pricegriddeviationCalculator(/*api.local.pricebeforeDistance*/salesPrice, channelPrice, keyChannel,distance,method)
                salesPrice=suggestedPrice
            }
        }
    }
    if(out.MaterialType!="HAWA"){
        suggestedPrice = distanceKeyChannelCalculator("ST",keyChannel,suggestedPrice)
    }
    return suggestedPrice
}


def salespricedeviationCalculator(BigDecimal suggestedPrice, BigDecimal channelPrice, String keyChannel, BigDecimal NewRP,String method,BigDecimal distance) {
    //versions?.each {method->
        //distance = method == "Percentage" ? api.local.priceBuildupMatrix.DistanceToKeyChannelPercentage: api.local.priceBuildupMatrix.DistanceToKeyChannelABS
        api.local.priceBuildupMatrix.SalesDistanceToKeyKeyChannelPrice = channelPrice
        api.local.previousPriceResaon = api.local.priceBuildupMatrix.PricingReason
        api.local.priceBuildupMatrix.salesSuggestPrice = suggestedPrice
        channelPrice == 0.0 && !api.local.DistanceToKey.AppliedCondition ? api.local.DistanceToKey.AppliedCondition = 'No Key Channel Price' : ''
        api.local.priceBuildupMatrix.SalesDistanceToKeyChannelfailWarning = (keyChannel == out.CalculationChannel) ? 'Key and Calculation are on same channel :' + keyChannel : null
        api.local.DistanceToKey.AppliedCondition = (keyChannel == out.ChannelPerspTool) ? 'Key and Article Channel are same :' + keyChannel : null
    api.local.priceBuildupMatrix.('SalesDistanceToKeyApplied'+"$method") = false
    boolean keychannelCheck = out.MaterialType!="HAWA" ? true : (keyChannel != out.CalculationChannel)
        if (keyChannel != out.ChannelPerspTool && channelPrice && keychannelCheck && suggestedPrice) {
            api.local.priceBuildupMatrix.('SalesDistanceToKeyApplied'+"$method") = false
            channelPrice == suggestedPrice ? api.local.DistanceToKey.AppliedCondition = 'No Price Overridden' : ''
            if (distance == 0 && out.BrandType == '1' && channelPrice != suggestedPrice) {
                api.addWarning('Distance to Key Channel Applied')
                api.local.priceBuildupMatrix.PricingReason = 'Distance to Key Channel adjusted'
                api.local.priceBuildupMatrix.("SalesDistanceToKeyAppliedCondition"+"$method") = 'Key Channel Price Overridden '
                api.local.priceBuildupMatrix.("SalesDistanceToKeyApplied"+"$method") = true
                api.local.priceBuildupMatrix.("SalesDistanceToKeyAppliedPrice"+"$method") = channelPrice
                api.local.priceBuildupMatrix.("SalesDistanceToKeyMaxAllowedDev"+"$method") = null
                api.local.priceBuildupMatrix.("SalesDistanceToKeyMinAllowedDev"+"$method") = null
                return channelPrice
            } else if (distance && out.BrandType == '2') {
                BigDecimal minAllowedDeviation
                BigDecimal maxAllowedDeviation
                if(method=="Percentage"||method=="PercentageST"){
                    minAllowedDeviation = channelPrice * ((100 - distance) / 100)
                    api.local.priceBuildupMatrix.("SalesDistanceToKeyMinAllowedDevSTFormula"+"$method") = 'Key Channel Price * ((100 - Distance to Key Channel)/100)'
                    api.local.priceBuildupMatrix.("SalesDistanceToKeyMinAllowedDevFormula"+"$method") = channelPrice + '*((100 - ' + distance + ')/100)'
                    api.local.priceBuildupMatrix.("SalesDistanceToKeyMinAllowedDev"+"$method") = minAllowedDeviation
                    maxAllowedDeviation = channelPrice * ((100 + distance) / 100)
                    api.local.priceBuildupMatrix.("SalesDistanceToKeymaxAllowedDeviationSTFormula"+"$method") = 'Key Channel Price * ((100 + Distance to Key Channel)/100)'
                    api.local.priceBuildupMatrix.("SalesDistanceToKeymaxAllowedDeviationFormula"+"$method") = channelPrice + '*((100 + ' + distance + ')/100)'
                    api.local.priceBuildupMatrix.("SalesDistanceToKeyMaxAllowedDev"+"$method") = maxAllowedDeviation
                }else if(method=="ABS"||method=="ABSST"){
                    minAllowedDeviation = (channelPrice - distance)
                    api.local.priceBuildupMatrix.("SalesDistanceToKeyMinAllowedDevSTFormula"+"$method") = 'Key Channel Price - Distance to Key Channel'
                    api.local.priceBuildupMatrix.("SalesDistanceToKeyMinAllowedDevFormula"+"$method") = channelPrice + ' - ' + distance
                    api.local.priceBuildupMatrix.("SalesDistanceToKeyMinAllowedDev"+"$method") = minAllowedDeviation
                    maxAllowedDeviation = channelPrice + distance
                    api.local.priceBuildupMatrix.("SalesDistanceToKeymaxAllowedDeviationSTFormula"+"$method") = 'Key Channel Price + Distance to Key Channel'
                    api.local.priceBuildupMatrix.("SalesDistanceToKeymaxAllowedDeviationFormula"+"$method") = channelPrice + ' + ' + distance
                    api.local.priceBuildupMatrix.("SalesDistanceToKeyMaxAllowedDev"+"$method") = maxAllowedDeviation
                }
                if (suggestedPrice >= minAllowedDeviation && suggestedPrice <= maxAllowedDeviation) {
                    api.local.priceBuildupMatrix.("SalesDistanceToKeyApplied"+"$method") = false
                    api.local.priceBuildupMatrix.("SalesDistanceToKeyAppliedConditionStatement"+"$method") = true
                    api.local.priceBuildupMatrix.("SalesDistanceToKeyAppliedCondition"+"$method") = 'Not exceeded the Limit'
                    api.local.priceBuildupMatrix.("SalesDistanceToKeyAppliedPrice"+"$method") = suggestedPrice
                    return suggestedPrice
                } else if (suggestedPrice < minAllowedDeviation) {
                    api.local.priceBuildupMatrix.PricingReason = 'Distance to Key Channel adjusted'
                    api.local.priceBuildupMatrix.SalesPricingReason = 'Distance to Key Channel adjusted'
                    api.addWarning('Distance to Key Channel Applied')
                    api.local.priceBuildupMatrix.("SalesDistanceToKeyApplied"+"$method") = true
                    api.local.priceBuildupMatrix.("SalesDistanceToKeyAppliedCondition"+"$method") = 'Min Deviation Applied'
                    api.local.priceBuildupMatrix.("SalesDistanceToKeyAppliedPrice"+"$method") = minAllowedDeviation
                    return minAllowedDeviation
                } else if (suggestedPrice > maxAllowedDeviation) {
                    api.local.priceBuildupMatrix.PricingReason = 'Distance to Key Channel adjusted'
                    api.local.priceBuildupMatrix.SalesPricingReason = 'Distance to Key Channel adjusted'
                    api.addWarning('Distance to Key Channel Applied')
                    api.local.priceBuildupMatrix.("SalesDistanceToKeyApplied"+"$method") = true
                    api.local.priceBuildupMatrix.("SalesDistanceToKeyAppliedCondition"+"$method") = 'Max Deviation Applied'
                    api.local.priceBuildupMatrix.("SalesDistanceToKeyAppliedPrice"+"$method") = maxAllowedDeviation
                    return maxAllowedDeviation
                }
            }
        } else {
            api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedCondition = (keyChannel == out.ChannelPerspTool || keyChannel == out.CalculationChannel) ? 'Key And Follower in same Channel' : ''
            (keyChannel != out.ChannelPerspTool || keyChannel != out.CalculationChannel) && !channelPrice ? api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedCondition = 'Key Channel Price Not Available' : ''
        }
   // }
    return NewRP
}

def pricegriddeviationCalculator(BigDecimal suggestedPrice, BigDecimal channelPrice, String keyChannel,BigDecimal distance,String method) {
    api.local.priceBuildupMatrix.DistanceToKeyKeyChannelPrice = channelPrice
    api.local.priceBuildupMatrix.priceSuggestPrice = suggestedPrice
    channelPrice == 0.0 ? api.local.DistanceToKey.AppliedCondition = 'No Key Channel Price' : ''
    api.local.priceBuildupMatrix.DistanceToKeyChannelfailWarning = (keyChannel == out.CalculationChannel) ? 'Key and Calculation are on same channel :' + keyChannel : null
    api.local.DistanceToKey.AppliedCondition = (keyChannel == out.ChannelPerspTool) ? 'Key and Article Channel are same :' + keyChannel : null
    api.local.priceBuildupMatrix.DistanceToKeyChannelfailWarning = (channelPrice == suggestedPrice) ? 'Key & Suggested Price is same' : ''
    api.local.priceBuildupMatrix.("DistanceToKeyApplied"+"$method") = false
    boolean keychannelCheck = out.MaterialType!="HAWA" ? true : (keyChannel != out.CalculationChannel)
    if (keyChannel != out.ChannelPerspTool && channelPrice && keychannelCheck && suggestedPrice) {
        api.local.priceBuildupMatrix.("DistanceToKeyApplied"+"$method") = false
        channelPrice == suggestedPrice ? api.local.DistanceToKey.AppliedCondition = 'No Price Overridden' : ''
        if (distance == 0 && out.BrandType == '1' && channelPrice != suggestedPrice) {
            api.addWarning('Distance to Key Channel Applied')
            api.local.priceBuildupMatrix.PricingReason = 'Distance to Key Channel adjusted'
            api.local.priceBuildupMatrix.("DistanceToKeyAppliedCondition"+"$method") = 'Key Channel Price Overridden '
            api.local.priceBuildupMatrix.("DistanceToKeyApplied"+"$method") = true
            api.local.priceBuildupMatrix.("DistanceToKeyAppliedPrice"+"$method") = channelPrice
            api.local.priceBuildupMatrix.("DistanceToKeyMaxAllowedDev"+"$method") = null
            api.local.priceBuildupMatrix.("DistanceToKeyMinAllowedDev"+"$method") = null
            return channelPrice
        } else if (distance && out.BrandType == '2') {
            BigDecimal minAllowedDeviation
            BigDecimal maxAllowedDeviation
            if(method=="Percentage"||method=="PercentageST"){
                minAllowedDeviation = channelPrice * ((100 - distance) / 100)
                api.local.priceBuildupMatrix.("DistanceToKeyMinAllowedDevSTFormula"+"$method") = 'Key Channel Price * ((100 - Distance to Key Channel)/100)'
                api.local.priceBuildupMatrix.("DistanceToKeyMinAllowedDevFormula"+"$method") = channelPrice + '*((100 - ' + distance + ')/100)'
                api.local.priceBuildupMatrix.("DistanceToKeyMinAllowedDev"+"$method") = minAllowedDeviation
                maxAllowedDeviation = channelPrice * ((100 + distance) / 100)
                api.local.priceBuildupMatrix.("DistanceToKeymaxAllowedDeviationSTFormula"+"$method") = 'Key Channel Price * ((100 + Distance to Key Channel)/100)'
                api.local.priceBuildupMatrix.("DistanceToKeymaxAllowedDeviationFormula"+"$method") = channelPrice + '*((100 + ' + distance + ')/100)'
                api.local.priceBuildupMatrix.("DistanceToKeyMaxAllowedDev"+"$method") = maxAllowedDeviation
            }else if(method=="ABS"||method=="ABSST"){
                minAllowedDeviation = channelPrice  - distance
                api.local.priceBuildupMatrix.("DistanceToKeyMinAllowedDevSTFormula"+"$method") = 'Key Channel Price Distance to Key Channel'
                api.local.priceBuildupMatrix.("DistanceToKeyMinAllowedDevFormula"+"$method") = channelPrice + ' - ' + distance
                api.local.priceBuildupMatrix.("DistanceToKeyMinAllowedDev"+"$method") = minAllowedDeviation
                maxAllowedDeviation = channelPrice  + distance
                api.local.priceBuildupMatrix.("DistanceToKeymaxAllowedDeviationSTFormula"+"$method") = 'Key Channel Price + Distance to Key Channel'
                api.local.priceBuildupMatrix.("DistanceToKeymaxAllowedDeviationFormula"+"$method") = channelPrice + ' + ' + distance
                api.local.priceBuildupMatrix.("DistanceToKeyMaxAllowedDev"+"$method") = maxAllowedDeviation
            }
            if (suggestedPrice >= minAllowedDeviation && suggestedPrice <= maxAllowedDeviation) {
                api.local.priceBuildupMatrix.("DistanceToKeyApplied"+"$method") = false
                api.local.priceBuildupMatrix.("DistanceToKeyAppliedConditionStatement"+"$method") = true
                api.local.priceBuildupMatrix.("DistanceToKeyAppliedCondition"+"$method") = 'Not exceeded the Limit - reverted to Suggested Price'
                api.local.priceBuildupMatrix.PricingReason = api.local.previousPriceResaon
                api.local.priceBuildupMatrix.("DistanceToKeyAppliedPrice"+"$method") = suggestedPrice
                return suggestedPrice
            } else if (suggestedPrice < minAllowedDeviation) {
                api.local.priceBuildupMatrix.("DistanceToKeyApplied"+"$method") = true
                api.local.priceBuildupMatrix.PricingReason = 'Distance to Key Channel adjusted'
                api.addWarning('Distance to Key Channel Applied')
                api.local.priceBuildupMatrix.("DistanceToKeyApplied"+"$method") = true
                api.local.priceBuildupMatrix.("DistanceToKeyAppliedCondition"+"$method") = 'Min Deviation Applied'
                api.local.priceBuildupMatrix.("DistanceToKeyAppliedPrice"+"$method") = minAllowedDeviation
                return minAllowedDeviation
            } else if (suggestedPrice > maxAllowedDeviation) {
                api.local.priceBuildupMatrix.("DistanceToKeyApplied"+"$method") = true
                api.local.priceBuildupMatrix.PricingReason = 'Distance to Key Channel adjusted'
                api.addWarning('Distance to Key Channel Applied')
                api.local.priceBuildupMatrix.("DistanceToKeyApplied"+"$method") = true
                api.local.priceBuildupMatrix.("DistanceToKeyAppliedCondition"+"$method") = 'Max Deviation Applied'
                api.local.priceBuildupMatrix.("DistanceToKeyAppliedPrice"+"$method") = maxAllowedDeviation
                return maxAllowedDeviation
            }
        }
    } else {
        keyChannel == out.ChannelPerspTool || keyChannel == out.CalculationChannel ? api.local.priceBuildupMatrix.DistanceToKeyAppliedCondition = 'Key And Follower in same Channel' : ''
        (keyChannel != out.ChannelPerspTool || keyChannel != out.CalculationChannel) && !channelPrice ? api.local.priceBuildupMatrix.DistanceToKeyAppliedCondition = 'Key Channel Price Not Available' : ''
    }
    return suggestedPrice
}
def materialTypeBasedDistanceToKeyChannel(BigDecimal suggestedPrice){
    List bomData = api.bomList()
    if (bomData.size() > 1){
        api.local.priceBuildupMatrix.setTrayDistanceToKeyChannelFailure = ("More than one component found")
        return suggestedPrice
    }
    else if(!bomData){
        api.local.priceBuildupMatrix.setTrayDistanceToKeyChannelFailure = ("No component found")
        return suggestedPrice
    }
    else if(bomData.size() == 1){
        hawasku = bomData.getAt(0)?.label
        quantity = bomData.getAt(0)?.quantity?:0
        sellingPrice = api.global.sellingPrices[hawasku]?.find { it.salesOrg == "DE01" }?.salesPrice
        hawaSKUContext = api.currentContext(hawasku)
        hawaskuPrice = sellingPrice ?: hawaSKUContext?.getAt(api.global.metaData?.getAt("Curr. SP Offline")) as BigDecimal
        api.local.priceBuildupMatrix.OfflinesetTrayDistanceToKeyChannelSPrice = hawaskuPrice
        BigDecimal offlineSetTrayPrice = (hawaskuPrice?:0) * quantity
        api.local.priceBuildupMatrix.OfflinesetTrayDistanceToKeyChannelRPriceBD = offlineSetTrayPrice
        BigDecimal discountedOfflineSetTrayPrice = Lib.applySetTrayDiscount(offlineSetTrayPrice)
        api.local.priceBuildupMatrix.OfflinesetTrayDistanceToKeyChannelRPriceAF = discountedOfflineSetTrayPrice
        api.local.priceBuildupMatrix.OfflinesetTrayDistanceToKeyChannelcomp = hawasku + "( "+"Quantity : "+quantity+" Price : "+hawaskuPrice+")"
        return discountedOfflineSetTrayPrice
    }
return
}

BigDecimal distanceKeyChannelCalculator(String materialType,String keyChannel, BigDecimal suggestedPrice){
    BigDecimal channelPrice = materialTypeBasedDistanceToKeyChannel(suggestedPrice)
    def price = ['SalesPrice', 'PriceGrid']
    def versions = ['Percentage', 'ABS']
    price.each { it ->
        if (it.contains('SalesPrice')) {
            //keyChannel == 'Online' ? out.SalesPriceOnline : out.SalesPriceOffline
            BigDecimal sellingPrice = suggestedPrice///*keyChannel == 'Online' ? out.SalesPriceOffline : */out.SalesPriceOnline
            versions?.each{method->
                distance = (method == "Percentage" ? api.local.priceBuildupMatrix.DistanceToKeyChannelPercentage: api.local.priceBuildupMatrix.DistanceToKeyChannelABS)?:0.0
                suggestedPrice = (salespricedeviationCalculator(sellingPrice, channelPrice, keyChannel, suggestedPrice, (method+materialType),distance))
                sellingPrice = suggestedPrice
            }
        }
        BigDecimal referencePrice = 0
        if (it.contains('PriceGrid')) {
            //BigDecimal channelPrice = keyChannel == 'Online' ? out.SalesPriceOnline : out.SalesPriceOffline
            versions?.each { method ->
                distance = (method == "Percentage" ? api.local.priceBuildupMatrix.DistanceToKeyChannelPercentage: api.local.priceBuildupMatrix.DistanceToKeyChannelABS)?:0.0
                suggestedPrice = pricegriddeviationCalculator(referencePrice?:api.local.pricebeforeDistance, channelPrice, keyChannel,distance,(method+materialType))
                api.addWarning("api.local.pricebeforeDistance"+api.local.pricebeforeDistance)
                referencePrice = suggestedPrice
            }
        }
    }
    return suggestedPrice
}
