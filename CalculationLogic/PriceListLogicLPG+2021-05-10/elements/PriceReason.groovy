if (out.CalculatedListPrice != out.ListPrice && !out.ManualOverrideChecker) {
    Map PRG = libs.FressnapfLib.DataUtils.getPRGMaster(out.Country)
    baseFamilyValue = PRG?.values()?.find { it.BaseFamily == out.PriceFamilyCode }
    relatedFamilyValue = PRG?.values()?.find { it.RelatedFamily == out.PriceFamilyCode }
    if (baseFamilyValue && (!api.isFullListCalculation())) {
        api.criticalAlert("Price family " + out.PriceFamilyCode + " is a Basic family and it has dependent Related family")
    }
}
if (!out.ListPrice) {
    return ""
}
if (api.local.manualOverride) {
    return "Manual Override - Calculated Price blocked"
}
if (api.local.priceBuildupMatrix?.marketingCamp) {
    return " Price blocked - Marketing Campaign"
}
if (api.local.contextValues) {
    return " Price blocked - Price Change Frequency Low"
}

return api.local.priceBuildupMatrix.PricingReason