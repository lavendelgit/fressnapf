def chartUtils = libs.FressnapfLib.ChartUtils
List waterfallSeries = [
        chartUtils.buildColumnSeries("Online",
                "Online",
                out.PurchasePriceOnline,
                out.SalesPriceOnline,
                out.CalculatedListPrice,
                out.ListPrice),
        chartUtils.buildColumnSeries("Offline",
                "Offline",
                out.PurchasePriceOffline,
                out.SalesPriceOffline,
                out.CalculatedListPrice,
                out.ListPrice)
]
Map waterfallDef = chartUtils.getPriceComparisionColumnChartDef(waterfallSeries,
        out.TargetCurrency,
        'Online v/s Offline Price Comparision')

return api.buildHighchart(waterfallDef)