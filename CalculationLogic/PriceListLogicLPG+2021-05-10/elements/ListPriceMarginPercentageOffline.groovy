if(out.ListPrice  && out.PurchasePriceOffline!=0 && out.PurchasePriceOffline) {
    BigDecimal listPrice = out.PriceExVAT != out.CalculatedListPrice ? (out.PriceExVAT ?: 0) : api.local.ListPriceOffline
    api.local.offlineMarginPrice = listPrice
    BigDecimal markUpPercentage = listPrice > 0 ?
            ((listPrice - out.PurchasePriceOffline) / listPrice)
            : 0.0
    return out.CommonLib.UIUtils.getArrowedCell(markUpPercentage)
}
return
