if(api.global.calculationEndDate != 0 && api.local.contextValues){
    return out.ArticleContext?.RegionalMaterialDescription
}
def sku = out.Article
def country = out.Country
def articleDesc = api.global.batch[sku].ArticleDescription
def desc

switch (country) {
    case "Germany":
        desc = (articleDesc)?.attribute1
        break;
    case "Poland":
        desc = (articleDesc)?.attribute2
        break;
    case "France":
        desc = (articleDesc)?.attribute3
        break;
    case "Austria":
        desc = (articleDesc)?.attribute4
        break;
    case "Belgium":
        desc = (articleDesc)?.attribute5
        break;
    case "Hungary":
        desc = (articleDesc)?.attribute6
        break;
    case "Italy":
        desc = (articleDesc)?.attribute7
        break;
    case "Luxemburg":
        desc = (articleDesc)?.attribute8
        break;
    case "Republic of Ireland":
        desc = (articleDesc)?.attribute9
        break;
    case "Switzerland":
        desc = (articleDesc)?.attribute10
        break;
}

if (desc) return desc