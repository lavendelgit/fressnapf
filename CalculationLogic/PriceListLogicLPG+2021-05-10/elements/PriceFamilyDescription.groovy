if(api.global.calculationEndDate!= 0 && api.local.contextValues){
    return out.ArticleContext?.PriceFamilyDescription

}
def getPricingFamilyMaster = out.CommonLib.DataUtils.getPricingFamilyMaster()
return getPricingFamilyMaster?.getAt(out.Product?.attribute20)?.value ?: ""