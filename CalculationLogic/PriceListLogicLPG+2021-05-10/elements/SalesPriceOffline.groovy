if((api.local.priceBuildupMatrix?.marketingCamp && api.local.contextValues) || api.local.contextValues){
    return out.ArticleContext?.SalesPriceOffline

}
//def getSalesPriceMaster = out.CommonLib.DataUtils.getSalesPriceMaster()
def channel = "Offline"
def sku = out.Article
def salesOrg = api.global.countryChannelMap[channel]

def sellingPriceRecord = api.global.batch[sku]?.SellingPrice
def offlineSellingData = (sellingPriceRecord)?.findAll { it.attribute2 == salesOrg }?.find{ it.attribute5 <= out.PricingDate?.format("YYYY-MM-dd")}
def offlineSellingPrice = (offlineSellingData) ? offlineSellingData?.attribute8 : null
return out.CommonLib.CommonUtils.applyExchangeRate((offlineSellingPrice) ? offlineSellingPrice : 0,
        out.SalesPriceOfflineCurrency,
        out.TargetCurrency,
        out.PricingDate)