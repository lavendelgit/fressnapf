if (out.PurchasePriceOffline && out.PurchasePriceRetail) {
    BigDecimal markUpPercentage = out.PurchasePriceRetail > 0 ?
            ((out.PurchasePriceRetail - out.PurchasePriceOffline) / out.PurchasePriceRetail)
            : 0.0
    return out.CommonLib.UIUtils.getArrowedCell(markUpPercentage)
}