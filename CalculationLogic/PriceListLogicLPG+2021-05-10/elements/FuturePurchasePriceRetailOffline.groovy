def sortofArticle = out.MaterialType
def sku = out.Article
api.local.beforeCConversionRetail = [:]
if ((api.local.priceBuildupMatrix?.marketingCamp && api.local.contextValues) || api.local.contextValues) {
    return out.ArticleContext?.FuturePurchasePriceRetailOffline ?: null

}

if (sortofArticle == "HAWA") {
    def country = out.Country
    def countryCode = api.global.countriesMap[country]
    def getPurchasePriceMaster = api.global.purchasePrice?.getAt(out.Article)?.sort { it.attribute8 }?.find { it.attribute8 > out.PricingDate?.format("YYYY-MM-dd") }
    api.local.priceBuildupMatrix.FutureRetailPrice = getPurchasePriceMaster?.attribute6
    api.local.priceBuildupMatrix.FutureRetailCurrency = getPurchasePriceMaster?.attribute7
    api.local.futurePPRFValidFrom = getPurchasePriceMaster?.attribute8

    def purPriceOfflinePX = getPurchasePriceMaster?.attribute6 ?: 0
    def purPriceOfflineCurrency = getPurchasePriceMaster?.attribute7
    def purPriceOffline = purPriceOfflinePX //* (1 - out.TripleNetFactor)
/*api.trace("purPriceOfflinePX", purPriceOfflinePX)
api.trace("TripleNetFactor", (1 - out.TripleNetFactor))*/

    return libs.FressnapfLib.RoundingUtils.roundNumber(out.CommonLib.CommonUtils.applyExchangeRate(purPriceOffline,
            purPriceOfflineCurrency,
            out.TargetCurrency,
            out.PricingDate), 5) ?: null
} else {
    def setTrayCostData = api.global.batch[sku].SetTrayCost
    def cost = (setTrayCostData)?.attribute6
    def retailCurr = (setTrayCostData)?.attribute7
    retailCurr = (retailCurr) ?: out.TargetCurrency
    cost = (cost) ?: 0
    return libs.FressnapfLib.RoundingUtils.roundNumber(out.CommonLib.CommonUtils.applyExchangeRate(cost,
            retailCurr,
            out.TargetCurrency,
            out.PricingDate), 5) ?: null
}