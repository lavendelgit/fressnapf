if(api.local.contextValues ){
    return out.ArticleContext?.SendToSAP
}

if(api.currentItem()?.approvalState == "APPROVED" && (api.currentItem()?.approvalDate?.split('T')?.getAt(0)) == (api.targetDate()?.format("yyyy-MM-dd"))){
    return out.ArticleContext?.SendToSAP
}
if(out.AutoApprove){
    if(out["SalesPrice"+out.CalculationChannel] && out.ListPrice ){
        return out["SalesPrice"+out.CalculationChannel] != out.ListPrice?"Yes":"No"
    }
    if(out["SalesPrice"+out.CalculationChannel] && out.ListPrice == 0){
        return "No"
    }
    if( out.ListPrice && (out["SalesPrice"+out.CalculationChannel]==0 || !out["SalesPrice"+out.CalculationChannel]) ){
        return "Yes"
    }
}
