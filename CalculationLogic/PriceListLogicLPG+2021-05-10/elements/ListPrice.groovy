/*api.logInfo("api.currentItem()?.manualResultPrice",api.currentItem()?.manualResultPrice)
if (api.currentItem()?.manualResultPrice) {
    api.logInfo("api.currentItem()?.manualResultPrice",api.currentItem()?.manualResultPrice)
    Map PRG = libs.FressnapfLib.DataUtils.getPRGMaster(out.Country)
    baseFamilyValue = PRG?.values()?.find { it.BaseFamily == out.PriceFamilyCode }
    relatedFamilyValue = PRG?.values()?.find { it.RelatedFamily == out.PriceFamilyCode }
    if (baseFamilyValue) {
        api.criticalAlert("Price family "+ out.PriceFamilyCode +" is a Basic family and it has dependent Related family")
    }
    return api.currentItem()?.manualResultPrice
}*/

if (api.targetDate()?.format("yyyy-MM-dd") < (out.ArticleContext?.FutureSPValidFrom)) {
    api.local.priceBuildupMatrix.PricingReason = "Blocked due to future selling price"
    api.addWarning("Blocked due to future selling price")
    return out.ArticleContext?.FutureSP as BigDecimal
}
return out.ListPriceOverride ? (out.ListPriceOverride as BigDecimal) : (out.CalculatedListPrice ?: 0.0)
