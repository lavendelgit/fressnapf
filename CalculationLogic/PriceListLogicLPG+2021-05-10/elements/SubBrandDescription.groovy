if (!api.global.subBrandDesc) {
    api.global.subBrandDesc = api.findLookupTableValues("SubBrand")?.collectEntries { it -> [(it.key1 + "-" + it.key2): it.attribute1] }
}
return api.global.subBrandDesc?.getAt(out.Brand + "-" + out.SubBrand)