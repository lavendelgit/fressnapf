if((api.local.priceBuildupMatrix?.marketingCamp && api.local.contextValues) || api.local.contextValues){
    return out.ArticleContext?.PricingApproach1Price

}

price = (out.PricingApproach1?.trim() == "Competitor-based Pricing" ? api.local.competitorSuggestedPrice : api.local.costPlusSuggestedPrice) ?: null
if (out.PricingApproach1 ) {
    return price && !out.ListPrice ?api.addWarning("No Calculated List Price for Pricing Approach 1 " + out.PricingApproach1): price
}
return