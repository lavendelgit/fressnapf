if (out.PurchasePriceOnline ) {
    BigDecimal markUpPercentage = out.SalesPriceOnlineEXVAT > 0 ?
            (((out.SalesPriceOnlineEXVAT - out.PurchasePriceOnline) / out.SalesPriceOnlineEXVAT))
            : 0.0
    return out.CommonLib.UIUtils.getArrowedCell(markUpPercentage)
}
else if( out.CalculationChannel =="Online"){
    api.addWarning("Online Purchase Price not available for Margin Calculation")
}