List CompetitorList = []
if (api.local.contextValues) {
    CompetitorList = out.CalculationMap?.CompetitorList
    api.local.priceBuildupMatrix.CompetitorList = CompetitorList
}
if (api.local.priceBuildupMatrix.CompetitorList || CompetitorList) {
    BigDecimal price = out["SalesPrice" + out.CalculationChannel]
    String partitionURL = api.findLookupTableValues("URLConfiguration", Filter.equal("name", "URL"))?.value?.getAt(0)
    def link = partitionURL + "modules/#/dashboards/NewCompTabDB?Article=" + out.Article + "&Country=" + out.Country + "&Basket=" + out.Basket + "&Channel=" + out.CalculationChannel + "&SalesPrice=" + price
    complink = "<a href='${link}'> Show </a>"
    return complink
}
return