BigDecimal previousPurchasePrice = out.FuturePurchasePriceOnline as BigDecimal
BigDecimal currentPurchasePrice = out.PurchasePriceOnline as BigDecimal
if (currentPurchasePrice && previousPurchasePrice && previousPurchasePrice != currentPurchasePrice /*&& api.global.purchasePriceUpdate > api.global.lpgLastrun*/) {
    percentage = (previousPurchasePrice - currentPurchasePrice) / currentPurchasePrice
    if (percentage > 0.05) {
        api.redAlert("Percentage difference is greater than 5%")
    } else if (percentage < -0.05) {
        api.redAlert("Percentage difference is less than -5%")
    }
    return out.CommonLib.UIUtils.getArrowedCell(percentage)
}