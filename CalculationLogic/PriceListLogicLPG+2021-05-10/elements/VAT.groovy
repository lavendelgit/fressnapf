if(api.global.calculationEndDate!= 0 && api.local.contextValues){
    return out.ArticleContext?.VAT

}
def sku = out.Article
def country = out.Country
def articleCountryAtt = api.global.batch[sku].ArticleCountryAttributes
return (articleCountryAtt)?.attribute10