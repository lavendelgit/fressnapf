def resultPrice = out.SalesPriceOffline
def vat = out.VAT
vat = (vat)?:0
def pricexVAT = (resultPrice)?(resultPrice/(1+(vat/100))):0
return pricexVAT