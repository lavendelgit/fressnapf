BigDecimal previousPurchasePrice = (api.global.previousPurchasePrice?.getAt(out.Article)?.PPOfflineRetail) ?: out.ArticleContext?.PurchasePriceRetail
if (previousPurchasePrice && previousPurchasePrice != out.PurchasePriceRetail) {
    return previousPurchasePrice as BigDecimal
} else if (out.ArticleContext?.getAt("PreviousPurchasePriceRetail") && out.ArticleContext?.getAt("PreviousPurchasePriceRetail") != out.PurchasePriceOffline) {
    return out.ArticleContext?.getAt("PreviousPurchasePriceRetail") as BigDecimal
}