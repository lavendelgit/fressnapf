if((api.local.priceBuildupMatrix?.marketingCamp && api.local.contextValues) || api.local.contextValues){
    return out.ArticleContext?.PricingApproach2Price

}

price = (out.PricingApproach2?.trim() == "Competitor-based Pricing" ? api.local.competitorSuggestedPrice : api.local.costPlusSuggestedPrice) ?: null
if (out.PricingApproach2 ) {
    if (out.PricingApproach1Price){
        return price
    }else if(!out.PricingApproach1Price){
        return price ?: api.addWarning("No Calculated List Price for Pricing Approach 2 " + out.PricingApproach2)
    }

}
return