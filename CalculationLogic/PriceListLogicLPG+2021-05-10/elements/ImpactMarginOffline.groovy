def sales= (out.PriceExVAT?:0) - (out.SalesPriceOfflineEXVAT?:0)
def quantity= out.QuantityDataSource ?:0
def impactSales = (sales * quantity) as BigDecimal
return impactSales
