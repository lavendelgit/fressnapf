if(out.NumberOfComponents && out.Component){
   def resultMatrix = api.newMatrix("Component","No of Component")
    out.BOMDetails?.each {Component->
        row = ["Component":Component?.label,
               "No of Component":Component?.quantity as Integer]
        resultMatrix?.addRow(row)
    }
    return resultMatrix
}