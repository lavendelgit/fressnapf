if(api.global.calculationEndDate != 0 && api.local.contextValues){
    return out.ArticleContext?.RegionalCommodityGroupDescription
}
def getCommodityGroupMaster = out.CommonLib.DataUtils.getCommodityGroupMaster()

def country = out.Country
def desc

switch (country) {
    case "Germany":
        desc = getCommodityGroupMaster?.getAt(out.CGRNo)?.germanDescription ?: null
        break;
    case "Poland":
        desc = getCommodityGroupMaster?.getAt(out.CGRNo)?.polandDescription ?: null
        break;
    case "France":
        desc = getCommodityGroupMaster?.getAt(out.CGRNo)?.franceDescription ?: null
        break;
    case "Austria":
        desc = getCommodityGroupMaster?.getAt(out.CGRNo)?.austriaDescription ?: null
        break;
    case "Belgium":
        desc = getCommodityGroupMaster?.getAt(out.CGRNo)?.belgiumDescription ?: null
        break;
    case "Hungary":
        desc = getCommodityGroupMaster?.getAt(out.CGRNo)?.hungaryDescription ?: null
        break;
    case "Italy":
        desc = getCommodityGroupMaster?.getAt(out.CGRNo)?.italyDescription ?: null
        break;
    case "Luxemburg":
        desc = getCommodityGroupMaster?.getAt(out.CGRNo)?.luxemDescription ?: null
        break;
    case "Republic of Ireland":
        desc = getCommodityGroupMaster?.getAt(out.CGRNo)?.republicDescription ?: null
        break;
    case "Switzerland":
        desc = getCommodityGroupMaster?.getAt(out.CGRNo)?.switzDescription ?: null
        break;
}

if (desc) return desc