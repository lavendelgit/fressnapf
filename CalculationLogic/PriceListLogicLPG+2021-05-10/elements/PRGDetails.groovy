Map PRG = libs.FressnapfLib.DataUtils.getPRGMaster(out.Country)

baseFamilyValue = PRG?.values()?.find { it.BaseFamily == out.PriceFamilyCode }
relatedFamilyValue = PRG?.values()?.find { it.RelatedFamily == out.PriceFamilyCode }
if (baseFamilyValue) {
    api.yellowAlert("Price family is Basic family : " + baseFamilyValue?.BaseFamily)
    return baseFamilyValue?.BaseFamily
} else if (relatedFamilyValue) {
    api.yellowAlert("Price family is dependent on Basic family : " + relatedFamilyValue?.BaseFamily)
    return relatedFamilyValue?.BaseFamily
}
return