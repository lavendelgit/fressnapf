if (out.CalculatedListPrice != out.ListPrice && !out.ManualOverrideChecker) {
    Map pricingChange = libs.FressnapfLib.DataUtils.getPriceChangeThresholdsMaster("Germany")
    String basket = out.Basket
    String channel = out.CalculationChannel
    String key = out.Country + "_" + channel + "_" + basket
    api.local.priceChangeThreshold = pricingChange[key]
    CalculatedListPrice.priceChangeThreshold(out.ListPrice)
    CalculatedListPrice.priceGuardrailCheck(out.ListPrice)
    return api.local.priceBuildupMatrix.guardRailsVoilated ? api.local.approvelCondition : marginValidate()
}
return api.local.priceBuildupMatrix.guardRailsVoilated ? api.local.approvelCondition : marginValidate()

String marginValidate() {
    BigDecimal listPrice = out.ListPrice ?: 0
    BigDecimal currentSellingPrice = out["SalesPrice" + out.CalculationChannel] ?: 0
    if (listPrice && currentSellingPrice) {
        percentage = (listPrice - currentSellingPrice) / currentSellingPrice
        if (percentage > api.local.priceBuildupMatrix.followupMAXCPFactor) {
            return "Max% price change violated"
        } else if (percentage < (api.local.priceBuildupMatrix.followupMAXCPFactor) * -1) {
            return "Max% price change violated"
        }
        return null
    }
    return null
}