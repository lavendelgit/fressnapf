Map articleInfoOnline = api.global.articleAttributes?.getAt(out.Article + '_' + "1110" + "_" + out.Country) as Map
Map articleInfoOffline = api.global.articleAttributes?.getAt(out.Article + '_' + "DE01" + "_" + out.Country) as Map
String supplyModeOnline = articleInfoOnline?.supplyModeVTL ?: articleInfoOnline?.supplyMode
String supplyModeOffline = articleInfoOffline?.supplyModeVTL ?: articleInfoOffline?.supplyMode
if(out.ChannelPerspTool == "Online&Offline") {
    if ((supplyModeOnline != "01") && (supplyModeOffline != "01")) {
        return (articleInfoOnline?.articlesStatusOverall == "20" || articleInfoOnline?.articlesStatusOverall == "30") && (articleInfoOffline?.articlesStatusOverall == "20" || articleInfoOffline?.articlesStatusOverall == "30") ? "Online&Offline" : null
    }
}