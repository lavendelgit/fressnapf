if ((out.ListPriceOverride && out.ArticleContext?.PriceReason == " Price blocked - Marketing Campaign") || out.ArticleContext?.ExitingMarketingCampgainCheck == "Price blocked - Marketing Campaign") {
    if (out.MCActiveChecker >= out.ValidFrom?.format("yyyy-MM-dd")) {
        return "Price blocked - Marketing Campaign"
    }
}
if ((out.ListPriceOverride && out.ArticleContext?.PriceReason == "Blocked due to future selling price") || api.targetDate()?.format("yyyy-MM-dd") <= out.FutureSPValidFrom?.format("yyyy-MM-dd")) {
    if (api.targetDate()?.format("yyyy-MM-dd") <= out.FutureSPValidFrom?.format("yyyy-MM-dd")) {
        if (api.currentItem()?.ValidFrom?.format("yyyy-MM-dd") > out.FutureSPValidFrom) {
            return
        }
        return "Blocked due to future selling price"
    }
}
if (out.ListPriceOverride && !(out.ArticleContext?.ExitingMarketingCampgainCheck)) {
    api.local.priceBuildupMatrix.previousPriceReason = out.ArticleContext?.PriceReason
    return out.ArticleContext?.PriceReason
}else if((out.ArticleContext?.ExitingMarketingCampgainCheck) && out.ListPriceOverride){
    api.local.priceBuildupMatrix.previousPriceReason = out.ArticleContext?.PreviousPriceReason
    return out.ArticleContext?.ExitingMarketingCampgainCheck
}

return