if(api.global.calculationEndDate!= 0 && api.local.contextValues){
    return out.ArticleContext?.ItemClass
}
def sku = out.Article
def articleBasket = api.global.batch[sku].ArticleBasket
if (articleBasket) return articleBasket.attribute3
