def sortofArticle = out.MaterialType
def sku = out.Article
api.local.beforeCConversionOnline = [:]

if((api.local.priceBuildupMatrix?.marketingCamp && api.local.contextValues)|| api.local.contextValues){
    return out.ArticleContext?.PurchasePriceOnline?:null

}

if (sortofArticle == "HAWA") {
    def country = out.Country
    def countryCode = api.global.countriesMap[country]
    def getPurchasePriceMaster = api.global.purchasePrice
    if(getPurchasePriceMaster && getPurchasePriceMaster?.getAt(out.Article)?.purchaseCurrencyOnline != out.Currency){
        api.local.priceBuildupMatrix.OnlinePrice = getPurchasePriceMaster?.getAt(out.Article)?.purchasePriceOffline
        api.local.priceBuildupMatrix.OnlineCurrency = getPurchasePriceMaster?.getAt(out.Article)?.purchaseCurrencyOnline
        !api.local.priceBuildupMatrix.OnlinePrice && out.CalculationChannel == "Online" ?api.addWarning("Purchase Price Online not available for "+out.Article):""
    }
    def purPriceOnlinePX = getPurchasePriceMaster?.getAt(out.Article)?.purchasePriceOnline ?: 0
def purPriceOnlineCurrency = getPurchasePriceMaster?.getAt(out.Article)?.purchaseCurrencyOnline
    def purPriceOnline = purPriceOnlinePX //* (1 - out.TripleNetFactor)
/*api.trace("purPriceOnlinePX", purPriceOnlinePX)
api.trace("TripleNetFactor", (1 - out.TripleNetFactor))*/
    return libs.FressnapfLib.RoundingUtils.roundNumber(out.CommonLib.CommonUtils.applyExchangeRate(purPriceOnline,
            purPriceOnlineCurrency,
            out.TargetCurrency,
            out.PricingDate),5)?:null
}else{
    def setTrayCostData = api.global.batch[sku].SetTrayCost
    def cost = (setTrayCostData)?.attribute1
    !cost && out.CalculationChannel == "Online" ?api.addWarning("Purchase Price Online not available for "+out.Article):""
    cost = (cost)?:0
    return libs.FressnapfLib.RoundingUtils.roundNumber(out.CommonLib.CommonUtils.applyExchangeRate(cost,
            out.PurchasePriceOnlineCurrency,
            out.TargetCurrency,
            out.PricingDate),5)?:null
}