if(api.global.calculationEndDate != 0 && api.local.contextValues){
    return api.parseDate("yyyy-MM-dd",out.ArticleContext?.PricingDate)
}

return getPricingDate(out.LPGId)?:api.targetDate()

Date getPricingDate(Long lpgId) {
    def targetDate = api.find("PG", Filter.equal("id", lpgId))?.find()?.targetDate
    return libs.SharedLib.CacheUtils.getOrSet("PRICING_DATE", {
        return out.CommonLib.CommonUtils.parseDate(targetDate)
    })
}