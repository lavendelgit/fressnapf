api.trace(!api.isFullListCalculation())
if(api.currentItem()?.manualResultPrice && (!api.isFullListCalculation())){
    api.local.priceBuildupMatrix = [:]
    api.local.manualOverride = true
    return false
}
return true