BigDecimal listPrice = out.ListPrice ?: 0
BigDecimal currentSellingPrice = out["SalesPrice" +out.CalculationChannel] ?: 0
return out.CommonLib.UIUtils.getArrowedCell(listPrice - currentSellingPrice)
