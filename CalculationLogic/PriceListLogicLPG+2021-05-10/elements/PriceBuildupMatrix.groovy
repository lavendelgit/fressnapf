import groovy.transform.Field
import net.pricefx.server.dto.calculation.ResultMatrix

int iteration = out.Iteration as int
Map currentMatrix = [:]
Map calculationResult = [:]

if ((api.local.priceBuildupMatrix?.marketingCamp || api.local.contextValues)) {
    api.local.gaurdrailsvalidation = [:]
    if (api.local.manualOverride) {
        api.local.gaurdrailsvalidation = api.local.priceBuildupMatrix
    }
    calculationResult = api.getItemCompleteCalculationResults(out.CurrentItem?.typedId)?.CalculationMap?.result
    oldPriceBuildupData = calculationResult ?: [:]
    if (api.local.manualOverride) {
        oldPriceBuildupData << api.local.gaurdrailsvalidation
    }
    priceChangeThresholdFre = api.local.priceBuildupMatrix.PriceChangeThresholdConditionApplied
    api.local.priceBuildupMatrix << oldPriceBuildupData
    api.local.priceBuildupMatrix.PriceChangeThresholdConditionApplied = priceChangeThresholdFre
}
@Field Map OPERATORS = [
        EQUALS : '=',
        IN     : '∈',
        MIN    : '⋘',
        MAX    : '⋙',
        AVG    : 'μ',
        SUM    : '∑',
        SECTION: ''
]
String stringRoundScheme = ""
String productSKU = out.Product?.sku
String leadArticle = out.Product?.attribute22

def roundingUtils = libs.FressnapfLib.RoundingUtils
List competitors = api.local.priceBuildupMatrix.CompetitorList?.findAll { Map competitorRow ->
    competitorRow.prio == api.local.priceBuildupMatrix.CompetitorPriority
}?.collect { Map competitorRow ->
    "${competitorRow.competitorSku} - ${competitorRow.competitor} ( ${roundingUtils.roundNumber(competitorRow.price, 2)}  ${competitorRow?.currency} )"
}
Map refRow = api.local.priceBuildupMatrix.CompetitorList?.findAll { it.prio != null }?.getAt(0)
ResultMatrix resultMatrix = api.newMatrix("Name", " ", "Value", "Source", "Formula")
resultMatrix.setEnableClientFilter(false)
resultMatrix.setDisableSorting(true)
resultMatrix.addRow(newRow(resultMatrix, 'PRICE LIST SUMMARY', 'SECTION', "", "", ""))
resultMatrix.addRow(newRow(resultMatrix, 'Article', 'EQUALS', out.Article + "-" + out.MaterialDescription, resultMatrix.linkCell("Material Master", "productsPage", out.Article), ""))
resultMatrix.addRow(newRow(resultMatrix, 'Article Status', 'EQUALS', out.ArticleStatus ? out.ArticleStatus + "-" + out.CommonLib.DataUtils.getPriceParameterMaster("ArticleStatus")[out.ArticleStatus] : "", "Product Master", ""))
resultMatrix.addRow(newRow(resultMatrix, 'Basket', 'EQUALS', out.Basket, "Basket Item Class Mapping PX", ""))
resultMatrix.addRow(newRow(resultMatrix, 'Item Class', 'EQUALS', out.ItemClass, "Basket Item Class Mapping PX", ""))
resultMatrix.addRow(newRow(resultMatrix, 'Sort Of Article', 'EQUALS', out.MaterialType, "Product Master", ""))
resultMatrix.addRow(newRow(resultMatrix, 'Channel', 'EQUALS', out.ChannelPerspTool, "Article Channel Mapping PX", ""))
resultMatrix.addRow(newRow(resultMatrix, 'Country', 'EQUALS', out.Country, "LPG Input Configuration", ""))
resultMatrix.addRow(newRow(resultMatrix, 'Currency', 'EQUALS', out.Currency, "LPG Input Configuration", ""))
resultMatrix.addRow(newRow(resultMatrix, 'Price Family', 'EQUALS', out.PriceFamilyCode, "Price Families PX", ""))
resultMatrix.addRow(newRow(resultMatrix, 'Lead Article', 'EQUALS', out.LeadArticle, "Price Families PX", ""))
resultMatrix.addRow(newRow(resultMatrix, 'CGR No', 'EQUALS', out.CGRNo + "-" + out.CommodityGroupDescription, "Product Master", ""))
resultMatrix.addRow(newRow(resultMatrix, 'Category', 'EQUALS', out.Category, "Product Hierarchy PP", ""))
resultMatrix.addRow(newRow(resultMatrix, 'Sub Category', 'EQUALS', out.SubCategory, "Product Hierarchy PP", ""))
resultMatrix.addRow(newRow(resultMatrix, 'Supply Mode', 'EQUALS', out.SupplyMode, "Article Country Attributes", ""))
resultMatrix.addRow(newRow(resultMatrix, 'Brand', 'EQUALS', out.Brand + "-" + out.BrandLabel, "Product Master", ""))
resultMatrix.addRow(newRow(resultMatrix, 'Sub Brand', 'EQUALS', out.SubBrand ? out.SubBrand + "-" + out.SubBrandDescription : "", "Product Master", ""))
resultMatrix.addRow(newRow(resultMatrix, 'Brand Type', 'EQUALS', out.BrandType ? out.BrandType + "-" + out.CommonLib.DataUtils.getPriceParameterMaster("BrandType")[out.BrandType] : "", "Product Master", ""))
resultMatrix.addRow(newRow(resultMatrix, 'VAT', 'EQUALS', out.VAT, "Product Master", ""))
api.local.priceBuildupMatrix?.ExchangeRate?.each { conversion ->
    resultMatrix.addRow(newRow(resultMatrix, "Exchange Rate (" + conversion?.key + " )", 'EQUALS', conversion?.value ? roundingUtils.roundNumber(conversion?.value, 5) : "", "Exchange Rates PP", ""))
}
if (api.local.priceBuildupMatrix?.OnlinePrice) {
    onlineValue = out.PurchasePriceOnline + " " + out.Currency + "(" + api.local.priceBuildupMatrix?.OnlinePrice + " " + api.local.priceBuildupMatrix?.OnlineCurrency + ")"
} else {
    onlineValue = out.PurchasePriceOnline + " " + out.Currency
}
if (api.local.priceBuildupMatrix?.OfflinePrice) {
    offlineValue = out.PurchasePriceOffline + " " + out.Currency + "(" + api.local.priceBuildupMatrix?.OfflinePrice + " " + api.local.priceBuildupMatrix?.OfflineCurrency + ")"
} else {
    offlineValue = out.PurchasePriceOffline + " " + out.Currency
}
exchangeCCOnline = api.local.appliedCurrencyExchange[out.PurchasePriceOnlineCurrency + " - " + out.TargetCurrency]
exchangeCCPPOnline = exchangeCCOnline ? out.PurchasePriceOnline * roundingUtils.roundNumber(exchangeCCOnline, 5) : null
exchangeCCOffline = api.local.appliedCurrencyExchange[out.PurchasePriceOfflineCurrency + " - " + out.TargetCurrency]
exchangeCCPPOffline = exchangeCCOffline ? out.PurchasePriceOffline * roundingUtils.roundNumber(exchangeCCOffline, 5) : null
out.PurchasePriceOnline && out.CalculationChannel == "Online" ? resultMatrix.addRow(newRow(resultMatrix, 'Purchase Price Online', 'EQUALS', onlineValue, out.MaterialType == "HAWA" ? "HAWA Purchase Prices PX" : "Set Tray Purchase Prices PX", "")) : ""
out.PurchasePriceOffline && out.CalculationChannel == "Offline" ? resultMatrix.addRow(newRow(resultMatrix, 'Purchase Price Offline', 'EQUALS', offlineValue, out.MaterialType == "HAWA" ? "HAWA Purchase Prices PX" : "Set Tray Purchase Prices PX", "")) : ""
out.PurchasePriceRetail && out.CalculationChannel == "Offline" ? resultMatrix.addRow(newRow(resultMatrix, 'Purchase Price Retail', 'EQUALS', out.PurchasePriceRetail + " " + out.TargetCurrency, out.MaterialType == "HAWA" ? "HAWA Purchase Prices PX" : "Set Tray Purchase Prices PX", "")) : ""
if (out.PreviousPurchasePriceOnline && out.CalculationChannel == "Online") {
    resultMatrix.addRow(newRow(resultMatrix, 'Previous Purchase Price Online', 'EQUALS', out.PreviousPurchasePriceOnline, out.MaterialType == "HAWA" ? "HAWA Purchase Prices PX" : "Set Tray Purchase Prices PX", ""))
    resultMatrix.addRow(newRow(resultMatrix, 'Δ Purchase Price Online %', 'EQUALS', out.PurchasePriceOnlineDelta ? roundingUtils.roundNumber(out.PurchasePriceOnlineDelta * 100, 2) + "%" : "", "((Current Purchase Price Online - Previous Purchase Price Online) /  Previous Purchase Price Online )", "((" + out.PurchasePriceOnline + " -" + out.PreviousPurchasePriceOnline + ") / " + out.PreviousPurchasePriceOnline + ")"))
}
if (out.PreviousPurchasePriceOffline && out.CalculationChannel == "Offline") {
    resultMatrix.addRow(newRow(resultMatrix, 'Previous Purchase Price Offline', 'EQUALS', out.PreviousPurchasePriceOffline, out.MaterialType == "HAWA" ? "HAWA Purchase Prices PX" : "Set Tray Purchase Prices PX", ""))
    resultMatrix.addRow(newRow(resultMatrix, 'Δ Purchase Price Offline %', 'EQUALS', out.PurchasePriceOfflineDelta ? roundingUtils.roundNumber(out.PurchasePriceOfflineDelta * 100, 2) + "%" : "", "((Current Purchase Price Offline - Previous Purchase Price Offline) /  Previous Purchase Price Offline)", "((" + out.PurchasePriceOffline + " -" + out.PreviousPurchasePriceOffline + ") / " + out.PreviousPurchasePriceOffline + ")"))
}
if (out.PreviousPurchasePriceRetail && out.CalculationChannel == "Offline") {
    resultMatrix.addRow(newRow(resultMatrix, 'Previous Purchase Price Retail', 'EQUALS', out.PreviousPurchasePriceRetail, out.MaterialType == "HAWA" ? "HAWA Purchase Prices PX" : "Set Tray Purchase Prices PX", ""))
    resultMatrix.addRow(newRow(resultMatrix, 'Δ Purchase Price Retail %', 'EQUALS', out.PurchasePriceRetailDelta ? roundingUtils.roundNumber(out.PurchasePriceRetailDelta * 100, 2) + "%" : "", "((Current Purchase Price Retail - Previous Purchase Price Retail) /  Previous Purchase Price Retail )", "((" + out.PurchasePriceRetail + " -" + out.PreviousPurchasePriceRetail + ") / " + out.PreviousPurchasePriceRetail + ")"))
}
exchangeCCOnline = api.local.appliedCurrencyExchange[out.SalesPriceOnlineCurrency + " - " + out.TargetCurrency]
exchangeCCSPOnline = exchangeCCOnline ? out.SalesPriceOnline * roundingUtils.roundNumber(exchangeCCOnline, 5) : null
exchangeCCOffline = api.local.appliedCurrencyExchange[out.SalesPriceOfflineCurrency + " - " + out.TargetCurrency]
exchangeCCSPOffline = exchangeCCOffline ? out.SalesPriceOffline * roundingUtils.roundNumber(exchangeCCOffline, 5) : null
out.SalesPriceOnline && out.CalculationChannel == "Online" ? resultMatrix.addRow(newRow(resultMatrix, 'Sales Price Online', 'EQUALS', exchangeCCSPOnline ? exchangeCCSPOnline + " " + out.TargetCurrency + "(" + (out.SalesPriceOnline + " " + out.SalesPriceOnlineCurrency) + ")" : out.SalesPriceOnline + " " + out.TargetCurrency, "Selling Prices PX", "")) : ""
out.SalesPriceOffline && out.CalculationChannel == "Offline" ? resultMatrix.addRow(newRow(resultMatrix, 'Sales Price Offline', 'EQUALS', exchangeCCSPOffline ? exchangeCCSPOffline + " " + out.TargetCurrency + "(" + (out.SalesPriceOffline + " " + out.SalesPriceOfflineCurrency) + ")" : out.SalesPriceOffline + " " + out.TargetCurrency, "Selling Prices  PX", "")) : ""
resultMatrix.addRow(newRow(resultMatrix, 'Supplier RRP', 'EQUALS', (out.SupplierRRP + " " + out.TargetCurrency), "Supplier RRP PX", ""))
if (out.ListPrice && out.ListPrice != 0) {
    if (out.PricingApproach1Price) {
        resultMatrix.addRow(newRow(resultMatrix, 'PRICING APPROACH 1' + " : " + out.PricingApproach1 ?: "", 'SECTION', "", "", ""))
        if (out.PricingApproach1 != "Competitor-based Pricing" && out.PricingApproach1Price) {
            costPlusResultMatrixBuild(resultMatrix, "pricingApproach1", out.PricingApproach1)
            resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))
        } else if (out.PricingApproach1 == "Competitor-based Pricing" && out.PricingApproach1Price) {
            competitorResultMatrixBuilder(resultMatrix, "pricingApproach1", competitors, refRow)
            resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))
        }
    } else {
        pricingApporach = out.PricingApproach1 ?: ""
        resultMatrix.addRow(newRow(resultMatrix, 'PRICING APPROACH 1' + " : " + pricingApporach, 'SECTION', "", "", ""))
        resultMatrix.addRow(newRow(resultMatrix, out.PricingApproach1 ?: "Pricing Approach 1", "EQUALS", "Price Not Available", "", ""))
        resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))
    }
    if (out.PricingApproach2Price) {
        resultMatrix.addRow(newRow(resultMatrix, 'PRICING APPROACH 2' + " : " + out.PricingApproach2 ?: "", 'SECTION', "", "", ""))
        if (out.PricingApproach2 != "Competitor-based Pricing" && out.PricingApproach2Price) {
            costPlusResultMatrixBuild(resultMatrix, "pricingApproach2", out.PricingApproach2)
            resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))
        } else if (out.PricingApproach2 == "Competitor-based Pricing" && out.PricingApproach2Price) {
            competitorResultMatrixBuilder(resultMatrix, "pricingApproach2", competitors, refRow)
            resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))
        }
    } else {
        pricingApporach = out.PricingApproach2 ?: ""
        resultMatrix.addRow(newRow(resultMatrix, 'PRICING APPROACH 2' + " : " + pricingApporach, 'SECTION', "", "", ""))
        resultMatrix.addRow(newRow(resultMatrix, out.PricingApproach2 ?: "Pricing Approach2 ", "EQUALS", "Price Not Available", "", ""))
        resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))
    }

    if (out.MaterialType != "HAWA" && api.local.priceBuildupMatrix.setTrayApplied) {
        resultMatrix.addRow(newRow(resultMatrix, 'SET TRAY PRICING', 'SECTION', "", "", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'BOM Components', 'IN', api.local.priceBuildupMatrix.setTrayPricebomArticleComp, "BOM Data | Selling Price PX", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Total Components', 'EQUALS', out.NumberOfComponents, "BOM Data", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Set Tray Price', 'EQUALS', api.local.priceBuildupMatrix.setTrayPriceSetTrayPrice, "", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Set Tray Discount', 'EQUALS', api.local.priceBuildupMatrix.setTrayPriceDiscount, "Set and Tray Discount PP", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Discounted Set Tray Price', 'EQUALS', api.local.priceBuildupMatrix.setTrayPriceDiscounted, "Discounted Set and Tray Price", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Price validation', 'EQUALS', api.local.priceBuildupMatrix.setTrayPriceDiscounted ? api.local.priceBuildupMatrix.setTrayPriceDiscountedPrice : "", api.local.priceBuildupMatrix.setTrayPriceDiscounted ? api.local.priceBuildupMatrix.setTrayCompareString : "Set Tray Price By Component not available", api.local.priceBuildupMatrix.setTrayPriceDiscounted ? api.local.priceBuildupMatrix.setTrayCompareStringValue : "Set Tray Price By Component not available"))
        resultMatrix.addRow(newRow(resultMatrix, 'Suggested Set Tray Price', 'EQUALS', api.local.priceBuildupMatrix.setTrayPriceDiscountedPrice, api.local.priceBuildupMatrix.setTrayPricePricingApporach, ""))
        resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))
        stringRoundScheme = "-" + api.local.priceBuildupMatrix.setTrayPricePricingApporach
    }

    if (api.local.priceBuildupMatrix.PRGFamily) {
        resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'PRG Pricing', 'SECTION', "", "", ""))
        //resultMatrix.addRow(newRow(resultMatrix, 'Base Family Context', 'EQUALS', api.local.PRGBasePriceFamily.context, "LPG Configuration", ""))
        baseContent = api.local.priceBuildupMatrix.PRGBasePriceFamilyNetContent ?: ""
        baseContentUnit = api.local.priceBuildupMatrix.PRGBasePriceFamilyContentUnit ?: ""
        basePrice = api.local.priceBuildupMatrix.PRGBasePriceFamilyPrice ?: ""
        resultMatrix.addRow(newRow(resultMatrix, 'Base Price Family', 'EQUALS', api.local.priceBuildupMatrix.PRGBasePriceFamily + "(Lead Article : " + api.local.priceBuildupMatrix.PRGBasePriceFamilyLeadArticle + ")" + "(Price :" + basePrice + ", Content:" + baseContent + " " + baseContentUnit + ")", "Product Master & Selling PX", ""))
        api.local.priceBuildupMatrix.appliedConversionFactor?.each { it ->
            resultMatrix.addRow(newRow(resultMatrix, 'Conversion', 'EQUALS', it?.key + "(" + it?.value + ")", "UoM PP", ""))
        }
        relatedContent = api.local.priceBuildupMatrix.PRGRelatedPriceFamilyNetContent ?: ""
        relatedContentUnit = api.local.priceBuildupMatrix.PRGRelatedPriceFamilyContentUnit ?: ""
        resultMatrix.addRow(newRow(resultMatrix, 'Related Price Family', 'EQUALS', api.local.priceBuildupMatrix.PRGRelatedPriceFamily ? api.local.priceBuildupMatrix.PRGRelatedPriceFamily + " ( Content:" + relatedContent + " " + relatedContentUnit + ")" : "", "Product Master", ""))
        api.local.priceBuildupMatrix.PRGPrice ? resultMatrix.addRow(newRow(resultMatrix, 'Factor', 'EQUALS', api.local.priceBuildupMatrix.PRGPricingAppliedMethod ?: "", "PRG PP", "")) : ""
        api.local.priceBuildupMatrix.PRGPrice ? resultMatrix.addRow(newRow(resultMatrix, 'Price', 'EQUALS', roundingUtils.roundNumber(api.local.priceBuildupMatrix.PRGPrice, 2), api.local.priceBuildupMatrix.PRGPricingAppliedStringFormula, api.local.priceBuildupMatrix.PRGPricingAppliedFormula)) : ""
        api.local.priceBuildupMatrix.PRGPrice ? resultMatrix.addRow(newRow(resultMatrix, 'Price Validation', 'EQUALS', api.local.priceBuildupMatrix.PRGPricingApplied, api.local.priceBuildupMatrix.PRGPricingAppliedValidationStringFur, api.local.priceBuildupMatrix.PRGPricingAppliedValidationFur)) : resultMatrix.addRow(newRow(resultMatrix, 'Price Validation', 'EQUALS', api.local.priceBuildupMatrix.PRGPricingApplied, api.local.priceBuildupMatrix.PRGPricingFailReason, ""))
        resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))
        api.local.priceBuildupMatrix.PRGPricingApplied ? stringRoundScheme = "PRG Pricing" : ""
    } else {
        resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'PRG Pricing', 'SECTION', "", "", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Price Validation', 'EQUALS', api.local.priceBuildupMatrix.PRGPricingApplied, "", ""))
        resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))
    }
   /* resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))
    resultMatrix.addRow(newRow(resultMatrix, 'SUGGESTED PRICE', 'SECTION', "", "", ""))
    api.local.priceBuildupMatrix.BeforeRoundingScheme ? resultMatrix.addRow(newRow(resultMatrix, 'Suggested Price (Un Rounded)', 'EQUALS', roundingUtils.roundNumber(api.local.priceBuildupMatrix.BeforeRoundingScheme, 5) + " " + out.Currency, "", "")) : ""
*/
    out.WholeSaleMargin && out.CalculationChannel == "Offline" ? resultMatrix.addRow(newRow(resultMatrix, 'Whole Sale Margin %', 'EQUALS', out.WholeSaleMargin ? roundingUtils.roundNumber(out.WholeSaleMargin * 100, 2) + "%" : "", "((Purchase Price Retail - Purchase Price Offline) /  Purchase Price Retail)", "((" + out.PurchasePriceRetail + " -" + out.PurchasePriceOffline + ") / " + out.PurchasePriceRetail + ")")) : ""
    exchangeCCSPOnline = exchangeCCSPOnline ? roundingUtils.roundNumber(exchangeCCSPOnline, 5) : out.SalesPriceOnline ?: 0
    exchangeCCPPOnline = exchangeCCPPOnline ? roundingUtils.roundNumber(exchangeCCPPOnline, 5) : out.PurchasePriceOffline ?: 0
    exchangeCCSPOffline = exchangeCCSPOffline ? roundingUtils.roundNumber(exchangeCCSPOffline, 5) : out.SalesPriceOffline ?: 0
    exchangeCCPPOffline = exchangeCCPPOffline ? roundingUtils.roundNumber(exchangeCCPPOffline, 5) : out.PurchasePriceOffline ?: 0
    formula = "((" + out.SalesPriceOnlineEXVAT + " - " + exchangeCCPPOnline + ") / " + out.SalesPriceOnlineEXVAT + ")"
    stringFormula = "((Curr.SP Online Ex Vat - Purchase Price Online) / Curr.SP Online Ex Vat )"
    out.SalesPriceMarginPercentageOnline && out.CalculationChannel == "Online" ? resultMatrix.addRow(newRow(resultMatrix, 'Curr. SP Margin % Online', 'EQUALS', out.SalesPriceMarginPercentageOnline ? roundingUtils.roundNumber(out.SalesPriceMarginPercentageOnline * 100, 2) + "%" : "", out.SalesPriceMarginPercentageOnline ? stringFormula : "", out.SalesPriceMarginPercentageOnline ? formula : "")) : ""
    formula = "((" + out.SalesPriceOfflineEXVAT + " - " + exchangeCCPPOffline + ") / " + out.SalesPriceOfflineEXVAT + ")"
    stringFormula = "((Curr.SP Offline Ex Vat  - Purchase Price Offline) / Curr.SP Offline Ex Vat )"
    out.SalesPriceMarginPercentageOffline && out.CalculationChannel == "Offline" ? resultMatrix.addRow(newRow(resultMatrix, 'Curr. SP Margin % Offline', 'EQUALS', out.SalesPriceMarginPercentageOffline ? roundingUtils.roundNumber(out.SalesPriceMarginPercentageOffline * 100, 2) + "%" : "", out.SalesPriceMarginPercentageOnline ? stringFormula : "", out.SalesPriceMarginPercentageOnline ? formula : "")) : ""
    formula = "((" + roundingUtils.roundNumber(out.PriceExVAT, 5) + " - " + exchangeCCPPOnline + ") / " + roundingUtils.roundNumber(out.PriceExVAT, 5) + ")"
    stringFormula = "((New RP Ex VAT - Purchase Price Online) / New RP Ex VAT)"
    out.ListPriceMarginPercentageOnline && out.CalculationChannel == "Online" ? resultMatrix.addRow(newRow(resultMatrix, 'New RP Margin % Online', 'EQUALS', out.ListPriceMarginPercentageOnline ? roundingUtils.roundNumber(out.ListPriceMarginPercentageOnline * 100, 2) + "%" : "", out.SalesPriceMarginPercentageOnline ? stringFormula : "", out.SalesPriceMarginPercentageOnline ? formula : "")) : ""
    formula = "((" + roundingUtils.roundNumber(out.PriceExVAT, 5) + " - " + exchangeCCPPOffline + ") / " + roundingUtils.roundNumber(out.PriceExVAT, 5) + ")"
    stringFormula = "((New RP Ex VAT - Purchase Price Offline) / New RP Ex VAT)"
    out.ListPriceMarginPercentageOffline && out.CalculationChannel == "Offline" ? resultMatrix.addRow(newRow(resultMatrix, 'New RP Margin % Offline', 'EQUALS', out.ListPriceMarginPercentageOffline ? roundingUtils.roundNumber(out.ListPriceMarginPercentageOffline * 100, 2) + "%" : "", out.SalesPriceMarginPercentageOffline ? stringFormula : "", out.SalesPriceMarginPercentageOffline ? formula : "")) : ""
    out.CurrentSPRetailMargin && out.CalculationChannel == "Offline" ? resultMatrix.addRow(newRow(resultMatrix, 'Curr. SP Margin % Retail', 'EQUALS', out.CurrentSPRetailMargin ? roundingUtils.roundNumber(out.CurrentSPRetailMargin * 100, 2) + "%" : "", "((Curr.SP Offline Ex Vat - Purchase Price Retail) /  Curr.SP Offline Ex Vat)", "((" + out["SalesPrice" + out.CalculationChannel + "EXVAT"] + " -" + out.PurchasePriceRetail + ") / " + out["SalesPrice" + out.CalculationChannel + "EXVAT"] + ")")) : ""
    out.RetailMargin && out.CalculationChannel == "Offline" ? resultMatrix.addRow(newRow(resultMatrix, 'New RP Margin % Retail', 'EQUALS', out.RetailMargin ? roundingUtils.roundNumber(out.RetailMargin * 100, 2) + "%" : "", "((New RP Ex VAT - Purchase Price Retail) /  New RP Ex VAT )", "((" + out.PriceExVAT + " -" + out.PurchasePriceRetail + ") / " + out.PriceExVAT + ")")) : ""


    if (api.local.priceChangeThreshold) {
        resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'FOLLOW UP PRICING', 'SECTION', "", "", ""))
        if (api.local.PriceChangeThresholdCondition || api.local.priceChangeThreshold) {
            api.local.priceBuildupMatrix.PriceChangeThresholdPeriod ? resultMatrix.addRow(newRow(resultMatrix, "Price Change Frequency", "EQUALS", api.local.priceBuildupMatrix.PriceChangeThresholdPeriod, "Price Change Thresholds PP", "")) : ""
            api.local.priceBuildupMatrix.PriceChangeThresholdPeriod ? resultMatrix.addRow(newRow(resultMatrix, "Price Change Frequency Applied", "EQUALS", api.local.priceBuildupMatrix.PriceChangeThresholdConditionApplied ?: "false", "", "")) : ""
            api.local.priceBuildupMatrix.PriceChangeThresholdConditionApplied ? resultMatrix.addRow(newRow(resultMatrix, "Price Change Frequency Condition", "EQUALS", api.local.priceBuildupMatrix.PriceChangeThresholdCondition, "", "")) : ""
            api.local.priceBuildupMatrix.PriceChangeThresholdConditionApplied ? resultMatrix.addRow(newRow(resultMatrix, "Pricing Date", "EQUALS", api.local.priceBuildupMatrix.PriceChangeThresholdApprovedDate, "", "")) : ""
            api.local.priceBuildupMatrix.PriceChangeThresholdConditionApplied ? resultMatrix.addRow(newRow(resultMatrix, "Next Pricing Date", "EQUALS", api.local.priceBuildupMatrix.PriceChangeThresholdBlockedUntill, "", "")) : ""
            api.local.priceBuildupMatrix.PriceChangeThresholdConditionApplied ? stringRoundScheme = "Price Change Frequency Below" : ""
            api.local.priceBuildupMatrix.followupMINAAppliedCondition ? resultMatrix.addRow(newRow(resultMatrix, 'Condition :' + api.local.priceBuildupMatrix.followupMINAAppliedCondition, 'EQUALS', api.local.priceBuildupMatrix.followupMINAFactor, "Price Change Thresholds PP", "")) : ""
            api.local.priceBuildupMatrix.followupMINAAppliedCondition ? resultMatrix.addRow(newRow(resultMatrix, 'Condition Validation', 'EQUALS', api.local.priceBuildupMatrix.followupMINAApplied, api.local.priceBuildupMatrix.followupMINAAppliedStringFormula, api.local.priceBuildupMatrix.followupMINAAppliedFormula)) : ""
            api.local.priceBuildupMatrix.followupMINAApplied ? resultMatrix.addRow(newRow(resultMatrix, 'Final Price Applied(' + api.local.priceBuildupMatrix.followupMINAAppliedCondition + ")", 'EQUALS', api.local.priceBuildupMatrix.followupMINAAppliedPrice, "Selling Prices PX", "")) : ""
            api.local.priceBuildupMatrix.followupMAXCPAppliedCondition ? resultMatrix.addRow(newRow(resultMatrix, 'Condition :' + api.local.priceBuildupMatrix.followupMAXCPAppliedCondition, 'EQUALS', api.local.priceBuildupMatrix.followupMAXCPFactor ? api.local.priceBuildupMatrix.followupMAXCPFactor * 100 + " %" : "", "Price Change Thresholds PP", "")) : ""
            api.local.priceBuildupMatrix.followupMAXCPAppliedCondition ? resultMatrix.addRow(newRow(resultMatrix, 'Condition Validation', 'EQUALS', api.local.priceBuildupMatrix.followupMAXCPApplied, api.local.priceBuildupMatrix.followupMAXCPAppliedStringFormula, api.local.priceBuildupMatrix.followupMAXCPAppliedFormula)) : ""
            api.local.priceBuildupMatrix.followupMAXCPApplied ? resultMatrix.addRow(newRow(resultMatrix, 'Final Price Applied(' + api.local.priceBuildupMatrix.followupMAXCPAppliedCondition + ")", 'EQUALS', api.local.priceBuildupMatrix.followupMAXCPAppliedPrice, api.local.priceBuildupMatrix.followupMAXCPFinalPriceSF, api.local.priceBuildupMatrix.followupMAXCPFinalPriceF)) : ""
            api.local.priceBuildupMatrix.followupAUTOPAppliedCondition ? resultMatrix.addRow(newRow(resultMatrix, 'Condition :' + api.local.priceBuildupMatrix.followupAUTOPAppliedCondition, 'EQUALS', api.local.priceBuildupMatrix.followupAUTOPAppliedFactor ? api.local.priceBuildupMatrix.followupAUTOPAppliedFactor * 100 + " %" : "", "Price Change Thresholds PP", "")) : ""
            api.local.priceBuildupMatrix.followupAUTOPAppliedCondition ? resultMatrix.addRow(newRow(resultMatrix, 'Condition Validation', 'EQUALS', api.local.priceBuildupMatrix.followupAUTOPApplied, api.local.priceBuildupMatrix.followupAUTOPAppliedStringFormula, api.local.priceBuildupMatrix.followupAUTOPAppliedFormula)) : ""
            api.local.priceBuildupMatrix.followupautoApprovel ? resultMatrix.addRow(newRow(resultMatrix, 'Auto Approval(' + api.local.priceBuildupMatrix.followupAUTOPAppliedCondition + ")", 'EQUALS', api.local.priceBuildupMatrix.followupautoApprovel, "Auto Approved", "")) : ""
            api.local.priceBuildupMatrix.followupApplied ? stringRoundScheme = "Follow Up Pricing Applied" : ""

        }
    } else {
        resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'FOLLOW UP PRICING', 'SECTION', "", "", ""))
        resultMatrix.addRow(newRow(resultMatrix, "Condition Validation", "EQUALS", "false", "Price Change Thresholds PP", "No Price Change Threshold"))
    }
    if (api.local.priceBuildupMatrix.priceGuardRail == "Present") {
        resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'PRICING GAURDRAILS', 'SECTION', "", "", ""))
        if (api.local.priceBuildupMatrix.priceGuardRailApplied) {
            resultMatrix.addRow(newRow(resultMatrix, 'Condition', 'EQUALS', api.local.priceBuildupMatrix.priceGuardRailAppliedCondition, "Pricing Guardrails PX", ""))
            resultMatrix.addRow(newRow(resultMatrix, 'Applied Margin', 'EQUALS', api.local.priceBuildupMatrix.priceGuardRailAppliedMargin, "Pricing Guardrails PX", ""))
            resultMatrix.addRow(newRow(resultMatrix, 'Condition Validation', 'EQUALS', api.local.priceBuildupMatrix.priceGuardRailApplied, api.local.priceBuildupMatrix.priceGuardRailConditionValidStringF, api.local.priceBuildupMatrix.priceGuardRailValidation))
            resultMatrix.addRow(newRow(resultMatrix, 'Price', 'EQUALS', api.local.priceBuildupMatrix.priceGuardRailPrice, api.local.priceBuildupMatrix.priceGuardRailStringFormula, api.local.priceBuildupMatrix.priceGuardRailFormula))
            stringRoundScheme = " Guard Rails Pricing"
        } else {
            resultMatrix.addRow(newRow(resultMatrix, 'Condition', 'EQUALS', api.local.priceBuildupMatrix.priceGuardRailAppliedCondition, "Pricing Guardrails PX", ""))
            resultMatrix.addRow(newRow(resultMatrix, 'Applied Margin', 'EQUALS', api.local.priceBuildupMatrix.priceGuardRailAppliedMargin, "Pricing Guardrails PX", ""))
            resultMatrix.addRow(newRow(resultMatrix, "Condition Validation", "EQUALS", api.local.priceBuildupMatrix.priceGuardRailApplied, "(Sales Margin < Guardrail MIN Margin)", api.local.priceBuildupMatrix.priceGuardRailValidation))
        }
        resultMatrix.addRow(newRow(resultMatrix, 'Condition', 'EQUALS', api.local.priceBuildupMatrix.legalMinMarginCondition, "Pricing Guardrails PX", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Applied Margin', 'EQUALS', api.local.priceBuildupMatrix.legalMargin, "Pricing Guardrails PX", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Condition Validation', 'EQUALS', api.local.priceBuildupMatrix.legalMinMarginVoilated ? " Required Approval from Team Leader Category Manager" : api.local.priceBuildupMatrix.legalMinMarginVoilated, api.local.priceBuildupMatrix.legalMinMarginValidationStringF, api.local.priceBuildupMatrix.legalMinMarginValidation))
    }
    resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))
    api.local.priceBuildupMatrix?.marketingCamp ? resultMatrix.addRow(newRow(resultMatrix, 'MARKETING CAMPAIGNS', 'SECTION', "", "", "")) : ""
    api.local.priceBuildupMatrix?.marketingCamp ? resultMatrix.addRow(newRow(resultMatrix, 'Marketing Campaigns Event', 'EQUALS', api.local.priceBuildupMatrix.appliedCampEventID + "(" + api.local.priceBuildupMatrix.appliedCampType + ")", "Marketing Campaigns PX", "")) : ""
    api.local.priceBuildupMatrix?.marketingCamp ? resultMatrix.addRow(newRow(resultMatrix, 'From', 'EQUALS', api.local.priceBuildupMatrix.appliedCampFrom, "Marketing Campaigns PX", "")) : ""
    api.local.priceBuildupMatrix?.marketingCamp ? resultMatrix.addRow(newRow(resultMatrix, 'To', 'EQUALS', api.local.priceBuildupMatrix.appliedCampTo, "Marketing Campaigns PX", "")) : ""
    api.local.priceBuildupMatrix?.marketingCamp ? resultMatrix.addRow(newRow(resultMatrix, 'Price', 'EQUALS', out.ListPrice + " " + out.Currency, "Selling Prices PX", "")) : ""
    api.local.priceBuildupMatrix?.marketingCamp ? stringRoundScheme = " In Marketing Campaigns" : ""
    api.local.priceBuildupMatrix.SuggestedPrice = out.ListPrice
    api.local.priceBuildupMatrix?.marketingCamp ? api.local.priceBuildupMatrix.PricingReason = " Price blocked - Marketing Campaign" : api.local.priceBuildupMatrix.PricingReason
    // api.local.priceBuildupMatrix.PricingReason = api.local.priceBuildupMatrix.marketingCamp? " In Marketing Campaigns(Curr. Selling Price) " : api.local.priceBuildupMatrix.PricingReason
    resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))
    if (out.ImpactForecast) {
        resultMatrix.addRow(newRow(resultMatrix, 'HISTORICAL SUMMARY(TRANSACTION BASED)', 'SECTION', "", "", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Impact Forecast', 'EQUALS', out.ImpactForecast, api.local.priceBuildupMatrix.impactForecastStringFormula, api.local.priceBuildupMatrix.impactForecastActualFormula))
    }
    resultMatrix.addRow(newRow(resultMatrix, 'CALCULATED LIST PRICE', 'SECTION', "", "", ""))
    resultMatrix.addRow(newRow(resultMatrix, 'Calculated List Price (Un Rounded Suggested Price)', 'EQUALS', roundingUtils.roundNumber(api.local.pricebeforeDistance, 2) ?: roundingUtils.roundNumber(out.CalculatedListPrice, 2) + " " + out.Currency, "", ""))
    resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))

if(out.MaterialType!="HAWA"){
    addSetTrayCheck(resultMatrix)
    resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))
}else{
    resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))
    resultMatrix.addRow(newRow(resultMatrix, 'DISTANCE TO KEY CHANNEL VALIDATION', 'SECTION', "", "", ""))
          if (api.local.priceBuildupMatrix.DistanceToKeyKeyChannel || api.local.priceBuildupMatrix.DistanceToKeyKeyChannel == 0) {
        resultMatrix.addRow(newRow(resultMatrix, 'SalesPrice - Distance to key Channel(%)', 'SECTION', "", "", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyChannelPercentage, "Distance To Key Channel PP", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Key Channel', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyKeyChannel, "Basket Key Channel PP", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel Applied', 'EQUALS', api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedPercentage, (!api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedPercentage) ? (api.local.priceBuildupMatrix.SalesDistanceToKeyChannelfailWarning ?: api.local.DistanceToKey?.AppliedConditionPercentage) : "", ""))
              api.local.priceBuildupMatrix.salesSuggestPrice?resultMatrix.addRow(newRow(resultMatrix, 'Comparison Price', 'EQUALS', api.local.priceBuildupMatrix.salesSuggestPrice, "Selling Prices PX", "")) : ""
              api.local.priceBuildupMatrix.SalesDistanceToKeyKeyChannelPrice ? resultMatrix.addRow(newRow(resultMatrix, 'Key Channel Price', 'EQUALS', api.local.priceBuildupMatrix.SalesDistanceToKeyKeyChannelPrice, "Selling Prices PX", "")) : ""
        api.local.priceBuildupMatrix.SalesDistanceToKeyMaxAllowedDevPercentage ? resultMatrix.addRow(newRow(resultMatrix, 'MIN Deviation Allowed', 'EQUALS', api.local.priceBuildupMatrix.SalesDistanceToKeyMinAllowedDevPercentage, api.local.priceBuildupMatrix.SalesDistanceToKeyMinAllowedDevSTFormulaPercentage, api.local.priceBuildupMatrix.SalesDistanceToKeyMinAllowedDevFormulaPercentage)) : ""
        api.local.priceBuildupMatrix.SalesDistanceToKeyMaxAllowedDevPercentage ? resultMatrix.addRow(newRow(resultMatrix, 'MAX Deviation Allowed', 'EQUALS', api.local.priceBuildupMatrix.SalesDistanceToKeyMaxAllowedDevPercentage, api.local.priceBuildupMatrix.SalesDistanceToKeymaxAllowedDeviationSTFormulaPercentage, api.local.priceBuildupMatrix.SalesDistanceToKeymaxAllowedDeviationFormulaPercentage)) : ""
        api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedConditionStatementPercentage ? resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel Condition', 'EQUALS', api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedConditionPercentage, "", "")) : ""
        api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedPricePercentage ? resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel Applied Price', 'EQUALS', api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedPricePercentage, api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedConditionPercentage, "")) : ""
        api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedPercentage ? stringRoundScheme = "Distance to Key Channel" : ""
//
        resultMatrix.addRow(newRow(resultMatrix, 'SalesPrice - Distance to key Channel (ABS)', 'SECTION', "", "", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyChannelABS, "Distance To Key Channel PP", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Key Channel', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyKeyChannel, "Basket Key Channel PP", ""))
        //resultMatrix.addRow(newRow(resultMatrix, 'SalesPrice - Distance to key Channel (ABS)', 'SECTION', "", "", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel Applied', 'EQUALS', api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedABS, (!api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedABS) ? (api.local.priceBuildupMatrix.SalesDistanceToKeyChannelfailWarning ?: api.local.DistanceToKey?.AppliedConditionABS) : "", ""))
              api.local.priceBuildupMatrix.salesSuggestPrice?resultMatrix.addRow(newRow(resultMatrix, 'Comparison Price', 'EQUALS', api.local.priceBuildupMatrix.salesSuggestPrice, "Selling Prices PX", "")) : ""
              api.local.priceBuildupMatrix.SalesDistanceToKeyKeyChannelPrice ? resultMatrix.addRow(newRow(resultMatrix, 'Key Channel Price', 'EQUALS', api.local.priceBuildupMatrix.SalesDistanceToKeyKeyChannelPrice, "Selling Prices PX", "")) : ""
        api.local.priceBuildupMatrix.SalesDistanceToKeyMaxAllowedDevABS ? resultMatrix.addRow(newRow(resultMatrix, 'MIN Deviation Allowed', 'EQUALS', api.local.priceBuildupMatrix.SalesDistanceToKeyMinAllowedDevABS, api.local.priceBuildupMatrix.SalesDistanceToKeyMinAllowedDevSTFormulaABS, api.local.priceBuildupMatrix.SalesDistanceToKeyMinAllowedDevFormulaABS)) : ""
        api.local.priceBuildupMatrix.SalesDistanceToKeyMaxAllowedDevABS ? resultMatrix.addRow(newRow(resultMatrix, 'MAX Deviation Allowed', 'EQUALS', api.local.priceBuildupMatrix.SalesDistanceToKeyMaxAllowedDevABS, api.local.priceBuildupMatrix.SalesDistanceToKeymaxAllowedDeviationSTFormulaABS, api.local.priceBuildupMatrix.SalesDistanceToKeymaxAllowedDeviationFormulaABS)) : ""
        api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedConditionStatementABS ? resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel Condition', 'EQUALS', api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedConditionABS, "", "")) : ""
        api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedPriceABS ? resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel Applied Price', 'EQUALS', api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedPriceABS, api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedConditionABS, "")) : ""
        api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedABS ? stringRoundScheme = "Distance to Key Channel" : ""
//
        //resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyChannel , "Distance To Key Channel PP", ""))
        //resultMatrix.addRow(newRow(resultMatrix, 'Key Channel', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyKeyChannel , "Basket Key Channel PP", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'PriceGrid - Distance to key Channel (%)', 'SECTION', "", "", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyChannelPercentage, "Distance To Key Channel PP", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel Applied', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyAppliedPercentage, api.local.priceBuildupMatrix.DistanceToKeyChannelfailWarning ?: api.local.DistanceToKey?.AppliedConditionPercentage, ""))
        api.local.priceBuildupMatrix.DistanceToKeyKeyChannelPrice ? resultMatrix.addRow(newRow(resultMatrix, 'Key Channel Price', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyKeyChannelPrice, "Selling Price PX", "")) : ""
              api.local.priceBuildupMatrix.priceSuggestPrice?resultMatrix.addRow(newRow(resultMatrix, 'Comparison Price', 'EQUALS', api.local.priceBuildupMatrix.priceSuggestPrice, "Price Grid", "")) : ""
              api.local.priceBuildupMatrix.DistanceToKeyMaxAllowedDevPercentage ? resultMatrix.addRow(newRow(resultMatrix, 'MIN Deviation Allowed', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyMinAllowedDevPercentage, api.local.priceBuildupMatrix.DistanceToKeyMinAllowedDevSTFormulaPercentage, api.local.priceBuildupMatrix.DistanceToKeyMinAllowedDevFormulaPercentage)) : ""
        api.local.priceBuildupMatrix.DistanceToKeyMaxAllowedDevPercentage ? resultMatrix.addRow(newRow(resultMatrix, 'MAX Deviation Allowed', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyMaxAllowedDevPercentage, api.local.priceBuildupMatrix.DistanceToKeymaxAllowedDeviationSTFormulaPercentage, api.local.priceBuildupMatrix.DistanceToKeymaxAllowedDeviationFormulaPercentage)) : ""
        api.local.priceBuildupMatrix.DistanceToKeyAppliedConditionStatementPercentage ? resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel Condition', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyAppliedConditionPercentage, "", "")) : ""
        api.local.priceBuildupMatrix.DistanceToKeyAppliedPricePercentage ? resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel Applied Price', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyAppliedPricePercentage, api.local.priceBuildupMatrix.DistanceToKeyAppliedConditionPercentage, "")) : ""
        api.local.priceBuildupMatrix.DistanceToKeyAppliedPercentage ? stringRoundScheme = " Distance to Key Channel" : ""
    //
        resultMatrix.addRow(newRow(resultMatrix, 'PriceGrid - Distance to key Channel (ABS)', 'SECTION', "", "", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyChannelABS, "Distance To Key Channel PP", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel Applied', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyAppliedABS, api.local.priceBuildupMatrix.DistanceToKeyChannelfailWarning ?: api.local.DistanceToKey?.AppliedConditionABS, ""))
              api.local.priceBuildupMatrix.priceSuggestPrice?resultMatrix.addRow(newRow(resultMatrix, 'Comparison Price', 'EQUALS', api.local.priceBuildupMatrix.priceSuggestPrice, "Price Grid", "")) : ""
              api.local.priceBuildupMatrix.DistanceToKeyKeyChannelPrice ? resultMatrix.addRow(newRow(resultMatrix, 'Key Channel Price', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyKeyChannelPrice, "Selling Price PX", "")) : ""
        api.local.priceBuildupMatrix.DistanceToKeyMaxAllowedDevABS ? resultMatrix.addRow(newRow(resultMatrix, 'MIN Deviation Allowed', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyMinAllowedDevABS, api.local.priceBuildupMatrix.DistanceToKeyMinAllowedDevSTFormulaABS, api.local.priceBuildupMatrix.DistanceToKeyMinAllowedDevFormulaABS)) : ""
        api.local.priceBuildupMatrix.DistanceToKeyMaxAllowedDevABS ? resultMatrix.addRow(newRow(resultMatrix, 'MAX Deviation Allowed', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyMaxAllowedDevABS, api.local.priceBuildupMatrix.DistanceToKeymaxAllowedDeviationSTFormulaABS, api.local.priceBuildupMatrix.DistanceToKeymaxAllowedDeviationFormulaABS)) : ""
        api.local.priceBuildupMatrix.DistanceToKeyAppliedConditionStatementABS ? resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel Condition', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyAppliedConditionABS, "", "")) : ""
        api.local.priceBuildupMatrix.DistanceToKeyAppliedPriceABS ? resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel Applied Price', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyAppliedPriceABS, api.local.priceBuildupMatrix.DistanceToKeyAppliedConditionABS, "")) : ""
        api.local.priceBuildupMatrix.DistanceToKeyAppliedABS ? stringRoundScheme = " Distance to Key Channel" : ""

    } else {

        resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel Applied', 'EQUALS', "false", "", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyChannel, "Distance To Key Channel PP", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Key Channel', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyKeyChannel, "Basket Key Channel PP", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel', 'EQUALS', "Distance to Key Channel Not Available", api.local.DistanceToKey?.AppliedCondition, ""))
    }
}
    resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))
    resultMatrix.addRow(newRow(resultMatrix, 'ROUNDING & MARGIN CALCULATION', 'SECTION', "", "", ""))
    if (api.local.priceBuildupMatrix.IsRoundingSchemeApplied == "Yes") {
        resultMatrix.addRow(newRow(resultMatrix, 'Is Rounding Scheme Applied', 'EQUALS', api.local.priceBuildupMatrix.IsRoundingSchemeApplied ?: "No", "", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Calculated Price (No Rounding)', 'EQUALS', api.local.priceBuildupMatrix.Roundingbefore, "", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Rounding Scheme Used', 'EQUALS', api.local.priceBuildupMatrix.RoundingSchemColumn + "(" + api.local.priceBuildupMatrix.appliedRoundingPrice + " " + out.Currency + ")" ?: "", "Rounding Scheme PP", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Rounding Scheme Falls Between', 'EQUALS', api.local.priceBuildupMatrix.RoundingSchemeExistBetween ?: "", "Rounding Scheme PP", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'List Price (Rounding Scheme Applied)', 'EQUALS', api.local.priceBuildupMatrix.appliedRoundingPrice ? roundingUtils.roundNumber(api.local.priceBuildupMatrix.appliedRoundingPrice, 2) + " " + out.Currency : "", "", ""))
    }
    if (api.local.priceBuildupMatrix?.marketingCamp) {
        resultMatrix.addRow(newRow(resultMatrix, 'Calculated List Price (Rounded ' + api.local.priceBuildupMatrix.PricingReason + ')', 'EQUALS', roundingUtils.roundNumber(out.CalculatedListPrice, 2) + " " + out.Currency, "Selling Prices PX", ""))
    } else {
        api.local.priceBuildupMatrix.appliedConversionFactor?.each { it ->
            resultMatrix.addRow(newRow(resultMatrix, 'Conversion', 'EQUALS', it?.key + "(" + it?.value + ")", "UoM PP", ""))
        }

        resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'CALCULATED LIST PRICE (GROSS)', 'SECTION', "", "", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Calculated List Price (Rounded ' + api.local.priceBuildupMatrix.PricingReason + ')', 'EQUALS', roundingUtils.roundNumber(out.CalculatedListPrice, 2) + " " + out.Currency, "", ""))
        out.CalculatedListPrice != out.ListPrice ? resultMatrix.addRow(newRow(resultMatrix, 'List Price Override', 'EQUALS', roundingUtils.roundNumber(out.ListPrice, 2) + " " + out.Currency, "", "")) : ""
        resultMatrix.addRow(newRow(resultMatrix, 'Price Per Unit', 'EQUALS', (roundingUtils.roundNumber((out.PricePerUnit), 2) ?: roundingUtils.roundNumber(out.ListPrice, 2)) + " " + out.Currency, "(New RP / Net Content)", "(" + (out.ListPrice ?: "") + " / " + (api.local.converted ?: 1) + ")"))
        if (api.local.priceBuildupMatrix.futureOverride) {
            resultMatrix.addRow(newRow(resultMatrix, 'Future SP', 'EQUALS', (out.FutureSP ?: 0) + " " + out.Currency, "", ""))
            resultMatrix.addRow(newRow(resultMatrix, 'Future SP Valid From', 'EQUALS', (out.FutureSPValidFrom), "", ""))
        }
    }
} else {
    resultMatrix.addRow(newRow(resultMatrix, "List Price", "EQUALS", "Pricing Calculation not Available", "", ""))
}
return resultMatrix


Map newRow(ResultMatrix resultMatrix, String name, String operator, result, source, String formula) {

    List valueStyle = ["Calculated", "SP Margin Offline %", "SP Margin Online %", "List Price Override", "List Price Online Margin %", "List Price Offline Margin %"]
    if (name?.contains("Margin %") || name?.contains("Calculated")) {

        [
                Name   : getNameCell(resultMatrix, name, operator),
                " "    : getOperatorCell(resultMatrix, operator),
                Value  : getValueCell(resultMatrix, name, result),
                Source : source,
                Formula: formula
        ]
    } else {
        return [
                Name   : getNameCell(resultMatrix, name, operator),
                " "    : getOperatorCell(resultMatrix, operator),
                Value  : result,
                Source : source,
                Formula: formula
        ]
    }
}

def getOperatorCell(ResultMatrix resultMatrix, String operator) {
    //OPERATORS[operator]
    return resultMatrix.styledCell(OPERATORS[operator])
            .withColor("orange")
            .withFontWeight("bold")
            .withAlignment("center")
            .withCSS("font-size: 26px;color:orange;")
}

def getNameCell(ResultMatrix resultMatrix, String sectionName, String operator) {
    if (operator == 'SECTION') {
        return resultMatrix.styledCell(sectionName)
                .withColor("grey")
                .withFontWeight("bold")
                .withAlignment("left")
                .withCSS("font-size: 20px;")
    }
    return resultMatrix.styledCell(sectionName)
}

def getValueCell(ResultMatrix resultMatrix, String sectionName, def result) {
    List valueStyle = ["Calculated", "SP Margin Offline %", "SP Margin Online %"]
    if (sectionName?.contains("%") && result && !sectionName?.contains("Calculated")) {
        if (result?.contains("-")) {
            return resultMatrix.styledCell(result + "↓")
                    .withColor("red")
                    .withFontWeight("bold")
                    .withAlignment("left")
                    .withCSS("font-size: 12px;")
        } else {
            return resultMatrix.styledCell(result + "↑")
                    .withColor("green")
                    .withFontWeight("bold")
                    .withAlignment("left")
                    .withCSS("font-size: 12px;")
        }
    } else if (result) {
        return resultMatrix.styledCell(result)
                .withColor('green')
                .withFontWeight('bold')
                .withAlignment("left")
                .withCSS("font-size: 15px;")
    }

}

def competitorResultMatrixBuilder(ResultMatrix resultMatrix, String pricingApproach, List competitors, Map refRow) {
    resultMatrix.addRow(newRow(resultMatrix, 'Competitors', 'IN', competitors?.join(' | '), 'Competition Data', ''))
    resultMatrix.addRow(newRow(resultMatrix, 'Priority', 'EQUALS', api.local.priceBuildupMatrix.CompetitorPriority, 'Competitor Basket Mapping PP', ''))
    resultMatrix.addRow(newRow(resultMatrix, 'Calculation Mode', 'EQUALS', refRow?.calculationMode ?: '', 'Competitor Basket Mapping PP', ""))
    resultMatrix.addRow(newRow(resultMatrix, 'Target Price Index', 'EQUALS', refRow?.targetPriceIndex ?: '', 'Target Price Index PP', ""))
    resultMatrix.addRow(newRow(resultMatrix, 'Competitor Price', 'EQUALS', pricingApproach == 'pricingApproach1' ? libs.FressnapfLib.RoundingUtils.roundNumber(out.PricingApproach1Price, 5) + ' ' + out.Currency : libs.FressnapfLib.RoundingUtils.roundNumber(out.PricingApproach2Price, 5) + ' ' + out.Currency, refRow?.calculationMode, ""))


    return resultMatrix
}

def costPlusResultMatrixBuild(ResultMatrix resultMatrix, String pricingApproach, def costPlusPricingApporach) {

    if (out.CalculationChannel == 'Online') {
        exchangeCCOnline = api.local.priceBuildupMatrix.ppPrice?:out.PurchasePriceOnline
        formula = '(-' + libs.FressnapfLib.RoundingUtils.roundNumber(exchangeCCOnline, 5) + ') /((' + api.local.priceBuildupMatrix.CostPlusmargin + '/ 100) -1))'
        stringFormula = '(-'+api.local.priceBuildupMatrix.ppApplied+'PurchasePriceOnline /((' + costPlusPricingApporach?.split('-')?.getAt(1) + '/ 100)- 1)'
    } else if (out.CalculationChannel == 'Offline') {
        purchasePrice = out.PurchasePriceConfiguration == 'PP Offline Price' ? 'PurchasePriceOffline' : 'PurchasePriceRetail'
        purchasePrice = api.local.priceBuildupMatrix.ppApplied+purchasePrice
        exchangeCCOffline = api.local.priceBuildupMatrix.ppPrice?:(out.PurchasePriceConfiguration == 'PP Offline Price' ? out.PurchasePriceOffline : out.PurchasePriceRetail)
        formula = '(-' + libs.FressnapfLib.RoundingUtils.roundNumber(exchangeCCOffline, 5) + ')  /((' + api.local.priceBuildupMatrix.CostPlusmargin + ' / 100 ) - 1))'
        stringFormula = '(-' + purchasePrice + ' /((' + costPlusPricingApporach?.split('-')?.getAt(1) + "/ 100)-1)"
    }


    resultMatrix.addRow(newRow(resultMatrix, 'Applied Margin', 'EQUALS', costPlusPricingApporach?.split('-')?.getAt(1), '', ""))
    if( api.local.costPlusWarning){
        resultMatrix.addRow(newRow(resultMatrix, 'Cost Plus Warning', 'EQUALS', api.local.costPlusWarning, '', ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Cost Plus - Price', 'EQUALS', api.local.costPlusMarginprice, 'Current Selling Price', ""))
        return resultMatrix
    }
    resultMatrix.addRow(newRow(resultMatrix, 'Margin', 'EQUALS', api.local.priceBuildupMatrix.CostPlusmargin, (api.local.priceBuildupMatrix.ppAppliedMargin?:'Cost Plus Margins PX'), ''))
    price = (pricingApproach == 'pricingApproach1' ? libs.FressnapfLib.RoundingUtils.roundNumber(out.PricingApproach1Price, 5) : libs.FressnapfLib.RoundingUtils.roundNumber(out.PricingApproach2Price, 5))
    nonVat = (price) ? (price / (1 + (out.VAT / 100))) : 0
    stringListPrice = 'CostPlus ' + costPlusPricingApporach?.split('-')?.getAt(1) + ' Price'
    resultMatrix.addRow(newRow(resultMatrix, costPlusPricingApporach + ' Price', 'EQUALS', nonVat + ' ' + out.TargetCurrency, stringFormula, formula))
    appMargin = costPlusPricingApporach?.split('-')?.getAt(1)
    api.local.priceBuildupMatrix.CostPlusmarginCompareRes != null && api.local.priceBuildupMatrix.CostPlusmarginCompareRes ? resultMatrix.addRow(newRow(resultMatrix, 'Margin Validation', 'EQUALS', api.local.priceBuildupMatrix.CostPlusmarginCompareRes, '(' + api.local.priceBuildupMatrix.appliedCostPlusmarginCompRes + ' > ' + appMargin + ')', '(' + (api.local.priceBuildupMatrix.appliedCostPlusmargin * 100) + " > " + api.local.priceBuildupMatrix.CostPlusmargin + ")")) : ""
    api.local.priceBuildupMatrix.CostPlusmarginCompareRes ? resultMatrix.addRow(newRow(resultMatrix, 'Margin violation Overwritten Price', 'EQUALS', out['SalesPrice' + out.CalculationChannel], 'Selling Price PX', 'Selling Price PX')) : ""
    formula = '(' + nonVat + ' * (1 + (' + out.VAT + ' / 100)))'
    !api.local.priceBuildupMatrix.CostPlusmarginCompareRes ? resultMatrix.addRow(newRow(resultMatrix, 'Gross Price (VAT Applied)', 'EQUALS', (pricingApproach == 'pricingApproach1' ? libs.FressnapfLib.RoundingUtils.roundNumber(out.PricingApproach1Price, 5) : libs.FressnapfLib.RoundingUtils.roundNumber(out.PricingApproach2Price, 5)) + ' ' + out.Currency, '(' + stringListPrice + ' +( 1 + (VAT/100)))', formula)) : ''
    return resultMatrix
}

def addSetTrayCheck(resultMatrix){
if(!out.ZSETZTRADetails){
    return
}
    resultMatrix.addRow(newRow(resultMatrix, 'DISTANCE TO KEY CHANNEL VALIDATION (Set/Tray)', 'SECTION', "", "", ""))

        resultMatrix.addRow(newRow(resultMatrix, 'SET TRAY PRICING (OFFLINE)', 'SECTION', "", "", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'BOM Components', 'IN', api.local.priceBuildupMatrix.OfflinesetTrayDistanceToKeyChannelcomp, "BOM Data | Selling Price PX", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Total Components', 'EQUALS', out.NumberOfComponents, "BOM Data", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Set Tray Price', 'EQUALS', api.local.priceBuildupMatrix.OfflinesetTrayDistanceToKeyChannelRPriceBD, "", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Set Tray Discount', 'EQUALS', api.local.priceBuildupMatrix.setTrayPriceDiscount, "Set and Tray Discount PP", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Discounted Set Tray Price', 'EQUALS', api.local.priceBuildupMatrix.OfflinesetTrayDistanceToKeyChannelRPriceAF, "Discounted Set and Tray Price", ""))
        resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))


    if (api.local.priceBuildupMatrix.DistanceToKeyKeyChannel || api.local.priceBuildupMatrix.DistanceToKeyKeyChannel == 0) {
        resultMatrix.addRow(newRow(resultMatrix, 'SalesPrice - Distance to key Channel(%)', 'SECTION', "", "", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyChannelPercentage, "Distance To Key Channel PP", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Key Channel', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyKeyChannel, "Basket Key Channel PP", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel Applied', 'EQUALS', api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedPercentageST, (!api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedPercentageST) ? (api.local.priceBuildupMatrix.SalesDistanceToKeyChannelfailWarning ?: api.local.DistanceToKey?.AppliedConditionPercentageST) : "", ""))
        api.local.priceBuildupMatrix.salesSuggestPrice?resultMatrix.addRow(newRow(resultMatrix, 'Comparison Price', 'EQUALS', api.local.priceBuildupMatrix.salesSuggestPrice, "Selling Prices PX", "")) : ""
        api.local.priceBuildupMatrix.SalesDistanceToKeyKeyChannelPrice ? resultMatrix.addRow(newRow(resultMatrix, 'Key Channel Price', 'EQUALS', api.local.priceBuildupMatrix.SalesDistanceToKeyKeyChannelPrice, "Calculated Set / Tray Offline", "")) : ""
        api.local.priceBuildupMatrix.SalesDistanceToKeyMaxAllowedDevPercentageST ? resultMatrix.addRow(newRow(resultMatrix, 'MIN Deviation Allowed', 'EQUALS', api.local.priceBuildupMatrix.SalesDistanceToKeyMinAllowedDevPercentageST, api.local.priceBuildupMatrix.SalesDistanceToKeyMinAllowedDevSTFormulaPercentageST, api.local.priceBuildupMatrix.SalesDistanceToKeyMinAllowedDevFormulaPercentageST)) : ""
        api.local.priceBuildupMatrix.SalesDistanceToKeyMaxAllowedDevPercentageST ? resultMatrix.addRow(newRow(resultMatrix, 'MAX Deviation Allowed', 'EQUALS', api.local.priceBuildupMatrix.SalesDistanceToKeyMaxAllowedDevPercentageST, api.local.priceBuildupMatrix.SalesDistanceToKeymaxAllowedDeviationSTFormulaPercentageST, api.local.priceBuildupMatrix.SalesDistanceToKeymaxAllowedDeviationFormulaPercentageST)) : ""
        api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedConditionStatementPercentageST ? resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel Condition', 'EQUALS', api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedConditionPercentageST, "", "")) : ""
        api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedPricePercentageST ? resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel Applied Price', 'EQUALS', api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedPricePercentageST, api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedConditionPercentageST, "")) : ""
        api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedPercentageST ? stringRoundScheme = "Distance to Key Channel" : ""
//
        resultMatrix.addRow(newRow(resultMatrix, 'SalesPrice - Distance to key Channel (ABS)', 'SECTION', "", "", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyChannelABS, "Distance To Key Channel PP", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Key Channel', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyKeyChannel, "Basket Key Channel PP", ""))
        //resultMatrix.addRow(newRow(resultMatrix, 'SalesPrice - Distance to key Channel (ABS)', 'SECTION', "", "", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel Applied', 'EQUALS', api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedABSST, (!api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedABSST) ? (api.local.priceBuildupMatrix.SalesDistanceToKeyChannelfailWarning ?: api.local.DistanceToKey?.AppliedConditionABSST) : "", ""))
        api.local.priceBuildupMatrix.salesSuggestPrice?resultMatrix.addRow(newRow(resultMatrix, 'Comparison Price', 'EQUALS', api.local.priceBuildupMatrix.salesSuggestPrice, "Selling Prices PX", "")) : ""
        api.local.priceBuildupMatrix.SalesDistanceToKeyKeyChannelPrice ? resultMatrix.addRow(newRow(resultMatrix, 'Key Channel Price', 'EQUALS', api.local.priceBuildupMatrix.SalesDistanceToKeyKeyChannelPrice, "Calculated Set / Tray Offline", "")) : ""
        api.local.priceBuildupMatrix.SalesDistanceToKeyMaxAllowedDevABSST ? resultMatrix.addRow(newRow(resultMatrix, 'MIN Deviation Allowed', 'EQUALS', api.local.priceBuildupMatrix.SalesDistanceToKeyMinAllowedDevABSST, api.local.priceBuildupMatrix.SalesDistanceToKeyMinAllowedDevSTFormulaABSST, api.local.priceBuildupMatrix.SalesDistanceToKeyMinAllowedDevFormulaABSST)) : ""
        api.local.priceBuildupMatrix.SalesDistanceToKeyMaxAllowedDevABSST ? resultMatrix.addRow(newRow(resultMatrix, 'MAX Deviation Allowed', 'EQUALS', api.local.priceBuildupMatrix.SalesDistanceToKeyMaxAllowedDevABSST, api.local.priceBuildupMatrix.SalesDistanceToKeymaxAllowedDeviationSTFormulaABSST, api.local.priceBuildupMatrix.SalesDistanceToKeymaxAllowedDeviationFormulaABSST)) : ""
        api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedConditionStatementABSST ? resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel Condition', 'EQUALS', api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedConditionABSST, "", "")) : ""
        api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedPriceABSST ? resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel Applied Price', 'EQUALS', api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedPriceABSST, api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedConditionABSST, "")) : ""
        api.local.priceBuildupMatrix.SalesDistanceToKeyAppliedABSST ? stringRoundScheme = "Distance to Key Channel" : ""
//
        //resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyChannel , "Distance To Key Channel PP", ""))
        //resultMatrix.addRow(newRow(resultMatrix, 'Key Channel', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyKeyChannel , "Basket Key Channel PP", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'PriceGrid - Distance to key Channel (%)', 'SECTION', "", "", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyChannelPercentage, "Distance To Key Channel PP", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel Applied', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyAppliedPercentageST, api.local.priceBuildupMatrix.DistanceToKeyChannelfailWarning ?: api.local.DistanceToKey?.AppliedConditionPercentageST, ""))
        api.local.priceBuildupMatrix.priceSuggestPrice?resultMatrix.addRow(newRow(resultMatrix, 'Comparison Price', 'EQUALS', api.local.pricebeforeDistance, "Price Grid", "")) : ""
        api.local.priceBuildupMatrix.DistanceToKeyKeyChannelPrice ? resultMatrix.addRow(newRow(resultMatrix, 'Key Channel Price', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyKeyChannelPrice, "Calculated Set / Tray Offline", "")) : ""
        api.local.priceBuildupMatrix.DistanceToKeyMaxAllowedDevPercentageST ? resultMatrix.addRow(newRow(resultMatrix, 'MIN Deviation Allowed', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyMinAllowedDevPercentageST, api.local.priceBuildupMatrix.DistanceToKeyMinAllowedDevSTFormulaPercentageST, api.local.priceBuildupMatrix.DistanceToKeyMinAllowedDevFormulaPercentageST)) : ""
        api.local.priceBuildupMatrix.DistanceToKeyMaxAllowedDevPercentageST ? resultMatrix.addRow(newRow(resultMatrix, 'MAX Deviation Allowed', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyMaxAllowedDevPercentageST, api.local.priceBuildupMatrix.DistanceToKeymaxAllowedDeviationSTFormulaPercentageST, api.local.priceBuildupMatrix.DistanceToKeymaxAllowedDeviationFormulaPercentageST)) : ""
        api.local.priceBuildupMatrix.DistanceToKeyAppliedConditionStatementPercentageST ? resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel Condition', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyAppliedConditionPercentageST, "", "")) : ""
        api.local.priceBuildupMatrix.DistanceToKeyAppliedPricePercentageST ? resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel Applied Price', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyAppliedPricePercentageST, api.local.priceBuildupMatrix.DistanceToKeyAppliedConditionPercentageST, "")) : ""
        api.local.priceBuildupMatrix.DistanceToKeyAppliedPercentageST ? stringRoundScheme = " Distance to Key Channel" : ""
        //
        resultMatrix.addRow(newRow(resultMatrix, 'PriceGrid - Distance to key Channel (ABS)', 'SECTION', "", "", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyChannelABS, "Distance To Key Channel PP", ""))
        resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel Applied', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyAppliedABSST, api.local.priceBuildupMatrix.DistanceToKeyChannelfailWarning ?: api.local.DistanceToKey?.AppliedConditionABSST, ""))
        api.local.priceBuildupMatrix.priceSuggestPrice?resultMatrix.addRow(newRow(resultMatrix, 'Comparison Price', 'EQUALS', api.local.pricebeforeDistance, "Price Grid", "")) : ""
        api.local.priceBuildupMatrix.DistanceToKeyKeyChannelPrice ? resultMatrix.addRow(newRow(resultMatrix, 'Key Channel Price', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyKeyChannelPrice, "Calculated Set / Tray Offline", "")) : ""
        api.local.priceBuildupMatrix.DistanceToKeyMaxAllowedDevABSST ? resultMatrix.addRow(newRow(resultMatrix, 'MIN Deviation Allowed', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyMinAllowedDevABSST, api.local.priceBuildupMatrix.DistanceToKeyMinAllowedDevSTFormulaABSST, api.local.priceBuildupMatrix.DistanceToKeyMinAllowedDevFormulaABSST)) : ""
        api.local.priceBuildupMatrix.DistanceToKeyMaxAllowedDevABSST ? resultMatrix.addRow(newRow(resultMatrix, 'MAX Deviation Allowed', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyMaxAllowedDevABSST, api.local.priceBuildupMatrix.DistanceToKeymaxAllowedDeviationSTFormulaABSST, api.local.priceBuildupMatrix.DistanceToKeymaxAllowedDeviationFormulaABSST)) : ""
        api.local.priceBuildupMatrix.DistanceToKeyAppliedConditionStatementABSST ? resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel Condition', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyAppliedConditionABSST, "", "")) : ""
        api.local.priceBuildupMatrix.DistanceToKeyAppliedPriceABSST ? resultMatrix.addRow(newRow(resultMatrix, 'Distance to Key Channel Applied Price', 'EQUALS', api.local.priceBuildupMatrix.DistanceToKeyAppliedPriceABSST, api.local.priceBuildupMatrix.DistanceToKeyAppliedConditionABSST, "")) : ""
        api.local.priceBuildupMatrix.DistanceToKeyAppliedABSST ? stringRoundScheme = " Distance to Key Channel" : ""
    }
}