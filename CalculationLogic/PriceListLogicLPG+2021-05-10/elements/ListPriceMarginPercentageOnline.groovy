if(out.ListPrice && out.PurchasePriceOnline!=0 && out.PurchasePriceOnline){
    BigDecimal listPrice = out.PriceExVAT != out.CalculatedListPrice ? out.PriceExVAT : api.local.ListPriceOnline
    api.local.onlineMarginPrice = listPrice
    BigDecimal markUpPercentage = listPrice > 0 ?
            ((listPrice - out.PurchasePriceOnline) / listPrice)
            : 0.0
    return out.CommonLib.UIUtils.getArrowedCell(markUpPercentage)
}
return
