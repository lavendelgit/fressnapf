List filter = [
        Filter.equal("type", "CALCULATION"),
        Filter.equal("label", out.Configuration?.DataLoad)

]
return api.find("DMDL", *filter)?.getAt(0)?.calculationDate