List duplicateRecord = []
List deleteDuplicateRecord = []
api.local.duplicateRecords?.each { record ->
    duplicateRecord = []
    api.trace("record",record)
    if (record?.value?.size() > 1) {
        duplicateRecord = record?.value?.minus(record?.value?.getAt(0))
        if(out.SourceTable == "Competition Data" && duplicateRecord?.size()>0){
            duplicateRecord = duplicateRecord?.findAll {it.infoDate != (record?.value?.getAt(0))?.infoDate}
        }
        loadToDataSource(duplicateRecord)
    }
}
return

void loadToDataSource(Object duplicateRecord) {
    def target = api.getDatamartRowSet("target")
    Map rowRecord = [:]
    if (out.SourceTable == "Competition Data") {
        metaData = api.findLookupTableValues("CompetitionMetaData")?.collectEntries { [(it.value): it.name] }
    } else {
        metaData = api.find("PXAM", Filter.equal("name", out.SourceTable))?.collectEntries { it -> [((api.jsonDecode(it.labelTranslations)?.values()?.getAt(0))?.replaceAll("\\s", "")?.replaceAll("3", "")): it.fieldName] }
    }
    duplicateRecord?.each { record ->
        metaData?.each {
            rowRecord[it.key] = (record?.getAt(it.value)) ?: null
        }
        rowRecord.name = out.SourceTable
        target?.addRow(rowRecord)
    }
}