duplicateData = [:]
bussinuesKey = out.Configuration["BusinessKey"]?.split(',')*.replaceAll("[\n\r{\\\"\\\":\\\"}]", "")*.trim()
exceptionKeys = out.Configuration["ExceptionKey"]?.split(',')*.replaceAll("[\n\r{\\\"\\\":\\\"}]", "")*.trim()
if (out.SourceTable == "Competition Data") {
    List filter = []
    if(out.Configuration?.Incremental=="Yes"){
        filter << Filter.greaterOrEqual("lastUpdateDate", out.LastDataLoadRun)
    }
    List filterAttributes = bussinuesKey
    iterarter = api.stream(out.Configuration?.SourceTypeCode, "lastUpdateDate", filterAttributes, *filter)
    duplicatRecords = iterarter?.collect { it }
    iterarter?.close()
    filter = []
    bussinuesKey?.each {
        filter << Filter.in(it, duplicatRecords[it])
    }
    String key
    iterarter = api.stream(out.Configuration?.SourceTypeCode, "-lastUpdateDate", *filter)
    iterarter?.collect { attribute ->
        key = ""
        bussinuesKey?.each { it ->
            key = key ? key + "_" + attribute[it] : attribute[it]
        }
        if (!duplicateData[key]) {
            duplicateData[key] = []
            duplicateData[key] << attribute
        } else {
            duplicateData[key] << attribute
        }
    }
    iterarter?.close()
   // api.trace("duplicateData",duplicateData)
    api.local.duplicateRecords = duplicateData
    return
}
duplicateDetails = findDuplicateEntrys(out.SourceTable, out.Configuration?.SourceTypeCode, bussinuesKey)
List filter = [Filter.equal("name", out.SourceTable),
               /* Filter.lessOrEqual("PPRetailValidFrom",new Date()?.format("YYYY-MM-dd")),
 Filter.lessOrEqual("PPOnlineValidFrom",new Date()?.format("YYYY-MM-dd")),
 Filter.lessOrEqual("PPOfflineValidFrom",new Date()?.format("YYYY-MM-dd"))*/]
bussinuesKey?.each {
    filter << Filter.in(it, duplicateDetails[it])
}
exceptionKeys?.each {
    filter << Filter.lessOrEqual(it, new Date()?.format("YYYY-MM-dd"))
}
String key
iterarter = api.stream(out.Configuration?.SourceTypeCode, "-lastUpdateDate", *filter)
iterarter?.collect { attribute ->
    key = ""
    bussinuesKey?.each { it ->
        key = key ? key + "_" + attribute[it] : attribute[it]
    }
    if (!duplicateData[key]) {
        duplicateData[key] = []
        duplicateData[key] << attribute
    } else {
        duplicateData[key] << attribute
    }
}
iterarter?.close()
api.local.duplicateRecords = duplicateData
return

List findDuplicateEntrys(String sourceTable, String sourceTypeCode, List bKey) {
    List filter = [Filter.equal("name", sourceTable),
                   Filter.greaterOrEqual("lastUpdateDate", out.LastDataLoadRun),
    ]
    List filterAttributes = bKey
    iterarter = api.stream(sourceTypeCode, "lastUpdateDate", filterAttributes, *filter)
    duplicatRecords = iterarter?.collect { it }
    iterarter?.close()
    return duplicatRecords
}