sourceTables = api.findLookupTableValues("ArchivingConfiguration")?.name
return api.option("SourceTable", sourceTables)