if (!api.global.hawaPurchasePriceMap){
    api.global.hawaPurchasePriceMap =[:]
    def filters = [
            Filter.equal("name", "PurchasePrice")
    ]

    def iterator = api.stream("PX10", null, *filters)

    iterator.collectEntries{
        api.global.hawaPurchasePriceMap[it.sku + "~" + it.attribute5] = it
    }

    iterator.close()

}

if (!api.global.exchangRateMap){
    api.global.exchangRateMap=[:]
    api.global.exchangRateMap = api.findLookupTableValues("ExchangeRates","key3",null)?.collectEntries{
        [(it.key1 + "~" + it.key2):it.attribute1]
    }

}