def hawaPurchasePriceMap = api.global.hawaPurchasePriceMap
def tripleNetOnlineCost
def tripleNetOnlineCurrency
def tripleNetOnlineCostSum = 0
def tripleNetOfflineWholeSaleCost
def tripleNetOfflineWholeSaleCurrency
def tripleNetOfflineWholeSaleCostSum = 0
def offlineRetailCost
def offlineRetailCurrency
def offlineRetailCostSum = 0

def comment
def hawaPurchasePriceRecord
def country = out.Country


def hawasku
def bomData = api.bomList()
def quantity


def toCurrency = out.ToCurrency

if (!bomData) comment = "No BOM data available"
def componentCount = 0
def tripleNetOnlineCurrencyList = []
def tripleNetOfflineWholeSaleCurrencyList =[]
def offlineRetailCurrencyList =[]
def correctTripleNetOnlineCurrency
def correctTripleNetOfflineWholeSaleCurrency
def correctOfflineRetailCurrency

bomData.each{
    hawasku = it.label
    quantity = it.quantity
    hawaPurchasePriceRecord = hawaPurchasePriceMap[hawasku + "~" +country]
    if (!hawaPurchasePriceRecord) {componentCount += 1 }

        tripleNetOnlineCost = (hawaPurchasePriceRecord)?.attribute1
        tripleNetOnlineCurrency = (hawaPurchasePriceRecord)?.attribute2
        if (tripleNetOnlineCurrency) tripleNetOnlineCurrencyList.add(tripleNetOnlineCurrency)

        tripleNetOfflineWholeSaleCost = (hawaPurchasePriceRecord)?.attribute3
        tripleNetOfflineWholeSaleCurrency = (hawaPurchasePriceRecord)?.attribute4
        if (tripleNetOfflineWholeSaleCurrency) tripleNetOfflineWholeSaleCurrencyList.add(tripleNetOfflineWholeSaleCurrency)

        offlineRetailCost = (hawaPurchasePriceRecord)?.attribute6
        offlineRetailCurrency = (hawaPurchasePriceRecord)?.attribute7
        if (offlineRetailCurrency) offlineRetailCurrencyList.add(offlineRetailCurrency)


        tripleNetOnlineCostSum += (tripleNetOnlineCost && quantity) ?  tripleNetOnlineCost * quantity : 0
        tripleNetOfflineWholeSaleCostSum += (tripleNetOfflineWholeSaleCost && quantity) ? tripleNetOfflineWholeSaleCost * quantity : 0
        offlineRetailCostSum += (offlineRetailCost && quantity) ? offlineRetailCost * quantity : 0

}
if (bomData && (componentCount == bomData.size())) {comment = "HAWA Purchase Price Data Not Available"}

if (tripleNetOnlineCurrencyList){
    if(tripleNetOnlineCurrencyList.unique().size() > 1){
        tripleNetOnlineCostSum = 0
        bomData.each{
            hawasku = it.label
            quantity = it.quantity
            hawaPurchasePriceRecord = hawaPurchasePriceMap[hawasku + "~" +country]

            tripleNetOnlineCost = (hawaPurchasePriceRecord)?.attribute1
            tripleNetOnlineCurrency = (hawaPurchasePriceRecord)?.attribute2

            tripleNetOnlineCostSum += (tripleNetOnlineCost && quantity) ? Library.getExchangeRate(tripleNetOnlineCurrency,toCurrency) * tripleNetOnlineCost * quantity : 0

        }
        correctTripleNetOnlineCurrency = toCurrency

    }else correctTripleNetOnlineCurrency = tripleNetOnlineCurrencyList.getAt(0)
}

if (tripleNetOfflineWholeSaleCurrencyList){
    if(tripleNetOfflineWholeSaleCurrencyList.unique().size() > 1){
        tripleNetOfflineWholeSaleCostSum = 0
        bomData.each{
            hawasku = it.label
            quantity = it.quantity
            hawaPurchasePriceRecord = hawaPurchasePriceMap[hawasku + "~" +country]

            tripleNetOfflineWholeSaleCost = (hawaPurchasePriceRecord)?.attribute3
            tripleNetOfflineWholeSaleCurrency = (hawaPurchasePriceRecord)?.attribute4

            tripleNetOfflineWholeSaleCostSum += (tripleNetOfflineWholeSaleCost && quantity) ? Library.getExchangeRate(tripleNetOfflineWholeSaleCurrency,toCurrency) *  tripleNetOfflineWholeSaleCost * quantity : 0

        }
        correctTripleNetOfflineWholeSaleCurrency = toCurrency

    }else correctTripleNetOfflineWholeSaleCurrency = tripleNetOfflineWholeSaleCurrencyList.getAt(0)
}

if (offlineRetailCurrencyList){
    if(offlineRetailCurrencyList.unique().size() > 1){
        offlineRetailCostSum = 0
        bomData.each{
            hawasku = it.label
            quantity = it.quantity
            hawaPurchasePriceRecord = hawaPurchasePriceMap[hawasku + "~" +country]

            offlineRetailCost = (hawaPurchasePriceRecord)?.attribute6
            offlineRetailCurrency = (hawaPurchasePriceRecord)?.attribute7

            offlineRetailCostSum += (offlineRetailCost && quantity) ? Library.getExchangeRate(offlineRetailCurrency,toCurrency) * offlineRetailCost * quantity : 0

        }
        correctOfflineRetailCurrency = toCurrency

    }else correctOfflineRetailCurrency = offlineRetailCurrencyList.getAt(0)
}

def purchasePriceMap =[
        "name" : "SetTrayPurchasePrices",
        "sku": out.sku,
        "attribute5" : out.Country,
        "attribute1" : (tripleNetOnlineCostSum)?:null,
        "attribute2" : (correctTripleNetOnlineCurrency)?:null,
        "attribute3" : (tripleNetOfflineWholeSaleCostSum)?:null,
        "attribute4" : (correctTripleNetOfflineWholeSaleCurrency)?:null,
        "attribute6" : (offlineRetailCostSum)?:null,
        "attribute7" : (correctOfflineRetailCurrency)?:null,
        "attribute8" : comment
]

api.addOrUpdate("PX10",purchasePriceMap)