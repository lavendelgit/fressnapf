def getExchangeRate(fromCurrency, toCurrency){
    if (fromCurrency == toCurrency ) return 1
    rate = api.global.exchangRateMap[fromCurrency + "~" + toCurrency]
    return (rate)?:1
}