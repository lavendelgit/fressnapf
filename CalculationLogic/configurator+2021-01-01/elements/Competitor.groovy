if(input.Channel){
    defaultValue = "Default Competitor "+input.Channel
    allComp = api.findLookupTableValues("CompetitorsMaster")?.collect{it.name+"-"+it.attribute1}
    priotityComp = api.findLookupTableValues("DefaultParamaterConfiguration",Filter.equal("name",defaultValue))?.getAt(0)?.value
    api.global.comp = priotityComp!=input.Competitor?((input.Competitor)?:priotityComp):priotityComp//(priotityComp+"-"+allComp?.getAt(priotityComp)) != input.Competitor ?  (input.Competitor?(allComp?.getAt(input.Competitor)?priotityComp+"-"+allComp?.getAt(input.Competitor):input.Competitor):(priotityComp+"-"+allComp?.getAt(priotityComp))) : (priotityComp+"-"+allComp?.getAt(priotityComp))
    def competitorSection = api.createConfiguratorEntry()
    competitorSection.createParameter(InputType.OPTION, "Competitor")
            .setValueOptions((allComp))
            .setValue(api.global.comp)
            .setRequired(true)
    return competitorSection
}
