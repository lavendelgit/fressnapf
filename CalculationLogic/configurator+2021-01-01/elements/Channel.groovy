// define a Section
def section = api.createConfiguratorEntry()

// add one dropdown input field
section.createParameter(InputType.OPTION, "Channel")
        .setValueOptions("Online", "Offline")
        .setRequired(true)

//return the Section definition. The logic Element containing this code must have DisplayMode set to Everywhere
return section