import net.pricefx.common.api.InputType

    if (input.Channel == "Offline") {
        /* when the value of input field "Meat" will not be "Beef", we will not build the "Weight" section neither the input field, so it will simply not appear on the screen at all. */
        def section = api.createConfiguratorEntry()
        section.createParameter(InputType.OPTION, "PurchasePriceConfiguration")
                .setValueOptions("PP Offline Price", "PP Retail Price")
                .setRequired(true)
        return section
    }
