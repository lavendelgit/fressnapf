import net.pricefx.common.api.InputType

if(input.Channel) {
    def currencySection = api.createConfiguratorEntry()
    currencySection.createParameter(InputType.OPTION, "Currency")
            .setValueOptions(api.findLookupTableValues("Currency")?.value)
            .setRequired(true)
    return currencySection
}