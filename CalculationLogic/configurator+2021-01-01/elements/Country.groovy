import net.pricefx.common.api.InputType

if(input.Channel) {
    def countrySection = api.createConfiguratorEntry()
    countrySection.createParameter(InputType.OPTION, "Country")
            .setValueOptions(api.findLookupTableValues("Countries")?.value)
            .setRequired(true)
    return countrySection
}