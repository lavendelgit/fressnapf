List findAll(Closure finder) {
    int startRow = 0
    List allRows = []
    int maxRowsLimit = api.getMaxFindResultsLimit()
    while (result = finder.call(startRow, maxRowsLimit)) {
        startRow += result.size()
        allRows.addAll(result)
    }

    return allRows
}

Map getCompetitionDataMaster() {
    return libs.SharedLib.CacheUtils.getOrSet("CompetitionDataMaster", {
        getAllCompetitionDataByChannel()
    }, true)
}

Map getCommodityGroupMaster() {
    return libs.SharedLib.CacheUtils.getOrSet("CommodityGroupMaster", {
        getCommodityGroup()
    }, true)
}

Map getBrandsMaster() {
    return libs.SharedLib.CacheUtils.getOrSet("BrandsMaster", {
        getBrands()
    }, true)
}

Map getBrandRoleMappingMaster(){
    return libs.SharedLib.CacheUtils.getOrSet("BrandRoleMapping",{
        getBrandRoleMapping()
    },true)
}
Map getCompetitorBasketMappingMaster(country) {
    return libs.SharedLib.CacheUtils.getOrSet("CompetitorBasketMappingMaster", {
        getCompetitorBasketMapping(country)
    }, true)
}

//Added CountryCode as a parameter : FRES:75
Map getPurchasePriceMaster(countryCode) {
    return libs.SharedLib.CacheUtils.getOrSet("PurchasePriceMaster", {
        getPurchasePriceFromPX(countryCode)
    }, true)
}

Map getSalesPriceMaster() {
    return libs.SharedLib.CacheUtils.getOrSet("SalesPriceMaster", {
        getSalesPriceFromPX()
    })
}

Map getGermanySalesPriceMaster() {
    return libs.SharedLib.CacheUtils.getOrSet("GermanySalesPriceMaster", {
        getGermanySalesPriceFromPX()
    })
}

Map getTargetPriceIndexMaster(country,cgrNo,channel) {
    return libs.SharedLib.CacheUtils.getOrSet("TargetPriceIndexMaster", {
        getTargetPriceIndex(country,cgrNo,channel)
    })
}

Object getPricingSegmentsMaster() {
    return libs.SharedLib.CacheUtils.getOrSet("PricingSegmentsMaster", {
        getPricingSegments()
    })
}

Map getMCChannelBasketMappingMaster(country) {
    return libs.SharedLib.CacheUtils.getOrSet("MCChannelBasketMappingMaster", {
        getMCChannelBasketMapping(country)
    })
}

Map getBasketKeyChannelMaster(country) {
    return libs.SharedLib.CacheUtils.getOrSet("BasketKeyChannelMaster", {
        getBasketKeyChannel(country)
    })
}

Map getIndexedPricesMaster() {
    return libs.SharedLib.CacheUtils.getOrSet("IndexedPricesMaster", {
        getIndexedPrices()
    })
}

Map getTripleNetFactorMaster() {
    return libs.SharedLib.CacheUtils.getOrSet("TripleNetFactorMaster", {
        getTripleNetFactor()
    })
}


Map getExchangeRatesMaster() {
    return libs.SharedLib.CacheUtils.getOrSet("ExchangeRatesMaster", {
        getExchangeRates()
    })
}

Map getUOMConversionMaster() {
    return libs.SharedLib.CacheUtils.getOrSet("UOMConversion", {
        getUOMConversion()
    })
}

List getRoundingSchemeMaster(String country) {
    return libs.SharedLib.CacheUtils.getOrSet("RoundingSchemeMaster", {
        getRoundingScheme(country)
    })
}

List getMaxAbsDeviationMaster(String country) {
    return libs.SharedLib.CacheUtils.getOrSet("MaxAbsDeviation", {
        getMaxAbsDeviation()
    })
}
Map getPricingFamilyMaster() {
    return libs.SharedLib.CacheUtils.getOrSet("PricingFamilyMaster", {
        getPricingFamily()
    })
}

Map getCompetitorMaster() {
    return libs.SharedLib.CacheUtils.getOrSet("CompetitorMaster", {
        getCompetitor()
    })
}


Map getSalesDistributionStoreMaster() {
    return libs.SharedLib.CacheUtils.getOrSet("SalesDistributionStoreMaster", {
        getSalesDistributionStore()
    })
}
/*
Map getArticleCountryAttributesMater() {
    return libs.SharedLib.CacheUtils.getOrSet("ArticleCountryAttributesMater", {
        getCompetitorBasketMapping()
    }, true)
}*/

List getBasketItemClassRulesMaster() {
    return libs.SharedLib.CacheUtils.getOrSet("BasketItemClassRulesMaster", {
        getBasketItemClassRules()
    }, true)
}

Map getHighInvolvementMaster() {
    return libs.SharedLib.CacheUtils.getOrSet("HighInvolvementMaster", {
        getHighInvolvement()
    })
}

Map getEmailConfigurationMaster() {
    return libs.SharedLib.CacheUtils.getOrSet("EmailConfiguration",{
        getEmailConfiguration()
    }, true)
}

Map getSellingPriceMaster(country) {
    return libs.SharedLib.CacheUtils.getOrSet("SellingPrice", {
        getSellingPrice(country)
    })
}

Map getCostPlusPricingMaster(country) {
    return libs.SharedLib.CacheUtils.getOrSet("CostPlusPricing", {
        getCostPlusMargin(country)
    })
}

Map getProductHierarchyMaster() {
    return libs.SharedLib.CacheUtils.getOrSet("ProductHierarchy", {
        getProductHierarchy()
    })
}

Map getPriceParameterMaster(String tableName) {
    return libs.SharedLib.CacheUtils.getOrSet(tableName, {
        getPriceParameter(tableName)
    })
}

Map getArticleCountryAttributesMaster(String country) {
    return libs.SharedLib.CacheUtils.getOrSet("CountryArticleAttributes", {
        getArticleCountryAttributesFromPX(country)
    })
}


List getMarketingCampaignsMaster(String country) {
    return libs.SharedLib.CacheUtils.getOrSet("MarketingCampaigns", {
        getMarketingCampaigns(country)
    })
}
Map getPRGMaster(String country) {
    return libs.SharedLib.CacheUtils.getOrSet("PRG", {
        getPRG(country)
    })
}

List getLeadTimesMaster() {
    return libs.SharedLib.CacheUtils.getOrSet("LeadTimes", {
        getLeadTimes()
    })
}


Map getArticleChannelMappingMaster(String country) {
    return libs.SharedLib.CacheUtils.getOrSet("Article Channel", {
        getArticleChannel(country)
    })
}

Map getPriceChangeThresholdsMaster(String country) {
    return libs.SharedLib.CacheUtils.getOrSet("PriceChangeThresholds", {
        getPriceChangeThresholds(country)
    })
}
Map getPricingGuardrailsMaster(String country) {
    return libs.SharedLib.CacheUtils.getOrSet("PricingGuardrails", {
        getPricingGuardrails(country)
    })
}
/**
 * Get all the purchasePrices from product extension.
 * @return
 * Added CountryCode as a parameter
 */
Map getPurchasePriceFromPX(countryCode) {
    List filters = [
            Filter.equal("name", libs.FressnapfLib.Constants.PurchasePrice_PX),
            Filter.equal("attribute5", countryCode)
    ]
    return libs.SharedLib.StreamUtils.stream("PX", "sku", filters, { stream ->
        stream?.collectEntries { row ->
            [
                    (row.sku): [
                            articles               : row.sku,
                            purchasePriceOnline    : row.attribute1,
                            purchaseCurrencyOnline : row.attribute2,
                            purchasePriceOffline   : row.attribute3,
                            purchaseCurrencyOffline: row.attribute4,
                            country                : row.attribute5,
                            purchasePriceRetail    : row.attribute6,
                            purchaseCurrencyRetail : row.attribute7
                    ]
            ]
        }
    })
}

/**
 * Get all the purchasePrices from product extension.
 * @return
 */
Map getSalesPriceFromPX() {
    List filters = [
            Filter.equal("name", libs.FressnapfLib.Constants.SalesPrice_PX),
            Filter.equal("attribute6", "Poland")
    ]
    return libs.SharedLib.StreamUtils.stream("PX6", "sku", filters, { stream ->
        stream?.collectEntries { row ->
            [
                    (row.sku): [
                            articles            : row.sku,
                            salesPriceOnline    : row.attribute1,
                            salesCurrencyOnline : row.attribute2,
                            salesPriceOffline   : row.attribute3,
                            salesCurrencyOffline: row.attribute4,
                            vat                 : row.attribute5,
                            country             : row.attribute6
                    ]
            ]
        }
    })
}

/**
 * Get all the purchasePrices from product extension.
 * @return
 */
Map getArticleCountryAttributesFromPX(String country) {
    List filters = [
            Filter.equal("name", libs.FressnapfLib.Constants.ArticleCountryAttributes_PX),
            Filter.equal("attribute7",country)
    ]
    return libs.SharedLib.StreamUtils.stream("PX20", "sku", filters, { stream ->
        stream?.collectEntries { row ->
            [
                    (row.sku + "_" + row.attribute1 + "_" + row.attribute7 ): [
                            article              : row.sku,
                            salesorg              : row.attribute1,
                            articlesStatusOverall : row.attribute2,
                            articlesStatus        : row.attribute3,
                            dChainSpecStatus      : row.attribute4,
                            disturbtionChannel    : row.attribute5,
                            purchaseOrg           : row.attribute6,
                            countryKey            : row.attribute7,
                            supplyMode            : row.attribute8,
                            supplyModeVTL         : row.attribute9,
                            VAT                   : row.attribute10,
                    ]
            ]
        }
    })
}


/**
 * Get all the GetCommodityGroup from price parameter.
 * @return
 */
Map getCommodityGroup() {
    return api.namedEntities(api.findLookupTableValues(libs.FressnapfLib.Constants.CommodityGroup_PP)?.findAll())?.collectEntries {
        row ->
            [
                    (row.CGRNo): [
                            CGRNo       : row.CGRNo,
                            description : row.CommodityGroupDescription,
                            cluster     : row.Cluster,
                            germanDescription : row.CommodityGroupDescriptionDE,
                            polandDescription : row.CommodityGroupDescriptionPL,
                            franceDescription : row.CommodityGroupDescriptionFR,
                            austriaDescription : row.CommodityGroupDescriptionAT,
                            belgiumDescription : row.CommodityGroupDescriptionBE,
                            hungaryDescription : row.CommodityGroupDescriptionHU,
                            italyDescription : row.CommodityGroupDescriptionIT,
                            luxemDescription : row.CommodityGroupDescriptionLU,
                            republicDescription : row.CommodityGroupDescriptionIE,
                            switzDescription : row.CommodityGroupDescriptionCH
                    ]
            ]
    }
}

/**
 * Get all the Brands from price parameter.
 * @return
 */
Map getBrands() {
    return api.findLookupTableValues(libs.FressnapfLib.Constants.Brand_PP)?.findAll()?.collectEntries {
        row ->
            [
                    (row.name): [
                            name : row.name,
                            value: row.value
                    ]
            ]
    }
}

Map getBrandRoleMapping() {
    return api.findLookupTableValues(libs.FressnapfLib.Constants.BrandRoleMapping_PP)?.findAll()?.collectEntries {
        row ->
            [
                    (row.key1+ "_" + row.key2 + "_" + row.key3): [
                            country: row.key1,
                            brand: row.key2,
                            subBrand: row.key3,
                            groceryDriven: row.attribute1,
                            priceStarter : row.attribute2,
                            vet : row.attribute3
                    ]
            ]
    }
}
/**
 * Get all the TargetPriceIndex from price parameter.
 * @return
 */
Map getTargetPriceIndex(country,cgrNo,channel) {
    filters =[
            Filter.equal("key1",country),
            Filter.equal("key2",cgrNo),
            Filter.equal("key3",channel)
    ]
    return api.findLookupTableValues(libs.FressnapfLib.Constants.TargetPriceIndex_PP,*filters)?.findAll()?.collectEntries {
        row ->
            [
                    (row.key4): row.attribute1
            ]
    }
}

/**
 * Get all the TargetPriceIndex from price parameter.
 * @return
 */
Map getAllCompetitionDataByChannel() {
    return libs.SharedLib.StreamUtils.stream("PCOMP", null, null, {
        stream ->
            def response = [:]
            stream?.each { it ->
                if (response[it.competitionType + "_" + it.sku?.replaceFirst("^0*", "")] == null) {
                    response[it.competitionType + "_" + it.sku?.replaceFirst("^0*", "")] = [:]
                }
                response[it.competitionType + "_" + it.sku?.replaceFirst("^0*", "")].put(it.competitor, it)
            }
            return response
    })
}

/*
online:[
array of map //article:[
compe1:[], comp2:[]
]
]
*/

/**
 * Get all the CompetitorBasketMapping from price parameter.
 * @return
 */
Map getCompetitorBasketMapping(country) {
    filters =[
            Filter.equal("key4",country)
    ]
    return api.namedEntities(api.findLookupTableValues(libs.FressnapfLib.Constants.CompetitorBasketMapping_PP,*filters))?.findAll()?.collectEntries {
        row ->
            [
                    (row.DistributionChannel + "_" + row.Basket + "_" + row.CompetitorNo): [
                            basket         : row.Basket,
                            competitorNo   : row.CompetitorNo,
                            competitorName : row.competitorName,
                            calculationMode: row.CalculationMode,
                            weight         : row.Weight,
                            prio           : row.Priority,
                            channel        : row.DistributionChannel
                    ]
            ]
    }
}

/**
 * Get all the PricingSegments from price parameter.
 * @return
 */
Object getPricingSegments() {
    /*List filters = [
            Filter.equal("name", libs.FressnapfLib.Constants.PricingSegments_PX),
    ]

    def pxTableName = "PricingSegments"
    def pxAttrs = ["sku"        : "country",
                   "attribute1" : "channel",
                   "attribute2" : "brandType",
                   "attribute3" : "brand",
                   "attribute4" : "subBrand",
                   "attribute5" : "merchandiseCategory",
                   "attribute6" : "subCategory",
                   "attribute7" : "category",
                   "attribute8" : "categoryType",
                   "attribute9" : "itemClass",
                   "attribute10": "basket",
                   "attribute11": "priceStrategy",
                   "attribute12": "pricingMethod1",
                   "attribute13": "pricingMethod2"]

    def inMemoryTableName = "PricingSegments"
    def fields = pxAttrs.keySet()
    def tableContext = api.getTableContext()

    tableContext.createTableFromProductExtension(inMemoryTableName, pxTableName, fields)

    def resultIterator = libs.SharedLib.StreamUtils.stream("PX", "sku", filters, { stream ->
        stream?.collect {
            row -> row
        }

    })

    if (resultIterator) {
        tableContext.loadRows(inMemoryTableName, resultIterator)
    }

    return tableContext*/
    List filters = [
            Filter.equal("name", libs.FressnapfLib.Constants.PricingSegments_PX),
    ]
    return libs.SharedLib.StreamUtils.stream("PX20", "sku", filters, { stream ->
        stream?.collect { row ->
            [
                    country            : row.sku,
                    channel            : row.attribute1,
                    brandType          : row.attribute2,
                    brand              : row.attribute3,
                    subBrand           : row.attribute4,
                    CGRNo              : row.attribute5,
                    subCategory        : row.attribute6,
                    category           : row.attribute7,
                    categoryType       : row.attribute8,
                    itemClass          : row.attribute9,
                    basket             : row.attribute10,
                    priceStrategy      : row.attribute11,
                    pricingMethod1     : row.attribute12,
                    pricingMethod2     : row.attribute13,

            ]
        }
    })
}

/**
 * Get all the MCChannelBasketMapping from price parameter.
 * @return
 */
Map getMCChannelBasketMapping(country) {
    filters =[
            Filter.equal("key1",country)
    ]
    return api.namedEntities(api.findLookupTableValues(libs.FressnapfLib.Constants.MCChannelBasketMapping_PP,*filters))?.findAll()?.collectEntries {
        row ->
            [
                    (row.Basket + "_" + row.CGRNo): row
            ]
    }
}

Map getGermanySalesPriceFromPX() {
    List filters = [
            Filter.equal("name", libs.FressnapfLib.Constants.GermanyPrices_PX),
    ]
    return libs.SharedLib.StreamUtils.stream("PX3", "sku", filters, { stream ->
        stream?.collectEntries { row ->
            [
                    (row.sku): [
                            articles: row.sku,
                            price   : row.attribute1,
                            currency: row.attribute2
                    ]
            ]
        }
    })
}

Map getBasketKeyChannel(country) {
    List filters = [
            Filter.equal("key1", country),
    ]
    return api.namedEntities(api.findLookupTableValues(libs.FressnapfLib.Constants.BasketKeyChannel_PP, *filters))?.findAll()?.collectEntries {
        row ->
            [
                    (row?.basket): row
            ]
    }
}

Map getIndexedPrices() {
    List filters = [
            Filter.equal("country", "Poland"),
    ]
    return api.namedEntities(api.findLookupTableValues(libs.FressnapfLib.Constants.IndexedPrices_PP, *filters))?.findAll()?.collectEntries {
        row ->
            [
                    (row?.CGRNo): row
            ]
    }
}

Map getTripleNetFactor() {
    List filters = [
            Filter.equal("Country", "Germany"),
    ]
    return api.namedEntities(api.findLookupTableValues(libs.FressnapfLib.Constants.TripleNetFactor_PP, *filters))?.findAll()?.collectEntries {
        row ->
            [
                    (row?.Channel + "_" + row?.Brand + "_" + row?.SubBrand): row
            ]
    }
}

Map getExchangeRates() {
    return api.namedEntities(api.findLookupTableValues(libs.FressnapfLib.Constants.ExchangeRates_PP, "date"))?.findAll()?.collectEntries {
        row ->
            [
                    (row?.fromCurrency + "_" + row?.toCurrency): row
            ]
    }
}

Map getUOMConversion() {
    return api.namedEntities(api.findLookupTableValues(libs.FressnapfLib.Constants.UOMConversion_PP))?.findAll()?.collectEntries {
        row ->
            [
                    (row?.FromUnit + "_" + row?.ToUnit): row
            ]
    }
}
List getRoundingScheme( String country) {
    List filters = [
            Filter.equal("Country", country),
    ]
    return api.namedEntities(api.findLookupTableValues(libs.FressnapfLib.Constants.RoundingScheme_PP, *filters))?.findAll()?.collect {
        row -> row
    }
}

List getMaxAbsDeviation() {

    return api.namedEntities(api.findLookupTableValues(libs.FressnapfLib.Constants.MaxAbsDeviation_PP))?.findAll()?.collect {
        row -> row
    }
}

Map getPricingFamily() {
    return api.findLookupTableValues("PriceFamily")?.findAll()?.collectEntries {
        row ->
            [
                    (row.name): [
                            name : row.name,
                            value: row.value
                    ]
            ]
    }
}


/**
 * Get all the Competitor from price parameter.
 * @return
 */
Map getCompetitor() {
    return api.namedEntities(api.findLookupTableValues(libs.FressnapfLib.Constants.Competitor_PP))?.findAll()?.collectEntries {
        row ->
            [
                    (row?.WTBID + "_" + row?.CompetitorName) : row
            ]
    }
}

/**
 * Get all the Sales and Distribution store from price parameter.
 * @return
 */
Map getSalesDistributionStore() {
    return api.namedEntities(api.findLookupTableValues(libs.FressnapfLib.Constants.SaleDistributionStore_PP))?.findAll()?.collectEntries {
        row ->
            [
                    (row?.Country + "_" + row?.Storenumber) : row
            ]
    }
}


/**
 * Get all Basket and item Class Rule from price parameter.
 * @return
 */
List getBasketItemClassRules() {

    return api.namedEntities(api.findLookupTableValues(libs.FressnapfLib.Constants.BasketItemClassRule_PP, "Basket", null)?.findAll())
}

/**
 * Get HighInvolvement from price parameter.
 * @return
 */

Map getHighInvolvement() {
    return api.namedEntities(api.findLookupTableValues(libs.FressnapfLib.Constants.HighInvolvement_PP, null)?.findAll())?.collectEntries {
        row ->
            [
                    (row.country): [
                            sellingPriceThreshold: row.sellingPriceThreshold,
                            country              : row.country,
                            sptCurrency          : row.sptCurrency,
                    ]
            ]
    }
}

Map getEmailConfiguration(){
    return api.findLookupTableValues(libs.FressnapfLib.Constants.EmailConfiguration_PP)?.collectEntries { row->
        [(row.name):row.attribute1]
    }
}


Map getSellingPrice(country){
    List filters = [
            Filter.equal("name",libs.FressnapfLib.Constants.ArticleSellingPrice_PX),
            Filter.equal("attribute1",country)
    ]
    return libs.SharedLib.StreamUtils.stream("PX20", "sku", filters, { stream ->
        stream?.collect { row ->
            [
                    articles            : row.sku,
                    salesPrice          : row.attribute8,
                    country             : row.attribute1,
                    salesOrg            : row.attribute2,
                    currency            : row.attribute9
            ]
        }
    })?.groupBy{it.articles}
}

Map getCostPlusMargin(country){
    List filters = [
            Filter.equal("name",libs.FressnapfLib.Constants.CostPlusPricingMargin_PX),
            Filter.equal("sku",country)
    ]
    return libs.SharedLib.StreamUtils.stream("PX10", "sku", filters, { stream ->
        stream?.collectEntries { row ->
            [(row.sku+"_"+row.attribute1+"_"+row.attribute2+"_"+row.attribute3+"_"+row.attribute4+"_"+row.attribute9+"_"+row.attribute5+"_"+row.attribute6): [
                "TargetMargin" : row.attribute7,
                "StretchMargin" : row.attribute8
            ]]
        }
    })
}

Map getProductHierarchy(){
    return api.findLookupTableValues(libs.FressnapfLib.Constants.ProductHierarchy_PP)?.collectEntries { row->
        [(row.name):["Category":row.attribute3,"SubCategory":row.attribute4]]
    }
}

Map getPriceParameter(String tableName){
    return api.findLookupTableValues(tableName)?.collectEntries { row->
        [(row.name):row.value]
    }
}

Map getPRG(String country) {

    return api.findLookupTableValues(libs.FressnapfLib.Constants.PRG_PP,Filter.equal("key1",country))?.findAll()?.collectEntries {
        row ->
            [
                    (row.key1+ "_" + row.key2 + "_" + row.key3) :[
                            "Article":row.key1,
                            "BaseFamily":row.key2,
                            "RelatedFamily":row.key3,
                            "FactorPct":row.attribute1,
                            "FactorAbs":row.attribute2]
            ]
    }
}

List getMarketingCampaigns(String country){
    List filters = [
            Filter.equal("name",libs.FressnapfLib.Constants.MarketingCampaigns_PX),
            Filter.equal("attribute6",country)
    ]
    return libs.SharedLib.StreamUtils.stream("PX8", "sku", filters, { stream ->
        stream?.collect { row ->
            [
                    article            : row.sku,
                    adType              : row.attribute1,
                    eventId             : row.attribute2,
                    eventName           : row.attribute3,
                    activeFrom          : row.attribute4,
                    activeTo            : row.attribute5
            ]
        }
    })
}

List getLeadTimes() {

    return api.namedEntities(api.findLookupTableValues(libs.FressnapfLib.Constants.LeadTimes_PP)?.findAll())
}


Map getArticleChannel (String country) {
    List filters = [
            Filter.equal("name", libs.FressnapfLib.Constants.ArticleChannelMapping_PX),
            Filter.equal("attribute1",country)
    ]
    return libs.SharedLib.StreamUtils.stream("PX10", "sku", filters, { stream ->
        stream?.collectEntries { row ->
            [
                    (row.sku):row.attribute2
            ]
        }
    })
}


Map getPriceChangeThresholds(String country) {

    return api.findLookupTableValues(libs.FressnapfLib.Constants.PriceChangeThresholds_PP,Filter.equal("key1",country))?.findAll()?.collectEntries {
        row ->
            [
                    (row.key1+ "_" + row.key2 + "_" + row.key3) :[
                            "Country":row.key1,
                            "Channel":row.key2,
                            "Basket":row.key3,
                            "MaxChangePct":row.attribute1,
                            "MinChangeAbs":row.attribute2,
                            "PriceChangeFrequency":row.attribute3,
                            "AutomaticPricePct":row.attribute4,
                            "MaxPriceChangeNum":row.attribute5]
            ]
    }
}

Map getPricingGuardrails(String country) {
    List filters = [
            Filter.equal("name", libs.FressnapfLib.Constants.PricingGuardrails_PX),
            Filter.equal("sku",country)
    ]
    return libs.SharedLib.StreamUtils.stream("PX20", "sku", filters, { stream ->
        stream?.collectEntries { row ->
            [
                    (row.sku + "_" + row.attribute1 + "_" + row.attribute2+ "_" + row.attribute3+ "_" + row.attribute4+ "_" + row.attribute5+ "_" + row.attribute6+ "_" + row.attribute7 ): [
                            Country              : row.sku,
                            Channel              : row.attribute1,
                            Category             : row.attribute2,
                            SubCategory          : row.attribute3,
                            CGRNo                : row.attribute4,
                            BrandType            : row.attribute5,
                            Brand                : row.attribute6,
                            SubBrand             : row.attribute7,
                            LegalMinMargin       : row.attribute8,
                            GuardRailsMinMargin  : row.attribute9,
                            ElasticityMinMargin  : row.attribute10,
                            ElasticityMaxMargin  : row.attribute10,
                    ]
            ]
        }
    })
}
