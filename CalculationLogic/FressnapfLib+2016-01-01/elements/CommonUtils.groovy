BigDecimal nvl(value) {
    return (value ?: 0.0) as BigDecimal
}

Date parseDate(String date) {
    return api.parseDate("yyyy-MM-dd", date)
}

BigDecimal applyExchangeRate(BigDecimal value, String sourceCurrency, String targetCurrency, Date pricingDate) {
    api.local.priceBuildupMatrix.ExchangeRate = api.local.priceBuildupMatrix.ExchangeRate?api.local.priceBuildupMatrix.ExchangeRate:[:]
    def price = value
    exchangeRates = libs.FressnapfLib.DataUtils.getExchangeRatesMaster()
    exchangeRate = exchangeRates?.findAll { it.key == sourceCurrency + "_" + targetCurrency }?.values()?.getAt(0)?.currencyExchangeRate
    if (exchangeRate && sourceCurrency!=targetCurrency) {
        api.local.priceBuildupMatrix.ExchangeRate <<[(sourceCurrency+" - "+targetCurrency) : exchangeRate]
        price = exchangeRate * value
    }
    return price
}

BigDecimal applyUnitConversion(BigDecimal value, String formUnit, String toUnit) {
    api.local.priceBuildupMatrix.appliedConversionFactor = api.local.priceBuildupMatrix.appliedConversionFactor?api.local.priceBuildupMatrix.appliedConversionFactor:[:]
    def netContent = value
    conversionFactor = libs.FressnapfLib.DataUtils.getUOMConversionMaster()
    conversionFactor = conversionFactor?.findAll { it.key == formUnit + "_" + toUnit }?.values()?.getAt(0)?.ConversionFactor
    if (conversionFactor && formUnit!=toUnit) {
        api.local.priceBuildupMatrix.appliedConversionFactor[formUnit+" - "+toUnit] = conversionFactor
        netContent = conversionFactor * netContent
    }
    return netContent
}

boolean asBoolean(String boolStr) {
    return boolStr == 'Y' ? true : false
}
