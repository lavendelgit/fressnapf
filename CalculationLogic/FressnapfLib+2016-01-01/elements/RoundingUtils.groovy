BigDecimal roundNumber(BigDecimal number, int decimalPlace) {
    return libs.SharedLib.RoundingUtils.round(number, decimalPlace)
}

/**
 * Rounding the Decimal digit point to two decimals.
 * @param decimal value
 * @return
 */
BigDecimal round(BigDecimal value) {
    return Math.round(value * 100) / 100
}
