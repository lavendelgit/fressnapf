import groovy.transform.Field

@Field final String PurchasePrice_PX = "PurchasePrice"
@Field final String SalesPrice_PX = "SalesPrice"
@Field final String Brand_PP = "Brand"
@Field final String CommodityGroup_PP = "CommodityGroup"
@Field final String TargetPriceIndex_PP = "TargetPriceIndex"
@Field final String BasketItemClassRule_PP = "BasketItemClassRules"
@Field final String HighInvolvement_PP = "HighInvolvement"
@Field final String EmailConfiguration_PP = "EmailConfiguration_BasketItemClassfail"
@Field final String CompetitionTypeOnline = "Online"
@Field final String CompetitionTypeStationary = "Stationary"
@Field final String CompetitorBasketMapping_PP = "CompetitorBasketMapping"
@Field final String PricingSegments_PX = "PricingSegments"
@Field final String PriceFamilies_PX = "PriceFamilies"
@Field final String MCChannelBasketMapping_PP = "DistancetoKeyChannel"
@Field final String GermanyPrices_PX = "GermanyPrices"
@Field final String BasketKeyChannel_PP = "BasketKeyChannel"
@Field final String IndexedPrices_PP = "IndexedPrices"
@Field final String ExchangeRates_PP = "ExchangeRates"
@Field final String RoundingScheme_PP = "RoundingScheme"
@Field final String MaxAbsDeviation_PP = "MaxAbsDeviation"
@Field final String PRG_PP = "PRG"
@Field final String LeadTimes_PP = "LeadTimes"
@Field final String MarketingCampaigns_PX = "MarketingCampaigns"
@Field final String TripleNetFactor_PP = "TripleNetFactor"
@Field final Integer ItemClassBasketUpdate_CFS_ID = 69
@Field final String Competitor_PP = "Competitor"
@Field final String SaleDistributionStore_PP = "SaleandDistributionStore"
@Field final String ArticleCountryAttributes_PX = "ArticleCountryAttributes"
@Field final String BrandRoleMapping_PP = "BrandRoleMapping"
@Field final String CostPlusPricingMargin_PX = "CostPlusMargins"
@Field final String ProductHierarchy_PP = "ProductHierarchy"
//FRES-75 changes : Begin
@Field final String Countries_PP = "Countries"
@Field final String ArticleChannelMapping_PX = "ArticleChannelMapping"
@Field final String Currency_PP = "Currency"
@Field final String ItemClassBasketMapping_PX = "ItemClassBasketMapping"
@Field final String CountrySalesOrgMapping_PP = "CountrySalesOrgMapping"
@Field final String ArticleSellingPrice_PX = "ArticleSellingPrice"
@Field final String ArticleDescription_PX = "ArticleDescription"
@Field final String PriceChangeThresholds_PP = "PriceChangeThresholds"
@Field final String PricingGuardrails_PX = "PricingGuardrails"
@Field final String UOMConversion_PP = "UOM"
//FRES-75 changes : End
@Field final String SetandTrayDiscounts_PP = "SetandTrayDiscounts"
@Field final String SetTrayPurchasePrices_PX = "SetTrayPurchasePrices"
@Field Map SYMBOLS = [
        DECREASE: "↓",
        INCREASE: "↑",
        FLAT    : ""
]

@Field Map TEXT_COLORS = [
        DECREASE: "white",
        INCREASE: "green",
        FLAT    : null
]

@Field Map BG_COLORS = [
        DECREASE: "#FA8072",
        INCREASE: null,
        FLAT    : null
]

@Field Map SERIES_COLOR = [
        Online : [
                NEGATIVE: "#D0021B",
                POSITIVE: "#4A90E2",
        ],
        Offline: [
                NEGATIVE: "#F5A623",
                POSITIVE: "#4A4A4A",
        ]
]