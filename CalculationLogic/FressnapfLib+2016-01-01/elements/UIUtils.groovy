import net.pricefx.server.dto.calculation.AttributedResult

AttributedResult getArrowedCell(BigDecimal numberValue) {
    return getArrowedCell(numberValue, null)
}

AttributedResult getArrowedCell(BigDecimal numberValue, String suffix) {
    return getArrowedCell(numberValue, numberValue ? (numberValue < 0 ? -1 : 1) : 0, suffix)
}

AttributedResult getArrowedCell(BigDecimal numberValue, int equals, String suffix) {
    def lib = libs.FressnapfLib
    String compareCode = numberValue ? (numberValue < 0 ? 'DECREASE' : 'INCREASE') : 'FLAT'
    suffix = (suffix ? " " + suffix : "") + lib.Constants.SYMBOLS[compareCode]
   return api.attributedResult(lib.RoundingUtils.roundNumber(numberValue, 4))
              .withTextColor(lib.Constants.TEXT_COLORS[compareCode])
              .withSuffix(suffix)
              .withBackgroundColor(lib.Constants.BG_COLORS[compareCode])
}