Map buildColumnSeries(String seriesName,
                      String seriesLabel,
                      BigDecimal purchasePrice,
                      BigDecimal salesPrice,
                      BigDecimal calculatedListPrice,
                      BigDecimal listPrice) {
    def commonUtils = libs.FressnapfLib.CommonUtils
    def SERIES_COLOR = libs.FressnapfLib.Constants.SERIES_COLOR
    return [
            name         : seriesName,
            label        : seriesLabel,
            data         : [
                    commonUtils.nvl(purchasePrice),
                    commonUtils.nvl(salesPrice) - commonUtils.nvl(purchasePrice),
                    commonUtils.nvl(salesPrice),
                    commonUtils.nvl(calculatedListPrice) - commonUtils.nvl(salesPrice),
                    commonUtils.nvl(calculatedListPrice),
                    commonUtils.nvl(listPrice) - commonUtils.nvl(calculatedListPrice),
                    commonUtils.nvl(listPrice)
            ],
            color        : SERIES_COLOR[seriesName]?.POSITIVE,
            negativeColor: SERIES_COLOR[seriesName]?.NEGATIVE
    ]
}


Map getPriceComparisionColumnChartDef(List seriesData,
                                      String currency,
                                      String chartTitle) {
    return [
            chart  : [
                    type: 'column'
            ],
            title  : [
                    text: chartTitle
            ],
            credits: [
                    enabled: false
            ],
            xAxis  : [
                    categories: [
                            'Purchase Price',
                            'Price Adjustment',
                            'Sales Price',
                            'Competitor Price Adjustment',
                            'Competitor Based List Price',
                            'Price Adjustment',
                            'List Price'
                    ],
                    labels    : [
                            style: [
                                    fontSize: '8px'
                            ]
                    ]
            ],
            yAxis  : [
                    title: [
                            text: currency
                    ]
            ],
            legend : [
                    layout       : 'horizontal',
                    verticalAlign: 'bottom',
                    style        : [
                            fontSize: '8px'
                    ]
            ],
            tooltip: [
                    shared     : true,
                    valueSuffix: ' ' + currency
            ],
            series : seriesData
    ]
}