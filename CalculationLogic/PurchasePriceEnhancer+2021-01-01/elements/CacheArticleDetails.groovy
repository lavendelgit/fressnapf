api.local.pid = out.Article
api.global.batch = [:]
api.global.purchasePriceData

List filter = [
        Filter.equal("name",libs.FressnapfLib.Constants.PurchasePrice_PX),
        Filter.equal("sku",out.Article),
        Filter.equal("attribute5",out.Country)
]
cacheRecords = api.find("PX10",0,"-attribute8",*filter)
if(cacheRecords?.size()>1){
    return cacheRecords
}
return
