List comp = []
int start = 0
while (items = api.find("PCOMP", start, null, null)) {
    start += items.size()
    comp.addAll(items)
}

def competitor = api.namedEntities(api.findLookupTableValues(libs.FressnapfLib.Constants.CompetitorBasketMapping_PP))?.findAll()?.collectEntries {
    row ->
        [
                (row.competitorNo): [
                        competitorNo  : row.competitorNo,
                        competitorName: row.competitorName,
                        weight        : row.weight,
                ]
        ]
}

def target = api.getDatamartRowSet("target")

for (result in comp) {
    Map row = [
            "sku"             : result?.sku,
            "CompetitionType" : result?.competitionType,
            "Competitor"      : result?.competitor,
            "CompetitorName"  : competitor?.getAt(result?.competitor)?.competitorName,
            "Price"           : result?.price,
            "Currency"        : result?.currency,
            "Weightage"       : competitor?.getAt(result?.competitor)?.weight,
            "CompetitiveIndex": result?.additionalInfo2,

    ]
    if (row.sku != null) {
        target?.addRow(row)
    }
}
