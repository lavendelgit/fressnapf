import groovy.transform.Field

@Field PER_LEVEL_BINDINGS = "___WF_APPROVABLE_PER_LEVEL_BINDINGS___"

/**
 * Enhancing approvable list with additional variable.
 * @param vars variable value map. The structure of map is:
 *  [variableName: [filterValueAsKey: variableValue]
 * @param varLookupConfigs variables lookup configurations
 * @param approvables
 * @return enhanced approvables
 * */
List enhanceApprovable(Map vars, List varLookupConfigs, List approvables) {
    Map tblConfig = libs.ApprovalWorkflow.ConfigurationManager.TABLE_CONFIG
    Map lookupConfigCol = tblConfig.lookupConfigTable.columns
    if (vars && approvables && varLookupConfigs) {
        Map lookupConfigByVariable = varLookupConfigs.groupBy { it[lookupConfigCol.variableName] }
        Map varPerApprovableItemMap
        String key
        approvables.each { Map approvable ->
            varPerApprovableItemMap = [:]
            vars.each { String varName, Map varMap ->
                key = getPerVarValueGroupByKey(approvable, lookupConfigByVariable[varName], lookupConfigCol.lookupValue, true)
                varPerApprovableItemMap << [(varName): varMap?.getAt(key)]
            }
            approvable << varPerApprovableItemMap
        }
    }

    return approvables
}

/**
 * Build groupBy key using filter value from input record
 * @param record record map to get filter value
 * @param perVarLookupConfigs lookup config list for a variable, each item contains filter config
 * @param configKey Key on lookup config define which key to get on record map
 * @return key values on record map join by configured separator
 * */
String getPerVarValueGroupByKey(Map record, List perVarLookupConfigs, String configKey, boolean valueProvider = false) {
    Map consts = libs.ApprovalWorkflow.ConfigurationManager.CONST
    String key
    List keys = []
    perVarLookupConfigs.each {
        key = it?.getAt(configKey)
        keys << (valueProvider ? getApprovaleObjectValue(it, record) : record?.getAt(key))
    }
    return keys.join(consts.GROUP_BY_KEY_SEPARATOR)
}

/**
 * Lookup variable value
 * @param varConfigs
 * @param varLookupConfigs
 * @param approvables
 * @return variable value map
 * */
Map lookupVariableValue(List varConfigs, List varLookupConfigs, List approvables) {
    if (varConfigs && varLookupConfigs && approvables) {
        Map tblConfig = libs.ApprovalWorkflow.ConfigurationManager.TABLE_CONFIG
        Map varConfigCol = tblConfig.variableConfigTable.columns
        Map records = [:]
        List recordsByType
        Map recordsGroup
        List lookupConfigFiltered
        Map varLookupConfigGroup
        varConfigs.groupBy { it?.getAt(varConfigCol.variableLookupType) }
                  .each { type, configsByType ->
                      lookupConfigFiltered = getLookupConfigByVariableList(configsByType, varLookupConfigs)
                      varLookupConfigGroup = groupLookupConfigByTableNameAndVariable(lookupConfigFiltered)
                      recordsByType = lookup(type, configsByType, varLookupConfigGroup, approvables)
                      recordsGroup = groupLookupResultByTypeAndName(recordsByType)
                      records << mapVariableValue(configsByType, lookupConfigFiltered, recordsGroup)
                  }
        return records
    }
    return [:]
}

/**
 * Group variable lookup result by type and name
 * */
Map groupLookupResultByTypeAndName(List records) {
    Map consts = libs.ApprovalWorkflow.ConfigurationManager.CONST
    return records?.groupBy({ it[consts.LOOKUP_TYPE_KEY] }, { it[consts.LOOKUP_NAME_KEY] })
}

/**
 * Map variable value by variable name
 * */
Map mapVariableValue(List varConfigs, List lookupConfig, Map records) {
    Map tblConfig = libs.ApprovalWorkflow.ConfigurationManager.TABLE_CONFIG
    Map varConfigCol = tblConfig.variableConfigTable.columns
    Map lookupConfigCol = tblConfig.lookupConfigTable.columns
    Map lookupConfigByVariable = lookupConfig?.groupBy { it?.getAt(lookupConfigCol.variableName) }
    Map perVariableValueMap
    List perVarLookupConfigs
    String varName

    return varConfigs?.collectEntries { perVarConfig ->
        varName = perVarConfig?.getAt(varConfigCol.variableName)
        perVarLookupConfigs = lookupConfigByVariable?.getAt(varName)
        perVariableValueMap = getPerVariableValue(perVarConfig, perVarLookupConfigs, records)
        return [varName, perVariableValueMap]
    } ?: [:]
}

/**
 * Get variable values map for each variable config
 * */
Map getPerVariableValue(Map perVarConfig, List perVarLookupConfigs, Map records) {
    Map tblConfig = libs.ApprovalWorkflow.ConfigurationManager.TABLE_CONFIG
    Map varConfigCol = tblConfig.variableConfigTable.columns
    Map lookupConfigCol = tblConfig.lookupConfigTable.columns
    String key
    Object value
    String variableValue = perVarConfig?.getAt(varConfigCol.variableValue)
    return getPerVarResultList(perVarConfig, records)?.collectEntries {
        key = getPerVarValueGroupByKey(it, perVarLookupConfigs, lookupConfigCol.lookupKey)
        value = castValue(it?.getAt(variableValue), perVarConfig?.getAt(varConfigCol.variableType))
        return [key, value]
    }
}

/**
 * Get variable result list per variable by lookup type and name
 * */
List getPerVarResultList(Map perVarConfig, Map records) {
    Map tblConfig = libs.ApprovalWorkflow.ConfigurationManager.TABLE_CONFIG
    Map lookupTypes = libs.ApprovalWorkflow.ConfigurationManager.LOOKUP_TYPE
    Map varConfigCol = tblConfig.variableConfigTable.columns
    String lookupType = perVarConfig?.getAt(varConfigCol.variableLookupType)
    String lookupName = lookupType in [lookupTypes.P, lookupTypes.C] ? lookupType : perVarConfig?.getAt(varConfigCol.variableLookupTable)
    return records?.getAt(lookupType)
                  ?.getAt(lookupName)
}

/**
 * Perform PP type lookup
 * */
List lookup(String type, List varConfigs, Map varLookupConfigs, List approvables) {
    Set lookupTables = getLookupTableFromVariableConfig(varConfigs)
    Map lookupType = libs.ApprovalWorkflow.ConfigurationManager.LOOKUP_TYPE
    Map cachingKey = libs.ApprovalWorkflow.ConfigurationManager.CACHING_KEY
    List records = []
    List queryData
    List filters
    Filter filter
    String lookupResultCacheKey
    List data
    Map lookupTableDefinitions = getLookupTableDefinitions(type, lookupTables)
    lookupTables?.each { table ->
        validateLookupTableExisting(type, table, lookupTableDefinitions)
        filter = getLookupFilterPerTable(varLookupConfigs?.getAt(table), approvables)
        if (filter) {
            filters = []
            filters << filter
            lookupResultCacheKey = [cachingKey.variableLookupResultCachePrefix, type, table, filter.hashCode()].join("_")
            queryData = libs.SharedLib.CacheUtils.getOrSet(lookupResultCacheKey, []) {
                switch (type) {
                    case lookupType.PP:
                        data = api.findLookupTableValues(table, *filters)?.collect {
                            appendTableNameAndType(it, table, lookupType.PP)
                        }
                        break
                    default:
                        boolean extensionLookup = (type in [lookupType.PX, lookupType.CX])
                        String sortBy
                        if (extensionLookup) {
                            filters << Filter.equal("name", table)
                            sortBy = "name"
                        }
                        String nameAttr
                        Closure resultCollector = { stream ->
                            stream.collect {
                                nameAttr = extensionLookup ? it?.name : type
                                return appendTableNameAndType(it, nameAttr, type)
                            }
                        }
                        data = libs.SharedLib.StreamUtils.stream(type, sortBy, filters, resultCollector)
                        break
                }
                return data
            }
            records.addAll(queryData ?: [])
        }
    }

    return records
}

void validateLookupTableExisting(String type, String lookupName, Map lookupDefinitions) {
    def configManager = libs.ApprovalWorkflow.ConfigurationManager
    Map types = configManager.LOOKUP_TYPE
    Map errors = configManager.ERROR_MESSAGE
    lookupName = (type in [types.P, types.C]) ? type : lookupName
    if (!lookupDefinitions.getAt(lookupName)) {
        api.throwException(sprintf(errors.lookupTableNotExist, type, lookupName))
    }
}

Map getLookupTableDefinitions(String type, Collection lookupTableNames) {
    Map types = libs.ApprovalWorkflow.ConfigurationManager.LOOKUP_TYPE
    Map definitions
    type = type?.toUpperCase()
    switch (type) {
        case types.PX:
        case types.CX:
            definitions = getExtensionDefinitions(type, lookupTableNames)
            break
        case types.PP:
            definitions = getPPTableDefinitions(lookupTableNames)
            break
        default:
            definitions = [(type): type]
            break
    }

    return definitions
}

Map getExtensionDefinitions(String type, Collection extensionNames) {
    final String productExtension = "productextension"
    final String customerExtension = "customerextension"
    Map types = libs.ApprovalWorkflow.ConfigurationManager.LOOKUP_TYPE
    String lookupType = types.PX.equalsIgnoreCase(type) ? productExtension : customerExtension
    String configString = api.find(types.AP, 0, 1, null, Filter.equal("uniqueName", lookupType))
                             ?.getAt(0)
                             ?.value
    Map configs = configString ? api.jsonDecode(configString) : [:]

    return configs.subMap(extensionNames)
}

Map getPPTableDefinitions(Collection ppNames) {
    def resultLimit = api.getMaxFindResultsLimit()
    Map types = libs.ApprovalWorkflow.ConfigurationManager.LOOKUP_TYPE
    List definitions = api.find(types.LT, 0, resultLimit, "id", Filter.in("uniqueName", ppNames))

    return definitions?.collectEntries { [it.uniqueName, it] } ?: [:]
}

/**
 * Build lookup filter per table
 * */
Filter getLookupFilterPerTable(Map perTableLookupConfig, List approvables) {
    Map tblConfig = libs.ApprovalWorkflow.ConfigurationManager.TABLE_CONFIG
    Map columns = tblConfig.lookupConfigTable.columns
    Set filters = []
    Set perVarFilters
    Set values
    String property
    String operator
    perTableLookupConfig?.each { String varName, List lookupConfigs ->
        perVarFilters = []
        lookupConfigs?.each { lookup ->
            values = approvables?.collect { approvable -> getApprovaleObjectValue(lookup, approvable) }
            property = lookup?.getAt(columns.lookupKey)

            if ((values != null) && property) {
                perVarFilters << buildFilter(property, values)
            }
        }

        if (perVarFilters) {
            filters.add(Filter.and(*(perVarFilters as List)))
        }
    }

    return filters ? Filter.or(*(filters as List)) : null
}

/**
 * Get value from approvale map
 * */
Object getApprovaleObjectValue(Map lookupConfig, Map approvable) {
    Map tblConfig = libs.ApprovalWorkflow.ConfigurationManager.TABLE_CONFIG
    Map columns = tblConfig.lookupConfigTable.columns
    String providerName = lookupConfig?.getAt(columns.valueProvider)
    String lookupValue = lookupConfig?.getAt(columns.lookupValue)

    return providerName ? getValueFromProvider(providerName, lookupConfig, approvable) : approvable?.getAt(lookupValue)
}

/**
 * Group lookup config
 * */
Map groupLookupConfigByTableNameAndVariable(List varLookupConfigs) {
    Map tblConfig = libs.ApprovalWorkflow.ConfigurationManager.TABLE_CONFIG
    Map lookupConfigCol = tblConfig.lookupConfigTable.columns
    return varLookupConfigs?.groupBy({
                                         it?.getAt(lookupConfigCol.lookupTable)
                                     }, {
                                         it?.getAt(lookupConfigCol.variableName)
                                     })
}
/**
 * Get lookup config for list of variable
 * */
List getLookupConfigByVariableList(List varConfigs, List varLookupConfigs) {
    Map tblConfig = libs.ApprovalWorkflow.ConfigurationManager.TABLE_CONFIG
    Map varConfigCol = tblConfig.variableConfigTable.columns
    Map lookupConfigCol = tblConfig.lookupConfigTable.columns
    Map availableVariable = varConfigs?.groupBy { it?.getAt(varConfigCol.variableName) }
    String varName
    return varLookupConfigs?.findAll { lookupConfig ->
        varName = lookupConfig?.getAt(lookupConfigCol.variableName)
        return availableVariable?.getAt(varName)
    }
}

/**
 * Get lookup table name from config list
 * */
private Set getLookupTableFromVariableConfig(List varConfigs) {
    Map tblConfig = libs.ApprovalWorkflow.ConfigurationManager.TABLE_CONFIG
    Map varConfigCol = tblConfig.variableConfigTable.columns
    return varConfigs?.collect { it?.getAt(varConfigCol.variableLookupTable) } as Set
}

/**
 * Append table name and type to result record
 * */
private Map appendTableNameAndType(Map record, String tblName, String type) {
    String nameKey = libs.ApprovalWorkflow.ConfigurationManager.CONST.LOOKUP_NAME_KEY
    String typeKey = libs.ApprovalWorkflow.ConfigurationManager.CONST.LOOKUP_TYPE_KEY
    record << [(nameKey): tblName]
    record << [(typeKey): type]
    return record
}

/**
 * Cast value to expected type
 * */
private castValue(Object value, String type) {
    type = type ?: "BIGDECIMAL"
    switch (type.toUpperCase()) {
        case "BIGDECIMAL":
        case "NUMBER":
            return value as BigDecimal
        case "STRING":
            return value as String
        default:
            return value
    }
}

/*****************Common Utilities****************************/


Object getValueFromProvider(String providerName, Map lookupConfig, Map approvable) {
    def configManager = libs.ApprovalWorkflow.ConfigurationManager
    def providerPath = configManager.VALUE_PROVIDER[providerName] ?: providerName
    if (!providerPath) {
        api.throwException(configManager.ERROR_MESSAGE
                                        .invalidProvider)
    }
    return configManager.runScript(providerPath, lookupConfig, approvable)
}

protected Filter buildFilter(String property, Object value, String operator = null) {
    if (value instanceof Filter) {
        return value
    }
    def defaultEqual = "OP_EQUAL"
    def defaultIn = "OP_IN"
    operator = operator ?: (value instanceof Collection ? defaultIn : defaultEqual)
    operator = operator?.trim()
                       ?.toUpperCase()
    try {
        def operatorCode = Filter[operator]
        return new Filter(property, value, operatorCode)
    }
    catch (e) {
        def configManager = libs.ApprovalWorkflow.ConfigurationManager
        String error = sprintf(configManager.ERROR_MESSAGE
                                            .invalidFilterOperator, operator)
        api.throwException(error)
    }

}


List prepareApprovableItems(def approvableObject) {
    def approvable = approvableObject
    List result = []
    Map lineMap
    rootEnhance(approvable)
    Map rootEnhanced = approvable.remove(PER_LEVEL_BINDINGS) ?: [:]
    if (approvable.lineItems) {
        Map lineItems = approvable.remove("lineItems")
                                  .collectEntries { [it.lineId, it] }

        Map folder
        Map additionalBindings
        Map itemClone
        String folderId
        lineItems?.each { String lineId, Map lineItem ->
            lineMap = [:]
            additionalBindings = [:]

            folderId = lineItem.folder ? lineId : lineItem.parentId
            folder = getFolder(folderId, lineItems)

            itemClone = [:]
            if (!lineItem.folder) {
                itemClone << lineItem
                lineItemEnhance(itemClone)
            }

            lineMap << approvable
            lineMap << (folder?.self ?: [:])
            lineMap << itemClone

            additionalBindings << rootEnhanced
            additionalBindings << (itemClone.remove(PER_LEVEL_BINDINGS) ?: [:])
            if (additionalBindings.line) {
                additionalBindings.line << [folder: (folder?.enhancedMap
                                                           ?.line
                                                           ?.folder ?: [:])]
            }

            lineMap << [(PER_LEVEL_BINDINGS): additionalBindings]

            result << lineMap
        }
    }
    if (!result) {
        approvable << [(PER_LEVEL_BINDINGS): rootEnhanced]
        result << approvable
    }

    return result
}

void rootEnhance(Map header) {
    if (header) {
        def prefixHeader = "header"
        selfEnhance(header, prefixHeader)
    }
}

Map getFolder(String folderId, Map lineItems) {
    if (folderId) {
        return libs.SharedLib.CacheUtils.getOrSet("Folder_$folderId", [], {
            List prefixFolders = ["line", "folder"]
            def folder = lineItems?.getAt(folderId)
            if (folder) {
                def folderCloned = [:]
                folderCloned << folder
                selfEnhance(folderCloned, *prefixFolders)
                return [self: folderCloned, enhancedMap: folderCloned.remove(PER_LEVEL_BINDINGS)]
            }
        })
    }
}

void lineItemEnhance(Map lineItem) {
    if (lineItem) {
        def prefixLineItem = "line"
        selfEnhance(lineItem, prefixLineItem)
    }
}

void selfEnhance(Map self, String... prefix) {
    if (self) {
        if (self.inputs) {
            for (item in self.remove("inputs")) {
                mapEnhance(self, item, "name", "value", *prefix, "inputs")
            }
        }

        if (self.outputs) {
            for (item in self.remove("outputs")) {
                mapEnhance(self, item, "resultName", "result", *prefix, "outputs")
            }
        }

        if (self.calculationResults) {
            for (item in self.remove("calculationResults")) {
                mapEnhance(self, item, "resultName", "result", *prefix, "results")
            }
        }

        if (self.allCalculationResults) {
            for (item in self.remove("allCalculationResults")) {
                mapEnhance(self, item, "resultName", "result", *prefix, "allResults")
            }
        }
    }
}


void mapEnhance(Map source, Map input, String nameKey, String resultKey, String... prefix) {
    def name = input.getAt(nameKey)
    def val = input.getAt(resultKey)
    def key = ""
    def sep = "."
    def item
    source << [(name): val]
    def parent = source[PER_LEVEL_BINDINGS] = (source[PER_LEVEL_BINDINGS] ?: [:])
    for (p in prefix) {
        key = "$key$p$sep" as String
        item = [(key + name): val]
        source << item
        parent[p] = (parent[p] ?: [:])
        parent[p] << [(name): val]
        parent = parent[p]
    }
}

Map getTypeIDMap(String typeId) {
    List tokens = typeId?.tokenize(".")
    return [type: tokens?.getAt(1),
            id  : tokens?.getAt(0)]
}

Map getLineItemSearch(String type) {
    Map needPrepare = libs.ApprovalWorkflow.ConfigurationManager.LINEITEM_QUERY_TYPE
    return needPrepare.getAt(type)
}

boolean needApproval(String condition, List approvables) {
    if (!condition) {
        return true
    }

    def evaluator = libs.ApprovalWorkflow.ConfigurationManager.getEvaluator()
    boolean result
    for (approvable in approvables) {
        try {
            def mapPerLevelBindings = approvable.remove(PER_LEVEL_BINDINGS)
            result = evaluator.setExpression(condition)
                              .setBinding(approvable, mapPerLevelBindings)
                              .eval()
            if (result) {
                break
            }
        }
        catch (e) {
            def configManager = libs.ApprovalWorkflow.ConfigurationManager
            def message = sprintf(configManager.ERROR_MESSAGE
                                               .expressionEvaluateError, condition)
            api.logWarn(message, e.getMessage())
            result = false
        }
    }

    return result
}

def getLoginNameByBusinessRoleName(String businessRoleName) {
    Filter filter = Filter.equal("businessRoles.uniqueName", businessRoleName)
    def users = api.find("U", 0, 0, "loginName", ["loginName"], filter)
    return users?.loginName
}

Map prepareApprover(List approvers, boolean isWatchStep) {
    def cfgManager = libs.ApprovalWorkflow.ConfigurationManager

    Map tblConfig = cfgManager.TABLE_CONFIG
    Map approverCols = tblConfig.approversTable.columns
    List groupApprovers = []
    List userApprovers = []
    List groupWatchers = []
    List userWatchers = []
    String approverType
    String approverName

    for (approver in approvers) {
        approverType = approver[approverCols.type]
        approverName = approver[approverCols.name]

        if (!approverName) {
            continue
        }

        if (cfgManager.APPROVER_TYPE
                      .userGroup
                      .equalsIgnoreCase(approverType)) {
            addGroupApprover(groupWatchers, groupApprovers, approverName, isWatchStep)
        } else if (cfgManager.APPROVER_TYPE
                             .user
                             .equalsIgnoreCase(approverType)) {
            addUserApprover(userWatchers, userApprovers, approverName, isWatchStep)
        } else if (cfgManager.APPROVER_TYPE
                             .businessRole
                             .equalsIgnoreCase(approverType)) {
            addRoleApprover(userWatchers, userApprovers, approverName, isWatchStep)
        }
    }
    Map approversGroup = [groupApprovers: groupApprovers,
                          userApprovers : userApprovers,
                          groupWatchers : groupWatchers,
                          userWatchers  : userWatchers]
    boolean validApprovers = approversGroup.values()
                                           .any()
    if (!validApprovers) {
        def configManager = libs.ApprovalWorkflow.ConfigurationManager
        api.throwException(configManager.ERROR_MESSAGE
                                        .invalidApprover)
    }
    return approversGroup
}

void addGroupApprover(List groupWatchers, List groupApprovers, String approverName, boolean isWatchStep) {
    if (isWatchStep) {
        groupWatchers.add(approverName)
    } else {
        groupApprovers.add(approverName)
    }
}

void addUserApprover(List userWatchers, List userApprovers, String approverName, boolean isWatchStep) {
    if (isWatchStep) {
        userWatchers.add(approverName)
    } else {
        userApprovers.add(approverName)
    }
}

void addRoleApprover(List userWatchers, List userApprovers, String approverName, boolean isWatchStep) {
    if (isWatchStep) {
        userWatchers.addAll(getLoginNameByBusinessRoleName(approverName))
    } else {
        userApprovers.addAll(getLoginNameByBusinessRoleName(approverName))
    }
}

Object getApprovalStep(Object workflow, Map step, boolean isWatchStep) {
    def cfgManager = libs.ApprovalWorkflow.ConfigurationManager
    Map tblConfig = cfgManager.TABLE_CONFIG
    Map stepCols = tblConfig.approvalStepsTable.columns
    Object stepDTO
    if (isWatchStep) {
        stepDTO = workflow.addWatcherStep(step[stepCols.label])
    } else {
        stepDTO = workflow.addApprovalStep(step[stepCols.label]).withPostStepLogic("WorkflowPostSetupLogic_LPG")
    }
    stepDTO.setReason(step[stepCols.reason])

    return stepDTO
}

void setGroupApprover(Object stepDTO, List groupApprovers, Integer minApprovers) {
    if (groupApprovers) {
        stepDTO.withUserGroupApprovers(*groupApprovers)
               .withMinApprovalsNeeded(minApprovers)
    }
}

void setUserApprover(Object stepDTO, List userApprovers) {
    if (userApprovers) {
        stepDTO.withApprovers(*userApprovers)
    }
}

void setGroupWatchers(Object stepDTO, List groupWatchers) {
    if (groupWatchers) {
        stepDTO.withUserGroupWatchers(*groupWatchers)
    }
}

void setUserWatchers(Object stepDTO, List userWatchers) {
    if (userWatchers) {
        stepDTO.withUserWatchers(*userWatchers)
    }
}