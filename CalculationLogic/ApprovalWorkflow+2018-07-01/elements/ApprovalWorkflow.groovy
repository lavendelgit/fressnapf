def getApprovalWorkflow(String workflowType, Object workflow, Object approvableObject) {
    List lineItems = approvableObject.lineItems
    approvableObject.lineItems = null
    calculateApprovalWorkflow(workflowType, workflow, approvableObject)
    approvableObject.lineItems = lineItems

    fetchAndCalculateApprovalWorkflowOnLineItems(workflowType, workflow, approvableObject)
}

def fetchAndCalculateApprovalWorkflowOnLineItems(String workflowType, Object workflow, Object approvableObject) {
    def cfgManager = libs.ApprovalWorkflow.ConfigurationManager
    def utils = libs.ApprovalWorkflow.WorkflowUtils
    def paging = libs.ApprovalWorkflow.Paging.setInstanceName('workflow')
    def pageProcessor = libs.ApprovalWorkflow.PageProcessor.setInstanceName('workflow')

    List existingLineItems = approvableObject.lineItems

    int expectedItemsPerPage = [cfgManager.DEFAULT_ITEMS_PER_PAGE, api.getMaxFindResultsLimit()].min()

    List existingLineItemsPaging = existingLineItems?.collate(expectedItemsPerPage)
    String typeIdString = approvableObject.typedId

    paging.setNumberOfItemsPerPage(expectedItemsPerPage) //items per page to get
          .setDataProvider { int page, int itemsPerPage ->
              Map typeId = utils.getTypeIDMap(typeIdString)
              String type = typeId.type
              String id = typeId.id
              Map lineItemSearch = utils.getLineItemSearch(type)

              if (!existingLineItems && id && lineItemSearch) {
                  Filter idFilter = Filter.equal(lineItemSearch.parentId, id)
                  String sortBy = lineItemSearch.sortBy
                  List lineItems = []
                  int start = (page * itemsPerPage)

                  for (lineItemType in lineItemSearch.type) {
                      lineItems.addAll(
                              api.find(lineItemType, start, itemsPerPage, sortBy, idFilter)
                                 .collect { return (it << [lineId: it.getAt(lineItemSearch.lineId)]) }
                      )
                  }

                  return lineItems
              }

              return existingLineItemsPaging?.getAt(page) ?: []
          }

    pageProcessor.setProcessingPages(0, null) //Process all pages
                 .setPageProcessor { List lineItems ->
                     approvableObject.lineItems = lineItems
                     calculateApprovalWorkflow(workflowType, workflow, approvableObject)
                 }
                 .processPages()
}

def calculateApprovalWorkflow(String workflowType, Object workflow, Object approvableObject) {
    def cfgManager = libs.ApprovalWorkflow.ConfigurationManager
    def utils = libs.ApprovalWorkflow.WorkflowUtils

    List approvables = utils.prepareApprovableItems(approvableObject)
    List varConfigs = cfgManager.fetchVariableConfigurations(workflowType)
    List varLookupConfigs = cfgManager.fetchVariableLookupConfiguration(workflowType)

    Map vars = utils.lookupVariableValue(varConfigs, varLookupConfigs, approvables)
    utils.enhanceApprovable(vars, varLookupConfigs, approvables)
    configApprovalWorkflow(workflowType, workflow, approvables)
}

def configApprovalWorkflow(String workflowType, Object workflow, List approvables) {
    def cfgManager = libs.ApprovalWorkflow.ConfigurationManager
    def utils = libs.ApprovalWorkflow.WorkflowUtils

    Map conditionCols = cfgManager.TABLE_CONFIG.conditionsTable.columns
    Map stepCols = cfgManager.TABLE_CONFIG.approvalStepsTable.columns
    Map approversByStep = libs.SharedLib.CacheUtils.getOrSet("approval_wf_approvers", []) {
        return cfgManager.fetchApprovers(workflowType)
                         ?.groupBy { it[stepCols.id] }
    }

    Map conditionsByStep = libs.SharedLib.CacheUtils.getOrSet("approval_wf_conditions", []) {
        return cfgManager.fetchApprovalConditions(workflowType)
                         ?.groupBy { it[stepCols.id] }
    }

    List steps = libs.SharedLib.CacheUtils.getOrSet("approval_wf_steps", []) {
        return cfgManager.fetchApprovalSteps(workflowType)
    }

    List conditions
    Integer minApprovers
    Map approvers
    boolean result
    boolean isWatchStep
    def stepDTO
    steps.each { step ->
        result = true
        conditions = conditionsByStep?.getAt(step[stepCols.id])
        for (condition in conditions) {
            result = result && utils.needApproval(condition[conditionCols.condition], approvables)
            if (!result) {
                workflow.withRunDefaultPostApprovalStepLogicOnEmptyWorkflow(true).withDefaultPostApprovalStepLogic("WorkflowPostSetupLogic_LPG")
                break
            }
        }
        if (result) {
            isWatchStep = cfgManager.STEP_TYPE
                                    .watcher
                                    .equalsIgnoreCase(step[stepCols.type])
            approvers = utils.prepareApprover(approversByStep?.getAt(step[stepCols.id]), isWatchStep)

            stepDTO = utils.getApprovalStep(workflow, step, isWatchStep)
            minApprovers = (step[stepCols.minApprover] ?: 1) as Integer
            utils.setGroupApprover(stepDTO, approvers.groupApprovers, minApprovers)
            utils.setUserApprover(stepDTO, approvers.userApprovers)
            utils.setGroupWatchers(stepDTO, approvers.groupWatchers)
            utils.setUserWatchers(stepDTO, approvers.userWatchers)
        }
    }
}

def enhanceApproval(String workflowType, List approvables) {
    def cfgManager = libs.ApprovalWorkflow.ConfigurationManager
    List varConfigs = cfgManager.fetchVariableConfigurations(workflowType)
    List varLookupConfigs = cfgManager.fetchVariableLookupConfiguration(workflowType)
    def utils = libs.ApprovalWorkflow.WorkflowUtils
    Map vars = utils.lookupVariableValue(varConfigs, varLookupConfigs, approvables)
    utils.enhanceApprovable(vars, varLookupConfigs, approvables)
    return approvables
}
