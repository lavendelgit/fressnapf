import groovy.transform.Field


@Field Map VALUE_PROVIDER = [customerGroup: "libs.ApprovalWorkflow.ValueProvider.customerGroup",
                             productGroup : "libs.ApprovalWorkflow.ValueProvider.productGroup",
                             customer     : "libs.ApprovalWorkflow.ValueProvider.customer",
                             product      : "libs.ApprovalWorkflow.ValueProvider.product"]


@Field Map TABLE_CONFIG = [variableConfigTable: [name   : "WFExpressionVariableConfiguration",
                                                 columns: [variableName       : "key1",
                                                           wfType             : "key2",
                                                           variableLookupTable: "attribute1",
                                                           variableLookupType : "attribute2",
                                                           variableValue      : "attribute3",
                                                           variableType       : "attribute4",
                                                           skip               : "attribute5"]],
                           lookupConfigTable  : [name   : "WFLookupFilterConfiguration",
                                                 columns: [variableName : "key1",
                                                           lookupTable  : "key2",
                                                           lookupKey    : "key3",
                                                           lookupValue  : "attribute1",
                                                           valueProvider: "attribute2"]],
                           approvalStepsTable : [name   : "ApprovalWorkflowSetup",
                                                 type   : "MLTV2",
                                                 columns: [workflowType: "key1",
                                                           id          : "key2",
                                                           order       : "attribute1",
                                                           label       : "attribute2",
                                                           reason      : "attribute3",
                                                           skip        : "attribute4",
                                                           type        : "attribute5",
                                                           minApprover : "attribute6"]],
                           approversTable     : [name   : "Approvers",
                                                 columns: [workflowType: "key1",
                                                           stepId      : "key2",
                                                           id          : "key3",
                                                           type        : "attribute1",
                                                           name        : "attribute2",
                                                           skip        : "attribute3"]],
                           conditionsTable    : [name   : "ApprovalCondition",
                                                 columns: [workflowType: "key1",
                                                           stepId      : "key2",
                                                           id          : "key3",
                                                           description : "attribute1",
                                                           condition   : "attribute2",
                                                           skip        : "attribute3"]]]

@Field Map CACHING_KEY = [variableConfigCachePrefix      : "APPROVAL_WF_VAR_CONFIG_",
                          variableLookupConfigCachePrefix: "APPROVAL_WF_VAR_LOOKUP_CONFIG_",
                          conditionExpressionCache       : "APPROVAL_CONDITION_EXPRESSION"]

@Field Map CONST = [ALL                   : "All",
                    APPROVAL_SKIP         : "Yes",
                    LOOKUP_NAME_KEY       : "lookup.name",
                    LOOKUP_TYPE_KEY       : "lookup.type",
                    GROUP_BY_KEY_SEPARATOR: "-"]

@Field Map STEP_TYPE = [watcher: "Watcher"]

@Field Map APPROVER_TYPE = [userGroup   : "User Group",
                            user        : "User",
                            businessRole: "Business Role"]

@Field Map LOOKUP_TYPE = [PX : "PX",
                          CX : "CX",
                          PP : "PP",
                          P  : "P",
                          AP : "AP",
                          LT : "LT",
                          LTV: "LTV",
                          C  : "C"]

@Field Map LINEITEM_QUERY_TYPE = [PL: [type    : ["PLI", "XPLI"],
                                       parentId: "pricelistId",
                                       sortBy  : "id",
                                       lineId  : "sku"]]

@Field Map ERROR_MESSAGE = [lookupTableNotExist    : "Lookup table [type: %s, name: %s] not exist",
                            invalidFilterOperator  : "Invalid Filter Operator %s",
                            expressionEvaluateError: "Error while evaluation expression %s",
                            sharedLibNotDeploy     : "SharedLib not deployed",
                            invalidApprover        : "Please check value of Approver Name in Price Parameter table.",
                            invalidLibPath         : "Incorrect library path",
                            invalidLibrary         : "Library not exist",
                            invalidProvider        : "Invalid value provider"]

@Field Map THIRD_PARTY_UTILS = [metaUtils: "libs.SharedLib.MetadataUtils"]

@Field List READ_ONLY_BINDING = ["api",
                                 "quoteProcessor",
                                 "quote",
                                 "pricelist",
                                 "workflow",
                                 "pricegriditem",
                                 "rebateagreement",
                                 "rebaterecord",
                                 "dealplan"]

@Field final int DEFAULT_ITEMS_PER_PAGE = 800

/**
 * Fetch configuration for additional variable using in approval condition.
 * Configurations are fetched from PP table named "WFExpressionVariableConfiguration".
 * Configurations are fetched for specific workflow type and step
 * @param workflowType Name of workflow type
 * @param stepIds List of step id
 * @return List of records from PP table
 * */
List fetchVariableConfigurations(String workflowType) {
    String cacheKey = [CACHING_KEY.variableConfigCachePrefix.toString(),
                       workflowType.hashCode().toString()].join("_")
    return workflowType ? libs.SharedLib.CacheUtils.getOrSet(cacheKey, [], {
        Map tblConfig = TABLE_CONFIG.variableConfigTable
        String tblName = tblConfig.name
        Map tblColumns = tblConfig.columns
        String wfTypeCol = tblColumns.wfType
        List wfTypeFilter = [Filter.equal(wfTypeCol, workflowType),
                             getSkipFilter(tblColumns.skip)]
        Map collectingConfig
        List queryData = api.findLookupTableValues(tblName, *wfTypeFilter)?.collect {
            collectingConfig = [:]
            collectingConfig << it
            collectingConfig << [(tblColumns.variableValue): getVariableLookupTableAttributeId(it, it[tblColumns.variableValue])]
            return collectingConfig
        }
        return queryData
    }) : []
}

/**
 * Fetch lookup configuration from PP table named "WFLookupFilterConfiguration"
 * */
List fetchVariableLookupConfiguration(String workflowType) {
    String cacheKey = [CACHING_KEY.variableLookupConfigCachePrefix.toString(),
                       workflowType.hashCode().toString()].join("_")
    Map varConfigCol = TABLE_CONFIG.variableConfigTable.columns
    Map varConfigs = fetchVariableConfigurations(workflowType)?.collectEntries {
        [it[varConfigCol.variableName], it]
    }
    Set variableNames = varConfigs?.keySet()
    return variableNames ? libs.SharedLib.CacheUtils.getOrSet(cacheKey, [], {
        String tblName = TABLE_CONFIG.lookupConfigTable.name
        Map tblColumns = TABLE_CONFIG.lookupConfigTable.columns
        String variableName = tblColumns.variableName
        Map varConfig
        Map collectingConfig
        List configs = api.findLookupTableValues(tblName, Filter.in(variableName, variableNames))?.collect {
            varConfig = varConfigs.getAt(it[variableName])
            collectingConfig = [:]
            collectingConfig << it
            collectingConfig << [(tblColumns.lookupKey): getVariableLookupTableAttributeId(varConfig, it[tblColumns.lookupKey])]
            return collectingConfig
        }

        return configs
    }) : []
}

String getVariableLookupTableAttributeId(Map varConfig, String label) {
    Map varConfigCol = TABLE_CONFIG.variableConfigTable.columns
    String type, source
    type = varConfig[varConfigCol.variableLookupType]
    source = varConfig[varConfigCol.variableLookupTable]
    type = type == LOOKUP_TYPE.PP ? LOOKUP_TYPE.LTV : type
    return getAttributeIdFromLabel(type, source, label)
}

/**
 * Fetch approval steps configuration
 * */
List fetchApprovalSteps(String workflowType) {
    String tblName = TABLE_CONFIG.approvalStepsTable.name
    String tblType = TABLE_CONFIG.approvalStepsTable.type
    Map tblColumns = TABLE_CONFIG.approvalStepsTable.columns
    List filters = [Filter.equal("lookupTable.uniqueName", tblName),
                    Filter.equal(tblColumns.workflowType, workflowType),
                    getSkipFilter(tblColumns.skip)]
    String sortBy = tblColumns.order
    return libs.SharedLib.StreamUtils.stream(tblType, sortBy, filters, { it.collect() })
}


List fetchApprovers(String workflowType) {
    String tblName = TABLE_CONFIG.approversTable.name
    Map tblColumns = TABLE_CONFIG.approversTable.columns
    List filters = [Filter.equal(tblColumns.workflowType, workflowType),
                    getSkipFilter(tblColumns.skip)]
    return api.findLookupTableValues(tblName, *filters)
}

List fetchApprovalConditions(String workflowType) {
    String tblName = TABLE_CONFIG.conditionsTable.name
    Map tblColumns = TABLE_CONFIG.conditionsTable.columns
    List filters = [Filter.equal(tblColumns.workflowType, workflowType),
                    getSkipFilter(tblColumns.skip)]

    return api.findLookupTableValues(tblName, *filters)
}

Map getEvaluator() {
    String cacheKey = libs.ApprovalWorkflow.ConfigurationManager.CACHING_KEY.conditionExpressionCache
    Map conditionCache = libs.SharedLib.CacheUtils.getOrSet(cacheKey, [], { [:] })
    def evaluator = libs.FormulaEvaluator.Builder
    return evaluator.setFormula(null)
                    .setCached(conditionCache)
}

Filter getSkipFilter(String column) {
    return Filter.or(Filter.notEqual(column, CONST.APPROVAL_SKIP),
                     Filter.isNull(column),
                     Filter.isEmpty(column))
}

Object runScript(String path, Object... params) {
    return getScriptFromPath(path).run(*params)
}

List getScriptPath(String path) {
    return path?.tokenize("\\.")
}

Map getScriptFromPath(String path, Map binding = null) {
    List paths = getScriptPath(path)

    return getScript(paths, binding)

}

Map getScript(List paths, Map binding = null) {
    if (!paths || paths.size() != 4 || paths[0] != "libs") {
        api.throwException(ERROR_MESSAGE
                                   .invalidLibPath)
    }
    Script script = libs.get(paths[1]).get(paths[2])
    String function = paths[3]

    if (!script) {
        api.throwException(ERROR_MESSAGE
                                   .invalidLibrary)
    }
    binding?.each { name, value ->
        if (!(name in READ_ONLY_BINDING) || !script.hasProperty(name)) {
            script.setProperty(name, value)
        }
    }
    Map scriptWrapper = [run: { Object... params ->
        return runScriptFunc(script, function, *params)
    }]

    return scriptWrapper
}

Object runScriptFunc(Script script, String function, Object... params) {
    return script.(function as String)(*params)
}

String getAttributeIdFromLabel(String type, String source, String label, Map binding = null) {
    boolean alreadyTechnicalName = label.startsWith("attribute") || label.startsWith("key")
    if (label && alreadyTechnicalName) {
        return label
    }

    try {
        String libPath = "${THIRD_PARTY_UTILS.metaUtils}.getAttributeIdFromLabel" as String
        Map script = getScriptFromPath(libPath, binding)
        String attr = script.run(type, source, label)
        return attr ?: label
    }
    catch (exception) {
        def message = ERROR_MESSAGE.sharedLibNotDeploy
        api.logWarn(message)
        return label
    }
}