import groovy.transform.Field

@Field final String SAP_KVI_Flag = "2"
@Field final String Pricefx_KVI_Flag = "KVI"
@Field final String Rule_Configurater_X = "X"
@Field final String ALL = "*"
@Field final String Rule_Yes = "Yes"
@Field final String EXT_KVI = "Ext.KVI"