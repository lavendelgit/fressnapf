List getProductsWithKVI() {
    List filters = [
            Filter.equal("attribute2", "2")
    ]
    List articleID = api.find("PX8", 0, api.getMaxFindResultsLimit(), null, *filters)?.sku
    return productPriceFamily(articleID)
}

List productPriceFamily(List articleID) {
    List filters = [
            Filter.in("sku", articleID)
    ]
    return api.find("P", 0, api.getMaxFindResultsLimit(), null, *filters)
}

