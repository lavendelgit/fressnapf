Map generatedElements = [
        itemClass: null,
        basket   : null
]
List validMaterialType = ["HAWA","ZSET","ZTRA"]
Object product = api.currentItem() ?: []
String productSKU = out.ArticalNo?.sku
Map articleDetails = [:]
articleDetails.productSKU = out.ArticalNo?.sku
articleDetails.sapItemClass = out.ArticalNo?.attribute2
articleDetails.brandType = out.ArticleCache?.attribute13
articleDetails.materialType = out.ArticleCache?.attribute6
articleDetails.brand = out.ArticleCache?.attribute11
articleDetails.priceFamily = api.global.priceFamily[out.ArticalNo?.sku]?.attribute1//out.ArticleCache?.attribute20
articleDetails.cluster = libs.FressnapfLib.DataUtils.getCommodityGroupMaster()[out.ArticleCache?.attribute3]?.cluster
api.local.warning = !articleDetails.cluster?"Commodity Group data not available" : ""
api.local.warning = !validMaterialType?.contains(articleDetails.materialType) ? "Not Valid Sort of Article":api.local.warning
List sellingPrice = libs.FressnapfLib.DataUtils.getSellingPriceMaster(out.Country)[productSKU]
BigDecimal currentSellingPrice = sellingPrice?.sort{-it.salesPrice}?.getAt(0)?.salesPrice
Map countrySellingPriceThreshold = libs.FressnapfLib.DataUtils.getHighInvolvementMaster()[out.Country]
if(sellingPrice?.currency?.unique()?.getAt(0) != countrySellingPriceThreshold?.sptCurrency ){
    key = sellingPrice?.currency?.unique()?.getAt(0)+"_"+ countrySellingPriceThreshold?.sptCurrency
    exchangeRate = libs.FressnapfLib.DataUtils.getExchangeRatesMaster()
    if(currentSellingPrice && exchangeRate?.getAt(key)?.currencyExchangeRate){
        currentSellingPrice = currentSellingPrice * exchangeRate?.getAt(key)?.currencyExchangeRate
    }
}

BigDecimal sellingPriceThreshold = libs.FressnapfLib.DataUtils.getHighInvolvementMaster()[out.Country]?.sellingPriceThreshold
Map categoryTypeMapping = libs.FressnapfLib.DataUtils.getBrandRoleMappingMaster()?.values()?.findAll { it.country == out.Country }?.find { it.brand == articleDetails.brand }
Boolean priceStarter = categoryTypeMapping?.priceStarter == Constants.Rule_Configurater_X ?: false
Boolean groceryDriven = categoryTypeMapping?.groceryDriven == Constants.Rule_Configurater_X ?: false
Boolean vet = categoryTypeMapping?.vet == Constants.Rule_Configurater_X ?: false
Boolean subBrand = categoryTypeMapping?.subBrand == "*" ?: (categoryTypeMapping?.subBrand == out.ArticleCache?.attribute12)
Boolean isPriceFamilyIsInKVI = false
String productFamilyBasket
if (articleDetails.priceFamily && !articleDetails.sapItemClass) {
    !api.global.KVIProductDetails ? api.global.KVIProductDetails = Lib.getProductsWithKVI() : ""
    def getProductsWithKVI = (api.global.priceFamily)?.values()?.findAll { it?.attribute1 == articleDetails.priceFamily  }?.sku
    for(article in getProductsWithKVI){
        if (api.global.ProductsWithKVI?.getAt(article)) {
            productFamilyBasket = api.global.ProductsWithKVI[article]?.basket
            if(productFamilyBasket) break
        }
    }

    Integer productFamilyWithKVI = getProductsWithKVI?.size()
    isPriceFamilyIsInKVI = productFamilyWithKVI > 0
}

List basketItemClassRules = libs.FressnapfLib.DataUtils.getBasketItemClassRulesMaster()?.findAll { it.Country == out.Country }


Boolean kviFlagCondition = false
Boolean articleTypeCondition = false
Boolean brandTypeCondition = false
Boolean groceryCondition = false
Boolean priceStarterCondition = false
Boolean higLevelInclusionCondition = false
Boolean vetCondition = false
if (productSKU /*&& out.ArticleCache?.lastUpdateDate >= out.ArticalNo?.lastUpdateDate*/ ) {
    for (rule in basketItemClassRules) {


        boolean condit = isRulePassed(rule, articleDetails, priceStarter, currentSellingPrice, sellingPriceThreshold, groceryDriven, vet, subBrand, categoryTypeMapping)

        if (condit) {
            generatedElements.itemClass = rule?.ItemClass
            generatedElements.basket = rule?.Basket
            break
        }
        kviFlagCondition = false
        articleTypeCondition = false
        brandTypeCondition = false
        groceryCondition = false
        priceStarterCondition = false
        higLevelInclusionCondition = false
        vetCondition = false


    }
/*
    Object b10icVI = basketItemClassRules?.find { it?.Basket == "10" && it?.ItemClass == "VI" }

    if (brandType == b10icVI?.BrandType && !priceStarter &&
            ((b10icVI?.ArticleType)?.contains(materialType) || currentSellingPrice >= sellingPriceThreshold)) {
        generatedElements["itemClass"] = b10icVI?.ItemClass
        generatedElements["basket"] = b10icVI?.Basket
    }*/

    if (articleDetails.sapItemClass != "2" && isPriceFamilyIsInKVI && productFamilyBasket) {
        generatedElements.itemClass = Constants.EXT_KVI
        generatedElements.basket = productFamilyBasket
    }

    if(!currentSellingPrice ){
        generatedElements = [
                itemClass: null,
                basket   : null
        ]
    }

} else {
    generatedElements.itemClass = out.ArticalNo?.attribute3
    generatedElements.basket = out.ArticalNo?.attribute4
}

if (articleDetails.sapItemClass == "2") {
    !api.global.ProductsWithKVI ? api.global.ProductsWithKVI = [:] : ""
    product["itemClass"] = generatedElements.itemClass
    product["basket"] = generatedElements.basket
    api.global.ProductsWithKVI?.putAt(articleDetails.productSKU, product)
}

boolean isRulePassed(rule, Map articleDetails, boolean priceStarter, BigDecimal currentSellingPrice, BigDecimal sellingPriceThreshold, boolean groceryDriven, boolean vet, boolean subBrand, Map categoryTypeMapping) {

    boolean groceryCondition, higLevelInclusionCondition, brandTypeCondition, articleTypeCondition, priceStarterCondition, kviFlagCondition, vetInclusionCondition

    List allowedArticleTypes = ((rule.ArticleType).split(','))?.collect { it.trim() }
    Boolean condit = true
    Integer count = 0
    kviFlagCondition = ( rule.KVIFlag == articleDetails.sapItemClass )
    if (rule.KVIFlag && rule.KVIFlag?.trim() != "") {
        condit = condit && kviFlagCondition
        count++
    }

    articleTypeCondition = allowedArticleTypes?.getAt(0) == "*"?:allowedArticleTypes.contains(articleDetails.materialType)
    if (allowedArticleTypes?.size() > 0|| allowedArticleTypes?.getAt(0) == "*") {
        condit = condit && articleTypeCondition

        count++
    }

    brandTypeCondition = (rule.BrandType == articleDetails.brandType)
    if (rule.BrandType) {
        condit = condit && brandTypeCondition

        count++
    }

    groceryCondition = (rule.GroceryBrand == Constants.Rule_Yes) ? groceryDriven : !groceryDriven
    if (rule.GroceryBrand) {
        condit = condit && groceryCondition

        count++
    }

    priceStarterCondition = (rule.PriceStarter == Constants.Rule_Yes) ? priceStarter : !priceStarter
    if (rule.PriceStarter) {
        condit = condit && priceStarterCondition

        count++
    }

    higLevelInclusionCondition = rule.HighInvolvementSalesPrice == Constants.Rule_Yes ? (currentSellingPrice >= sellingPriceThreshold) : (currentSellingPrice < sellingPriceThreshold)
    if (rule.HighInvolvementSalesPrice && rule.HighInvolvementSalesPrice.trim() != "") {
        condit = condit && higLevelInclusionCondition

        count++
    }

    vetInclusionCondition = rule?.VET == Constants.Rule_Yes ? vet : !vet
    if (rule.VET) {
        condit = condit && vetInclusionCondition && subBrand
        count++
    }

    if (rule.VME) {
        List validVME = rule?.VME?.split(',')*.trim() as List
        condit = condit && validVME?.contains("0" + out.ArticleCache?.attribute23)

        count++
    }

    if (rule.Cluster) {
        List validCluster = rule?.Cluster?.split(',') as List
        condit = condit && validCluster?.contains(articleDetails.cluster)

        count++
    }

    if (count < 1) {
        //To check at-least one condition is checked
        condit = false
    }

    return condit
}

return generatedElements
