if(!api.global.priceFamily){
    streamRecords = api.stream("PX3",null,Filter.equal("name","PriceFamilies"))
    api.global.priceFamily = streamRecords?.collectEntries{[(it.sku):it]}
    streamRecords?.close()
}
if(!api.global.basketItemClass){
    List filter = [
            Filter.equal("name","ItemClassBasketMapping"),
            Filter.isNotNull("attribute2")
    ]
    streamRecords = api.stream("PX8",null,*filter)
    api.global.basketItemClass = streamRecords?.collectEntries{[(it.sku):it]}
    streamRecords?.close()
}