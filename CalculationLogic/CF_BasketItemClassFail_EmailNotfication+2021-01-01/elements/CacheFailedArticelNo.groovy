Map failedArticle = [:]
List filter = [
        Filter.equal("name", "ItemClassbasketMapping"),
        Filter.isNull("attribute3"),
        Filter.isNull("attribute4")
]
streamRecords = api.stream("PX8", null, ["sku","attribute6"], *filter)
failedArticle = streamRecords?.collectEntries { [(it.sku) : it.attribute6]}
streamRecords?.close()
return failedArticle