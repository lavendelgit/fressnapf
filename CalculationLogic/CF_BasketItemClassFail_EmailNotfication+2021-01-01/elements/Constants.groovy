import groovy.transform.Field

@Field final String HTML_TAG_TR_START_ELEMENT = "<tr>"
@Field final String HTML_TAG_TR_END_ELEMENT = "</tr>"
@Field final String HTML_TAG_TD_START_ELEMENT = "<td>"
@Field final String HTML_TAG_TD_END_ELEMENT = "</td>"
@Field final String EMAIL_SUBJECT = " Total Failed Articles :"
@Field final String EMAIL_HEAD = " Dear "
@Field final String EMAIL_MESSAGE_FAILED = "\n\nThe number of Articles Failed in Basket and Item Class mapping : "
@Field final String Link = "https://fressnapf-qa.qa1.pricefx.com/app/modules/#/md/product-extensions/ItemClassBasketMapping"
@Field final String Link_To_PX = "<a\n" + "href=$Link>\n" + "<button style=\"background-color:Yellow\" type=\"button\" >Access Item Class Basket Table</button>\n" + "</a>\n"
@Field final String EMAIL_WARNING_MESSAGE = " \n (First 2000 Article will be displayed )"
@Field final String EMAIL_MESSAGE_TABLE_HEADER = "<html><head>\n" +
        "<style>\n" +
        "table, th, td {\n" +
        "  border: 1px solid black;\n" +
        "border-collapse: collapse;\n" +
        "text-align: center;\n" +
        " height: 30px;\n" +
        "}\n" +
        "</style>\n" +
        "</head>\n" +
        "<body>\n" +
        "\n" +
        "<h1>List Of Articles $Link_To_PX</h1>\n" +
        "\n" +
        "<table cellspacing='2'>\n" +
        "    <th>Article ID</th>\n" +
        "    <th>Fail Reason</th>\n" +
        "  </tr>\n"
        " <tr>\n"

@Field final String EMAIL_COLUMN = "<th>Article ID</th>\n"+"<th>Fail Reason</th>"
@Field final String EMAIL_MESSAGE_END_ELEMENT =
        "</tr>" +
                "</table>\n" +
                "</body>\n" +
                "</html>\n"