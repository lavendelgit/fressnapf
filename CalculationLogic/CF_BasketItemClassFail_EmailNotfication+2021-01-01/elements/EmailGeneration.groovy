if(out.CacheFailedArticelNo){
    String eMailContent = ""
    String eMailColumn = ""
    int count = 1
    for (article in out.CacheFailedArticelNo) {
        StringBuilder eMailContentBuilder = new StringBuilder()
        eMailContentBuilder.append("<tr>")
            eMailContentBuilder.append("<td>").append(article?.key).append("</td>").append("<td>").append(article?.value).append("</td>")
        eMailContentBuilder.append("</tr>")
        eMailText = eMailContentBuilder.toString()
        eMailContent = eMailText ? (eMailContent + eMailText) : eMailContent
    }
    out.CacheEmailConfiguration?.each { configuration ->
        api.sendEmail(configuration.value, "$Constants.EMAIL_SUBJECT" + out.CacheFailedArticelNo?.size(),
                "$Constants.EMAIL_HEAD" + configuration.key + ",<br>" + "$Constants.EMAIL_MESSAGE_FAILED" + out.CacheFailedArticelNo?.size() + (out.CacheFailedArticelNo?.size() > 2000 ? Constants.EMAIL_WARNING_MESSAGE : "") +
                        "$Constants.EMAIL_MESSAGE_TABLE_HEADER" +"</tr>"+ eMailContent + "$Constants.EMAIL_MESSAGE_END_ELEMENT")
    }
}
return