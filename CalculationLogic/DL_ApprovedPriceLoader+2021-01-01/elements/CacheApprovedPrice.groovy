api.local.approvedPrice = [:]
api.local.metaData = [:]
List filter = [
        Filter.equal("approvalState", "APPROVED"),
        Filter.in("priceGridId", out.LPGConfiguration)
]
api.local.approvedPrice = api.namedEntities(api.find("PGI", 0, api.getMaxFindResultsLimit(), null, *filter))?.findAll { (it["Valid From"] == out.PricingDate || it["Valid From"] >= out.PricingDate) && it["Send To SAP"] == "Yes" }
return api.local.approvedPrice