return api.find("PG", 0, api.getMaxFindResultsLimit(), null, ["id", "label", "configuration"])?.collectEntries {
    [(it.label): [Channel: api.jsonDecode(it.configuration)?.inputs?.getAt(0)?.value?.Channel, Label: it.label, id: it.id]]
}?.values()?.groupBy { it.Channel }?.getAt(out.PricingChannel)?.id