List allApprovedRecords = []
List apporvedPrices = []
Integer counter = 1
//api.local.metaDatamapping = api.findLookupTableValues("")
if (out.PricePickerCount == "*") {
    loadApprovedData(api.local.approvedPrice)
} else {
    allApprovedRecords = api.local.approvedPrice?.sort { a, b -> b["manualResultPrice"] <=> a["manualResultPrice"] }
    if (api.local.approvedPrice?.size() <= (out.PricePickerCount as Integer)) {
        for (record in allApprovedRecords) {
            record.putAt("Counter", counter)
            apporvedPrices << record
            counter++
        }
    } else {
        allApprovedRecords = api.local.approvedPrice?.findAll { it.manualResultPrice != null }
        //api.trace("allApprovedRecords",allApprovedRecords)
        allApprovedRecords = sortByBasket(allApprovedRecords)
        for (record in allApprovedRecords) {
            /*if(counter>(out.PricePickerCount as Integer)){
                break
            }*/
            if (record?.getAt("manualResultPrice")) {
                if (record?.getAt("Price Family Code") && (!apporvedPrices?.getAt("Article ID")?.contains(record?.getAt("Article ID")))) {
                    priceFamilyRecords = api.local.approvedPrice?.findAll { it["Price Family Code"] == record?.getAt("Price Family Code") }
                    priceFamilyRecords?.each { priceFamiles ->
                        priceFamiles.putAt("Counter", counter)
                        apporvedPrices << priceFamiles
                        counter++
                    }
                } else if ((!apporvedPrices?.getAt("Article ID")?.contains(record?.getAt("Article ID")))) {
                    record.putAt("Counter", counter)
                    apporvedPrices << record
                    counter++
                }
            }
        }
        allApprovedRecords = api.local.approvedPrice?.findAll { it.manualResultPrice == null }
        allApprovedRecords = sortByBasket(allApprovedRecords)
        for (record in allApprovedRecords) {
            /*if(counter>(out.PricePickerCount as Integer)){
                break
            }*/
            if (record?.getAt("Price Reason")?.contains("Competition") && !record?.getAt("Counter") && (!apporvedPrices?.getAt("Article ID")?.contains(record?.getAt("Article ID")))) {
                if (record?.getAt("Price Family Code") && (!apporvedPrices?.getAt("Article ID")?.contains(record?.getAt("Article ID")))) {
                    priceFamilyRecords = api.local.approvedPrice?.findAll { it["Price Family Code"] == record?.getAt("Price Family Code") }
                    priceFamilyRecords?.each { priceFamiles ->
                        priceFamiles.putAt("Counter", counter)
                        apporvedPrices << priceFamiles
                        counter++
                    }
                } else if ((!apporvedPrices?.getAt("Article ID")?.contains(record?.getAt("Article ID")))) {
                    record.putAt("Counter", counter)
                    apporvedPrices << record
                    counter++
                }
            }
        }
        allApprovedRecords = api.local.approvedPrice?.findAll { it["Price Reason"] != "Competition-based Price" && it.manualResultPrice == null }
//?.sort{it["ListPriceMarginPercentage"+out.PricingChannel]}
        allApprovedRecords = sortByBasket(allApprovedRecords)
        for (record in allApprovedRecords) {
            /*if(counter>=(out.PricePickerCount as Integer)){
                break
            }*/
            if (/*record?.getAt("Price Reason")?.contains("CostPlus") && */ !record?.getAt("Counter") && (!apporvedPrices?.getAt("Article ID")?.contains(record?.getAt("Article ID")))) {
                if (record?.getAt("Price Family Code") && (!apporvedPrices?.getAt("Article ID")?.contains(record?.getAt("Article ID")))) {
                    priceFamilyRecords = api.local.approvedPrice?.findAll { it["Price Family Code"] == record?.getAt("Price Family Code") }
                    priceFamilyRecords?.each { priceFamiles ->
                        priceFamiles.putAt("Counter", counter)
                        apporvedPrices << priceFamiles
                        counter++
                    }
                } else if ((!apporvedPrices?.getAt("Article ID")?.contains(record?.getAt("Article ID")))) {
                    record.putAt("Counter", counter)
                    apporvedPrices << record
                    counter++
                }
            }
        }
        allApprovedRecords = api.local.approvedPrice?.findAll { it["Price Reason"] != "Competition-based Price" && it.manualResultPrice == null
            /*&& it["Price Reason"]!="CostPlus-based Price"*/ }
        allApprovedRecords = sortByBasket(allApprovedRecords)
        if (counter > (out.PricePickerCount as Integer)) {
            for (record in allApprovedRecords) {
                /* if(counter>(out.PricePickerCount as Integer)){
                     break
                 }*/
                if (!record?.getAt("Counter") && (!apporvedPrices?.getAt("Article ID")?.contains(record?.getAt("Article ID")))) {
                    if (record?.getAt("Price Family Code") && (!apporvedPrices?.getAt("Article ID")?.contains(record?.getAt("Article ID")))) {
                        priceFamilyRecords = api.local.approvedPrice?.findAll { it["Price Family Code"] == record?.getAt("Price Family Code") }
                        priceFamilyRecords?.each { priceFamiles ->
                            priceFamiles.putAt("Counter", counter)
                            apporvedPrices << priceFamiles
                            counter++
                        }
                    } else if ((!apporvedPrices?.getAt("Article ID")?.contains(record?.getAt("Article ID")))) {
                        record.putAt("Counter", counter)
                        apporvedPrices << record
                        counter++
                    }
                }
            }
        }
    }
    loadApprovedData(apporvedPrices)
}
return

void loadApprovedData(approvedRecords) {
    id = api.findLookupTableValues("URLConfiguration",Filter.equal("name","DefaultMetaDataLPG"))?.getAt(0)?.value//api.find("PG",0,200,"-calculationDate",null)?.getAt(0)?.id//?.configuration
    Map metaData = api.find("PGIM",Filter.equal("priceGridId",id))?.collectEntries {[(it.label):it.elementName]}
    List priceRecords = []
    if (out.PricePickerCount == "*") {
        priceRecords = approvedRecords
    } else {
        priceRecords = approvedRecords?.findAll { (it.Counter as Integer) <= (out.PricePickerCount as Integer) }
    }
    api.trace("priceRecords",priceRecords)
    Map approvedRecord = [:]
    def target = api.getDatamartRowSet("target")
    for (records in priceRecords) {
        records?.each { attributes ->

            approvedRecord?.put((metaData?.getAt(attributes?.key)?:attributes?.key), attributes?.value)
        }
        //approvedRecord.priceGridId = records?.records
        approvedRecord.sku = records?.getAt("Article ID")?:records?.sku
        approvedRecord.Country = out.Country
        target?.addRow(approvedRecord)
    }
}


def sortByBasket(List approvedRecords) {
    List orderedApprovedRecords = []
    List tempApprovedPrices = []

    basketPriority = api.findLookupTableValues("ExportBasketPriority", "name", null)?.collectEntries { [(it.name): it.attribute1] }
    basketPriority?.values()?.each { basket ->
        if (approvedRecords?.findAll { it.Basket == basket }) {
            tempApprovedPrices = []
            approvedRecords?.findAll { it.Basket == basket }?.each { record ->
                tempApprovedPrices << record
            }
            tempApprovedPrices?.sort { it.getAt("Δ List Price % (Current v/s New)") }?.reverse()?.each { sortedList ->
                orderedApprovedRecords << sortedList
            }
        }
    }
    return orderedApprovedRecords
}