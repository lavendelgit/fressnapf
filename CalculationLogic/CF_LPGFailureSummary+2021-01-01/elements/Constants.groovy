import groovy.transform.Field
@Field final String LINK = "https://fressnapf-qa.qa1.pricefx.com/app/modules/#/pb/lpgs/"
@Field final String HTML_TAG_TR_START_ELEMENT = "<tr>"
@Field final String HTML_TAG_TR_END_ELEMENT = "</tr>"
@Field final String HTML_TAG_TD_START_ELEMENT = "<td>"
@Field final String HTML_TAG_TD_END_ELEMENT = "</td>"
@Field final String EMAIL_SUBJECT = " LPG : "
@Field final String EMAIL_HEAD = " Dear "
@Field final String EMAIL_MESSAGE_TABLE_HEADER = "<html><head>\n" +
        "<style>\n" +
        "table, th, td {\n" +
        "  border: 1px solid black;\n" +
        "border-collapse: collapse;\n" +
        "text-align: center;\n" +
        " height: 30px;\n" +
        "}\n" +
        "</style>\n" +
        "</head>\n" +
        "<body>\n" +
        "<table cellspacing='2'>\n" +
        " <tr>\n" +
        "    <th>Article ID</th>\n" +
        "    <th>Pricing Fail Reason</th>\n" +
        "  </tr>\n"

@Field final String EMAIL_MESSAGE_TABLE_ATTRIBUTE = "<html><head>\n" +
        "<style>\n" +
        "table, th, td {\n" +
        "border: 1px solid black;\n" +
        "border-collapse: collapse;\n" +
        "text-align: center;\n" +
        " height: 30px;\n" +
        "}\n" +
        "</style>\n" +
        "</head>\n" +
        "<body>\n" +
        "\n" +
        "<h1>Misc Attributes</h1>\n" +
        "\n" +
        "<table cellspacing='2'>\n" +
        " <tr>\n" +
        "    <th>Attribute</th>\n" +
        "    <th>Value</th>\n" +
        "  </tr>\n"


@Field final String EMAIL_MESSAGE_END_ELEMENT =
        "</tr>" +
                "</table>\n" +
                "\n" +
                "<br><br>Best Regards,<br>\n" +
                "Your Pricing Team.\n" +
                "</body>\n" +
                "</html>\n"
