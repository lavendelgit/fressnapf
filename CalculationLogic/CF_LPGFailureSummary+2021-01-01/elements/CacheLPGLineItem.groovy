List filter = []
Map metaData = [:]
api.local.LPGLineItem = [:]
for(lpgInfo in api.local.LPGInfo){
    filter = [
            Filter.equal("priceGridId",lpgInfo?.id),
            Filter.equal("resultPrice",0)
    ]
    streamRecords = api.stream("PGI","sku",*filter)
    api.local.LPGLineItem[lpgInfo?.id] = streamRecords?.collectEntries{[(it.sku): it]}
    streamRecords?.close()
}
return
