users =  api.find("U",0,"loginName")?.findAll{it.groups!=[]}
return users?.findAll{it.groups?.uniqueName?.contains("FailNotification")}?.collectEntries {[(it.firstName):it.email]}
/*
api.findLookupTableValues(libs.FressnapfLib.Constants.EmailConfiguration_PP,Filter.equal("attribute2","Yes"))?.collectEntries { row ->
    [(row.name): row.attribute1]
}*/
