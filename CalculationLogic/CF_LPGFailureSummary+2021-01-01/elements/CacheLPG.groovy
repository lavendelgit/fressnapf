String date = (api.targetDate() - 7 ) as String
List filter = [
        Filter.equal("status","Ready"),
        Filter.greaterOrEqual("calculationDate",date)
]
api.local.LPGInfo = api.find("PG",0,200,"id",["id","label"],*filter)
return