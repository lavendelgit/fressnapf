for(lpgInfo in api.local.LPGInfo){
    if( api.local.LPGLineItem[lpgInfo?.id]){
        metaData = api.find("PGIM", Filter.equal("priceGridId", lpgInfo?.id))?.collectEntries{[(it.label):it.fieldName]}
            eMailContent = generateMail(api.local.LPGLineItem[lpgInfo?.id],metaData)
            sendEmail(out.CacheEmailConfiguration,eMailContent,lpgInfo)
        }
    }

return

String generateMail(Map lineItems, Map metaData){
    String mailContent = ""
    String warning = ""
    StringBuilder eMailContentBuilder = new StringBuilder()
    for(lineItem in lineItems){

        warning = failReason(lineItem)
        mailContent = generateEMailContent(lineItem?.key,warning,mailContent, eMailContentBuilder)
    }
    return mailContent
}

String generateEMailContent(String sku, String pricingFailReason,String mailBodyContent, StringBuilder eMailContentBuilder) {
    if (sku) {
        eMailContentBuilder.append("<tr>").append("<td>").append(sku).append("</td>").append("<td>").append(pricingFailReason?:"").append("</td>")
        eMailText = eMailContentBuilder.toString()
        mailBodyContent = eMailText
    }
    return mailBodyContent
}

void sendEmail(Map users, String mailBody, Map lpgInfo){
    String mailHead = ""
    lpgLink = Constants.LINK+lpgInfo?.id+"/detail"
    lpgButton = "<a\n" + "href=$lpgLink\n" + "<button style=\"background-color:Yellow\" type=\"button\" >"+lpgInfo?.id +"-"+lpgInfo?.label+"</button>\n" + "</a>\n"
    for(user in users){
        mailHead = "$Constants.EMAIL_HEAD" + user?.key +",<br><br>" +"To take an action on the below items please click here: "+lpgButton +",<br><br>"
        eMailContent = mailHead + "$Constants.EMAIL_MESSAGE_TABLE_HEADER" + "$mailBody" + "$Constants.EMAIL_MESSAGE_END_ELEMENT"
        api.sendEmail(user?.value as String,Constants.EMAIL_SUBJECT+lpgInfo?.id +"-("+lpgInfo?.label+")",eMailContent)
    }
}

String failReason ( def lineItem){
    PA1warning = ""
    PA2warning = ""
    warning = ""
    if(lineItem?.value[metaData["Pricing Approach 1"]] == "Competitor-based Pricing"){
        keys = api.jsonDecode(lineItem?.value?.warnings)
        if(keys?.keySet()?.contains("CalculatedListPrice")){
            PA1warning = "PA1- " + (keys?.CalculatedListPrice?.getAt(0))
            PA1warning = PA1warning?.split('for')?.getAt(0)
        }else{
            PA1warning = "PA1- "+lineItem?.value[metaData["Pricing Approach 1"]] + "- Competition data not available"
        }
        if(keys?.keySet()?.contains("PurchasePriceOnline")||keys?.keySet()?.contains("PurchasePriceOffline")){
            PA2warning = "PA2- " + (keys?.PurchasePriceOffline?keys?.PurchasePriceOffline?.getAt(0):keys?.PurchasePriceOnline?.getAt(0))
            PA2warning = PA2warning?.split('for')?.getAt(0)
        }else{
            PA2warning = "PA2- " + lineItem?.value[metaData["Pricing Approach 2"]] + "- Purchase Price not available"
        }
    }else{
        if(keys?.keySet()?.contains("PurchasePriceOnline")||keys?.keySet()?.contains("PurchasePriceOffline")){
            PA1warning = "PA1- " + (keys?.PurchasePriceOffline?keys?.PurchasePriceOffline?.getAt(0):keys?.PurchasePriceOnline?.getAt(0))
            PA1warning = PA1warning?.split('for')?.getAt(0)
        }else{
            PA1warning = "PA1- " + lineItem?.value[metaData["Pricing Approach 1"]] + "- Purchase Price not available"
        }
        if(keys?.keySet()?.contains("CalculatedListPrice")){
            PA2warning = "PA2- " + keys?.CalculatedListPrice?.getAt(0)
            PA2warning = PA2warning?.split('for')?.getAt(0)
        }else{
            PA2warning = "PA2- " + lineItem?.value[metaData["Pricing Approach 2"]] + "- Competition data not available"
        }
    }

    warning = (PA1warning?:"") +" / "+ (PA2warning?:"")
    return warning

}
