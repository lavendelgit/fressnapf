def product = api.product("Part-ID")
def param = api.getParameter("Product")
if (param && param.getValue() == null) {
    param.setRequired(true)
}

return product