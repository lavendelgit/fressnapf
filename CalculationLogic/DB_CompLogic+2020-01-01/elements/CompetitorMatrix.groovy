import net.pricefx.common.api.FieldFormatType
api.logInfo("out.DataMap",out.DataMap)
def resultMatrix = api.newMatrix("Competitor ID", "Competitor Name", //"Competition Type",
        "Priority", "Aggregation Method", "Weight", "Competitor Price", "Currency","Price Date")
resultMatrix.setColumnFormat("Competitor Price", FieldFormatType.MONEY)
def roundingUtils = libs.FressnapfLib.RoundingUtils
CompetitorList = api.jsonDecodeList((out.DataMap as String))
(CompetitorList as List)?.each { Map competitorRow ->
    api.logInfo("competitorRow",competitorRow)
    def resultRow = [:]
    resultRow.put("Competitor ID", competitorRow.competitorSku)
    resultRow.put("Competitor Name", competitorRow.competitor)
    resultRow.put("Competition Type", competitorRow.competitionType)
    resultRow.put("Priority", competitorRow.prio)
    resultRow.put("Aggregation Method", competitorRow.calculationMode)
    resultRow.put("Weight", competitorRow.weight)
    resultRow.put("Competitor Price", roundingUtils.roundNumber(competitorRow.price, 2))
    resultRow.put("Currency", competitorRow.currency)
    resultRow.put("Price Date", competitorRow.infoDate?.split('T')?.getAt(0))
    resultMatrix.setEnableClientFilter(true)
    resultMatrix.addRow(resultRow)
}
return resultMatrix