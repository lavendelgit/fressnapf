String cfsRunDate= api.find("CFS", Filter.equal("id", libs.FressnapfLib.Constants.ItemClassBasketUpdate_CFS_ID ))?.find()?.lastUpdateDate
String productLastUpdateDate = api.stream("PX8","-lastUpdateDate",Filter.equal("name","ItemClassBasketMapping"))?.getAt(0)?.lastUpdateDate
if(productLastUpdateDate > cfsRunDate){
    api.logInfo("***** CFS for Item Class and Basket Generation is Triggered ***** ")
    actionBuilder.addCalculatedFieldSetAction("Generate ItemClass and Basket").setCalculate(true)
}else{
    api.addWarning("***** No Product needs item class or Basket Update *****")
}
return
