void createChart(String type, Long id, Map chartDef) {
    switch (type?.toUpperCase()) {
        case "PL":
            api.setPricelistCalculationChart(chartDef, id)
            break
        case "PG":
            api.setPricegridCalculationChart(chartDef, id)
            break
        default:
            break
    }
}

void createOutput(String type, Long id, String name, String label, String value, String message = null) {
    switch (type.toUpperCase()) {
        case "PL":
            api.setPricelistCalculationOutput(id, name, label, value, message)
            break
        case "PG":
            api.setPricegridCalculationOutput(id, name, label, value, message)
            break
        default:
            break
    }
}

List findAll(String type, String sortBy, List fields, List filters) {
    return findAll() { int startRow, int maxRowsLimit ->
        api.find(type, startRow, maxRowsLimit, sortBy, fields, *filters)
    }
}

List findAll(Closure finder) {
    int startRow = 0
    List allRows = []
    int maxRowsLimit = api.getMaxFindResultsLimit()
    while (result = finder.call(startRow, maxRowsLimit)) {
        startRow += result.size()
        allRows.addAll(result)
    }

    return allRows
}

List namedEntities(List rows, Map mappingFields) {
    return rows.collect { Map entiryRow ->
        return entiryRow.collectEntries { String fieldName, Object fieldValue ->
            [(mappingFields[fieldName]): fieldValue]
        }
    }
}

private List find(String type, int start, int max, String sortBy, List fields, List filters) {
    if (fields) {
        return api.find(type, start, max, sortBy, fields, *filters)
    }
    return api.find(type, start, max, sortBy, *filters)
}

def getHeaderInput(inputName) {
    def inputs = api.jsonDecode(api.currentItem("configuration"))?.headerInputs
    return inputs?.find { it.name == inputName }?.value
}

Map getLPGInputs() {
    List pgInputs = api.jsonDecode(api.currentItem("configuration"))?.inputs
    return pgInputs?.find { it.name == 'InputConfigurator' }?.value
}

def inlineConfigurator(configuratorLogic, configuratorName, label, params) {
    if (!configuratorName || !configuratorLogic) {
        api.throwException("Configurator name / logic not specified")
    }

    def configurator = api.inlineConfigurator(configuratorName, configuratorLogic)

    return configurator
}

List getFilteredSKUs(productFilterInput) {
    return api.find("P", api.filterFromMap(productFilterInput.productFilterCriteria)).collect { it -> it.sku }
}

List findColumnMeta(String type, id) {
    String lineTypeMeta = "${type}IM"
    String filterCol = "PG".equalsIgnoreCase(type) ? "priceGridId" : "priceListId"
    List fields = ["fieldName", "elementName", "label"]
    return findAll(lineTypeMeta, "id", fields, [Filter.equal(filterCol, id)])
}

List getFilters(Object lpgHeaderConfigurator, String context, Object columnMeta) {
    api.trace("lpgHeaderConfigurator", lpgHeaderConfigurator)
    if (!(lpgHeaderConfigurator instanceof Map)) {
        return []
    }

    List filters = []
    if (lpgHeaderConfigurator["${context}Category"]) {
        filters.add(Filter.in(columnMeta.get('Category'), lpgHeaderConfigurator["${context}Category"]))
    }

    if (lpgHeaderConfigurator["${context}Brand"]) {
        filters.add(Filter.in(columnMeta.get('Brand'), lpgHeaderConfigurator["${context}Brand"]))
    }

    return filters
}