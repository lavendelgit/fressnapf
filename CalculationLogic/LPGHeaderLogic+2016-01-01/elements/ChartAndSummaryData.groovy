import groovy.transform.Field

def chartUtils = libs.FressnapfLib.ChartUtils
Map chartAndSummaryData = [:]
@Field Map aggregationTypeMap = [PurchasePriceOnline        : 'AVG',
                                 PurchasePriceOnlineNonZero : 'SUM',
                                 SalesPriceOnline           : 'AVG',
                                 SalesPriceOnlineNonZero    : 'SUM',
                                 PurchasePriceOffline       : 'AVG',
                                 PurchasePriceOfflineNonZero: 'SUM',
                                 SalesPriceOffline          : 'AVG',
                                 SalesPriceOfflineNonZero   : 'SUM',
                                 CalculatedListPrice        : 'AVG',
                                 CalculatedListPriceNonZero : 'SUM',
                                 ListPrice                  : 'AVG',
                                 ListPriceNonZero           : 'SUM']

api.logInfo("#out.Behaviour", out.Behaviour)
api.logInfo("#out.ConfiguratorInputs", api.jsonEncode(out.ConfiguratorInputs))
if (out.Behaviour == 3 && out.CompItems) {
    List compItems = out.CompItems
    List baseItems = out.BaseItems

    Map baseValueMap = collectAggregatedDataFromItems(baseItems)
    Map compValueMap = collectAggregatedDataFromItems(compItems)

    String comparisionOn = out.ConfiguratorInputs?.ComparisionOn ?: "Online"
    if (comparisionOn) {
        chartAndSummaryData.waterfallSeries = [
                chartUtils.buildColumnSeries("${comparisionOn} (Base Filter)",
                                             "${comparisionOn} (Base Filter)",
                                             baseValueMap["PurchasePrice${comparisionOn}"],
                                             baseValueMap["SalesPrice${comparisionOn}"],
                                             baseValueMap.CalculatedListPrice,
                                             baseValueMap.ListPrice),
                chartUtils.buildColumnSeries("${comparisionOn} (Comparision Filter)",
                                             "${comparisionOn} (Comparision Filter)",
                                             compValueMap["PurchasePrice${comparisionOn}"],
                                             compValueMap["SalesPrice${comparisionOn}"],
                                             compValueMap.CalculatedListPrice,
                                             compValueMap.ListPrice)
        ]
        chartAndSummaryData.ChartTitle = "${comparisionOn} Avg Price Comparision (Base v/s Comparision Filters)"
    }
} else {
    List items = out.BaseItems
    Map aggValueMap = collectAggregatedDataFromItems(items)

    chartAndSummaryData.waterfallSeries = [
            chartUtils.buildColumnSeries("Online",
                                         "Online",
                                         aggValueMap.PurchasePriceOnline,
                                         aggValueMap.SalesPriceOnline,
                                         aggValueMap.CalculatedListPrice,
                                         aggValueMap.ListPrice),
            chartUtils.buildColumnSeries("Offline",
                                         "Offline",
                                         aggValueMap.PurchasePriceOffline,
                                         aggValueMap.SalesPriceOffline,
                                         aggValueMap.CalculatedListPrice,
                                         aggValueMap.ListPrice)
    ]
    chartAndSummaryData.ChartTitle = 'Online v/s Offline Avg Price Comparision'
}

return chartAndSummaryData ?: [:]

Map collectAggregatedDataFromItems(List items) {
    api.trace("collectAggregatedDataFromItems", items)
    Map summationMap = aggregationTypeMap.collectEntries { String fieldName, String aggrType ->
        [(fieldName): 0.0]
    }
    if (items) {
        items.each { Map item ->
            item.findAll { String fieldName, Object fieldValue ->
                summationMap[fieldName] != null
            }?.each { String fieldName, Object fieldValue ->
                BigDecimal value = fieldValue as BigDecimal
                summationMap[fieldName] += ((value ?: 0.0) as BigDecimal)
                summationMap[fieldName + 'NonZero'] += (value != 0 ? 1 : 0)
            }
        }
        def roundingUtils = libs.FressnapfLib.RoundingUtils
        summationMap = summationMap.collectEntries { String fieldName, BigDecimal fieldValue ->
            String aggrType = aggregationTypeMap[fieldName]
            Integer nonZeroCount = summationMap.getAt(fieldName + 'NonZero')
            return [(fieldName): (aggrType == 'AVG') ?
                                 roundingUtils.roundNumber(nonZeroCount ? (fieldValue / nonZeroCount) : 0.0, 2) :
                                 (
                                         (aggrType == 'SUM') ? roundingUtils.roundNumber(fieldValue, 0) : roundingUtils.roundNumber(fieldValue, 2)
                                 )
            ]
        }

    }
    api.trace("summationMap", summationMap)
    return summationMap
}