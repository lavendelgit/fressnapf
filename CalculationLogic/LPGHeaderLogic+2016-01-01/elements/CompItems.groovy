if (out.Behaviour == 3) {
    def type = "${out.Type}I" // PGI, PLI
    String filterField = out.Type == "PL" ? "priceListId" : "priceGridId"
    List filters = [Filter.equal(filterField, out.Id)] + out.CompFilters

    return Library.namedEntities(Library.findAll(type, "id", out.ColumnsToRead, filters), out.ColumnMetaTransposed)
}

return []