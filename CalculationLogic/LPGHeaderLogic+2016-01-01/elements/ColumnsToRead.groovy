return [
        out.ColumnMeta.PurchasePriceOnline,
        out.ColumnMeta.SalesPriceOnline,
        out.ColumnMeta.PurchasePriceOffline,
        out.ColumnMeta.SalesPriceOffline,
        out.ColumnMeta.CalculatedListPrice,
        out.ColumnMeta.ListPrice,
        out.ColumnMeta.Category
]