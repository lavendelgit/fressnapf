return Library.findColumnMeta(out.Type, out.Id)?.collectEntries { Map columnMetaEntry ->
    [(columnMetaEntry.elementName ?: columnMetaEntry.fieldName): columnMetaEntry.fieldName]
}