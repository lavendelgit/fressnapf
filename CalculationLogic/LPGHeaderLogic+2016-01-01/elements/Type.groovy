if (api.isDebugMode()) {
    return 'PG'
}
String typeId = out.CurrentItem.typedId
return typeId.tokenize(".")[1] ?: 'PG'