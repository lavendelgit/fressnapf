return Library.findColumnMeta(out.Type, out.Id)?.collectEntries { Map columnMetaEntry ->
    [(columnMetaEntry.fieldName): columnMetaEntry.elementName ?: columnMetaEntry.fieldName]
}