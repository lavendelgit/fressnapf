if (api.isDebugMode()) {
    return [
            "calculationType": "CompCategoryConfigurator",
            "BaseCategory"   : ["Food"],
            "CompBrand"      : [],
            "BaseBrand"      : [],
            "ComparisionOn"  : "Online",
            "CompCategory"   : ["Non-Food"]
    ]
}
return Library.getHeaderInput('LPGHeaderConfigurator')
