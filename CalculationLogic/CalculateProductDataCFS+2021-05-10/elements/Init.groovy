def getBasketItemClassRulesMaster() {
    return libs.SharedLib.CacheUtils.getOrSet("BasketItemClassRulesMaster", {
        Lib.getBasketItemClassRules()
    })
}

def getSalesPriceMaster() {
    return libs.SharedLib.CacheUtils.getOrSet("SalesPriceMaster", {
        Lib.getSalesPriceFromPX()
    })
}

def getCategoryTypeMappingMaster() {
    return libs.SharedLib.CacheUtils.getOrSet("CategoryTypeMappingMaster", {
        Lib.getCategoryTypeMapping()
    })
}

def getCalculationFieldSetDate() {
    return libs.SharedLib.CacheUtils.getOrSet("CalculationFieldSetDate", {
        Lib.getCalculationFieldSetDate()
    })
}

/*if (out.Product?.attribute17 != "KVI" && !api.global.ProductsWithKVI){
    api.logInfo("&&&&&&&&&&&INSIDE getProductsWithKVI^^6^^^^^^^^^^^^^^^")
    api.global.ProductsWithKVI = Lib.getProductsWithKVI()
}*/

//def getProductsWithKVIMaster() {
//api.global.ProductsWithKVI = []
if (!api.global.ProductsWithKVI && api.global.ProductsWithKVI != []) {
    api.global.ProductsWithKVI = Lib.getProductsWithKVI() ?: []
}
// return api.global.ProductsWithKVI
/*return libs.SharedLib.CacheUtils.getOrSet("ProductsWithKVI", {
    Lib.getProductsWithKVI()
})*/
//}

def getHighInvolvementMaster() {
    return libs.SharedLib.CacheUtils.getOrSet("HighInvolvementMaster", {
        Lib.getHighInvolvement()
    })
}

