List getBasketItemClassRules() {
    return api.namedEntities(api.findLookupTableValues(Constants.BasketItemClassRule_PP, "Basket", null)?.findAll())
}

Map getSalesPriceFromPX() {
    List filters = [
            Filter.equal("name", Constants.SalesPrice_PX)
    ]
    return libs.SharedLib.StreamUtils.stream("PX6", null, filters, { stream ->
        stream?.collectEntries { row ->
            [
                    (row.sku): [
                            articles            : row.sku,
                            salesPriceOnline    : row.attribute1,
                            salesCurrencyOnline : row.attribute2,
                            salesPriceOffline   : row.attribute3,
                            salesCurrencyOffline: row.attribute4
                    ]
            ]
        }
    })
}

Map getCategoryTypeMapping() {
    List filters = [
            Filter.equal("key1", "Poland"),
    ]
    return api.namedEntities(api.findLookupTableValues(Constants.BrandRoleMapping_PP, *filters)?.findAll())?.collectEntries {
        row ->
            [
                    (row.brand): [
                            brand        : row.brand,
                            country      : row.country,
                            groceryDriven: row.groceryDriven,
                            priceStarter : row.priceStarter
                    ]
            ]
    }
}

def getCalculationFieldSetDate() {
    return api.find("CFS", Filter.equal("id", Constants.CalculationFieldSetID))?.find()?.lastUpdateDate
}

List getProductsWithKVI() {
    List filters = [
            Filter.equal("itemClass", "KVI")
    ]
    return api.find("P", *filters)
}

Map getHighInvolvement() {
    return api.namedEntities(api.findLookupTableValues(Constants.HighInvolvement_PP, null)?.findAll())?.collectEntries {
        row ->
            [
                    (row.country): [
                            sellingPriceThreshold: row.sellingPriceThreshold,
                            country              : row.country,
                            sptCurrency          : row.sptCurrency,
                    ]
            ]
    }
}