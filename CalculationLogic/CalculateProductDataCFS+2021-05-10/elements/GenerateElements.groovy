Object product = api.currentItem() ?: []

if (api.isDebugMode()) {
    product = [
            "version": 5, "typedId": "1538715.P", "sku": "1001749001", "label": null, "unitOfMeasure": null, "userGroupEdit": null, "userGroupViewDetails": null, "currency": null, "formulaName": null, "image": null, "createDate": "2021-05-22T15:08:08", "createdBy": 364, "lastUpdateDate": "2021-05-24T09:43:53", "lastUpdateBy": 373, "attribute1": "8in1 chewing bone 7pcs xs", "attribute2": "4048422102373", "attribute3": "102007", "attribute4": "D/C snacks/addition", "attribute5": "Food", "attribute6": "HAWA", "attribute7": "0", "attribute8": "X", "attribute9": "KVI", "attribute10": "2", "attribute11": "8IN1", "attribute12": "", "attribute13": "2", "attribute14": "20", "attribute15": "Onl&Offl", "attribute16": "Specialist", "attribute17": "KVI", "attribute18": "Ext.KVI", "attribute19": null, "attribute20": "11863000000062", "attribute21": "2", "attribute22": null, "attribute23": null, "attribute24": null, "attribute25": null, "attribute26": null, "attribute27": null, "attribute28": null, "attribute29": null, "attribute30": null
    ]
}
String productSKU = product?.sku
String distribution = product?.attribute16
String sapItemClass = product?.attribute17 == "KVI" ? product?.attribute17 : ""
String brandType = product?.attribute13
String materialType = product?.attribute6
String brand = product?.attribute11
String priceFamily = product?.attribute20
BigDecimal sellingPriceThreshold = Init.getHighInvolvementMaster()["Poland"]?.sellingPriceThreshold


def csfLastUpdateDate = Init.getCalculationFieldSetDate()

Map sellingPrice = Init.getSalesPriceMaster()[productSKU]

BigDecimal currentSellingPrice = sellingPrice?.salesPriceOnline as BigDecimal
/*if (channel == "Online") {
    currentSellingPriceOnline = sellingPrice?.salesPriceOnline as BigDecimal
} else {
    currentSellingPriceOffline = sellingPrice?.salesPriceOffline as BigDecimal
}*/


Map categoryTypeMapping = Init.getCategoryTypeMappingMaster()[brand]
Boolean priceStarter = categoryTypeMapping?.priceStarter == "X" ?: false
String groceryDriven = categoryTypeMapping?.groceryDriven == "X" ?: false

Map generatedElements = [
        itemClass: "",
        basket   : ""
]

def getProductsWithKVI = (api.global.ProductsWithKVI)?.find { it?.attribute20 == priceFamily && it?.sku != productSKU }
def productFamilyBasket = getProductsWithKVI?.attribute21
Integer productFamilyWithKVI = getProductsWithKVI?.size()
Boolean isPriceFamilyIsInKVI = productFamilyWithKVI > 0

List basketItemClassRules = Init.getBasketItemClassRulesMaster()

/*Boolean kviFlagCondition = false
Boolean articleTypeCondition = false
Boolean brandTypeCondition = false
Boolean groceryCondition = false
Boolean priceStarterCondition = false
Boolean higLevelInclusionCondition = false*/

if (true) {//product?.lastUpdateDate > csf  LastUpdateDate){
    for (rule in basketItemClassRules) {
        boolean condit = isRulePassed(rule, materialType, sapItemClass, brandType, distribution, priceStarter, currentSellingPrice)

        if (condit) {
            generatedElements["itemClass"] = rule?.ItemClass
            generatedElements["basket"] = rule?.Basket
            break
        }
        /*kviFlagCondition = false
        articleTypeCondition = false
        brandTypeCondition = false
        groceryCondition = false
        priceStarterCondition = false
        higLevelInclusionCondition = false*/
    }

    /*api.trace("priceFamily", priceFamily)
    api.trace("isPriceFamilyIsInKVI", isPriceFamilyIsInKVI)
    api.trace("api.global.ProductsWithKVIItemClass", api.global.ProductsWithKVIItemClass)*/

    Object b10icVI = basketItemClassRules?.find { it?.Basket == "10" && it?.ItemClass == "VI" }

    if (brandType == b10icVI?.BrandType && !priceStarter &&
            ((b10icVI?.ArticleType)?.contains(materialType) || currentSellingPrice >= sellingPriceThreshold)) {
        generatedElements["itemClass"] = b10icVI?.ItemClass
        generatedElements["basket"] = b10icVI?.Basket
    }

    if (sapItemClass != "KVI" && isPriceFamilyIsInKVI) {
        generatedElements["itemClass"] = "Ext.KVI"
        generatedElements["basket"] = productFamilyBasket
    }

} else {
    api.logInfo("Do Not updates!!!! - cfs last update date is less than product update date")

}

if (sapItemClass == "KVI") {
    product["attribute18"] = generatedElements["itemClass"]
    product["attribute21"] = generatedElements["basket"]
    api.global.ProductsWithKVI << product
}

boolean isRulePassed(rule, String materialType, String sapItemClass, String brandType, String distribution,
                     boolean priceStarter, BigDecimal currentSellingPrice) {
    boolean groceryCondition, higLevelInclusionCondition, brandTypeCondition, articleTypeCondition, priceStarterCondition, kviFlagCondition


    BigDecimal sellingPriceThreshold = Init.getHighInvolvementMaster()["Poland"]?.sellingPriceThreshold
    List allowedArticleTypes = ((rule.ArticleType).split(','))?.collect { it.trim() }

    Boolean condit = true
    Integer count = 0
    kviFlagCondition = (rule.KVIFlag == sapItemClass)
    if (rule.KVIFlag && rule.KVIFlag?.trim() != "") {
        condit = condit && kviFlagCondition
        count++
    }

    articleTypeCondition = allowedArticleTypes?.contains(materialType)
    if (allowedArticleTypes?.size() > 0) {
        condit = condit && articleTypeCondition
        count++
    }

    brandTypeCondition = (rule.BrandType == brandType)
    if (rule.BrandType) {
        condit = condit && brandTypeCondition
        count++
    }

    groceryCondition = (rule.GroceryBrand == "Yes") ? (distribution == "Grocery") : (distribution == "Specialist")
    if (rule.GroceryBrand && rule.GroceryBrand.trim() != "") {
        condit = condit && groceryCondition
        count++
    }

    priceStarterCondition = (rule.PriceStarter == "Yes") ? priceStarter : !priceStarter
    if (rule.PriceStarter) {
        condit = condit && priceStarterCondition
        count++
    }

    higLevelInclusionCondition = rule.HighInvolvementSalesPrice == "Yes" ? (currentSellingPrice >= sellingPriceThreshold) : (currentSellingPrice < sellingPriceThreshold)
    if (rule.HighInvolvementSalesPrice && rule.HighInvolvementSalesPrice.trim() != "") {
        condit = condit && higLevelInclusionCondition
        count++
    }

    if (count < 1) {
        //To check at-least one condition is checked
        condit = false
    }
    return condit
}

return generatedElements