import groovy.transform.Field

@Field final String BasketItemClassRule_PP = "BasketItemClassRules"
@Field final String SalesPrice_PX = "SalesPrice"
@Field final String BrandRoleMapping_PP = "BrandRoleMapping"
@Field final String HighInvolvement_PP = "HighInvolvement"
@Field final Integer CalculationFieldSetID = 62