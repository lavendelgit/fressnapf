return api.currentItem()
/*[
        [sku:"1073468002", attribute13:"2", attribute6:"HAWA", attribute11:"FELI", attribute20: "11863000003527", sellingPrice:4.1, attribute16:"Grocery", attribute9:"KVI"], // 1 + KVI
        [sku:"1266901", attribute13:"2", attribute6:"HAWA", attribute11:"FELI", attribute20: "11863000003552", sellingPrice:5.9, attribute16:"Grocery", attribute9:"KVI"],// 1 + ext KVI

        [sku:"1015876", attribute13:"1", attribute6:"HAWA", attribute11:"ANIO", attribute20: "11863000013149", sellingPrice:138.9, attribute16:"Specialist", attribute9:""],// 10 + NON KVI
        [sku:"1081118", attribute13:"1", attribute6:"HAWA", attribute11:"ANIO", attribute20: "11863000012818", sellingPrice:358, attribute16:"Specialist", attribute9:""],// 10 + VI

        [sku:"1001750001", attribute13:"2", attribute6:"HAWA", attribute11:"8IN1", attribute20: "11863000000062", sellingPrice:12.7, attribute16:"Specialist", attribute9:"KVI"],// 2 +  KVI
        [sku:"1081118", attribute13:"2", attribute6:"HAWA", attribute11:"8IN1", attribute20: "11863000000062", sellingPrice:12.7, attribute16:"Specialist", attribute9:"KVI"],// 2 + ext kVI

        [sku:"1236947", attribute13:"2", attribute6:"ZSET", attribute11:"4CAT", attribute20: "11863000000039", sellingPrice:44.8, attribute16:"Specialist", attribute9:""],// 3 +  VI
        [sku:"1100683", attribute13:"2", attribute6:"ZTRA", attribute11:"8IN1", attribute20: "11863000000063", sellingPrice:78.2, attribute16:"Specialist", attribute9:""],// 3 + VI

        [sku:"1110837", attribute13:"2", attribute6:"HAWA", attribute11:"4PET", attribute20: "11863000000043", sellingPrice:682.1, attribute16:"Specialist", attribute9:""],// 4 +  VI
        [sku:"1268589", attribute13:"2", attribute6:"HAWA", attribute11:"4PET", attribute20: "11863000000053", sellingPrice:922.1, attribute16:"Specialist", attribute9:""],// 4 + VI
        [sku:"1004705", attribute13:"1", attribute6:"HAWA", attribute11:"FIFU", attribute20: "11863000013262", sellingPrice:20.2, attribute16:"Specialist", attribute9:""],// 5 +  VI
        [sku:"1005126", attribute13:"1", attribute6:"HAWA", attribute11:"FIFU", attribute20: "11863000011298", sellingPrice:20.2, attribute16:"Specialist", attribute9:""],// 5 + VI

        [sku:"1003002003", attribute13:"2", attribute6:"HAWA", attribute11:"CHAP", attribute20: "11863000002467", sellingPrice:63.4, attribute16:"Grocery", attribute9:""],// 6 +  non kVI
        [sku:"1003002001", attribute13:"2", attribute6:"HAWA", attribute11:"CHAP", attribute20: "11863000002467", sellingPrice:63.4, attribute16:"Grocery", attribute9:""],// 6 + non kVI

        [sku:"1069274", attribute13:"2", attribute6:"HAWA", attribute11:"8IN1", attribute20: "11863000000061", sellingPrice:34.7, attribute16:"Specialist", attribute9:""],// 7 +  non kVI
        [sku:"1202772", attribute13:"2", attribute6:"ZTRA", attribute11:"8IN1", attribute20: "11863000000062", sellingPrice:85.7, attribute16:"Specialist", attribute9:""],// 7 + ext kVI
]*/
